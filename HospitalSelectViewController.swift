//
//  HospitalSelectViewController.swift
//  亳州医保
//
//  Created by ljw on 15/2/13.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit

class HospitalSelectViewController: UIViewController,PopUpPickerViewDelegate  {
    @IBAction func btnCityClick(sender: AnyObject) {
        
        pickerView.showPicker()
    }
    @IBOutlet var btnCity: UIButton!

    @IBOutlet weak var seg城市: UISegmentedControl!
 
    var pickerView: PopUpPickerView!
    var provinces:NSArray!
    var cities:NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.edgesForExtendedLayout = UIRectEdge.None
        self.seg城市.setTitle("推荐", forSegmentAtIndex: 0)
        self.seg城市.setTitle("谯城", forSegmentAtIndex: 1)
        
        self.seg城市.insertSegmentWithTitle("南部新区", atIndex: 2,animated:false)
        self.seg城市.insertSegmentWithTitle("涡阳", atIndex: 3,animated:false)
        self.seg城市.insertSegmentWithTitle("蒙城", atIndex: 4,animated:false)
       
        self.navigationItem.titleView = self.btnCity
        
        pickerView = PopUpPickerView()
        pickerView.delegate = self
        
        provinces = NSArray(contentsOfFile: NSBundle.mainBundle().pathForResource("ProvincesAndCities.plist", ofType: nil)!)
        cities = provinces.objectAtIndex(0).objectForKey("Cities") as NSArray
        self.view.addSubview(pickerView)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
   
    // for delegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch(component)
        {
        case 0:
            return provinces.count
            
        case 1:
            return cities.count
            
        default :
            return 0
        }
        
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        switch(component)
        {
        case 0:
            return provinces.objectAtIndex(row).objectForKey("State") as String
            
        case 1:
            return  cities.objectAtIndex(row).objectForKey("city") as String
            
        default :
            return nil
        }
        
    }
    
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch(component)
        {
        case 0:
            cities=provinces.objectAtIndex(row).objectForKey("Cities") as NSArray
            pickerView.selectRow(0, inComponent: 1, animated: false)
            pickerView.reloadComponent(1)
            
            
        case 1:
            
            var cname = cities.objectAtIndex(row).objectForKey("city") as String
            btnCity.setTitle(cname, forState: UIControlState.Normal)
            
        default :
            btnCity.setTitle("选择城市", forState: .Normal)
        }
        
    }


}

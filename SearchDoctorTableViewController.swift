//
//  SearchDoctorTableViewController.swift
//  康岁健康
//
//  Created by ljw on 15/2/11.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit

class SearchDoctorTableViewController: UITableViewController,UISearchBarDelegate{
    
    @IBAction func btn返回Click(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
      //  self.navigationController?.navigationBarHidden=true
        
    }
    @IBOutlet var btn返回: UIBarButtonItem!
    
var doctors : [String] =  ["李含君","林紫薇","荣令","刘建刚","孙晓功"]
   // var doctorsimg : [String] = []
    var filteredColors = [String]()
    
    
    //what a name
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // I really need to read the swift book but I love Objective C
        
        //If this is an array from coredata you may want to do this in viewwillappear
      //  doctors = ["李含君","赵太云","林紫薇","荣令","刘建刚","孙晓功"]
       // doctorsimg=["01","02","03"]
        

        //searchbar
        searchBar.delegate = self
        searchBar.showsScopeBar = true
        
        
        self.navigationController?.navigationBarHidden=false
        
        self.navigationItem.leftBarButtonItem=btn返回
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.searchDisplayController!.searchResultsTableView{
            return self.filteredColors.count
        }else{
            return doctors.count
        }
        
        //return doctors.count
        
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 75
    }
    


    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        
        // This will be our color to load into the cell
        var doctor : String
        
        
        if tableView == self.searchDisplayController!.searchResultsTableView{
            doctor = self.filteredColors[indexPath.row]as (String)
        }
        else
        {
            doctor = self.doctors[indexPath.row]as (String)
        }
       // doctor = self.doctors[indexPath.row]as (String)

        
        //cell.textLabel.text = doctor
        (cell.contentView.viewWithTag(100) as UIImageView).image=UIImage(named: doctor)
        (cell.contentView.viewWithTag(110) as UILabel).text=doctor;
        return cell
    }
    
    
    
    // This is where we will search our color array
    func filterContentForSearchText(searchText: String) {
        
        self.filteredColors = self.doctors.filter({( colors: String) -> Bool in
            let stringMatch = colors.rangeOfString(searchText)
            return (stringMatch != nil)
        })
        println(self.filteredColors)
        
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
        self.filterContentForSearchText(searchString)
        return true
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        self.filterContentForSearchText(self.searchDisplayController!.searchBar.text)
        return true
    }
     override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var cell=tableView.cellForRowAtIndexPath(indexPath)
    //    var p:UIImageView
        
        // 跳转到预约详细界面
        var sb =  UIStoryboard(name: "Storyboard", bundle: nil)// [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        var view:YuyueDetailViewController = sb.instantiateViewControllerWithIdentifier("yuyuedetail")  as YuyueDetailViewController // [sb instantiateViewControllerWithIdentifier:@"yuyue"];  //test2为viewcontroller的StoryboardId
        
        self.navigationController?.pushViewController(view, animated: true)
    
    }
    
//    
//    // if you want to search by click of button
//    func searchBarSearchButtonClicked( searchBar: UISearchBar!)
//    {
//        println("Button Clicked")
//        self .filterContentForSearchText(searchBar.text)
//    }
//    
    
    
}

//
//  ExamSetBriefData.m
//  康岁健康
//
//  Created by 低调 on 14/12/13.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ExamSetBriefData.h"

@implementation ExamSetBriefData
-(void)setData:(NSDictionary *)data{
    self.code = [data objectForCJKey:@"code"];
    self.gender_allowed = [data objectForCJKey:@"gender_allowed"];
    self.Id = [data objectForCJKey:@"id"];
    self.name = [data objectForCJKey:@"name"];
    self.orderItemId = [data objectForCJKey:@"orderItemId"];
    self.price = [data objectForCJKey:@"price"];
}
@end

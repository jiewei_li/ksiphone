//
//  YuYueMainViewController.h
//  康岁健康
//
//  Created by 低调 on 14/12/9.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"

@interface YuYueMainViewController : BaseNextViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *_data;
}
@property  UITableView *tableView;
@property IBOutlet UIView *topView;
@property IBOutlet UILabel *sumValue;
@property IBOutlet UIView *bottomView;
@property IBOutlet UIView *bottomPayView;
@property IBOutlet UIButton *b1;
@property IBOutlet UIButton *b2;
@property IBOutlet UIButton *b3;
-(IBAction)waiting2Deall:(id)sender;
-(IBAction)alreadyDo:(id)sender;
-(IBAction)finished:(id)sender;
-(IBAction)selfYuYue:(id)sender;
-(IBAction)realityYuYue:(id)sender;
-(IBAction)goPay:(id)sender;
-(IBAction)cancelPay:(id)sender;
@end

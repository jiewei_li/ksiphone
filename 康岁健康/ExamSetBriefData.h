//
//  ExamSetBriefData.h
//  康岁健康
//
//  Created by 低调 on 14/12/13.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExamSetBriefData : NSObject
@property NSString *code;
@property NSString *gender_allowed;
@property NSString *Id;
@property NSString *name;
@property NSString *orderItemId;
@property NSString *price;
-(void)setData:(NSDictionary *)data;
@end

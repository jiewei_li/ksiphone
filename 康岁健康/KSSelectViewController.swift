//
//  KSSelectViewController.swift
//  亳州医保
//
//  Created by ljw on 15/2/13.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit
import SwiftyJSON
class KSSelectViewController: UIViewController {
    @IBAction func btn确定Click(sender: AnyObject) {
        
    }

    
    var lastks1:String!
    var lastks2:String!
    var ksjson:JSON!
    // var ksjson:NSDictionary!
    var  ks1:NSArray!
    var ks2:NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        if let file = NSBundle.mainBundle().pathForResource("ks", ofType: "json")
            
            
        {
            let data = NSData(contentsOfFile: file)!
            ksjson = JSON(data:data)
            // viewController.json = json
            
            
            // var   ksjson=json.object as NSDictionary;
            //  ksjson.allKeys
            ks1 =  ksjson.dictionaryValue.keys.array
            ks2  =  ksjson.dictionary?[ks1[0] as String]?.object as NSArray
            lastks1=ks1[0] as String
            lastks2=ks2[0] as String
            
            
        }
            
        else {
            
            NSLog("no json file")
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int)->Int {
        
        if(component==0)
        {
            return  ks1.count
        }
        
        return ks2.count
        
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        //return colors[row]
        
        
        if(component==0)
        {
            return ks1.objectAtIndex(row) as String
        }
        else
        {
            return ks2.objectAtIndex(row) as String
        }
        
        
        
    }
    
    func pickerView(pickerView:UIPickerView,didSelectRow row:Int,inComponent component:Int)
    {
        if(component == 0)
        {
            
            // ksjson.dictionaryValue
            //ksjson.dictionary?["内科"]
            var str=ks1[row] as String
            //     var st =    ksjson.dictionaryValue.keys.array[row]
            // ks2 = ksjson.dictionary?[str]
            ks2  =  ksjson.dictionary?[str]?.object as NSArray
            
            lastks1=str
            lastks2=ks2[0] as String
            
            pickerView.selectRow(0, inComponent: 1, animated: true)
            pickerView.reloadComponent(1)
            
            
            
        }
        else
        {
            
            lastks2=ks2[row] as String
            
        }
        
       // lab科室.text=lastks1+lastks2
        
    }


}

//
//  HealthFileViewController.m
//  康岁健康
//
//  Created by 低调 on 15/1/9.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

#import "HealthFileViewController.h"
#import "HealthFileTableViewCell.h"
#import "WebViewController.h"
#import "CJSelectViewViewController.h"
#import "CustomerData.h"
@interface HealthFileViewController ()
{
    UITableView *_tableView;
    NSArray     *_dataSourec;
}
@end

@implementation HealthFileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"健康档案";
    [self addSubView:self.topView];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, self.topView.bottom, self.view.width, _mainView.height-self.topView.bottom) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self addSubView:_tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(IBAction)choseUser:(id)sender{
    NSDictionary *rdic = [[NSDictionary alloc]initWithObjectsAndKeys:[UserInfos sessionId],@"sessionid",[UserInfos customerId],@"customerid",@"true",@"userself", nil];
    [NetRequest requestShopWithType:REATION withGetUrl:ADDRESS_REATION andGetInfo:rdic result:^(id resultSuccess) {
        NSArray *userIndfos = resultSuccess;
        
//        NSMutableArray *resultArr = resultSuccess;
        
        NSMutableArray *titleArr = [[NSMutableArray alloc]init];
        
        for (CustomerData *data in userIndfos) {
            [titleArr addObject:data.name];
        }
        
        CJSelectViewViewController *s = [[[NSBundle mainBundle]loadNibNamed:@"CJSelectViewViewController" owner:nil options:nil] lastObject];
        s.titleLabel.text = @"体检人";
        s.prppreitys =userIndfos;
        s.titles = titleArr;
        
        s.callBack = ^(id selectedValue){
            CustomerData *selecteddata = selectedValue;
            self.chosedUser.text = selecteddata.name;
            NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:selecteddata.Id,@"Customerid",@"1000",@"Pagecount",@"0",@"page", nil];
            [NetRequest requestShopWithType:EXAMREPORT withGetUrl:ADDRESS_EXAMREPORT andGetInfo:dic result:^(id resultSuccess) {
                _dataSourec = [resultSuccess objectForKey:@"Data"];
                [_tableView reloadData];
            } err:^(id resultError) {
                
            }];
        };
        
        s.frame = self.view.frame;
        [self.navigationController.view addSubview:s];
        

        
    } err:^(id resultError) {
        
    }];
    
    
}
-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataSourec.count;
}
-(int)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString*deq = @"HealthFileTableViewCell";
    HealthFileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:deq];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HealthFileTableViewCell" owner:nil options:nil] lastObject];
    }
    
    NSDictionary *dic = [_dataSourec objectAtIndex:indexPath.row];
    cell.dateLabel.text = [dic objectForKey:@"date"];
    cell.centerLabel.text = [dic objectForKey:@"exam_center_name"];
    cell.tcLabel.text = [dic objectForKey:@"exam_set_name"];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 66;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 102;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 66, tableView.width)];
    [v addSubview:self.tableTopView];
    self.tableTopView.left = 5;
    self.tableTopView.top = 10;
    return v;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dic = [_dataSourec objectAtIndex:indexPath.row];

    WebViewController *web = [[WebViewController alloc]init];
    web.url = [NSString stringWithFormat:@"http://www.ihealthink.com%@",[dic objectForKey:@"report_path"]];
    web.titleS = [dic objectForKey:@"exam_set_name"];
    [self.navigationController pushViewController:web animated:YES];
    
}
@end

//
//  HealthFileViewController.h
//  康岁健康
//
//  Created by 低调 on 15/1/9.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"

@interface HealthFileViewController : BaseNextViewController<UITableViewDataSource,UITableViewDelegate>
@property IBOutlet UIView      *topView;
@property IBOutlet UIView      *tableTopView;
@property IBOutlet UITextField      *chosedUser;
-(IBAction)choseUser:(id)sender;
@end

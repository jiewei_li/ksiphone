//
//  HealthFileTableViewCell.h
//  康岁健康
//
//  Created by 低调 on 15/1/9.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HealthFileTableViewCell : UITableViewCell
@property IBOutlet UILabel *dateLabel;
@property IBOutlet UILabel *centerLabel;
@property IBOutlet UILabel *tcLabel;
@end

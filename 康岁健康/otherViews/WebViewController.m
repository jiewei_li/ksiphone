//
//  WebViewController.m
//  康岁健康
//
//  Created by 低调 on 15/1/5.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.titleS;
    UIWebView *web = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, _mainView.height)];
    [web loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    
    [self addSubView:web];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  ZhiTngChe.m
//  康岁健康
//
//  Created by 低调 on 14/12/28.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ZhiTngChe.h"

@interface ZhiTngChe ()

@end

@implementation ZhiTngChe

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"就医直通车";
    UIScrollView *sc = [[UIScrollView alloc]initWithFrame:_mainView.bounds];
    UIImageView *im = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, _mainView.width*2)];
    im.image = [UIImage imageNamed:@"zhiTongChe"];
    [sc addSubview:im];
    sc.contentSize = CGSizeMake(_mainView.width, im.height);
    [self addSubView:sc];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

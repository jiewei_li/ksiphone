//
//  HeartTest.m
//  康岁健康
//
//  Created by 低调 on 14/12/14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "HeartTest.h"
#import "HeartFriendArticalsBrief.h"
#import "WebViewController.h"
@interface HeartTest ()

@end

@implementation HeartTest

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"心灵伙伴--心测试";
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, _mainView.height)];
    image.image = [UIImage imageNamed:@"heartScertBac"];
    [self addSubView:image];
    
    UIImageView *centerImage = [[UIImageView alloc]initWithFrame:CGRectMake(35, 70-42, 250, 250)];
    centerImage.image = [UIImage imageNamed:@"heartTestRound"];
    [self addSubView:centerImage];
    
    _arryImge = [[UIImageView alloc]initWithFrame:CGRectMake(105, 105, 40, 100)];
    _arryImge.image =[UIImage imageNamed:@"arryImage"];
    [centerImage addSubview:_arryImge];
    
    CGAffineTransform endAngle = CGAffineTransformMakeRotation( 0 *(M_PI / 180.0f));
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        _arryImge.layer.anchorPoint = CGPointMake(0.51f,0.24f);//围绕点
        _arryImge.layer.position = CGPointMake(125,129);//位置</span>
        _arryImge.transform = endAngle;
        
    } completion:^(BOOL finished) {
        
        
    }];
    
    _pressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _pressBtn.frame =CGRectMake(100, 350-42, 120, 44);
    [_pressBtn setBackgroundImage:[UIImage imageNamed:@"PressToChoseBtn"] forState:UIControlStateNormal];
    [self addSubView:_pressBtn];
    //        [pressBtn addTarget:self action:@selector(ss) forControlEvents:UIControlEventTouchUpInside];
    [_pressBtn addTarget:self action:@selector(stop) forControlEvents:UIControlEventTouchUpInside];
    [_pressBtn addTarget:self action:@selector(pressed) forControlEvents:UIControlEventTouchDown];
    _angle = 60;
    _shouldStop = YES;
    
    [self getArticalClass];
}

-(void)pressed{
    _shouldStop = NO;
    [self startAnimation];
}

-(void)stop{
    NSLog(@"stop");
    _shouldStop = YES;
    
    int i = _angle/60;
    int tag = i%6;
    NSLog(@"%d",i);
    
    _pressBtn.enabled = NO;
    [self performSelector:@selector(nextPage:) withObject:[NSNumber numberWithInt:tag] afterDelay:0.5];
    
    
}

-(void)nextPage:(NSNumber *)number
{
    
    NSDictionary *dic =  user_defaults_get_object(@"ADDRESS_GETSERVERURLS");
    NSDictionary *urldic =  [dic objectForKey:@"Data"];
    int tag = [number intValue];
    NSLog(@"******%d",tag);
    if (tag == 0) {
        
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"健康" andTag:_health];
        WebViewController *brief = [[WebViewController alloc]init];
        brief.titleS = @"健康";
        brief.url = [urldic objectForCJKey:@"url_hearttest_health"];
        [self.navigationController pushViewController:brief animated:YES];
    }
    else if(tag == 1){
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"爱情" andTag:_love];
        WebViewController *brief = [[WebViewController alloc]init];
        brief.titleS = @"爱情";
        brief.url = [urldic objectForCJKey:@"url_heartsecret_love"];
        [self.navigationController pushViewController:brief animated:YES];
    }
    else if(tag == 2){
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"能力" andTag:_ability];
        WebViewController *brief = [[WebViewController alloc]init];
        brief.titleS = @"能力";
        brief.url = [urldic objectForCJKey:@"url_hearttest_ability"];
        [self.navigationController pushViewController:brief animated:YES];
    }
    else if(tag == 3){
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"智力" andTag:_eq];
        WebViewController *brief = [[WebViewController alloc]init];
        brief.titleS = @"智力";
        brief.url = [urldic objectForCJKey:@"url_hearttest_IQ"];
        [self.navigationController pushViewController:brief animated:YES];
    }
    else if(tag == 4){
        //        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"性格" andTag:_iq];
        WebViewController *brief = [[WebViewController alloc]init];
        brief.titleS = @"情商";
        brief.url = [urldic objectForCJKey:@"url_hearttest_EQ"];
        [self.navigationController pushViewController:brief animated:YES];
    }
    else if(tag == 5){
        //        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"性格" andTag:_iq];
        WebViewController *brief = [[WebViewController alloc]init];
        brief.titleS = @"性格";
        brief.url = [urldic objectForCJKey:@"url_hearttest_character"];
        [self.navigationController pushViewController:brief animated:YES];
    }

//    if (tag == 0) {
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithFrame:rect andTag:_health title:@"健康"];
//        [Common viewAnimation:brief];
//        brief.delegate = self;
//        [self addSubview:brief];
//    }
//    else if(tag == 1){
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithFrame:rect andTag:_love title:@"爱情"];
//        [Common viewAnimation:brief];
//        brief.delegate = self;
//        [self addSubview:brief];
//    }
//    else if(tag == 2){
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithFrame:rect andTag:_ability title:@"能力"];
//        [Common viewAnimation:brief];
//        brief.delegate = self;
//        [self addSubview:brief];
//    }
//    else if(tag == 3){
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithFrame:rect andTag:_eq title:@"智力"];
//        [Common viewAnimation:brief];
//        brief.delegate = self;
//        [self addSubview:brief];
//    }
//    else if(tag == 4){
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithFrame:rect andTag:_iq title:@"性格"];
//        [Common viewAnimation:brief];
//        brief.delegate = self;
//        [self addSubview:brief];
//    }
//    else if(tag == 5){
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithFrame:rect andTag:_xinge title:@"情商"];
//        [Common viewAnimation:brief];
//        brief.delegate = self;
//        [self addSubview:brief];
//    }
    _pressBtn.enabled = YES;
}
- (void)startAnimation
{
    CGAffineTransform endAngle = CGAffineTransformMakeRotation( _angle *(M_PI / 180.0f));
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        _arryImge.layer.anchorPoint = CGPointMake(0.51f,0.24f);//围绕点
        _arryImge.layer.position = CGPointMake(125,129);//位置</span>
        _arryImge.transform = endAngle;
        
    } completion:^(BOOL finished) {
        _angle += 60;
        if (!_shouldStop) {
            [self startAnimation];
        }
        
    }];
    
}


-(void)getArticalClass{
//    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:@"0",@"Parent", nil];
//    __block ASIFormDataRequest *request2 = [Common requestType:@"appapi/GetArticalClass" andPostDatas:dic];
//    __weak ASIHTTPRequest *request_b2 = request2;
//    [request2 setCompletionBlock:^{
//        NSMutableDictionary *requestArry = [Common requestResult:[request_b2 responseData]];
//        NSLog(@"requestArry %@  %@",requestArry,[request_b2 responseData]);
//        NSArray *arr = [requestArry objectForKey:@"Data"];
//        for (NSDictionary *dic in arr) {
//            NSLog(@"%@",dic);
//            if ([[dic objectForKey:@"name"] isEqualToString:@"心测试"]) {
//                _id_class1 = [dic objectForKey:@"id"];
//                
//                [self GetArticalsBrief:_id_class1];
//            }
//        }
//        
//        
//    }];
//    [request2 setFailedBlock:^{
//        [Common alterShow:@"网络错误，请稍后重试"];
//    }];
    //        request2.delegate = self;
//    [request2 startAsynchronous];
    
}

-(void)GetArticalsBrief:(NSString *)classId{
//    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:classId,@"Parent", nil];
//    __block ASIFormDataRequest *request2 = [Common requestType:@"appapi/GetArticalClass" andPostDatas:dic];
//    __weak ASIHTTPRequest *request_b2 = request2;
//    [request2 setCompletionBlock:^{
//        NSMutableDictionary *requestArry = [Common requestResult:[request_b2 responseData]];
//        NSLog(@"requestArry %@  %@",requestArry,[request_b2 responseData]);
//        //        _id_class1 = [[requestArry objectForKey:@"Data"] objectForKey:@"心秘密"];
//        
//        NSArray *arr = [requestArry objectForKey:@"Data"];
//        for (NSDictionary *dic in arr) {
//            NSLog(@"%@",dic);
//            if ([[dic objectForKey:@"name"] isEqualToString:@"健康"]) {
//                _health = [dic objectForKey:@"id"];
//            }
//            else if ([[dic objectForKey:@"name"] isEqualToString:@"爱情"]) {
//                _love = [dic objectForKey:@"id"];
//            }
//            else if ([[dic objectForKey:@"name"] isEqualToString:@"能力"]) {
//                _ability = [dic objectForKey:@"id"];
//            }
//            else if ([[dic objectForKey:@"name"] isEqualToString:@"智力"]) {
//                _eq = [dic objectForKey:@"id"];
//            }
//            else if ([[dic objectForKey:@"name"] isEqualToString:@"情商"]) {
//                _iq = [dic objectForKey:@"id"];
//            }
//            else if ([[dic objectForKey:@"name"] isEqualToString:@"性格"]) {
//                _xinge = [dic objectForKey:@"id"];
//            }
//        }
//    }];
//    [request2 setFailedBlock:^{
//        [Common alterShow:@"网络错误，请稍后重试"];
//    }];
//    //        request2.delegate = self;
//    [request2 startAsynchronous];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

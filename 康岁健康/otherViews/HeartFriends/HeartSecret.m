//
//  HeartSecret.m
//  康岁健康
//
//  Created by 低调 on 14/12/14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "HeartSecret.h"
#import "HeartFriendArticalsBrief.h"
#import "WebViewController.h"
@interface HeartSecret ()

@end

@implementation HeartSecret

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title= @"心灵伙伴--心秘密";
    int height = 42;
    CGRect rect = [[UIScreen mainScreen] bounds];
    CGSize size = rect.size;
    if (size.height<500) {
        height = 35;
    }
    UIScrollView *sc = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, _mainView.height+8)];
    sc.contentSize = CGSizeMake(_mainView.width, 508);
    sc.backgroundColor = [UIColor whiteColor];
    [self addSubView:sc];
    
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, 508)];
    image.image = [UIImage imageNamed:@"heartScertBacMain"];
    [sc addSubview:image];
    
    _secretOfRelation = [UIButton buttonWithType:UIButtonTypeCustom];
    _secretOfSelf = [UIButton buttonWithType:UIButtonTypeCustom];
    _secretOfLove = [UIButton buttonWithType:UIButtonTypeCustom];
    _secretOfSex = [UIButton buttonWithType:UIButtonTypeCustom];
    _unBelieveAbleSecret = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [sc addSubview:_secretOfLove];
    [sc addSubview:_secretOfRelation];
    [sc addSubview:_secretOfSelf];
    [sc addSubview:_secretOfSex];
    [sc addSubview:_unBelieveAbleSecret];
    
    
    _secretOfRelation.frame = CGRectMake(80, 153-42, 73, 99);
    _secretOfSelf.frame = CGRectMake(170, 153-42, 73, 99);
    _secretOfLove.frame = CGRectMake(25, 260-42, 75, 105);
    _secretOfSex.frame = CGRectMake(195, 305-42, 80, 110);
    _unBelieveAbleSecret.frame = CGRectMake(70, 370-42, 95, 110);
    
    [_secretOfRelation addTarget:self action:@selector(toDetail:) forControlEvents:UIControlEventTouchUpInside];
    [_secretOfSelf addTarget:self action:@selector(toDetail:) forControlEvents:UIControlEventTouchUpInside];
    [_secretOfLove addTarget:self action:@selector(toDetail:) forControlEvents:UIControlEventTouchUpInside];
    [_secretOfSex addTarget:self action:@selector(toDetail:) forControlEvents:UIControlEventTouchUpInside];
    [_unBelieveAbleSecret addTarget:self action:@selector(toDetail:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _secretOfRelation.tag = 0;
    _secretOfSelf.tag = 1;
    _secretOfLove.tag = 2;
    _secretOfSex.tag = 3;
    _unBelieveAbleSecret.tag = 4;
    
    [self getArticalClass];
}
-(void)getArticalClass{
//    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:@"0",@"Parent", nil];
//    [NetRequest requestShopWithType:GET_ARTICAL_CLASS withGetUrl:ADDRESS_GET_ARTICAL_CLASS andGetInfo:dic result:^(id resultSuccess) {
//            NSMutableDictionary *requestArry = resultSuccess;
//            NSArray *arr = [requestArry objectForKey:@"Data"];
//            for (NSDictionary *dic in arr) {
//                NSLog(@"%@",dic);
//                if ([[dic objectForKey:@"name"] isEqualToString:@"心秘密"]) {
//                    _id_class1 = [dic objectForKey:@"id"];
//                    
//                    [self GetArticalsBrief:_id_class1];
//                }
//            }
//    } err:^(id resultError) {
//        
//    }];
    
}
-(void)GetArticalsBrief:(NSString *)classId{
//    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:classId,@"Parent", nil];
//    
//    [NetRequest requestShopWithType:GET_ARTICAL_CLASS withGetUrl:ADDRESS_GET_ARTICAL_CLASS andGetInfo:dic result:^(id resultSuccess) {
//        NSMutableDictionary *requestArry = resultSuccess;
////        NSLog(@"requestArry %@  %@",requestArry,[request_b2 responseData]);
//        //        _id_class1 = [[requestArry objectForKey:@"Data"] objectForKey:@"心秘密"];
//        
//        NSArray *arr = [requestArry objectForKey:@"Data"];
//        for (NSDictionary *dic in arr) {
//            NSLog(@"%@",dic);
//            
//            if ([[dic objectForKey:@"name"] isEqualToString:@"关系的秘密"]) {
//                _secretOfRelationstring = [dic objectForKey:@"id"];
//            }
//            else if ([[dic objectForKey:@"name"] isEqualToString:@"自我的秘密"]) {
//                _secretOfSelfstring = [dic objectForKey:@"id"];
//            }
//            else if ([[dic objectForKey:@"name"] isEqualToString:@"爱的秘密"]) {
//                _secretOfLovestring = [dic objectForKey:@"id"];
//            }
//            else if ([[dic objectForKey:@"name"] isEqualToString:@"性的秘密"]) {
//                _secretOfSexstring = [dic objectForKey:@"id"];
//            }
//            else if ([[dic objectForKey:@"name"] isEqualToString:@"不可思议的秘密"]) {
//                _unBelieveAbleSecretstring = [dic objectForKey:@"id"];
//            }
//        }
//    } err:^(id resultError) {
//        
//    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)toDetail:(UIButton *)sender{
    int tag = sender.tag;
    NSDictionary *dic =  user_defaults_get_object(@"ADDRESS_GETSERVERURLS");
    NSDictionary *urldic =  [dic objectForKey:@"Data"];
    
    if (tag == 0) {
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"关系的秘密" andTag:_secretOfRelationstring];

        WebViewController *brief = [[WebViewController alloc]init];
        brief.titleS = @"关系的秘密";
        brief.url = [urldic objectForCJKey:@"url_heartsecret_relation"];
        [self.navigationController pushViewController:brief animated:YES];
    }
    else if(tag == 1){
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"自我的秘密" andTag:_secretOfSelfstring];
        WebViewController *brief = [[WebViewController alloc]init];
        brief.titleS = @"自我的秘密";
        brief.url = [urldic objectForCJKey:@"url_heartsecret_self"];
        [self.navigationController pushViewController:brief animated:YES];
    }
    else if(tag == 2){
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"爱的秘密" andTag:_secretOfLovestring];
        WebViewController *brief = [[WebViewController alloc]init];
        brief.titleS = @"爱的秘密";
        brief.url = [urldic objectForCJKey:@"url_hearttest_love"];
        [self.navigationController pushViewController:brief animated:YES];
    }
    else if(tag == 3){
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"性的秘密" andTag:_secretOfSexstring];
        WebViewController *brief = [[WebViewController alloc]init];
        brief.titleS = @"性的秘密";
        brief.url = [urldic objectForCJKey:@"url_heartsecret_sex"];
        [self.navigationController pushViewController:brief animated:YES];
    }
    else if(tag == 4){
//        HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"不可思议的秘密" andTag:_unBelieveAbleSecretstring];
        WebViewController *brief = [[WebViewController alloc]init];
        brief.titleS = @"不可思议的秘密";
        brief.url = [urldic objectForCJKey:@"url_heartsecret_incredible"];
        [self.navigationController pushViewController:brief animated:YES];
    }
}

@end

//
//  HeartFriendArticalsBrief.h
//  康岁健康
//
//  Created by 低调 on 14/12/14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"
#import "DiseaseItemView.h"

@interface HeartFriendArticalsBrief : BaseNextViewController<toDetailDelegate>
@property NSString *titleS;
@property NSString *tagS;
-(id)initWithTitle:(NSString *)title andTag:(NSString *)tag;
@end

//
//  DseaseItem.h
//  康岁健康
//
//  Created by cerastes on 14-5-8.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol ToDiseaseDeatilDelegate <NSObject>

-(void)toDiseaseDatail:(id)sender;

@end
#import <UIKit/UIKit.h>

@interface DiseaseItem : UIView
@property id<ToDiseaseDeatilDelegate> delegate;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title tag:(int)tag;
@end

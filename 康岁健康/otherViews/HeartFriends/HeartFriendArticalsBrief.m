//
//  HeartFriendArticalsBrief.m
//  康岁健康
//
//  Created by 低调 on 14/12/14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "HeartFriendArticalsBrief.h"
#import "DetailWebView.h"
@interface HeartFriendArticalsBrief ()

@end

@implementation HeartFriendArticalsBrief
-(id)initWithTitle:(NSString *)title andTag:(NSString *)tag{
    self = [super init];
    
    self.title = title;
    self.titleS = title;
    self.tagS = tag;
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.titleS;
    
    DiseaseItemView *h =  [[DiseaseItemView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, _mainView.height) andTag:[self.tagS intValue]];
    h.backgroundColor = [UIColor clearColor];
    h.delegate = self;
    [self addSubView:h];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)todetail:(id)sender
{
    DiseaseItem *item = (DiseaseItem *)sender;
    DetailWebView *web = [[DetailWebView alloc]initWithTitle:self.title tag:[NSString stringWithFormat:@"%d",item.tag]];
    [self.navigationController pushViewController:web animated:YES];
//    DiseaseDetail *detail = [[DiseaseDetail alloc]initWithFrame:self.bounds title:self->_titleLabel.text tag:item.tag];
//    detail.delegate = self;
//    [self addSubview:detail];
}
@end

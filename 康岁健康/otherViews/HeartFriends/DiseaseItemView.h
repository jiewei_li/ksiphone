//
//  DiseaseItemView.h
//  康岁健康
//
//  Created by cerastes on 14-5-8.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol toDetailDelegate <NSObject>

-(void)todetail:(id)sender;

@end
#import <UIKit/UIKit.h>
#import "DiseaseItem.h"
@interface DiseaseItemView : UIView<ToDiseaseDeatilDelegate>
@property id<toDetailDelegate> delegate;
- (id)initWithFrame:(CGRect)frame andTag:(int)tag;
@end

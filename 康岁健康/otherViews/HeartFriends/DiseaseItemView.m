//
//  DiseaseItemView.m
//  康岁健康
//
//  Created by cerastes on 14-5-8.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "DiseaseItemView.h"

@implementation DiseaseItemView
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame andTag:(int)tag
{
    self = [super initWithFrame:frame];
    if (self) {
        self.tag = tag;
        UIScrollView *scro = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.width, self.height)];
        [self addSubview:scro];
        NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",self.tag],@"classId", nil];
        
        [NetRequest requestShopWithType:GETARTICALSBRIEF withGetUrl:ADDRESS_GETARTICALSBRIEF andGetInfo:dic result:^(id resultSuccess) {
            NSMutableDictionary *requestArry = resultSuccess;
            NSLog(@"requestArry %@  %@",requestArry,resultSuccess);
            NSArray *data = [requestArry objectForKey:@"Data"];
            for (int i = 0; i<[data count]; i++) {
                NSDictionary *dic = [data objectAtIndex:i];
                DiseaseItem *item = [[DiseaseItem alloc]initWithFrame:CGRectMake(0, 44*i, self.width, 44) title:[dic objectForKey:@"title"] tag:[[dic objectForKey:@"id"] intValue]];
                item.delegate = self;
                item.backgroundColor = i%2==1?CJGrayColor:CJLightGrayColor;
                [scro addSubview:item];
                scro.contentSize = CGSizeMake(item.width, item.bottom);
            }

        } err:^(id resultError) {
            [UIAlertView showMessage:@"网络错误，请稍后重试"];

        }];

    }
    return self;
}

-(void)toDiseaseDatail:(id)sender{
    [self.delegate todetail:sender];
}
@end

//
//  DseaseItem.m
//  康岁健康
//
//  Created by cerastes on 14-5-8.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "DiseaseItem.h"

@implementation DiseaseItem
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame title:(NSString *)title tag:(int)tag
{
    self = [super initWithFrame:frame];
    if (self) {
        self.tag = tag;
            
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, 0, self.width, self.height);
        [button addTarget:self action:@selector(toDetail) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:title forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"itemBack"] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:10];
        button.titleLabel.textAlignment = NSTextAlignmentLeft;
        button.titleLabel.text = title;
        button.backgroundColor = [UIColor clearColor];
        [self addSubview:button];
        
    }
    return self;
}

-(void)toDetail{
    [self.delegate toDiseaseDatail:self];
}
@end

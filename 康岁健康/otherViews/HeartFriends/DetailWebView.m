//
//  DetailWebView.m
//  康岁健康
//
//  Created by 低调 on 14/12/28.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "DetailWebView.h"

@interface DetailWebView ()
{
    NSString *_title;
    NSString *_tag;
}
@end

@implementation DetailWebView
-(id)initWithTitle:(NSString *)title tag:(NSString *)tag{
    self = [super init];
    _title =title;
    _tag   = tag;
    return  self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _title;
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:_tag,@"id", nil];

    [NetRequest requestShopWithType:GETARTICAL withGetUrl:ADDRESS_GETARTICAL andGetInfo:dic result:^(id resultSuccess) {
        NSMutableDictionary *requestArry = resultSuccess;
        NSLog(@"requestArry %@  %@",requestArry,resultSuccess);
        NSString *htmlString = [[[requestArry objectForKey:@"Data"] objectAtIndex:0] objectForKey:@"content"] ;
        UIWebView *web = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, _mainView.height)];
        self.title = [[[requestArry objectForKey:@"Data"] objectAtIndex:0] objectForKey:@"title"];
        [web loadHTMLString:htmlString baseURL:nil];
        //            web.delegate = self;
        [self addSubView:web];
    } err:^(id resultError) {
        
    }];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  HeartFriendsMain.m
//  康岁健康
//
//  Created by 低调 on 14/12/14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "HeartFriendsMain.h"
#import "HeartSecret.h"
#import "HeartTest.h"
@interface HeartFriendsMain ()

@end

@implementation HeartFriendsMain

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"心灵伙伴";
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, _mainView.height)];
    image.image = [UIImage imageNamed:@"HeartFriendsMain"];
    [self addSubView:image];
    
    UIButton *heartSecret = [UIButton buttonWithType:UIButtonTypeCustom];
    heartSecret.frame = CGRectMake(60, 300-42, 77, 77);
    [heartSecret setBackgroundImage:[UIImage imageNamed:@"heartScertBtn"] forState:UIControlStateNormal];
    [heartSecret addTarget:self action:@selector(toHeartSecret) forControlEvents:UIControlEventTouchUpInside];
    [self addSubView:heartSecret];
    
    UIButton *heartTest = [UIButton buttonWithType:UIButtonTypeCustom];
    heartTest.frame = CGRectMake(200, 330-42, 77, 77);
    [heartTest setBackgroundImage:[UIImage imageNamed:@"heartTestBtn"] forState:UIControlStateNormal];
    [heartTest addTarget:self action:@selector(toHeartTest) forControlEvents:UIControlEventTouchUpInside];
    [self addSubView:heartTest];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)toHeartSecret{
    HeartSecret *heartSecret = [[HeartSecret alloc]init];
    [self.navigationController pushViewController:heartSecret animated:YES];
}

-(void)toHeartTest{
    HeartTest *heartTest = [[HeartTest alloc]init];
    [self.navigationController pushViewController:heartTest animated:YES];
}


@end

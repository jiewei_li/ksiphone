//
//  HealthPlan.m
//  康岁健康
//
//  Created by 低调 on 14/12/14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "HealthPlan.h"
#import "WatchIntroduce.h"
@interface HealthPlan ()

@end

@implementation HealthPlan

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"健身计划";
    
    UIImageView *healthWatch = [[UIImageView alloc]init];
    healthWatch.frame = CGRectMake(0, 0, _mainView.width, 400);
    healthWatch.image = [UIImage imageNamed:@"healthPlaneB"];
    [self addSubView:healthWatch];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(10, 265, _mainView.width-20, 44);
    button.backgroundColor = [UIColor clearColor];
    [button addTarget:self action:@selector(toDetail) forControlEvents:UIControlEventTouchUpInside];
    [self addSubView:button];

    _mainView.backgroundColor = [UIColor colorWithRed:235/255. green:235/255. blue:235/255. alpha:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)toDetail{
    WatchIntroduce *intro = [[WatchIntroduce alloc]init];
    [self.navigationController pushViewController:intro animated:YES];
}
@end

//
//  WatchIntroduce.m
//  康岁健康
//
//  Created by 低调 on 14/12/14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "WatchIntroduce.h"

@interface WatchIntroduce ()

@end

@implementation WatchIntroduce

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"健康计划";
    UIScrollView *back = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, _mainView.height-40)];
    [self addSubView:back];
    
    UIImageView *shouldKnow = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, 1300)];
    shouldKnow.image = [UIImage imageNamed:@"healthWatchIntro"];
    [back addSubview:shouldKnow];
    
    back.contentSize = CGSizeMake(_mainView.width, 1300);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

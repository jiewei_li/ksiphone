//
//  DiseaseManagerViewController.m
//  康岁健康
//
//  Created by 低调 on 14/12/28.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
#import "WebViewController.h"
#import "DiseaseManagerViewController.h"
#import "HeartFriendArticalsBrief.h"
@interface DiseaseManagerViewController ()

@end

@implementation DiseaseManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"慢性病管理";
    [self addSubView:self.mainChoseView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)chose1:(id)sender{
    NSDictionary *dic =  user_defaults_get_object(@"ADDRESS_GETSERVERURLS");
    NSDictionary *urldic =  [dic objectForKey:@"Data"];

    WebViewController *brief = [[WebViewController alloc]init];
    brief.titleS = @"高血压";
    brief.url = [urldic objectForCJKey:@"url_chronic_GXY"];
    
//    HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"高血压" andTag:@"4"];
    [self.navigationController pushViewController:brief animated:YES];
}
-(IBAction)chose2:(id)sender{
    NSDictionary *dic =  user_defaults_get_object(@"ADDRESS_GETSERVERURLS");
    NSDictionary *urldic =  [dic objectForKey:@"Data"];
    
    WebViewController *brief = [[WebViewController alloc]init];
    brief.titleS = @"糖尿病";
    brief.url = [urldic objectForCJKey:@"url_chronic_TNB"];
//    HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"糖尿病" andTag:@"5"];
    [self.navigationController pushViewController:brief animated:YES];
}
-(IBAction)chose3:(id)sender{
    NSDictionary *dic =  user_defaults_get_object(@"ADDRESS_GETSERVERURLS");
    NSDictionary *urldic =  [dic objectForKey:@"Data"];
    
    WebViewController *brief = [[WebViewController alloc]init];
    brief.titleS = @"高血脂";
    brief.url = [urldic objectForCJKey:@"url_chronic_GXZ"];
//    HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"高血脂" andTag:@"7"];
    [self.navigationController pushViewController:brief animated:YES];
}
-(IBAction)chose4:(id)sender{
    NSDictionary *dic =  user_defaults_get_object(@"ADDRESS_GETSERVERURLS");
    NSDictionary *urldic =  [dic objectForKey:@"Data"];
    
    WebViewController *brief = [[WebViewController alloc]init];
    brief.titleS = @"高尿酸";
    brief.url = [urldic objectForCJKey:@"url_chronic_GNS"];
//    HeartFriendArticalsBrief *brief = [[HeartFriendArticalsBrief alloc]initWithTitle:@"高尿酸" andTag:@"8"];
    [self.navigationController pushViewController:brief animated:YES];
}

@end

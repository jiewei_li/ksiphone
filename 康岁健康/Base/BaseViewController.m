//
//  BaseViewController.m
//  康岁健康
//
//  Created by cerastes on 14-9-19.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    UISwipeGestureRecognizer *swip = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(goBack)];
    swip.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swip];

    _navView = [NavView defaultNavView];
    if (!self.navigationController || self.navigationController.viewControllers.count<2) {
        _navView.shouldGoBack = NO;
    }
    _navView.delegate = self;
    [self.view addSubview:_navView];
    
    float height = DEVICE_HEIGHT-_navView.height;
    if (IsIOS7) {
        height -= 49;
    }
    _mainView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, height)];
    [self.view addSubview:_mainView];
    _mainView.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor = [UIColor redColor];
    _mainView.top = _navView.bottom;
}

-(void)goBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)addSubView:(UIView *)view{
    [_mainView addSubview:view];
}

-(NSArray *)subviews{
    return _mainView.subviews;
}

-(void)setTitle:(NSString *)title{
    _navView.title = title;
}

-(void)setNavViewHidden:(BOOL)navViewHidden{
    if (navViewHidden) {
        _navView.hidden = YES;
        _mainView.height = _mainView.bottom;
        _mainView.top = 0;
    }
    else
    {
        _navView.hidden = NO;
        _mainView.height = _mainView.bottom-_navView.height;
        _mainView.top = _navView.height;
    }
}

-(void)enableSelf{
    self.view.userInteractionEnabled = YES;
}

-(void)unableSelf{
    self.view.userInteractionEnabled = NO;
}
@end

//
//  TabBarView.h
//  康岁健康
//
//  Created by cerastes on 14-9-21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol tabBarDelegate <NSObject>

-(void)selecttabOfIndex:(int)index;

@end
#import <UIKit/UIKit.h>

@interface TabBarView : UIView
{
    int _tabCount;
}
@property id<tabBarDelegate> delegate;
@property (nonatomic)int tabCount;
@end

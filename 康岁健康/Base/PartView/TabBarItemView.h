//
//  TabBarItemView.h
//  康岁健康
//
//  Created by cerastes on 14-9-21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarItemView : UIView
@property IBOutlet UIImageView *tabImage;
@property IBOutlet UILabel     *titleLabel;
@property (nonatomic) UIImage  *selectedImage;
@property (nonatomic) UIImage  *normalImage;
@property (nonatomic) NSString *title;
@property (nonatomic) UIColor  *normalColor;
@property (nonatomic) UIColor  *selectedTitleColor;
-(IBAction)selectAtIndex:(id)sender;
@end

//
//  TabBarView.m
//  康岁健康
//
//  Created by cerastes on 14-9-21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "TabBarView.h"
#import "TabBarItemView.h"
@implementation TabBarView
@synthesize tabCount;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *titles = @[@"首页",@"我的康岁",@"其他"];
        for (int i = 0;i<3; i++) {
            TabBarItemView *item = [[[NSBundle mainBundle]loadNibNamed:@"TabBarItemView" owner:self options:nil] lastObject];
            item.title = [NSString stringWithFormat:@"第%d个item",i];
            item.titleLabel.text = titles[i];
            item.tag = i;
            item.frame = CGRectMake(i*self.width/3, 0, self.width/3, self.height);
            [self addSubview:item];
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeState:) name:TabBarChangeNotifocation object:nil];
    }
    return self;
}
-(void)changeState:(id)notification{
    id obj = [notification object];//获取到传递的对象
    NSLog(@" obj = %@",obj);
    NSNumber *i = obj;
    if ([self.delegate respondsToSelector:@selector(selecttabOfIndex:)]) {
        [self.delegate selecttabOfIndex:[i intValue]];
    }
}

-(void)setTabCount:(int)tabcount{
    _tabCount = tabcount;
}

@end

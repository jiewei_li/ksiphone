//
//  NavView.h
//  康岁健康
//
//  Created by cerastes on 14-9-22.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol goBackDelegate <NSObject>

-(void)goBack;

@end
#import <UIKit/UIKit.h>

@interface NavView : UIView
{
    UILabel *_titleLabel;
    UIButton*_goBackButton;
}
+(id)defaultNavView;

@property (nonatomic)NSString *title;
@property (nonatomic)BOOL *shouldGoBack;

@property id<goBackDelegate> delegate;

@end

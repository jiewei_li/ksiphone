//
//  NavView.m
//  康岁健康
//
//  Created by cerastes on 14-9-22.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "NavView.h"

@implementation NavView
+(id)defaultNavView{
    float navHeight = 44;
    if (IsIOS7) {
        navHeight = 66;
    }
    NavView *nav = [[NavView alloc]initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, navHeight)];
    nav.backgroundColor = NavColor;
    
    nav->_titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, navHeight-55, DEVICE_WIDTH, navHeight)];
    nav->_titleLabel.backgroundColor = [UIColor clearColor];
    nav->_titleLabel.textColor = [UIColor whiteColor];
    [nav addSubview:nav->_titleLabel];
    if (!IsIOS7) {
        nav->_titleLabel.bottom = nav.height;
    }
    nav->_titleLabel.textAlignment = NSTextAlignmentCenter;
    
    nav->_goBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nav->_goBackButton.frame = CGRectMake(11, navHeight-33, 44, 22);
    [nav->_goBackButton setBackgroundImage:[UIImage imageNamed:@"backImage_unpressed"] forState:UIControlStateNormal];
    [nav->_goBackButton setBackgroundImage:[UIImage imageNamed:@"backImage_pressed"] forState:UIControlStateHighlighted];
    [nav addSubview:nav->_goBackButton];
    [nav->_goBackButton addTarget:nav action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    return nav;
}

-(void)setShouldGoBack:(BOOL *)shouldGoBack{
    if (shouldGoBack) {
        _goBackButton.hidden = NO;
    }
    else{
        _goBackButton.hidden = YES;
    }
}
//-(BOOL)sho
-(void)goBack{
    if([self.delegate respondsToSelector:@selector(goBack)]){
        [self.delegate goBack];
    }
}
-(void)setTitle:(NSString *)title{
    _titleLabel.text = title;
}
@end

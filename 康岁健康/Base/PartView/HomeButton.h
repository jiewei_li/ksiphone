//
//  HomeButton.h
//  康岁健康
//
//  Created by 低调 on 14/10/21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,HomeButtonType){
    CHECK_APOINTMENT = 0,      //体检预约
    
    HEALTH_FILE,               //健康档案
    PERSIONAL_DOCTOR,          //私人医生
    CHRONIC_DISEASE_MANAGER,   //慢性病管理
    MY_KANGSUI,                //我的康岁
    STRICT_TO_HEALTH,          //健康直通车
    HOMETOWN_OF_HEALTH,        //养生家园
    HELATH_PLANE,              //健身计划
    HELATH_MARKET,             //健康商城
    HELATH_BAOXIAN   ,          //健康保险
    J就医指南,
    Z掌上药房,
    J健康保险,
    Y预约挂号,
    F费用结算
    
    
    
};

@protocol HomeButtonDelegate <NSObject>

-(void)homeButtonPressed:(HomeButtonType)type;

@end
@interface HomeButton : UIView
{
    HomeButtonType _type;
    UIColor        *_beginColor;
    UIColor        *_endColor;
}
@property IBOutlet UILabel *titleLabel;
@property IBOutlet UIImageView *iconImage;
@property id<HomeButtonDelegate> delegate;
-(IBAction)toDetail:(id)sender;
-(void)setFrame:(CGRect)frame title:(NSString *)title image:(UIImage *)image beginColor:(UIColor *)beginColor endColor:(UIColor *)endColor type:(HomeButtonType)type;
@end

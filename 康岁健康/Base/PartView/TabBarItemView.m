//
//  TabBarItemView.m
//  康岁健康
//
//  Created by cerastes on 14-9-21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "TabBarItemView.h"

@implementation TabBarItemView
@synthesize  selectedImage;
@synthesize  normalImage;
@synthesize  title;
@synthesize  normalColor;
@synthesize  selectedTitleColor;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(void)awakeFromNib{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeState:) name:TabBarChangeNotifocation object:nil];
}
-(void)changeState:(id)notification{
    id obj = [notification userInfo];//获取到传递的对象
    NSLog(@" obj = %@",obj);
}

-(IBAction)selectAtIndex:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:TabBarChangeNotifocation object:[NSNumber numberWithInt:(int)self.tag] userInfo:nil];
}

-(void)setSelectedImage:(UIImage *)selectedImage{
    
}
-(void)setNormalImage:(UIImage *)normalImage{
    
}
-(void)settitle:(NSString *)title{
    
}
-(void)setNormalTitleColor:(UIColor *)normalColor{
    
}
-(void)setSelectedTitleColor:(UIColor *)selectedColor{
    
}

@end

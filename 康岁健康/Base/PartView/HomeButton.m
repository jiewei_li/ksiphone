//
//  HomeButton.m
//  康岁健康
//
//  Created by 低调 on 14/10/21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "HomeButton.h"
#import "CJAnimation.h"

@implementation HomeButton

-(void)setFrame:(CGRect)frame title:(NSString *)title image:(UIImage *)image beginColor:(UIColor *)beginColor endColor:(UIColor *)endColor type:(HomeButtonType)type{
    self.frame = frame;
    
    _type = type;
    _beginColor = beginColor;
    _endColor   = endColor;
    
    self.iconImage.image = image;
    self.iconImage.center = CGPointMake(self.width/2., self.height/2.);
    
    self.titleLabel.text = title;
    self.titleLabel.center = CGPointMake(self.width/2., self.height/2+25);
//    if (MY_KANGSUI == type) {
//        self.titleLabel.textColor = CJGrayColor;
//    }
    self.backgroundColor = [UIColor redColor];
}

-(void)drawRect:(CGRect)rect{
    CGContextRef context = UIGraphicsGetCurrentContext();
    NSArray *colors = [NSArray arrayWithObjects:
                       _beginColor,
                       _endColor,
                       nil];
    [self _drawGradientColor:context
                        rect:CGRectMake(0, 0, self.width, self.height)
                     options:kCGGradientDrawsAfterEndLocation
                      colors:colors];
    CGContextStrokePath(context);// 描线,即绘制形状
    CGContextFillPath(context);// 填充形状内的颜色
}
- (void)_drawGradientColor:(CGContextRef)p_context
                      rect:(CGRect)p_clipRect
                   options:(CGGradientDrawingOptions)p_options
                    colors:(NSArray *)p_colors {
    CGContextSaveGState(p_context);// 保持住现在的context
    CGContextClipToRect(p_context, p_clipRect);// 截取对应的context
    int colorCount = (int)p_colors.count;
    int numOfComponents = 4;
    CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
    CGFloat colorComponents[colorCount * numOfComponents];
    for (int i = 0; i < colorCount; i++) {
        UIColor *color = p_colors[i];
        CGColorRef temcolorRef = color.CGColor;
        const CGFloat *components = CGColorGetComponents(temcolorRef);
        for (int j = 0; j < numOfComponents; ++j) {
            colorComponents[i * numOfComponents + j] = components[j];
        }
    }
    CGGradientRef gradient =  CGGradientCreateWithColorComponents(rgb, colorComponents, NULL, colorCount);
    CGColorSpaceRelease(rgb);
    CGPoint startPoint = p_clipRect.origin;
    CGPoint endPoint = CGPointMake(CGRectGetMinX(p_clipRect), CGRectGetMaxY(p_clipRect));
    CGContextDrawLinearGradient(p_context, gradient, startPoint, endPoint, p_options);
    CGGradientRelease(gradient);
    CGContextRestoreGState(p_context);// 恢复到之前的context
}

-(IBAction)toDetail:(id)sender{
    CAKeyframeAnimation *popAnimation = [CJAnimation getMetroViewAnimation];
    [self.layer addAnimation:popAnimation forKey:nil];
    
    self.userInteractionEnabled = NO;
    [self performSelector:@selector(enableself) withObject:nil afterDelay:.5];
    
}

-(void)enableself{
    self.userInteractionEnabled = YES;
    if ([self.delegate respondsToSelector:@selector(homeButtonPressed:)]) {
        [self.delegate homeButtonPressed:_type];
    }
}
@end

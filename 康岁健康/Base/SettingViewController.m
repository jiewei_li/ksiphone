//
//  SettingViewController.m
//  康岁健康
//
//  Created by cerastes on 14-9-21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController ()
{
    NSArray *_imageArry;
}
@end

@implementation SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navViewHidden = YES;
    _imageArry = @[@"关于我们.png",@"检测更新.png",@"main_menu_exit.png"];
    
    
    
    UIButton *bu = [UIButton buttonWithType:UIButtonTypeCustom];
    bu.frame = CGRectMake(40, 0, 88, 88);
    bu.center = self.view.center;
    [bu setTitle:@"setting view" forState:UIControlStateNormal];
    bu.backgroundColor = [UIColor redColor];
    [self.view addSubview:bu];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  MainViewController.m
//  康岁健康
//
//  Created by cerastes on 14-9-19.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "MainViewController.h"
#import "AFHTTPRequestOperationManager.h"
@interface MainViewController ()
{
    UIImageView *_bottonView;
}
@end

@implementation MainViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    NSLog(@"DEVICE_HEIGHT = %f",DEVICE_HEIGHT);
    self.tabBar.hidden = YES;
    
    UIView *buttonView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.height-self.tabBar.height, self.view.width, self.tabBar.height)];
    _bottonView = [[UIImageView alloc]initWithFrame:buttonView.bounds];
    _bottonView.image = [UIImage imageNamed:@"tablebottom1.png"];
    [buttonView addSubview:_bottonView];
    
    UIButton *b1 = [UIButton buttonWithType:UIButtonTypeCustom];
    b1.frame = CGRectMake(0, 0, buttonView.width/4, buttonView.height);
    b1.tag = 1;
    b1.backgroundColor = CJClearColor;
    
    UIButton *b2 = [UIButton buttonWithType:UIButtonTypeCustom];
    b2.frame = CGRectMake(buttonView.width/4, 0, buttonView.width/4, buttonView.height);
    b2.tag = 2;
    b2.backgroundColor = CJClearColor;
    
    UIButton *b3 = [UIButton buttonWithType:UIButtonTypeCustom];
    b3.frame = CGRectMake(buttonView.width/4*2, 0, buttonView.width/4, buttonView.height);
    b3.tag = 3;
    b3.backgroundColor = CJClearColor;
    
    UIButton *b4 = [UIButton buttonWithType:UIButtonTypeCustom];
    b4.frame = CGRectMake(buttonView.width/4*3, 0, buttonView.width/4, buttonView.height);
    b4.tag = 4;
    b4.backgroundColor = CJClearColor;
    
    [buttonView addSubview:b1];
    [buttonView addSubview:b2];
    [buttonView addSubview:b3];
    [buttonView addSubview:b4];
    
    [b1 addTarget:self action:@selector(selecttabOfIndex:) forControlEvents:UIControlEventTouchUpInside];
    [b2 addTarget:self action:@selector(selecttabOfIndex:) forControlEvents:UIControlEventTouchUpInside];
    [b3 addTarget:self action:@selector(selecttabOfIndex:) forControlEvents:UIControlEventTouchUpInside];
    [b4 addTarget:self action:@selector(selecttabOfIndex:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:buttonView];
}

-(void)selecttabOfIndex:(UIButton *)sender{
    _bottonView.image = [UIImage imageNamed:[NSString stringWithFormat:@"tablebottom%d.png",sender.tag]];
    if (sender.tag<4) {
        self.selectedIndex = sender.tag-1;
    }
    
    else{
        UIActionSheet *as = [[UIActionSheet alloc]initWithTitle:@"我们将竭诚为您服务" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"请拨打400-800-0107" otherButtonTitles: nil];
        [as showInView:self.view];
    }
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        UIWebView *callWebview =[[UIWebView alloc] init] ;
        
        // tel:  或者 tel://
        NSURL *telURL =[NSURL URLWithString:@"tel://4008000107"];
        [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
        
        [self.view addSubview:callWebview];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

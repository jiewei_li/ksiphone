//
//  BaseNextTableViewController.m
//  康岁健康
//
//  Created by 低调 on 14-10-14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextTableViewController.h"

@implementation BaseNextTableViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, _mainView.height) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self addSubView:self.tableView];
    
    self.data = [[NSMutableArray alloc]init];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}
@end

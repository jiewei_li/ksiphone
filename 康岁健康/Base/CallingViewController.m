//
//  CallingViewController.m
//  康岁健康
//
//  Created by cerastes on 14-9-21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "CallingViewController.h"
#import "WebViewController.h"
#import"Common.h"
@interface CallingViewController ()
{
    NSArray *_imageArry;
    NSArray *_nameArry;
}
@end

@implementation CallingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.navViewHidden = YES;
    _imageArry = @[@"关于我们.png",@"检测更新.png",@"main_menu_exit.png"];
    _nameArry = @[@"关于我们",@"检测更新",@"退出"];

    self.title = @"设置";
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
// Dispose of any resources that can be recreated.
}

-(int)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _nameArry.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CallingViewController"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    cell.textLabel.text = [_nameArry objectAtIndex:indexPath.row];
//    cell.imageView.image = [UIImage imageNamed:@""];
    
    UILabel *l = [[UILabel alloc]initWithFrame:CGRectMake(50, 0, 300, 44)];
    l.backgroundColor = [UIColor clearColor];
    l.text = [_nameArry objectAtIndex:indexPath.row];
    [cell.contentView addSubview:l];
    
    UIImageView *im = [[UIImageView alloc]initWithFrame:CGRectMake(11, 11, 22, 22)];
    im.image = [UIImage imageNamed:[_imageArry objectAtIndex:indexPath.row]];
    [cell.contentView addSubview:im];
//    UIImageView 
//    if (indexPath.row == 0) {
//        cell.textLabel.text = [_nameArry objectAtIndex:indexPath.row];
//    }
//    else
//    {
//        cell.textLabel.text = @"检测更新";
//    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        NSDictionary *dic = user_defaults_get_object(@"ADDRESS_GETSERVERURLS");
        WebViewController *web = [[WebViewController alloc]init];
        web.titleS = @"关于康岁";
        web.url = [[dic objectForKey:@"Data"] objectForCJKey:@"url_aboutus"];
        [self.tabBarController.navigationController pushViewController:web animated:YES];
    }
    else
    {
        [Common checkUpDate];
    }
}
@end

//
//  BaseViewController.h
//  康岁健康
//
//  Created by cerastes on 14-9-19.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavView.h"
@interface BaseViewController : UIViewController<goBackDelegate>
{
    UIView *_mainView;
    NavView *_navView;
}
@property (nonatomic)BOOL navViewHidden;
@property(nonatomic,readonly,copy) NSArray *subviews;

-(void)addSubView:(UIView *)view;
-(void)unableSelf;
-(void)enableSelf;
@end

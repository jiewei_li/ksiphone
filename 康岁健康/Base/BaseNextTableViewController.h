//
//  BaseNextTableViewController.h
//  康岁健康
//
//  Created by 低调 on 14-10-14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"

@interface BaseNextTableViewController : BaseNextViewController<UITableViewDataSource,UITableViewDelegate>
@property UITableView *tableView;
@property NSMutableArray *data;

@end

//
//  HomeViewController.m
//  康岁健康
//
//  Created by cerastes on 14-9-21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "HomeViewController.h"
#import "ShoppingMainViewController.h"
#import "AddressManagerViewController.h"
#import "HomeButton.h"
#import "LogInViewController.h"
#import "YuYueMainViewController.h"
#import "PesionalDoctor.h"
#import "HeartFriendsMain.h"
#import "HealthPlan.h"
#import "ZhiTngChe.h"
#import "DiseaseManagerViewController.h"
#import "BaoXianMainViewController.h"
#import "HealthFileViewController.h"
#import "WebViewController.h"

@interface HomeViewController ()<HomeButtonDelegate>

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        
//        UIButton *bu = [UIButton buttonWithType:UIButtonTypeCustom];
//        bu.frame = CGRectMake(40, 0, 88, 88);
//        bu.center = self.view.center;
//        [bu setTitle:@"to shop" forState:UIControlStateNormal];
//        bu.backgroundColor = [UIColor redColor];
//        [self.view addSubview:bu];
////
//        [bu addTarget:self action:@selector(toadd) forControlEvents:UIControlEventTouchUpInside];
//        
//        
//        
//        [bu addTarget:self action:@selector(toShoppingCenter) forControlEvents:UIControlEventTouchUpInside];
//        
//        
//        UIButton *h = CJButtonN(CGRectMake(0, 80, 200, 44), @"地址选择", nil);
//        CJButtonBC(h,CJRedcolor);
//        CJButtonSB(h, self, @selector(toAddressChose));
//        [self addSubView:h];
        
        
        
        
        
    }
    return self;
}



-(void)toadd{
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navViewHidden = YES;
    
    // Custom initialization
    CGFloat topHeight = 0;
    if (IsIOS7) {
        topHeight = 20;
    }
    // Do any additional setup after loading the view.
    
    _mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, _mainView.height-topHeight)];
    _mainScrollView.backgroundColor = CJClearColor;
    [_mainView addSubview:_mainScrollView];
    
//        CHECK_APOINTMENT = 0,      //体检预约
//        HEALTH_FILE,               //健康档案
//        PERSIONAL_DOCTOR,          //私人医生
//        CHRONIC_DISEASE_MANAGER,   //慢性病管理
//        MY_KANGSUI,                //我的康岁
//        STRICT_TO_HEALTH,          //健康直通车
//        HOMETOWN_OF_HEALTH,        //养生家园
//        HELATH_PLANE,              //健身计划
//        HELATH_MARKET              //健康商城
//        HELATH_BAOXIAN             //健康保险
    UIImageView *topImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 80)];
    topImage.image = [UIImage imageNamed:@"HomeTopImage"];
    [_mainScrollView addSubview:topImage];
    HomeButton *checkAppointment = [[[NSBundle mainBundle]loadNibNamed:@"HomeButton" owner:self options:nil] lastObject];
    [checkAppointment setFrame:CGRectMake(5, 85, (self.view.width-15)/2., 200) title:@"就医指南" image:[UIImage imageNamed:@"02就医指南"] beginColor:CHECK_APOINTMENT_BEGIN_COLOR endColor:CHECK_APOINTMENT_END_COLOR type: J就医指南];
    checkAppointment.delegate = self;
    [_mainScrollView addSubview:checkAppointment];
    
    HomeButton *healthFile = [[[NSBundle mainBundle]loadNibNamed:@"HomeButton" owner:self options:nil] lastObject];
    [healthFile setFrame:CGRectMake(10+(self.view.width-15)/2., 85, 150, 97.5) title:@"预约/挂号" image:[UIImage imageNamed:@"03预约挂号"] beginColor:HEALTH_FILE_BEGIN_COLOR endColor:HEALTH_FILE_END_COLOR type:Y预约挂号];
    healthFile.delegate = self;
    [_mainScrollView addSubview:healthFile];
    
    HomeButton *persionalDoctor = [[[NSBundle mainBundle]loadNibNamed:@"HomeButton" owner:self options:nil] lastObject];
    [persionalDoctor setFrame:CGRectMake(10+(self.view.width-15)/2, 5+healthFile.bottom, 150, 97.5) title:@"费用结算" image:[UIImage imageNamed:@"04费用结算"] beginColor:PERSIONAL_DOCTOR_BEGIN_COLOR endColor:PERSIONAL_DOCTOR_END_COLOR type:F费用结算];
    persionalDoctor.delegate = self;
    [_mainScrollView addSubview:persionalDoctor];
    
    HomeButton *chronicDiseaseManager = [[[NSBundle mainBundle]loadNibNamed:@"HomeButton" owner:self options:nil] lastObject];
    [chronicDiseaseManager setFrame:CGRectMake(5, 5+checkAppointment.bottom,(self.view.width-20)/3., 100) title:@"健康档案" image:[UIImage imageNamed:@"05健康档案"] beginColor:CHRONIC_DISEASE_MANAGER_BEGIN_COLOR endColor:CHRONIC_DISEASE_MANAGER_END_COLOR type:HEALTH_FILE];
    chronicDiseaseManager.delegate = self;
    [_mainScrollView addSubview:chronicDiseaseManager];
    
    HomeButton *myKangsui = [[[NSBundle mainBundle]loadNibNamed:@"HomeButton" owner:self options:nil] lastObject];
    [myKangsui setFrame:CGRectMake(5+chronicDiseaseManager.right, 5+checkAppointment.bottom,(self.view.width-20)/3., 100) title:@"健康365" image:[UIImage imageNamed:@"health365"] beginColor:MY_KANGSUI_BEGIN_COLOR endColor:MY_KANGSUI_END_COLOR type:HELATH_MARKET];
    myKangsui.delegate = self;
    [_mainScrollView addSubview:myKangsui];
    
    HomeButton *strictToHealth = [[[NSBundle mainBundle]loadNibNamed:@"HomeButton" owner:self options:nil] lastObject];
    [strictToHealth setFrame:CGRectMake(5+myKangsui.right, 5+checkAppointment.bottom,(self.view.width-20)/3., 100) title:@"健康保险" image:[UIImage imageNamed:@"07健康保险"] beginColor:STRICT_TO_HEALTH_BEGIN_COLOR endColor:STRICT_TO_HEALTH_END_COLOR type:J健康保险];
    strictToHealth.delegate = self;
    [_mainScrollView addSubview:strictToHealth];
    
    HomeButton *homeTownOfHealth = [[[NSBundle mainBundle]loadNibNamed:@"HomeButton" owner:self options:nil] lastObject];
    [homeTownOfHealth setFrame:CGRectMake(5, 5+strictToHealth.bottom,(self.view.width-20)/3., 100) title:@"心灵伙伴" image:[UIImage imageNamed:@"heartFriends"] beginColor:HOMETOWN_OF_HEALTH_BEGIN_COLOR endColor:HOMETOWN_OF_HEALTH_END_COLOR type:HOMETOWN_OF_HEALTH];
    homeTownOfHealth.delegate = self;
    [_mainScrollView addSubview:homeTownOfHealth];

    HomeButton *healthPlane = [[[NSBundle mainBundle]loadNibNamed:@"HomeButton" owner:self options:nil] lastObject];
    [healthPlane setFrame:CGRectMake(5+homeTownOfHealth.right, 5+strictToHealth.bottom,(self.view.width-20)/3., 100) title:@"掌上药房" image:[UIImage imageNamed:@"09掌上药房"] beginColor:HELATH_PLANE_BEGIN_COLOR endColor:HELATH_PLANE_END_COLOR type:Z掌上药房];
    healthPlane.delegate = self;
    [_mainScrollView addSubview:healthPlane];

    HomeButton *healthMarket = [[[NSBundle mainBundle]loadNibNamed:@"HomeButton" owner:self options:nil] lastObject];
    [healthMarket setFrame:CGRectMake(5+healthPlane.right, 5+strictToHealth.bottom,(self.view.width-20)/3., 100) title:@"慢性病管理" image:[UIImage imageNamed:@"deseseManager"] beginColor:HELATH_MARKET_BEGIN_COLOR endColor:HELATH_MARKET_END_COLOR type:CHRONIC_DISEASE_MANAGER];
    healthMarket.delegate = self;
    [_mainScrollView addSubview:healthMarket];
    
    
    _mainScrollView.contentSize = CGSizeMake(self.view.width, healthMarket.bottom+40);

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - tmp
-(void)toAddressChose{
    AddressManagerViewController *manager = [[AddressManagerViewController alloc]init];
    [self.tabBarController.navigationController pushViewController:manager animated:YES];
}


-(void)homeButtonPressed:(HomeButtonType)type{
    if (type == CHECK_APOINTMENT) {
//        if (![UserInfos isUserLogIn]) {
//            [self toLogIn];
//            return;
//        }
//        YuYueMainViewController *yuyueMain = [[YuYueMainViewController alloc]initWithNibName:@"YuYueMainViewController" bundle:nil];
//        [self.tabBarController.navigationController pushViewController:yuyueMain animated:YES];
        
    }
    else if (type==Y预约挂号)
    {
        
        UIStoryboard *secondStoryBoard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        UIViewController* view = [secondStoryBoard instantiateViewControllerWithIdentifier:@"yuyue"];  //test2为viewcontroller的StoryboardId
      //  [self.navigationController pushViewController:view animated:YES];
       
        //
           self.tabBarController.navigationController.navigationBarHidden=false;
        
        
        [self.tabBarController.navigationController pushViewController:view animated:YES];

    }
    else if (type == Z掌上药房)
    {
        UIStoryboard *secondStoryBoard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        UIViewController* view = [secondStoryBoard instantiateViewControllerWithIdentifier:@"DrugsClass"];  //test2为viewcontroller的StoryboardId
        //  [self.navigationController pushViewController:view animated:YES];
        
        //
        
         self.tabBarController.navigationController.navigationBarHidden=false;
        [self.tabBarController.navigationController pushViewController:view animated:YES];
        
       
    }

    else if (type == J就医指南)
    {
        UIStoryboard *secondStoryBoard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        UIViewController* view = [secondStoryBoard instantiateViewControllerWithIdentifier:@"DoctorGuide"];  //test2为viewcontroller的StoryboardId
        //  [self.navigationController pushViewController:view animated:YES];
        
        //
        
        self.tabBarController.navigationController.navigationBarHidden=false;
        [self.tabBarController.navigationController pushViewController:view animated:YES];

    }
    else if(type==F费用结算)
    {
        UIStoryboard *secondStoryBoard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        UIViewController* view = [secondStoryBoard instantiateViewControllerWithIdentifier:@"F费用结算"];
        
        self.tabBarController.navigationController.navigationBarHidden=false;
        [self.tabBarController.navigationController pushViewController:view animated:YES];
        
    }
    else if (type == MY_KANGSUI) {
        if ([UserInfos isUserLogIn]) {
            [self toLogIn];
        }
        else
        {
            [self toLogIn];
        }
        
    }
    else if (type == HELATH_MARKET) {
        [self toShoppingCenter];
    }
    else if(type == PERSIONAL_DOCTOR){
        NSDictionary *dic = user_defaults_get_object(@"ADDRESS_GETSERVERURLS");
        WebViewController *web = [[WebViewController alloc]init];
        web.titleS = @"私人医生";
        web.url = [[dic objectForKey:@"Data"] objectForCJKey:@"url_personaldoctor"];
        [self.tabBarController.navigationController pushViewController:web animated:YES];
//        [self.navigationController pushViewController:[[PesionalDoctor alloc]init] animated:YES];
    }
    else if (type == HOMETOWN_OF_HEALTH){
        [self.tabBarController.navigationController pushViewController:[[HeartFriendsMain alloc]init] animated:YES];
    }
    else if (type == HELATH_PLANE){
        NSDictionary *dic = user_defaults_get_object(@"ADDRESS_GETSERVERURLS");
        WebViewController *web = [[WebViewController alloc]init];
        web.titleS = @"健身计划";
        web.url = [[dic objectForKey:@"Data"] objectForCJKey:@"url_sportplan"];
        [self.tabBarController.navigationController pushViewController:web animated:YES];

//        [self.tabBarController.navigationController pushViewController:[[HealthPlan alloc]init] animated:YES];

    }
    else if(type == STRICT_TO_HEALTH){
        NSDictionary *dic = user_defaults_get_object(@"ADDRESS_GETSERVERURLS");
        WebViewController *web = [[WebViewController alloc]init];
        web.titleS = @"就医直通车";
        web.url = [[dic objectForKey:@"Data"] objectForCJKey:@"url_expressdoctor"];
        [self.tabBarController.navigationController pushViewController:web animated:YES];
//        ZhiTngChe *z = [[ZhiTngChe alloc]init];
//        [self.tabBarController.navigationController pushViewController:z animated:YES];
    }
    else if (type == CHRONIC_DISEASE_MANAGER){
        DiseaseManagerViewController *m = [[DiseaseManagerViewController alloc]init];
        [self.tabBarController.navigationController pushViewController:m animated:YES];
    }
    else if (type == J健康保险){
        BaoXianMainViewController *b = [[BaoXianMainViewController alloc]initWithNibName:@"BaoXianMainViewController" bundle:nil];
        [self.tabBarController.navigationController pushViewController:b animated:YES];

    }
    else if (type == HEALTH_FILE){
        HealthFileViewController *h = [[HealthFileViewController alloc]initWithNibName:@"HealthFileViewController" bundle:nil];
        [self.tabBarController.navigationController pushViewController:h animated:YES];

    }
  }



-(void)toShoppingCenter{
    ShoppingMainViewController *shop = [[ShoppingMainViewController alloc]init];
    [self.tabBarController.navigationController pushViewController:shop animated:YES];
}

-(void)toMyKangsui{
    
}

-(void)toLogIn{
    LogInViewController *logIn = [[LogInViewController alloc]initWithNibName:@"LogInViewController" bundle:nil];
    [self.tabBarController.navigationController pushViewController:logIn animated:YES];
}
@end

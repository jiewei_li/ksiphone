//
//  ExamOrderData.h
//  康岁健康
//
//  Created by 低调 on 14/12/9.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExamOrderData : NSData
@property NSString *Id;
@property NSString *customer_id;
@property NSString *customer_name;
@property NSString *exam_center_id;
@property NSString *exam_center_name;
@property NSString *exam_set_id;
@property NSString *exam_set_name;
@property NSString *pay_mode_id;
@property NSString *date;
@property NSString *status_id;
@property NSString *price;
@property NSString *leftTime;

-(void)setData:(NSDictionary *)dic;
@end

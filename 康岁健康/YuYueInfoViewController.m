//
//  YuYueInfoViewController.m
//  康岁健康
//
//  Created by 低调 on 14/12/10.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "YuYueInfoViewController.h"

@interface YuYueInfoViewController ()

@end

@implementation YuYueInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.titleString;
    [self.infoLabel loadHTMLString:self.info baseURL:[NSURL fileURLWithPath: [[NSBundle mainBundle]  bundlePath]]];
    self.infoLabel.height = _mainView.height;
//    [self.infoLabel sizeToFit];
    [self addSubView:self.infoView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

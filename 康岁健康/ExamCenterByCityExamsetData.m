//
//  ExamCenterByCityExamsetData.m
//  康岁健康
//
//  Created by 低调 on 14/12/14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ExamCenterByCityExamsetData.h"

@implementation ExamCenterByCityExamsetData
-(void)setData:(NSDictionary *)data{
    self.Id = [data objectForCJKey:@"id"];
    self.addr = [data objectForCJKey:@"addr"];
    self.cost = [data objectForCJKey:@"cost"];
    self.name = [data objectForCJKey:@"name"];
}
@end

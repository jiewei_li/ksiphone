//
//  AppDelegate.m
//  康岁健康
//
//  Created by cerastes on 14-9-19.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "HomeViewController.h"
#import "SettingViewController.h"
#import "CallingViewController.h"
#import "AlixPayResult.h"
#import "DataVerifier.h"
#import "PartnerConfig.h"
#import "MyKangSuiViewController.h"
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    MainViewController *mainView = [[MainViewController alloc]init];
    UINavigationController *mainNav = [[UINavigationController alloc]initWithRootViewController:mainView];
    mainNav.navigationBarHidden = YES;
    self.window.rootViewController = mainNav;
    
    HomeViewController *home = [[HomeViewController alloc]init];
    UINavigationController *homeNav = [[UINavigationController alloc]initWithRootViewController:home];
    homeNav.navigationBarHidden = YES;

    
    
    MyKangSuiViewController *setting = [[MyKangSuiViewController alloc]init];
    UINavigationController *settingNav = [[UINavigationController alloc] initWithRootViewController:setting];
    settingNav.navigationBarHidden = YES;
    
    CallingViewController *call = [[CallingViewController alloc]init];
    UINavigationController *callNav = [[UINavigationController alloc]initWithRootViewController:call];
    callNav.navigationBarHidden = YES;
    
    NSArray *viewControllers = @[homeNav,settingNav,callNav];
    
    mainView.viewControllers = viewControllers;
    [NetRequest requestShopWithType:GETSERVERURLS withGetUrl:ADDRESS_GETSERVERURLS andGetInfo:nil result:^(id resultSuccess) {
        user_defaults_set_object(@"ADDRESS_GETSERVERURLS", resultSuccess);
    } err:^(id resultError) {
        
    }];
    [self.window makeKeyAndVisible];

    return YES;
}



- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
//独立客户端回调函数
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    [self parse:url application:application];
    return YES;
}

- (void)parse:(NSURL *)url application:(UIApplication *)application {
    
    //结果处理
    AlixPayResult* result = [self handleOpenURL:url];
    
    if (result)
    {
//        [UIAlertView showMessage:result.statusMessage];
        if (result.statusCode == 9000)
        {
            /*
             *用公钥验证签名 严格验证请使用result.resultString与result.signString验签
             */
            
            //交易成功
            NSString* key = AlipayPubKey;
            id<DataVerifier> verifier;
            verifier = CreateRSADataVerifier(key);
            
            if ([verifier verifyString:result.resultString withSign:result.signString])
            {
                [UIAlertView  showMessage:@"支付成功"];
            }
            else{
//                [UIAlertView showMessage:@"pay success"];
            }
            NSString *TransTips = user_defaults_get_string(@"TransTips");
            if (TransTips && TransTips.length>0) {
                [UIAlertView showMessage:TransTips];
                user_defaults_set_string(@"TransTips", @"");
            }
        }
        
        
        else
        {
            [UIAlertView showMessage:@"支付失败"];        }
    }
    else
    {
        [UIAlertView showMessage:@"pay fail"];
    }

}

- (AlixPayResult *)resultFromURL:(NSURL *)url {
    NSString * query = [[url query] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
#if ! __has_feature(objc_arc)
    return [[[AlixPayResult alloc] initWithString:query] autorelease];
#else
    return [[AlixPayResult alloc] initWithString:query];
#endif
}

- (AlixPayResult *)handleOpenURL:(NSURL *)url {
    AlixPayResult * result = nil;
    
    if (url != nil && [[url host] compare:@"safepay"] == 0) {
        result = [self resultFromURL:url];
    }
    
    return result;
}


@end

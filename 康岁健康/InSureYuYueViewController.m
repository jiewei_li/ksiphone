//
//  InSureYuYueViewController.m
//  康岁健康
//
//  Created by 低调 on 14/12/16.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "InSureYuYueViewController.h"
#import "ExamSetBriefData.h"
#import "CityByExamsetData.h"
#import "ExamCenterByCityExamsetData.h"
#import "CustomerData.h"
@interface InSureYuYueViewController ()

@end

@implementation InSureYuYueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"体检预约确认";
    [self addSubView:self.sureMainView];
    self.sureMainView.height = _mainView.height;
    self.mainScrollView.contentSize = CGSizeMake(self.view.width, 580);
    
    if (self.customerData) {
        self.userSexLabel.text  =[NSString stringWithFormat:@"%@  %@",[Common sexStringWithId:self.customerData.gender],[Common marryStringWithid:self.customerData.gender]];
        [Common sexStringWithId:self.customerData.gender];
        self.userNameLabel.text = self.customerData.name;
        self.userPhoneNumberLabel.text = self.customerData.tel;
        self.userCardNumberLabel.text = self.customerData.ID_number;
        self.userWorkNumberLabel.text = self.customerData.department;
        self.userBirthdayLabel.text = self.customerData.birthday;
        self.userCardTypeLabel.text = [Common idTypeWithid:self.customerData.ID_type];
        self.userMarriedLabel.text  = [Common marryStringWithid:self.customerData.married];
        self.userWorkNumberLabel.text = self.customerData.employee_num;
    }
    else
    {
        self.userNameLabel.text = [[UserInfos userInfo] Name];
        self.userSexLabel.text  = [NSString stringWithFormat:@"%@  %@",[Common sexStringWithId:[[UserInfos userInfo] Gender]],[Common marryStringWithid:[[UserInfos userInfo] Married]]];
        NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:[UserInfos sessionId],@"Sessionid",[UserInfos Id],@"Id", nil];

        [NetRequest requestShopWithType:GET_CUSTOMER withGetUrl:ADDRESS_GET_CUSTOMER andGetInfo:dic result:^(id resultSuccess) {
            CustomerData *customerData = resultSuccess;
            self.userNameLabel.text = customerData.name;
            self.userPhoneNumberLabel.text = customerData.tel;
            self.userCardNumberLabel.text = customerData.ID_number;
            self.userWorkNumberLabel.text = customerData.department;
            self.userMarriedLabel.text  = [Common marryStringWithid:customerData.married];

            self.userBirthdayLabel.text = customerData.birthday;
            self.userCardTypeLabel.text = [Common idTypeWithid:customerData.ID_type];
            self.userWorkNumberLabel.text = customerData.employee_num;

        } err:^(id resultError) {
            
        }];
    }
    self.eTextField.delegate = self;
    self.addTextField.delegate = self;
    self.pCodeField.delegate = self;
    self.examSetNameLabel.text = self.examSetBriefData.name;
    
    self.examDateLabel.text = [NSString stringWithFormat:@"%d%d%d",self.selectedDate.year,self.selectedDate.month,self.selectedDate.day];
    self.examTimeLabel.text = @"8:00-10:00";
    self.exametCenterLabel.text = self.examCenterByCityExamsetData.name;
    self.ExamAddressLabel.text = self.examCenterByCityExamsetData.addr;
    
    [self eChose:nil];
    
//    int userid：登录用户id（预约前）
//    int customerid,体检人id（预约前，如果是亲友预约，则与userid不相同）
//    int examsetid, 体检套餐id（预约前）
//    int examcenterid, 体检中心id（预约前）
//    DateTime bookDate,体检日期（预约前）
//    Int cityid, （暂未使用）
//    int provid（暂未使用）
    NSString *date = [NSString stringWithFormat:@"%@",[NSDate date]];
    date = [[date componentsSeparatedByString:@" "] objectAtIndex:0];
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:
                         [UserInfos customerId],@"userid",
                         self.customerData.Id?self.customerData.Id:[UserInfos customerId],@"customerid",
                         self.examSetBriefData.Id,@"examsetid",
                         self.examCenterByCityExamsetData.Id,@"examcenterid",
                         date,@"bookDate",
                         nil];
    [NetRequest requestShopWithType:GET_EXAMORDER_DETAIL withGetUrl:ADDRESS_GET_EXAMORDER_DETAIL andGetInfo:dic result:^(id resultSuccess) {
        NSDictionary *dic = [resultSuccess objectForKey:@"Data"];
        self.examTimeLabel.text = [dic objectForCJKey:@"exam_pretime"];
        self.examDateLabel.text = [dic objectForCJKey:@"exam_date"];
        self.examTypeLabel.text = [NSString stringWithFormat:@"%@ ¥ %@",[dic objectForCJKey:@"examset_type_name"],[dic objectForCJKey:@"price"]];
        int report_type = [[dic objectForKey:@"report_type"] integerValue];
        self.eTextField.text = [dic objectForKey:@"delivery_email"];
        self.addTextField.text = [dic objectForKey:@"delivery_address"];
        self.pCodeField.text = [dic objectForKey:@"delivery_zipcode"];
        if (report_type == 1) {
            [self eChose:nil];
        }
        else if(report_type == 2){
            [self pChose:nil];
        }
        else{
            [self epChose:nil];
        }
    } err:^(id resultError) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)eChose:(id)sender{
    self.eButton.selected = YES;
    self.pButton.selected = NO;
    self.epButton.selected = NO;
    self.eView.hidden = NO;
    self.pView.hidden = YES;
    self.bottomView.height = self.eView.bottom;
    self.mainScrollView.contentSize = CGSizeMake(22, self.bottomView.bottom+10);
}
-(IBAction)pChose:(id)sender{
    self.eButton.selected = NO;
    self.pButton.selected = YES;
    self.epButton.selected = NO;
    self.eView.hidden = YES;
    self.pView.hidden = NO;
    self.pView.top = self.eView.top;
    self.bottomView.height = self.pView.bottom;
    self.mainScrollView.contentSize = CGSizeMake(22, self.bottomView.bottom+10);
}
-(IBAction)epChose:(id)sender{
    self.eButton.selected = NO;
    self.pButton.selected = NO;
    self.epButton.selected = YES;
    self.eView.hidden = NO;
    self.pView.hidden = NO;
    self.pView.top = self.eView.bottom;
    self.bottomView.height = self.pView.bottom;
    self.mainScrollView.contentSize = CGSizeMake(22, self.bottomView.bottom+10);
}


-(IBAction)submit:(id)sender{
//    exam_set_id 体检套餐
//    exam_center_id 体检中心
//type: 0:单位套餐 1:个人套餐
//    price
//    payModeId 付款模式。忽略
//    bookDate
    NSString *customerid = [UserInfos userInfo].Id;
    if (self.customerData) {
        customerid = self.customerData.Id;
    }
    NSString *report_type = @"1";
    if (self.pButton.selected) {
        report_type = @"2";
    }
    else if(self.epButton.selected){
        report_type = @"3";
    }
        
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:
                         [UserInfos sessionId],@"sessionid",
                         [UserInfos userInfo].Id,@"user_id",
                         self.examSetBriefData.orderItemId,@"orderItemId",
                         customerid,@"customerid",
                         self.examSetBriefData.Id,@"exam_set_id",
                         self.examCenterByCityExamsetData.Id,@"exam_center_id",
                         self.examCenterByCityExamsetData.cost,@"exam_center_id",
                         @"",@"payModeId",
                         @"0",@"type",
                         self.examSetBriefData.price,@"price",
                         [NSString stringWithFormat:@"%d-%d-%d",[[NSDate date] year],[[NSDate date] month],[[NSDate date] day]],@"bookDate",
                         self.eTextField.text,@"email",
                         self.addTextField.text,@"addr",
                         self.pCodeField.text,@"zip_code",
                         report_type,@"report_type",
                         @"1",@"bUpdateAddr",
                         nil];

    
    [NetRequest requestShopWithType:ADD_EXAMORDER  withGetUrl:ADDRESS_ADD_EXAMORDER  andGetInfo:dic result:^(id resultSuccess) {
//        NSDictionary *dic = resultSuccess;
        [UIAlertView showMessage:@"预约成功" Content:@"体检预约成功，请在预约列表中查看你的预约信息" ConfrimButton:@"返回" handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
        }];
        
    } err:^(id resultError) {
        [UIAlertView showMessage:[resultError objectForKey:@"Data"]];
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    }];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    self.mainScrollView.top = -150;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.mainScrollView.top = 0;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.eTextField resignFirstResponder];
    [self.addTextField resignFirstResponder];
    [self.pCodeField resignFirstResponder];
    return YES;
}
@end

//
//  MyYuyueTableViewController.swift
//  亳州医保
//
//  Created by ljw on 15/3/7.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit

class MyYuyueTableViewController: UITableViewController {
    @IBOutlet var btnback: UIBarButtonItem!
    @IBAction func btn返回Click(sender: AnyObject) {
        
        self.navigationController?.popToRootViewControllerAnimated(true)
        self.navigationController?.navigationBarHidden=true
        
        
    }

    
    var doctors = [
        ["照片":"01d","订单":"332455","医生":"唐超","级别":"主任医师","医院":"亳州市人民医院","科室":"呼吸科","费用":"128.0","就诊人":"张学文","预约类型":"初诊","就诊时间":"2015-3-12 下午"]
        
        ,  ["照片":"02d","订单":"332457","医生":"杨淼","级别":"主任医师","医院":"亳州市人民医院","科室":"呼吸科","费用":"158.0","就诊人":"张学文","预约类型":"初诊","就诊时间":"2015-3-13 下午"]
        
        ,  ["照片":"03d","订单":"332459","医生":"张秀芳","级别":"主任医师","医院":"亳州市人民医院","科室":"呼吸科","费用":"228.0","就诊人":"张学文","预约类型":"初诊","就诊时间":"2015-3-14 下午"]
        

    ]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.navigationItem.leftBarButtonItem=self.btnback

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return doctors.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as MyYuyueTableViewCell

        var d =  doctors[indexPath.row] as NSDictionary
        var strphoto=d["照片"] as String
        cell.img照片.image = UIImage(named: strphoto)
        cell.lab订单.text = d["订单"] as String
        cell.lab就诊时间.text = d["就诊时间"] as String
        cell.lab就诊人.text = d["就诊人"] as String
        cell.lab费用.text = d["费用"] as String
        cell.lab医生.text = d["医生"] as String
//        var fm=CGRectMake(0, 28, 320, 1)
//        cell.line1.frame = fm
//        
        // Configure the cell...
        
        if(!cell.IsInit)
        {
            var line1=UIView(frame: CGRectMake(0, cell.img照片.frame.origin.y-2, 320, 1))
            var line2=UIView(frame: CGRectMake(0, cell.img照片.frame.height+cell.img照片.frame.origin.y+3, 320, 1))
            
            
            
            var line3=UIView(frame: CGRectMake(0, cell.lab就诊时间.frame.height+cell.lab就诊时间.frame.origin.y+3, 320, 1))
            line1.backgroundColor=UIColor.lightGrayColor()
            line2.backgroundColor=UIColor.lightGrayColor()
            line3.backgroundColor=UIColor.lightGrayColor()
            
            let cancelBtn: SFlatButton = SFlatButton(frame: CGRectMake(254, 174, 60, 30), sfButtonType: SFlatButton.SFlatButtonType.SFBDefault)
            cancelBtn.setTitle("取消预约", forState: UIControlState.Normal)
            cancelBtn.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
            cancelBtn.titleLabel?.font=UIFont.systemFontOfSize(13)
            
            cell.contentView.addSubview(cancelBtn)
            cell.contentView.addSubview(line1)
            cell.contentView.addSubview(line2)
            cell.contentView.addSubview(line3)
            cell.IsInit=true
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 220
    }



    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ChangePhone.m
//  康岁健康
//
//  Created by 低调 on 15/1/13.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

#import "ChangePhone.h"

@interface ChangePhone ()
{
    int seconds;
}
@end

@implementation ChangePhone

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"更换手机号";
    [self addSubView:self.cView];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)change:(id)sender{
    if ([self.pwdT.text isEmpty]) {
        [UIAlertView showMessage:@"请输入正确的密码"];
        return;
    }
    else if ([self.code.text isEmpty]) {
        [UIAlertView showMessage:@"请输入正确的密码"];
        return;
    }
       NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:
                         [UserInfos sessionId],@"sessionid",
                         [UserInfos customerId],@"id",
                         self.pwdT.text,@"tel",
                            self.code.text,@"verify_code",
                         nil];
    [NetRequest requestShopWithType:CHANGE_TEL  withGetUrl:ADDRESS_CHANGE_TEL  andGetInfo:dic result:^(id resultSuccess) {
        [UIAlertView showMessage:@"修改成功"];
    } err:^(id resultError) {
        [UIAlertView showMessage:[[resultError objectForCJKey:@"Data"] isEmpty]?@"修改失败":[resultError objectForCJKey:@"Data"]];
    }];
}
-(IBAction)getCode:(id)sender{
    seconds = 60;
    [self resetBut];
    self.codeBt.enabled = NO;
    NSDictionary *postDic = [[NSDictionary alloc]initWithObjectsAndKeys:self.pwdT.text,@"tel",@"2",@"validType", nil];
    //    }
    //
    [NetRequest requestShopWithType:SEND_VALIDSMS withGetUrl:ADDRESS_SEND_VALIDSMS andGetInfo:postDic result:^(id resultSuccess) {
        [UIAlertView showMessage:@"验证码已发送"];
    } err:^(id resultError) {
        if ([resultError objectForKey:@"Data"]) {
            [UIAlertView showMessage:[resultError objectForKey:@"Data"]];
        }
    }];
}
-(void)resetBut{
    [self.codeBt setTitle:[NSString stringWithFormat:@"%d秒后再试",seconds] forState:UIControlStateDisabled];
    seconds-=1;
    if (seconds>0) {
        [self performSelector:@selector(resetBut) withObject:nil afterDelay:1];
    }
    else
    {
        self.codeBt.enabled = YES;
        [self.codeBt setTitle:@"获取验证码" forState:UIControlStateNormal];

    }

}
@end

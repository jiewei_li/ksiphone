//
//  PersinalInfoViewController.m
//  康岁健康
//
//  Created by 低调 on 14/12/8.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "PersinalInfoViewController.h"
#import "CustomerData.h"
#import "CJSelectViewViewController.h"
#import "CountryData.h"
#import "PlaceData.h"
#import "YuYueViewController.h"
#import "ChangePhone.h"
#import "CompanyAccess.h"

@interface PersinalInfoViewController ()
{
    CustomerData *_customerData;
    CountryData  *_selectedCountry;
    PlaceData    *_selectedProvince;
    PlaceData    *_selectedCity;
    NSString     *_selectedCardType;
    UIScrollView *sc;
}
@end

@implementation PersinalInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"个人信息";
    self.view.backgroundColor = TAbBACCOLOR;
    _mainView.backgroundColor = CJClearColor;
    
    sc = [[UIScrollView alloc]initWithFrame:_mainView.frame];
    sc.backgroundColor = CJClearColor;
    [self addSubView:sc];
    sc.top = 0;
    _selectedCardType = @"0";
    [sc addSubview:self.persionMainView];
    
    if (self.type == SELFINFO) {
        [sc addSubview:self.selfHeadView];
        self.persionMainView.top = self.selfHeadView.bottom;
//        self.persionMainView.height = self.shenfenViewBottom.bottom;
        [sc addSubview:self.companyView];
        self.companyView.top = self.persionMainView.bottom;
        [sc addSubview:self.submitButton];
        self.submitButton.top = self.companyView.bottom+20;
        self.submitButton.centerX = sc.centerX;
        [self request];
    }
    else if (REALTIVEINFO == self.type){
        [sc addSubview:self.realityHeatyView];
        self.persionMainView.top = self.realityHeatyView.bottom;
        [sc addSubview:self.submitButton];
        self.submitButton.top = self.persionMainView.bottom+20;
        self.submitButton.centerX = sc.centerX;
        self.title = @"亲友信息";
    }
    else if (REALTIVEYUYUE == self.type){
        [sc addSubview:self.realityHeatyView];
        self.persionMainView.top = self.realityHeatyView.bottom;
        [sc addSubview:self.submitButton];
        self.submitButton.top = self.persionMainView.bottom+20;
        self.submitButton.centerX = sc.centerX;
        self.title = @"选择需预约的亲友";
        [self.submitButton setTitle:@"下一步" forState:UIControlStateNormal];
    }
    self.addressTextField.delegate = self;
    self.zipCodeTextField.delegate = self;
    self.mealeButton.selected = YES;
    self.unMarriedButton.selected = YES;
    sc.contentSize = CGSizeMake(self.view.width, self.submitButton.bottom+10);
    [self.view bringSubviewToFront:_navView];
}

-(void)request{
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:[UserInfos sessionId],@"Sessionid",[UserInfos Id],@"Id", nil];
    [NetRequest requestShopWithType:GET_CUSTOMER withGetUrl:ADDRESS_GET_CUSTOMER andGetInfo:dic result:^(id resultSuccess) {
        _customerData = resultSuccess;
        [self setMainView];
    } err:^(id resultError) {
        
    }];
}
-(void)setMainView{
    self.logInNameLabel.text = _customerData.login_name;
    self.nameTextField.text = _customerData.name;
    self.phoneTextField.text = _customerData.tel;
    if (!_customerData.tel || [_customerData.tel isEmpty]||self.type != SELFINFO) {
        self.changePhoneBtn.hidden = YES;
    }
    else if(self.type == SELFINFO)
    {
        self.changePhoneBtn.hidden = NO;
        
    }
    self.emailTextField.text = _customerData.email;
    self.cardNumberTextField.text = _customerData.ID_number;
    self.birthdayTextField.text = _customerData.birthday;
    self.departmentTextField.text = _customerData.department;
    self.provIdTextField.text  = _customerData.employee_num;
    self.addressTextField.text = _customerData.addr;
    self.zipCodeTextField.text = _customerData.zip_code;
    self.countryLabel.text = _customerData.country_name;
    self.provinceLabel.text = _customerData.prov_name;
    self.companyNameLabel.text = _customerData.company_name;
    self.company_verify_nameLabel.text = _customerData.company_verify_name;
    self.cityLabel.text = _customerData.city_name;
    self.cardNameTextField.text = [Common idTypeWithid:_customerData.ID_type];
    _selectedCardType = _customerData.ID_type;
    UIButton *b1 = [UIButton buttonWithType:UIButtonTypeCustom];
    b1.tag = [_customerData.married intValue];
    [self marryChose:b1];
    
    UIButton *b2 = [UIButton buttonWithType:UIButtonTypeCustom];
    b2.tag = [_customerData.gender intValue];
    [self sexChose:b2];
    if (self.type == SELFINFO) {
        if([[CompanyAccess birthday_change_allowed] isEqualToString:@"0"]){
            [self.birthdayTextField setEnabled:false];
        }
    
        if([[CompanyAccess depart_change_allowed] isEqualToString:@"0"] ){
            [self.departmentTextField setEnabled:false];
            self.provIdTextField.enabled = NO;
        }
        
        if([[CompanyAccess ID_change_allowed] isEqualToString:@"0"] ){
            [self.cardNameTextField setEnabled:false];
            [self.cardNumberTextField setEnabled:false];
        }
        
        if([[CompanyAccess name_change_allowed] isEqualToString:@"0"] ){
            [self.nameTextField setEnabled:false];
        }
      
        if([[CompanyAccess gender_change_allowed] isEqualToString:@"0"] ){
            if([[CompanyAccess gender_change_allowed] isEqualToString:@"0"] ){
                if([_customerData.gender isEqualToString:@"2"])
                    [self.mealeButton setEnabled:false];
                else if([_customerData.gender isEqualToString:@"1"])
                    [self.femealeButton setEnabled:false];
                else{
                    [self.mealeButton setEnabled:false];
                    [self.femealeButton setEnabled:false];
                }
            }
        }

        if([[CompanyAccess show_ID_number] isEqualToString:@"0"] ){
            self.shenfenViewBottom.top = self.shenfenView.top;
            self.shenfenView.hidden = YES;
            self.persionMainView.height -= self.shenfenView.height;
            self.companyView.top = self.persionMainView.bottom;

            self.submitButton.top = self.companyView.bottom+5;


            sc.contentSize = CGSizeMake(self.view.width, self.submitButton.bottom+10);

//            [self.cardNameTextField setHidden:true];
//            [self.cardNumberTextField setHidden:true];
            //[self.cardNameLabel setHidden:true];
            //[self.cardNumberLabel setHidden:true];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)sexChose:(id)sender{
    [self hideKeyBoard];
    UIButton *but = sender;
    if (but.tag == 1) {
        self.mealeButton.selected = YES;
        self.femealeButton.selected = NO;
    }
    else
    {
        self.mealeButton.selected = NO;
        self.femealeButton.selected = YES;
    }
}
-(IBAction)marryChose:(id)sender{
    [self hideKeyBoard];
    UIButton *but = sender;
    if (but.tag == 2) {
        self.unMarriedButton.selected = YES;
        self.marriedButton.selected = NO;
    }
    else
    {
        self.unMarriedButton.selected = NO;
        self.marriedButton.selected = YES;
    }
}
-(IBAction)realityChose:(id)sender{
    [self hideKeyBoard];
    
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:[UserInfos sessionId],@"sessionid",[UserInfos customerId],@"customerid", nil];
    [NetRequest requestShopWithType:REATION withGetUrl:ADDRESS_REATION andGetInfo:dic result:^(id resultSuccess) {
        NSArray *resultArr = resultSuccess;
        NSMutableArray *titleArr = [[NSMutableArray alloc]init];
        
        for (CustomerData *data in resultArr) {
            [titleArr addObject:data.name];
        }
        CJSelectViewViewController *s = [[[NSBundle mainBundle]loadNibNamed:@"CJSelectViewViewController" owner:nil options:nil] lastObject];
        s.titleLabel.text = @"亲友选择";
        s.prppreitys =resultArr;
        s.titles = titleArr;
        s.callBack = ^(id selectedValue){
            _customerData = selectedValue;
            NSDictionary *dicc = [[NSDictionary alloc]initWithObjectsAndKeys:[UserInfos sessionId],@"Sessionid",_customerData.Id,@"Id", nil];
            [NetRequest requestShopWithType:GET_CUSTOMER withGetUrl:ADDRESS_GET_CUSTOMER andGetInfo:dicc result:^(id resultSuccess) {
                _customerData = resultSuccess;
                [self setMainView];
            } err:^(id resultError) {
                
            }];
//            [self setMainView];
        };
        
        s.frame = self.view.frame;
        [self.navigationController.view addSubview:s];
    } err:^(id resultError) {
        
    }];
}

-(IBAction)cardTypeChose:(id)sender{

    NSArray *resultArr = @[@"0",@"1",@"2"];
    NSArray *titleArr  = @[@"居民身份证",@"护照",@"社保卡"];
    CJSelectViewViewController *s = [[[NSBundle mainBundle]loadNibNamed:@"CJSelectViewViewController" owner:nil options:nil] lastObject];
    s.titleLabel.text = @"证件类型选择";
    s.prppreitys =resultArr;
    s.titles = titleArr;
    s.callBack = ^(id selectedValue){
        _selectedCardType = selectedValue;
        self.cardNameTextField.text = [Common idTypeWithid:selectedValue];
    };
    s.frame = self.view.frame;
    [self.navigationController.view addSubview:s];
}

-(IBAction)countryChose:(id)sender{
    [self hideKeyBoard];
    [NetRequest requestShopWithType:COUNTRY withGetUrl:ADDRESS_COUNTRY andGetInfo:nil result:^(id resultSuccess) {
        NSArray *resultArr = resultSuccess;
        NSMutableArray *titleArr = [[NSMutableArray alloc]init];
        
        for (CountryData *data in resultArr) {
            [titleArr addObject:data.name];
        }
        CJSelectViewViewController *s = [[[NSBundle mainBundle]loadNibNamed:@"CJSelectViewViewController" owner:nil options:nil] lastObject];
        s.titleLabel.text = @"选择国家";
        s.prppreitys =resultArr;
        s.titles = titleArr;
        s.callBack = ^(id selectedValue){
            _selectedCountry = selectedValue;
            self.countryLabel.text = _selectedCountry.name;
            _selectedProvince = nil;
            _selectedCity     = nil;
            self.provinceLabel.text = @"";
            self.cityLabel.text = @"";
        };
        
        s.frame = self.view.frame;
        [self.navigationController.view addSubview:s];
    } err:^(id resultError) {
        
    }];
}
-(IBAction)provinceChose:(id)sender{
    [self hideKeyBoard];
    if (!_selectedCountry) {
        [self countryChose:nil];
        return;
    }
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:_selectedCountry.Id,@"countryId",nil];
    [NetRequest requestShopWithType:PROVINCE withGetUrl:ADDRESS_PROVINCE andGetInfo:dic result:^(id resultSuccess) {
        NSArray *resultArr = resultSuccess;
        NSMutableArray *titleArr = [[NSMutableArray alloc]init];
        
        for (PlaceData *data in resultArr) {
            [titleArr addObject:data.name];
        }
        CJSelectViewViewController *s = [[[NSBundle mainBundle]loadNibNamed:@"CJSelectViewViewController" owner:nil options:nil] lastObject];
        s.titleLabel.text = @"选择省";
        s.prppreitys =resultArr;
        s.titles = titleArr;
        s.callBack = ^(id selectedValue){
            _selectedProvince = selectedValue;
            self.provinceLabel.text = _selectedProvince.name;
            _selectedCity     = nil;
            self.cityLabel.text = @"";
        };
        
        s.frame = self.view.frame;
        [self.navigationController.view addSubview:s];
    } err:^(id resultError) {
        
    }];
    
}
-(IBAction)cityChose:(id)sender{
    [self hideKeyBoard];
    if (!_selectedProvince) {
        [self provinceChose:nil];
        return;
    }
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:_selectedCountry.Id,@"country",_selectedProvince.Id,@"province",nil];
    [NetRequest requestShopWithType:CITY withGetUrl:ADDRESS_CITY andGetInfo:dic result:^(id resultSuccess) {
        NSArray *resultArr = resultSuccess;
        NSMutableArray *titleArr = [[NSMutableArray alloc]init];
        
        for (PlaceData *data in resultArr) {
            [titleArr addObject:data.name];
        }
        CJSelectViewViewController *s = [[[NSBundle mainBundle]loadNibNamed:@"CJSelectViewViewController" owner:nil options:nil] lastObject];
        s.titleLabel.text = @"选择城市";
        s.prppreitys =resultArr;
        s.titles = titleArr;
        s.callBack = ^(id selectedValue){
            _selectedCity = selectedValue;
            self.cityLabel.text = _selectedCity.name;
        };
        
        s.frame = self.view.frame;
        [self.navigationController.view addSubview:s];
    } err:^(id resultError) {
        
    }];
}

-(IBAction)addNew:(id)sender{
    _customerData = nil;
    [self setMainView];
}



-(IBAction)submit:(id)sender{
    [self hideKeyBoard];
    if ([_nameTextField.text isEmpty]) {
        [UIAlertView showMessage:@"请输入姓名"];
        return;
    }
    if ([self.phoneTextField.text isEmpty] || ![self.phoneTextField.text isPhoneNum]) {
        [UIAlertView showMessage:@"请输入正确的手机号"];
        return;
    }
    if ([self.emailTextField.text isEmpty] || ![self.emailTextField.text isEmail]) {
        [UIAlertView showMessage:@"请输入正确的邮箱"];
        return;
    }
    if ([self.birthdayTextField.text isEmpty] || self.birthdayTextField.text.length!=8) {
        [UIAlertView showMessage:@"请输入正确的生日"];
        return;
    }
    if ([self.cardNumberTextField.text isEmpty]) {
        [UIAlertView showMessage:@"请输入身份证号"];
        return;
    }
    if (!_selectedCardType) {
        [UIAlertView showMessage:@"请选择正确的证件号"];
        return;
    }
    NSString *selectedId;
    if (_selectedCity) {
        selectedId = _selectedCity.Id;
    }
    else if(!self.companyCodeView.hidden && [self.companyCode.text isEmpty]){
        [UIAlertView showMessage:@"请输入公司代码"];
        return;
    }
    else{
        selectedId = _customerData.city_name;
    }
    if (self.type == SELFINFO) {
        NSMutableDictionary *userInf = [[NSMutableDictionary alloc]initWithObjectsAndKeys:
                                 [UserInfos sessionId],@"Sessionid",
                                 [UserInfos customerId],@"Id",
                                 _nameTextField.text,@"Name",
                                 self.mealeButton.selected?@"1":@"2",@"Gender",
                                 self.unMarriedButton.selected?@"2":@"1",@"Married",
                                 self.phoneTextField.text,@"Tel",
                                 self.emailTextField.text,@"Email",
                                 self.birthdayTextField.text,@"Birthday",
                                 _selectedCardType,@"IDtype",
                                 self.cardNumberTextField.text,@"ID_number",
                                 @"3",@"Report_type",
                                 [self.addressTextField.text notEmpty],@"Addr",
                                 [self.zipCodeTextField.text notEmpty],@"Zip_code",
                                 [self.departmentTextField.text notEmpty],@"department",
                                 [self.provIdTextField.text notEmpty],@"employee_number",
                                 [selectedId notEmpty],@"city_id",
                                 nil];
        if(!self.companyCodeView.hidden){
            [userInf setObject:self.companyCode.text forKey:@"company_code"];
        }
        [NetRequest requestShopWithType:UPDCUSTOMER  withGetUrl:ADDRESS_UPDCUSTOMER  andGetInfo:userInf result:^(id resultSuccess) {
            if ([[resultSuccess objectForKey:@"Suc"] intValue] == 1) {
                [UIAlertView showMessage:@"修改成功"];
            }
        } err:^(id resultError) {
            if ([resultError objectForKey:@"Data"]) {
                [UIAlertView showMessage:[resultError objectForKey:@"Data"]];
            }
        }];
    }
    else if(self.type == REALTIVEINFO || self.type == REALTIVEYUYUE){
        if (!_customerData) {
            [self addReality];
            return;
        }
        NSDictionary *userInf = [[NSDictionary alloc]initWithObjectsAndKeys:
                                 [UserInfos sessionId],@"Sessionid",
                                 _customerData.Id,@"Id",
                                 [UserInfos customerId],@"related_id",
                                 _nameTextField.text,@"Name",
                                 self.mealeButton.selected?@"1":@"2",@"Gender",
                                 self.unMarriedButton.selected?@"2":@"1",@"Married",
                                 self.phoneTextField.text,@"Tel",
                                 self.emailTextField.text,@"Email",
                                 self.birthdayTextField.text,@"Birthday",
                                 _selectedCardType,@"IDtype",
                                 self.cardNumberTextField.text,@"ID_number",
                                 @"3",@"Report_type",
                                 self.addressTextField.text,@"Addr",
                                 self.zipCodeTextField.text,@"Zip_code",
                                 selectedId,@"city_id",
                                 nil];
        [NetRequest requestShopWithType:UPDCUSTOMER  withGetUrl:ADDRESS_UPDCUSTOMER  andGetInfo:userInf result:^(id resultSuccess) {
            if ([[resultSuccess objectForKey:@"Suc"] intValue] == 1) {
                if(self.type == REALTIVEINFO)
                    [UIAlertView showMessage:@"修改成功"];
            }
        } err:^(id resultError) {
            
        }];
    }
    if(self.type == REALTIVEYUYUE){
        if (!_customerData) {
            [self addReality];
            return;
        }
        else{
            YuYueViewController *yuyue = [[YuYueViewController alloc]initWithNibName:@"YuYueViewController" bundle:nil];
            yuyue.customerData = _customerData;
            [self.navigationController pushViewController:yuyue animated:YES];
        }
    }
}

-(void)addReality{
    NSDictionary *userInf = [[NSDictionary alloc]initWithObjectsAndKeys:
                             [UserInfos sessionId],@"Sessionid",
                             @"0",@"id",
                             [UserInfos customerId],@"related_id",
                             _nameTextField.text,@"name",
                             self.mealeButton.selected?@"1":@"2",@"gender",
                             self.unMarriedButton.selected?@"2":@"1",@"married",
                             self.phoneTextField.text,@"tel",
                             self.emailTextField.text,@"email",
                             self.birthdayTextField.text,@"birthday",
                             @"1",@"IDtype",
                             self.cardNumberTextField.text,@"ID_number",
                             @"3",@"report_type",
                             self.addressTextField.text,@"addr",
                             self.zipCodeTextField.text,@"zip_code",
                             _selectedCity.Id,@"city_id",
                             nil];
    [NetRequest requestShopWithType:UPDCUSTOMER withGetUrl:ADDRESS_UPDCUSTOMER andGetInfo:userInf result:^(id resultSuccess) {
        NSDictionary *dicc = [[NSDictionary alloc]initWithObjectsAndKeys:[UserInfos sessionId],@"Sessionid",[[resultSuccess objectForKey:@"Data"] objectForCJKey:@"id"],@"Id", nil];
        [NetRequest requestShopWithType:GET_CUSTOMER withGetUrl:ADDRESS_GET_CUSTOMER andGetInfo:dicc result:^(id resultSuccess) {
            _customerData = resultSuccess;
            [self setMainView];
            [self submit:nil];
        } err:^(id resultError) {
            
        }];
    } err:^(id resultError) {
        
    }];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag != -1) {
        sc.top = -120;
    }
    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if (textView.tag != -1) {
        sc.top = -120;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    sc.top = 0;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(void)hideKeyBoard{
    for (UITextField *v in sc.subviews) {
        if ([v isKindOfClass:[UITextField class]]) {
            [v resignFirstResponder];
            
        }
    }
}

-(IBAction)changPhoneWord:(id)sender{
    ChangePhone *p = [[ChangePhone alloc]initWithNibName:@"ChangePhone" bundle:nil];
    [self.navigationController pushViewController:p animated:YES];
}

-(IBAction)changCompany:(id)sender{
    UIButton *bt = sender;
    if (bt.selected) {
        bt.selected = NO;
        self.companyCodeView.hidden = YES;
        self.companyView.height = self.companyCodeView.top+37;
        self.companyView2.height = self.companyCodeView.top;
    }
    else
    {
        bt.selected = YES;
        self.companyCodeView.hidden = NO;
        self.companyView.height = self.companyCodeView.bottom+37;
        self.companyView2.height = self.companyCodeView.bottom;
    }
    self.submitButton.top = self.companyView.bottom+20;
    self.submitButton.centerX = sc.centerX;
    sc.contentSize = CGSizeMake(self.view.width, self.submitButton.bottom+10);

}
@end

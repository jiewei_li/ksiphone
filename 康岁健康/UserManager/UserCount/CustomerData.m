//
//  CustomerData.m
//  康岁健康
//
//  Created by 低调 on 14/12/8.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "CustomerData.h"

@implementation CustomerData
-(void)setData:(NSDictionary *)dic{
    NSDictionary *data = [dic objectForKey:@"Data"];
    self.ID_number = [data objectForCJKey:@"ID_number"];
    self.ID_type = [data objectForCJKey:@"ID_type"];
    self.addr = [data objectForCJKey:@"addr"];
    self.birthday = [data objectForCJKey:@"birthday"];
    self.city_id = [data objectForCJKey:@"city_id"];
    self.company_code = [data objectForCJKey:@"company_code"];
    self.company_name = [data objectForCJKey:@"company_name"];
    self.companyid = [data objectForCJKey:@"companyid"];
    self.department = [data objectForCJKey:@"department"];
    self.email = [data objectForCJKey:@"email"];
    self.employee_num = [data objectForCJKey:@"employee_num"];
    self.employee_type_id = [data objectForCJKey:@"employee_type_id"];
    self.gender = [data objectForCJKey:@"gender"];
    self.Id = [data objectForCJKey:@"id"];
    self.login_name = [data objectForCJKey:@"login_name"];
    self.login_pw = [data objectForCJKey:@"login_pw"];
    self.married = [data objectForCJKey:@"married"];
    self.name = [data objectForCJKey:@"name"];
    self.prov_id = [data objectForCJKey:@"prov_id"];
    self.report_type = [data objectForCJKey:@"report_type"];
    self.tel = [data objectForCJKey:@"tel"];
    self.vip_type_id = [data objectForCJKey:@"vip_type_id"];
    self.zip_code = [data objectForCJKey:@"zip_code"];
    
    self.city_name = [data objectForCJKey:@"city_name"];
    self.company_verify_name = [data objectForCJKey:@"company_verify_name"];
    self.company_verify_status = [data objectForCJKey:@"company_verify_status"];
    self.country_id = [data objectForCJKey:@"country_id"];
    self.country_name = [data objectForCJKey:@"country_name"];
    self.prov_name = [data objectForCJKey:@"prov_name"];
}
-(void)setrData:(NSDictionary *)dic{
    self.ID_number = [dic objectForCJKey:@"ID_number"];
    self.ID_type = [dic objectForCJKey:@"ID_type"];
    self.birthday = [dic objectForCJKey:@"birthday"];
    self.gender = [dic objectForCJKey:@"gender"];
    self.Id = [dic objectForCJKey:@"id"];
    self.married = [dic objectForCJKey:@"married"];
    self.name = [dic objectForCJKey:@"name"];
    self.tel = [dic objectForCJKey:@"tel"];
}

@end

//
//  ChangePaswordViewController.h
//  康岁健康
//
//  Created by 低调 on 14/12/21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"

@interface ChangePaswordViewController : BaseNextViewController
@property IBOutlet UIView *cView;
@property IBOutlet UITextField *pwdT;
@property IBOutlet UITextField *rpwdT;
-(IBAction)change:(id)sender;
@end

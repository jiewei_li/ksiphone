//
//  ChangePhone.h
//  康岁健康
//
//  Created by 低调 on 15/1/13.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"

@interface ChangePhone : BaseNextViewController
@property IBOutlet UIView *cView;
@property IBOutlet UITextField *pwdT;
@property IBOutlet UITextField *code;
@property IBOutlet UIButton   *codeBt;

-(IBAction)change:(id)sender;
-(IBAction)getCode:(id)sender;
@end

//
//  MyKangSuiViewController.h
//  康岁健康
//
//  Created by 低调 on 14/12/7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"

@interface MyKangSuiViewController : BaseNextViewController
@property IBOutlet UIView *topView;
@property IBOutlet UIView *topLogInView;
@property IBOutlet UIView *userManagerView;

@property IBOutlet UIView *topUsrerInfoView;
@property IBOutlet UILabel *userNameLabel;
@property IBOutlet UILabel *jifengLabel;
@property IBOutlet UIImageView *userIcon;
@property IBOutlet UIButton    *logOutButton;
-(IBAction)logIn:(id)sender;
-(IBAction)logOut:(id)sender;
-(IBAction)toSelf:(id)sender;
-(IBAction)toRelatives:(id)sender;
-(IBAction)changePassWd:(id)sender;
@end

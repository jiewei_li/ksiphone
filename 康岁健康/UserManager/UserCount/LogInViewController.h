//
//  LogInViewController.h
//  康岁健康
//
//  Created by 低调 on 14/12/7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"

@interface LogInViewController : BaseNextViewController<UITextFieldDelegate>
@property IBOutlet UIView *topView;

@property IBOutlet UIView *middleView;
@property IBOutlet UIView *tapView;
@property IBOutlet UIButton *logInButton;
@property IBOutlet UITextField *userNunber;
@property IBOutlet UITextField *passWord;

-(IBAction)changePassWordType:(id)sender;
-(IBAction)logIn:(id)sender;
-(IBAction)regist:(id)sender;
-(IBAction)forgetPassword:(id)sender;

@end

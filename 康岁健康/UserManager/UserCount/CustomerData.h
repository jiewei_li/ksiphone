//
//  CustomerData.h
//  康岁健康
//
//  Created by 低调 on 14/12/8.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomerData : NSObject
@property NSString *ID_number ;
@property NSString *ID_type ;
@property NSString *addr ;
@property NSString *birthday ;
@property NSString *city_id ;
@property NSString *comment ;
@property NSString *company_code ;
@property NSString *company_name ;
@property NSString *companyid  ;
@property NSString *department ;
@property NSString *email ;
@property NSString *employee_num ;
@property NSString *employee_type_id ;
@property NSString *gender ;
@property NSString *Id;
@property NSString *login_name;
@property NSString *login_pw ;
@property NSString *married  ;
@property NSString *name ;
@property NSString *prov_id ;
@property NSString *report_type ;
@property NSString *tel ;
@property NSString *vip_type_id ;
@property NSString *zip_code ;
@property NSString *city_name ;
@property NSString *company_verify_name ;
@property NSString *company_verify_status ;
@property NSString *country_id ;
@property NSString *country_name ;
@property NSString *prov_name ;
-(void)setData:(NSDictionary *)dic;
-(void)setrData:(NSDictionary *)dic;
@end

//
//  PersinalInfoViewController.h
//  康岁健康
//
//  Created by 低调 on 14/12/8.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"
typedef NS_ENUM(int, PersionType){
    SELFINFO = 0,
    REALTIVEINFO,
    REALTIVEYUYUE
};
@interface PersinalInfoViewController : BaseNextViewController<UITextFieldDelegate,UITextViewDelegate>
@property PersionType type;
@property IBOutlet UIView *selfHeadView;
@property IBOutlet UIView *realityHeatyView;
@property IBOutlet UIView *persionMainView;
@property IBOutlet UIView *companyView;
@property IBOutlet UIView *companyView2;
@property IBOutlet UILabel *logInNameLabel;
@property IBOutlet UILabel *cardNameLabel;
@property IBOutlet UILabel *cardNumberLabel;

@property IBOutlet UITextField *nameTextField;
@property IBOutlet UITextField *phoneTextField;
@property IBOutlet UITextField *emailTextField;
@property IBOutlet UITextField *cardNumberTextField;
@property IBOutlet UITextField *cardNameTextField;
@property IBOutlet UITextField *birthdayTextField;
@property IBOutlet UITextField *departmentTextField;
@property IBOutlet UITextField *provIdTextField;
@property IBOutlet UITextView  *addressTextField;
@property IBOutlet UITextField *zipCodeTextField;
@property IBOutlet UILabel     *companyNameLabel;
@property IBOutlet UILabel     *company_verify_nameLabel;

@property IBOutlet UIButton *mealeButton;
@property IBOutlet UIButton *femealeButton;
@property IBOutlet UIButton *unMarriedButton;
@property IBOutlet UIButton *marriedButton;

@property IBOutlet UITextField *countryLabel;
@property IBOutlet UITextField *provinceLabel;
@property IBOutlet UITextField *cityLabel;
@property IBOutlet UITextField *companyCode;
@property IBOutlet UIView      *companyCodeView;
@property IBOutlet UIView      *compant_verify_statusView;
@property IBOutlet UIView      *shenfenView;
@property IBOutlet UIView      *shenfenViewBottom;

@property IBOutlet UIButton *submitButton;
@property IBOutlet UIButton *changePhoneBtn;
-(IBAction)sexChose:(id)sender;
-(IBAction)marryChose:(id)sender;
-(IBAction)realityChose:(id)sender;
-(IBAction)cardTypeChose:(id)sender;
-(IBAction)countryChose:(id)sender;
-(IBAction)provinceChose:(id)sender;
-(IBAction)cityChose:(id)sender;
-(IBAction)addNew:(id)sender;
-(IBAction)submit:(id)sender;
-(IBAction)changPhoneWord:(id)sender;
-(IBAction)changCompany:(id)sender;
@end

//
//  CompanyAccess.m
//  康岁健康
//
//  Created by cloudihome on 15-1-14.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CompanyAccess.h"
@implementation CompanyAccess

+(CompanyAccess *)companyAccess{
    CompanyAccess *companyAccess = [[CompanyAccess alloc]init];
    
    NSData *data = user_defaults_get_object(@"companyAccess"); //??
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    NSDictionary *myDictionary = [unarchiver decodeObjectForKey:@"Some Key Value"];
    [unarchiver finishDecoding];
    [companyAccess setCompanyAccess:myDictionary];
    return companyAccess;
}

-(void)setCompanyAccess:(NSDictionary *)dic{
    NSMutableData *mData = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:mData];
    [archiver encodeObject:dic forKey:@"Some Key Value"];
    [archiver finishEncoding];
    
    user_defaults_set_object(@"companyAccess", mData);
    
    NSDictionary *data = [dic objectForKey:@"Data"];
    self.company_id = [data objectForCJKey:@"company_id"];
    self.birthday_change_allowed = [data objectForCJKey:@"birthday_change_allowed"];
    self.depart_change_allowed = [data objectForCJKey:@"depart_change_allowed"];
    self.ID_change_allowed = [data objectForCJKey:@"ID_change_allowed"];
    self.name_change_allowed = [data objectForCJKey:@"name_change_allowed"];
    self.show_report_type = [data objectForCJKey:@"show_report_type"];
    self.show_ID_number  = [data objectForCJKey:@"show_ID_number"];
    self.gender_change_allowed = [data objectForCJKey:@"gender_change_allowed"];
}

+(NSString *)birthday_change_allowed{
    return [CompanyAccess companyAccess].birthday_change_allowed;
}

+(NSString *)depart_change_allowed{
    return [CompanyAccess companyAccess].depart_change_allowed;
}

+(NSString *)ID_change_allowed{
    return [CompanyAccess companyAccess].ID_change_allowed;
}
+(NSString *)name_change_allowed{
    return [CompanyAccess companyAccess].name_change_allowed;
}
+(NSString *)show_report_type{
    return [CompanyAccess companyAccess].show_report_type;
}

+(NSString *)show_ID_number{
    return [CompanyAccess companyAccess].show_ID_number;
}

+(NSString *)gender_change_allowed{
    return [CompanyAccess companyAccess].gender_change_allowed;
}


@end

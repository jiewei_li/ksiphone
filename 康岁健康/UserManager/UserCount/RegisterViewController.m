//
//  RegisterViewController.m
//  康岁健康
//
//  Created by 低调 on 14/12/7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.type == REGISTER) {
        self.title = @"注册";
    }
    else if(self.type == FORGETPASSWORD)
    {
        self.title = @"忘记密码";
        [self.regsitBtn setTitle:@"找回密码" forState:UIControlStateNormal];
    }
    
    _mainView.backgroundColor =TableBackColor;
    self.phoneTextField.delegate = self;
    self.yzmTextField.delegate = self;
    self.passWDTextField.delegate = self;
    self.rePassWDTextField.delegate = self;
    [self addSubView:self.regsitView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)getYZM:(id)sender{
    if (self.type == REGISTER) {
        [self checkPhone];
    }
    else if(self.type == FORGETPASSWORD)
    {
        [self getYzm];
    }
    
    
}

-(void)checkPhone{
    if ([self.phoneTextField.text isEmpty] || self.phoneTextField.text.length != 11) {
        [UIAlertView showMessage:@"请输入正确的手机号"];
    }
    else {
        NSDictionary *name = [[NSDictionary alloc]initWithObjectsAndKeys:self.phoneTextField.text,@"name", nil];
        [NetRequest requestShopWithType:TEL_CHECK withGetUrl:ADDRESS_TEL_CHECK andGetInfo:name result:^(id resultSuccess) {
                [self getYzm];
            
        } err:^(id resultError) {
            if ([[resultError objectForKey:@"Suc"] intValue] == 0) {
                [UIAlertView showMessage:@"手机号已注册"];
            }
        }];
    }
}

-(void)getYzm{
    NSDictionary *postDic;
    if (self.type == REGISTER) {
        postDic = [[NSDictionary alloc]initWithObjectsAndKeys:self.phoneTextField.text,@"tel",@"1",@"validType", nil];
    }
//    else if(self.type == FORGETPASSWORD)
//    {
//        postDic = [[NSDictionary alloc]initWithObjectsAndKeys:self.phoneTextField.text,@"tel",@"2",@"validType", nil];
//    }
//
    [NetRequest requestShopWithType:SEND_VALIDSMS withGetUrl:ADDRESS_SEND_VALIDSMS andGetInfo:postDic result:^(id resultSuccess) {
        
    } err:^(id resultError) {
        if ([[resultError objectForKey:@"ErrCode"] intValue] == -14) {
            [UIAlertView showMessage:@"手机号已被注册"];
        }
    }];
}

-(IBAction)registe:(id)sender{
    
}
#pragma mark - textFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self hideKeyboard];
    return YES;
}

-(void)hideKeyboard{
    [self.phoneTextField resignFirstResponder];
}

@end

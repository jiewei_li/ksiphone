//
//  ChangePaswordViewController.m
//  康岁健康
//
//  Created by 低调 on 14/12/21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ChangePaswordViewController.h"

@interface ChangePaswordViewController ()

@end

@implementation ChangePaswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"修改密码";
    [self addSubView:self.cView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(IBAction)change:(id)sender{
    if ([self.pwdT.text isEmpty]) {
        [UIAlertView showMessage:@"请输入正确的密码"];
        return;
    }
    else if ([self.rpwdT.text isEmpty]) {
        [UIAlertView showMessage:@"请输入正确的密码"];
        return;
    }
    else if (![self.rpwdT.text isEqualToString:self.pwdT.text]) {
        [UIAlertView showMessage:@"两次的密码不一样，请重新输入"];
        return;
    }
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:
                         [UserInfos sessionId],@"sessionid",
                         [UserInfos customerId],@"id",
                         self.rpwdT.text,@"pw",
                         nil];
    [NetRequest requestShopWithType:CHANGEPW  withGetUrl:ADDRESS_CHANGEPW  andGetInfo:dic result:^(id resultSuccess) {
        [UIAlertView showMessage:@"修改成功"];
    } err:^(id resultError) {
        [UIAlertView showMessage:[[resultError objectForCJKey:@"Data"] isEmpty]?@"修改失败":[resultError objectForCJKey:@"Data"]];
    }];
}
@end

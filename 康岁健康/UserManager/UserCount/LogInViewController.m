//
//  LogInViewController.m
//  康岁健康
//
//  Created by 低调 on 14/12/7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "LogInViewController.h"
#import "RegisterViewController.h"
#import "ForgetViewController.h"

@interface LogInViewController ()

@end

@implementation LogInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"登录";
    _mainView.backgroundColor =TableBackColor;
    [self addSubView:self.topView];
    [self addSubView:self.middleView];
    self.middleView.top = self.topView.bottom;
    self.passWord.delegate = self;
    self.userNunber.delegate = self;
//    self.logInButton.enabled = NO;
    
    self.tapView.layer.borderWidth = 1;
    self.tapView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.tapView.layer.masksToBounds = YES;
    self.tapView.layer.cornerRadius = 3.; //圆角

    [self.userNunber addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
    [self.passWord addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

-(IBAction)changePassWordType:(id)sender{
    UIButton *b = sender;
    if (b.selected) {
        b.selected = NO;
        self.passWord.secureTextEntry = YES;
    }
    else
    {
        b.selected = YES;
        self.passWord.secureTextEntry = NO;
    }
}
-(IBAction)logIn:(id)sender{
    if ([self.userNunber.text isEmpty]) {
        [UIAlertView showMessage:@"请输入用户名"];
        return;
    }
    else if ([self.passWord.text isEmpty]) {
        [UIAlertView showMessage:@"请输入密码"];
        return;
    }
    [self textFieldDidChange];
//#warning TMP
//    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:@"13918062580",@"user",@"062580",@"pw", nil];
//    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:@"kailoon",@"user",@"121212",@"pw", nil];
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:self.userNunber.text,@"user",self.passWord.text,@"pw", nil];
    [NetRequest requestShopWithType:LOG_IN withGetUrl:ADDRESS_LOG_IN andGetInfo:dic result:^(id resultSuccess) {
        [UIAlertView showMessage:@"登录成功"];
        [self.navigationController popViewControllerAnimated:YES];

        //2015-1-14 kailoon 登录成功后，获取公司信息控制参数
        NSDictionary *dic2 = [[NSDictionary alloc]initWithObjectsAndKeys:
                              ((UserInfos*)resultSuccess).Session,@"sessionid",((UserInfos*)resultSuccess).Id,@"id", nil];
        [NetRequest requestShopWithType:COMPANY_ACCESS withGetUrl:ADDRESS_COMPANY_ACCESS andGetInfo:dic2 result:^(id resultSuccess) {
            /*
            [UIAlertView showMessage:@"获取公司控制参数成功"];
            [self.navigationController popViewControllerAnimated:YES];
            */
            
            }err:^(id resultError){
    
            }];
        
    } err:^(id resultError) {
        [UIAlertView showMessage:@"用户名或者密码错误"];
    }];
}

-(IBAction)regist:(id)sender{
    RegisterViewController *reg = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
    reg.type = REGISTER;
    [self.navigationController pushViewController:reg animated:YES];
}

-(IBAction)forgetPassword:(id)sender{
    ForgetViewController *reg = [[ForgetViewController alloc]initWithNibName:@"ForgetViewController" bundle:nil];
    [self.navigationController pushViewController:reg animated:YES];

}
-(void)textFieldDidChange{
//    if ([self.passWord.text isEmpty] || [self.userNunber.text isEmpty]) {
//        self.logInButton.enabled = NO;
//    }
//    else
//    {
//        self.logInButton.enabled = YES;
//    }
}
#pragma mark - textFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.userNunber resignFirstResponder];
    [self.passWord resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

@end

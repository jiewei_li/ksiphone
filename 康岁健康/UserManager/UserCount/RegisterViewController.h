//
//  RegisterViewController.h
//  康岁健康
//
//  Created by 低调 on 14/12/7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"
typedef NS_ENUM(NSInteger,RegType){
    REGISTER = 0,
    FORGETPASSWORD
};

@interface RegisterViewController : BaseNextViewController<UITextFieldDelegate>
@property  RegType    type;
@property IBOutlet UITextField *phoneTextField;
@property IBOutlet UITextField *yzmTextField;
@property IBOutlet UITextField *passWDTextField;
@property IBOutlet UITextField *rePassWDTextField;

@property IBOutlet UIView      *regsitView;
@property IBOutlet UIButton    *regsitBtn;

-(IBAction)getYZM:(id)sender;
-(IBAction)registe:(id)sender;
@end

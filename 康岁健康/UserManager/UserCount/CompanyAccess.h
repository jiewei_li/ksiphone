//
//  CompanyAccess.h
//  康岁健康
//
//  Created by cloudihome on 15-1-14.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

#ifndef _____CompanyAccess_h
#define _____CompanyAccess_h
#import <Foundation/Foundation.h>

@interface CompanyAccess : NSObject
@property NSString *company_id;
@property NSString *birthday_change_allowed;
@property NSString *depart_change_allowed;
@property NSString *ID_change_allowed;
@property NSString *name_change_allowed;
@property NSString *show_report_type;
@property NSString *show_ID_number;
@property NSString *gender_change_allowed;


+(CompanyAccess *)companyAccess;
-(void)setCompanyAccess:(NSDictionary *)dic;

+(NSString *)birthday_change_allowed;
+(NSString *)depart_change_allowed;
+(NSString *)ID_change_allowed;
+(NSString *)name_change_allowed;
+(NSString *)show_report_type;
+(NSString *)show_ID_number;
+(NSString *)gender_change_allowed;
//+(NSString *)sessionId;
//+(NSString *)Id;

//+(NSString *)customerId;
//+(BOOL )isUserLogIn;
//+(void)logOut;

@end
#endif

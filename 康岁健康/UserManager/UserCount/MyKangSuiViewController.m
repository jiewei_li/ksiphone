//
//  MyKangSuiViewController.m
//  康岁健康
//
//  Created by 低调 on 14/12/7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "MyKangSuiViewController.h"
#import "LogInViewController.h"
#import "PersinalInfoViewController.h"
#import "ChangePaswordViewController.h"


@interface MyKangSuiViewController ()
- (IBAction)btn我的预约Click:(id)sender;

@end

@implementation MyKangSuiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的亳州医保";
    self.view.backgroundColor = TAbBACCOLOR;
    [self addSubView:self.topView];
    
    [self addSubView:self.userManagerView];
    self.userManagerView.top = self.topView.bottom;
    _mainView.backgroundColor = TableBackColor;

}

-(void)viewDidAppear:(BOOL)animated
{
    [self checkLogStatue];
}

-(void)checkLogStatue{
    if ([UserInfos isUserLogIn]) {
        [self showUserInfoView];
        self.logOutButton.hidden = NO;
    }
    else
    {
        [self showLogInView];
        self.logOutButton.hidden = YES;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


-(void)showUserInfoView{
    self.topLogInView.hidden = YES;
    self.topUsrerInfoView.hidden = NO;
    UserInfos *info = [UserInfos userInfo];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"nohead.jpg"]];
    self.userNameLabel.text = [NSString stringWithFormat:@"欢迎您 %@",info.Name];
    self.jifengLabel.text = [NSString stringWithFormat:@"%@",info.Gender];
    
}

-(void)showLogInView{
    self.topLogInView.hidden = NO;
    self.topUsrerInfoView.hidden = YES;
}

-(IBAction)changePassWd:(id)sender{
    if (![UserInfos isUserLogIn]) {
        [self logIn:nil];
        return;
    }
    ChangePaswordViewController *c = [[ChangePaswordViewController alloc]initWithNibName:@"ChangePaswordViewController" bundle:nil];
    [self.tabBarController.navigationController pushViewController:c animated:YES];
}


-(IBAction)logIn:(id)sender{
    LogInViewController *logIn = [[LogInViewController alloc]initWithNibName:@"LogInViewController" bundle:nil];
    [self.navigationController.tabBarController.navigationController pushViewController:logIn animated:YES];
}

-(IBAction)toSelf:(id)sender{
    if ([UserInfos isUserLogIn]) {
        PersinalInfoViewController *persional = [[PersinalInfoViewController alloc]initWithNibName:@"PersinalInfoViewController" bundle:nil];
        persional.type = SELFINFO;
        [self.tabBarController.navigationController pushViewController:persional animated:YES];
    }
    else
    {
        [self logIn:nil];
    }
}

-(IBAction)toRelatives:(id)sender{
    if ([UserInfos isUserLogIn]) {
        PersinalInfoViewController *persional = [[PersinalInfoViewController alloc]initWithNibName:@"PersinalInfoViewController" bundle:nil];
        persional.type = REALTIVEINFO;
        [self.tabBarController.navigationController pushViewController:persional animated:YES];

    }
    else
    {
        [self logIn:nil];
    }
}
-(IBAction)logOut:(id)sender{
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:[UserInfos sessionId],@"sessionid", nil];
    [NetRequest requestShopWithType:LOG_OUT withGetUrl:ADDRESS_LOG_OUT andGetInfo:dic result:^(id resultSuccess) {
        
    } err:^(id resultError) {
        
    }];
    [UserInfos logOut];
    [self checkLogStatue];
}

- (IBAction)btn我的预约Click:(id)sender {
    UIStoryboard *secondStoryBoard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    UIViewController* view = [secondStoryBoard instantiateViewControllerWithIdentifier:@"MyYuyue"];  //MyYuyue为viewcontroller的StoryboardId
    //  [self.navigationController pushViewController:view animated:YES];
    

   // MyYuyueViewController
    self.tabBarController.navigationController.navigationBarHidden=NO;
    
    [self.tabBarController.navigationController pushViewController:view animated:YES];

    
}
@end

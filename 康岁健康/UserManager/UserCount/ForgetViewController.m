//
//  ForgetViewController.m
//  康岁健康
//
//  Created by 低调 on 15/1/13.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

#import "ForgetViewController.h"

@interface ForgetViewController ()

@end

@implementation ForgetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"找回密码";
    [self addSubView:self.cView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)change:(id)sender{
    if (![[self.phonetext.text notNull] isPhoneNum]) {
        [UIAlertView showMessage:@"请输入正确的手机号"];
        return;
    }
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:self.phonetext.text,@"tel", nil];
    [NetRequest requestShopWithType:GET_NEW_PASSWORD_SMS withGetUrl:ADDRESS_GET_NEW_PASSWORD_SMS andGetInfo:dic result:^(id resultSuccess) {
        [UIAlertView showMessage:@"新密码已发送"];
    } err:^(id resultError) {
        if ([resultError objectForKey:@"Data"]) {
            [UIAlertView showMessage:[resultError objectForKey:@"Data"]];
        }
    }];
}
@end

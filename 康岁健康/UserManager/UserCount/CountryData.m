//
//  CountryData.m
//  康岁健康
//
//  Created by 低调 on 14/12/21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "CountryData.h"

@implementation CountryData
-(void)setData:(NSDictionary *)dic{
    self.code = [dic objectForCJKey:@"code"];
    self.Id   = [dic objectForCJKey:@"id"];
    self.name = [dic objectForCJKey:@"name"];
}
@end

//
//  DoctorTableViewController.swift
//  亳州医保
//
//  Created by ljw on 15/3/2.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit

class DoctorTableViewController: UITableViewController {
   
    
   // var doctors : [String] = ["01d"]
var doctors = [
    ["照片":"01d","姓名":"唐超","级别":"主任医师","擅长":"擅长：各种原因的肺炎、急慢性支气管炎、慢性阻塞性肺疾病、肺心病、呼吸衰竭、支气管哮喘、各种原因的胸腔积液、肺部肿瘤等呼吸系统疾病的诊治。"],
   ["照片":"02d","姓名":"杨淼","级别":"主任医师","擅长":"擅长：脑血管疾病、头晕、头痛、癫痫、帕金森病、肌病及脊髓疾病、神经内科疑难危重病的诊治。"],
    
    ]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
   // var dataArray = ["01医生","02医生","03医生","04医生","05医生","06医生","07医生","08医生","09医生"]
    


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return doctors.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as YuyueDoctorTableViewCell

     
       
        var d =  doctors[indexPath.row] as NSDictionary
        var strphoto=d["照片"] as String
        // Configure the cell...
        cell.img照片.image=UIImage(named: strphoto)
        cell.lab姓名.text=d["姓名"] as String
        cell.lab擅长.text=d["擅长"] as String
        cell.lab级别.text=d["级别"] as String
        
        
       var scrollView =  (cell.contentView.viewWithTag(200) as UIScrollView)
        
        if(scrollView.pagingEnabled==false)
{
    scrollView.contentSize=CGSizeMake(CGFloat(940),CGFloat(135))

           scrollView.pagingEnabled=true
        // 生成预约时间
        var x:CGFloat=5.0;
        var width:CGFloat=40.0;
        var date=NSDate()
       // var formater=NSDateFormatter()
        //formater.dateFormat="MM-dd"
        for i in 1...21
        {
              date =     date.dateByAddingDays(1)
            var lab=UILabel(frame: CGRectMake(x, 5, width, 37))
            lab.text = date.toString(format:.Custom( "MM-dd"))+"\n"+date.weekdayToString() //"2-27\n星期五"
      
           // date=date.dateByAddingTimeInterval(60*60*24*i)
            lab.numberOfLines=2
            lab.lineBreakMode =  NSLineBreakMode.ByCharWrapping// UILineBreakModeCharacterWrap
            lab.font=UIFont.systemFontOfSize(12)
            scrollView.addSubview(lab);
            x=x+width+4.5
        }
}

        return cell
    }

    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    
        return 245
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}

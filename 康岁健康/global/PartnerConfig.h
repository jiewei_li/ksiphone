//
//  PartnerConfig.h
//  AlipaySdkDemo
//
//  Created by ChaoGanYing on 13-5-3.
//  Copyright (c) 2013年 RenFei. All rights reserved.
//
//  提示：如何获取安全校验码和合作身份者id
//  1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
//  2.点击“商家服务”(https://b.alipay.com/order/myorder.htm)
//  3.点击“查询合作者身份(pid)”、“查询安全校验码(key)”
//

#ifndef MQPDemo_PartnerConfig_h
#define MQPDemo_PartnerConfig_h

//合作身份者id，以2088开头的16位纯数字
#define PartnerID @"2088311956422212"
//收款支付宝账号
#define SellerID  @"2088311956422212"

//安全校验码（MD5）密钥，以数字和字母组成的32位字符
#define MD5_KEY @""

//商户私钥，自助生成
#define PartnerPrivKey @"MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBANMNXzt7Xd812AQeErsiKjksAW1SXYTyOVMb1yqQT0g5YLFtbA5OAqbmDs1kjZF9mnssm2msP+I5w4UKrCbCkIbJI7C0g2M9wNAlSHKgffTvM4zJg37bBWLETOGxdYWqnFWxOBZJ9pWYVICZeC1RfUBVXm//ZZYscum0QMYbzJQRAgMBAAECgYEAguKBE79fKOUYcP1BeoImk0wiGm0BCEi7mjnix0e1k0HaIE6Ou5RPerejqGbakMbHPeoc+3zSJXbTNTe4KVVyELNGhyRTc8PT2zRZiVzx2p4s6aowNxuF/vr2Otfm+QArsxkoX73c6U4ie+SDyWKk59qfQLeYxS9Oz0gOWgbZKx0CQQD+S8aWC/SjjJnK+FlRNYXV5uXtUQaX3i2ZXImmS5M6PR9VywbeFrO+tRP78XSIBZVvHtTIIglcJQPNXQdW2e2DAkEA1HdqQvuhLFmR6pf4KaPoOtfchlvLQzcqYNRg8v8pBeMed4WRvbDvxSDBnFNBYQA6Qwp7BIMEgh/QDWpCvuf32wJBANjNPde7Ba1SN+RF69BCs4H0Q18az2Y28+VmcaynjjP+3HJGj+pUrJYQx6Lq7pDxQb/lJ0AhVJ2MqTsy7wjq74UCQGglpQvyhp3TxigFmFrpAoQdnfaqdmebWgWe9WsH0pcVLrEN41PlsJKjrOQxF2IyF10XfRfIFWSnrXd1+APWd3MCQQDBSEe3wj5TS2vQyK/CcBVi3kxBjS4OT3nkhUE2sa8PMooiJjKkUTwfL5P6PPYbfiAyLOh5GKg4T2siKRJPmpbP"


//支付宝公钥
#define AlipayPubKey   @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBh7z0Xc7myXQKOmgA4AA6ldHtE7ZlLN1Kns09 iOyKZlmahrrE5kKabrPAS2UdiKoBLl9YoIWtiQ7gBJetsa1xcs84yllFYHWjVic03ZEaR77aUuGK QWBenphhFjTQwTJUgtx2NmnCLBs2lvyD1NNHn3A4LWBWF2BmTf2lxF4r/wIDAQAB"

#endif

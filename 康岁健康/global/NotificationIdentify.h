//
//  NSObject_NotificationIdentify.h
//  康岁健康
//
//  Created by cerastes on 14-9-21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TabBarChangeNotifocation @"TabBarChangeNotifocation"
//已购买的体检套餐数
#define BookingCountNotifocation @"BookingCountNotifocation"
//购物车中商品数
#define CartCountNotifocation @"CartCountNotifocation"
//待处理订单
#define OrderCountNotifocation @"OrderCountNotifocation"
//购物车添加选中项
#define AddCartItem @"AddCartItem"
//购物车减去选中项
#define ReduceCartItem @"ReduceCartItem"
//购物车全选
#define SelectAllCartItem @"SelectAllCartItem"
//购物车取消全选
#define UnSelectAllCartItem @"UnSelectAllCartItem"
//购物车编辑
#define CartItemEdit   @"CartItemEdit"
//购物车取消编辑
#define CartItemFinishEdit @"CartItemFinishEdit"
//购物车数量
#define NumberOfCrat @"NumberOfCrat"

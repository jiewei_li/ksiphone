//
//  UserInfos.m
//  康岁健康
//
//  Created by 低调 on 14-10-11.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "UserInfos.h"

@implementation UserInfos

+(UserInfos *)userInfo{
    UserInfos *userInfo = [[UserInfos alloc]init];
    
    NSData *data = user_defaults_get_object(@"userInfo");
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    NSDictionary *myDictionary = [unarchiver decodeObjectForKey:@"Some Key Value"];
    [unarchiver finishDecoding];
    
    [userInfo setUserInfo:myDictionary];
    return userInfo;
}
-(void)setUserInfo:(NSDictionary *)dic{
    NSMutableData *mData = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:mData];
    [archiver encodeObject:dic forKey:@"Some Key Value"];
    [archiver finishEncoding];
    
    user_defaults_set_object(@"userInfo", mData);
    
    NSDictionary *data = [dic objectForKey:@"Data"];
    self.CompanyId = [data objectForCJKey:@"CompanyId"];
    self.DefaultPwChanged = [data objectForCJKey:@"DefaultPwChanged"];
    self.Gender = [data objectForCJKey:@"Gender"];
    self.Id = [data objectForCJKey:@"Id"];
    self.LastTimeActive = [data objectForCJKey:@"LastTimeActive"];
    self.LastTimeLogin = [data objectForCJKey:@"LastTimeLogin"];
    self.LastTimeUpd = [data objectForCJKey:@"LastTimeUpd"];
    self.Married = [data objectForCJKey:@"Married"];
    self.Name = [data objectForCJKey:@"Name"];
    self.Session = [data objectForCJKey:@"Session"];
}

+(NSString *)customerId{
    return [UserInfos userInfo].Id;
}
+(NSString *)sessionId{
   return  [UserInfos userInfo].Session;
}

+(NSString *)Id{
    return  [UserInfos userInfo].Id;
}
+(BOOL )isUserLogIn{
    
    NSData *data = user_defaults_get_object(@"userInfo");
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    NSDictionary *myDictionary = [unarchiver decodeObjectForKey:@"Some Key Value"];
    [unarchiver finishDecoding];
    if(myDictionary){
        return YES;
    }
    return NO;
}
+(void)logOut{
    user_defaults_set_object(@"userInfo", nil);
}
@end

//
//  urls.h
//  康岁健康
//
//  Created by cerastes on 14-9-23.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#ifndef _____urls_h
#define _____urls_h
#define BASE_ADDRESSa  @"http://www.ihealthink.com/"

#define BASE_ADDRESS  @"STORE_SERVERS_URL/"
#define BASE_ADDRESS2  @"SERVER_EXAM/"
#define BASE_ADDRESS3  @"SERVER_HOST/"

//#define BASE_ADDRESS  @"http://122.114.50.232:88/"
//#define BASE_ADDRESS2  @"http://122.114.50.232:85/"
//#define BASE_ADDRESS3  @"http://www.ihealthink.com/"


//#define SERVER_HOST @"http://www.ihealthink.com"
////以下url从主站获得
////体检预约系统及APP接口server
////#define SERVER_EXAM @"http://122.114.50.232:85"
////商城 APP 接口 Server
////商城主站（查看商品详情用）
//#define STORE_DOMAIN @"http://www.ihealthink.com:81"
////商城APP主接口
//#define STORE_SERVERS_URL  @"http://122.114.50.232:88"
//登录 Get  18918630199  121212
#define ADDRESS_LOG_IN                STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/login")
//Logout
#define ADDRESS_LOG_OUT               STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/logout")
//用户公司信息控制
#define ADDRESS_COMPANY_ACCESS        STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/CompanyAccessInfoByCustomer")

//TelCheck4.	手机号是否有相同
#define ADDRESS_TEL_CHECK             STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/TelCheck")
//appapi/ SendValidSMS
#define ADDRESS_SEND_VALIDSMS         STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/SendValidSMS")

#define ADDRESS_GET_NEW_PASSWORD_SMS  STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/GetNewPasswordSMS")









//获取用户资料 Get app/ GetCustomer
#define ADDRESS_GET_CUSTOMER                     STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/GetCustomer")
//订单获取 app/OrderStatus
#define ADDRESS_GET_ORDER_STATUS                     STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/OrderStatus")
//appapi/ExamSetDetail
#define ADDRESS_EXAM_SET_DETAIL                     STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/ExamSetDetail")
//27.	获取体检须知及交通路线ExamPrecaution
#define ADDRESS_EXAM_PRECAUTION                     STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/ExamPrecaution")
//26.	获取体检预约通知
#define ADDRESS_GET_NOTICE                     STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/GetNotice")
//16.	体检套餐查询appapi/ ExamSetBrief
#define ADDRESS_EXAM_SET_BRIEF                     STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/ExamSetBrief")
//appapi/ ExamOrderWithRelated
#define ADDRESS_EXAM_ORDER_WITH_RELATED                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/ExamOrderWithRelated")
//20.	查询城市appapi/ CityByExamset
#define ADDRESS_CITY_BY_EXAMSET                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/CityByExamset")
//21.	查询体检中心appapi/ ExamCenterByCityExamset
#define ADDRESS_EXAM_CENTER_BY_CITY_EXAMSET                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/ExamCenterByCityExamset")

//appapi/GetArticalClass
#define ADDRESS_GET_ARTICAL_CLASS                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/GetArticalClass")
//28.	可预约信息Appapi/bookinfo
#define ADDRESS_BOOK_INFO                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"Appapi/bookinfo")
//AddExamOrder
#define ADDRESS_ADD_EXAMORDER                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"Appapi/AddExamOrder")
#define ADDRESS_REATION                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"Appapi/relation")
#define ADDRESS_COUNTRY                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"Appapi/Country")
#define ADDRESS_PROVINCE                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"Appapi/Province")
#define ADDRESS_CITY                     STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"Appapi/city")
//9.	用户资料更新
#define ADDRESS_UPDCUSTOMER                     STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"Appapi/UpdCustomer")
//7.	保存亲友 SaveRelatives
#define ADDRESS_SAVE_RELATIVES                     STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/SaveRelative")

#define ADDRESS_CHANGEPW                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/ChangePw")

#define ADDRESS_GET_NEW_VERSION                    STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"App/GetNewVersion")


//19.	体检报告
#define ADDRESS_EXAMREPORT                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/ExamReport")

#define ADDRESS_SMS_CODE_MATCH                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/SMSCodeMatch")
#define ADDRESS_CHANGE_TEL                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/ChangeTel")


//appapi/ GetArticalsBrief
#define ADDRESS_GETARTICALSBRIEF                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/GetArticalsBrief")
//GetArtical
#define ADDRESS_GETARTICAL                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/GetArtical")

//appapi/getServerUrls
#define ADDRESS_GETSERVERURLS                    STRING_FORMAT( @"%@%@",BASE_ADDRESSa, @"appapi/getServerUrls")

//预约表单取消
#define ADDRESS_CANCEL_EXAMORDER                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/CancelExamOrder")

//手机安全支付
#define ADDRESS_BEGIN_PAY_MOBILE                    STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/BeginPayMobile")

#define ADDRESS_GET_EXAMORDER_DETAIL                     STRING_FORMAT( @"%@%@",BASE_ADDRESS2, @"appapi/GetExamOrderDetail")

//appapi/ 

//商品推广滚动广告
#define ADDRESS_GET_PRODUCT_SLIDE_IMAGE_INFO               STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Product/GetProductSlideImageInfo")
//主打商品显示区
#define ADDRESS_GET_PRODUCT_PAGE_LIST                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Product/GetProductPageList")
//快捷操作功能区
#define ADDRESS_GET_Store_OverView                         STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"iHealthinkStore/GetStoreOverview")
//获取产品图片列表和参数
#define ADDRESS_GET_PRODUCT_INFO                           STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Product/GetProductInfo")
//读取购物车条目
#define ADDRESS_GET_SHOPPING_CART_ITEM                     STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"ShoppingCart/GetShoppingCartItems")
//catagoryId
#define ADDRESS_GET_CATOGORY_PAGE_LIST                     STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"catalog/GetCatogoryPageList")


//获取产品图片列表
#define ADDRESS_GET_PRODUCT_PICTURES                       STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Product/GetProductPictures")
//获取商品属性选择参数
#define ADDRESS_GET_PRODUCT_ATTRIBUTE                     STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Product/GetProductAttribute")
//获取商品属性选择参数
#define ADDRESS_GET_PRODUCT_DETAIL                        STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Product/GetProductDetail")
//商品加入购物车
#define ADDRESS_Add2Cart                              STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"ShoppingCart/AddToCart")
//读取购物车条目
#define ADDRESS_GET_SHOPPING_CART_ITEMS                        STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"ShoppingCart/GetShoppingCartItems")
//3.2.	更新购货车 //客户端只负责向服务器发送更新数据，不接受服务器的返回
#define ADDRESS_GET_UPDATE_SHOPPING_CART_ITEMS                       STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"ShoppingCart/UpdateShoppingCartItems")
//4.1.	获得指定省份的城市列表
#define ADDRESS_GET_CITY_BY_STATE_PROVINCEID                       STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"city/GetCitysByStateProvinceId")
//4.2.	获得省份列表
#define ADDRESS_GET_STAEPROVINCES_BY_COUNTRY_ID                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"StateProvince/GetStateProvincesByCountryId")
//4.3.	获得国家列表
#define ADDRESS_GET_COUNTRIES                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"GetCountries")
//4.4.	获取单个国家信息
#define ADDRESS_GET_COUNTRY_BY_ID                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"GetCountryById")
//5.1.	提交订单
#define ADDRESS_PREPARE_NEW_ORDER                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Order/PrepareNewOrder")
//5.2.	订单列表
#define ADDRESS_GET_ORDER_PAGE_LIST                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Order/GetOrderPageList")
//5.4.	获取订单详细信息
#define ADDRESS_GET_ORDER_DETAIL                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Order/GetOrderDetail")
//5.5.	订单取消  Order/CancelOrder
#define ADDRESS_CANCEL_ORDER                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Order/CancelOrder")

#define ADDRESS_CONFIRMORDER_COMPLETED                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Order/ConfirmOrderCompleted")
//5.7.	读取用户的地址列表
#define ADDRESS_GET_CUSTOMER_ADDRESS                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Address/GetCustomerAddress")



//appapi/ GetExamOrderDetail
//1.2.	生成订单
#define ADDRESS_ADD_NEW_ORDER                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Order/AddNewOrder")
#define ADDRESS_POST_PAY_MENT                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Order/PostPayment")

//6.1添加用户地址信息
#define ADDRESS_ADD_NEW_ADDRESS                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Address/AddNewAddress")
//6.2更新用户地址信息
#define ADDRESS_EDIT_ADDRESS                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"Address/EditAddress")


#define alipay_notify_url                      @"http%3A%2F%2F122.114.50.232%3A88%2FPaymentAliPay%2Fnotifymobile"

//#define alipay_notify_url                      STRING_FORMAT( @"%@%@",BASE_ADDRESS, @"")


#endif

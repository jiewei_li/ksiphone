//
//  Common.m
//  康岁健康
//
//  Created by 低调 on 14/11/26.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "Common.h"

@implementation Common
+(NSString *)notNull:(NSString *)string{
    return [string isEmpty]?string:@"empty";
}
+(NSString *)idTypeWithid:(NSString *)type{
    if ([type isEqualToString:@"0"]) {
        return @"身份证";
    }
    else if ([type isEqualToString:@"1"]) {
         return @"护照";
    }
    else if ([type isEqualToString:@"3"]) {
         return @"社保卡";
    }
    else
        return @"其他";
}
+(NSString *)idTypeWithstring:(NSString *)type{
    if ([type isEqualToString:@"身份证"]) {
        return @"0";
    }
    else if ([type isEqualToString:@"护照"]) {
        return @"1";
    }
    else if ([type isEqualToString:@"社保卡"]) {
        return @"3";
    }
    else
        return @"2";

}
+(NSString *)marryStringWithid:(NSString *)type{
    if ([type isEqualToString:@"1"]) {
        return @"已婚";
    }
    else if ([type isEqualToString:@"2"]) {
        return @"未婚";
    }
    else{
        return @"未知";
    }
}
+(NSString *)marryidWithString:(NSString *)type{
    if ([type isEqualToString:@"已婚"]) {
        return @"1";
    }
    else if ([type isEqualToString:@"未婚"]) {
        return @"2";
    }
    else{
        return @"3";
    }
}
+(NSString *)sexStringWithId:(NSString *)type{
    if ([type isEqualToString:@"1"]) {
        return @"男";
    }
    else if ([type isEqualToString:@"2"]) {
        return @"女";
    }
    else{
        return @"未知";
    }
}
+(NSString *)sexIdWithString:(NSString *)string{
    if ([string isEqualToString:@"男"]) {
        return @"1";
    }
    else if ([string isEqualToString:@"女"]) {
        return @"2";
    }
    else{
        return @"3";
    }
}

+(NSString *)reportTypeWithId:(NSString *)type{
    if ([type isEqualToString:@"1"]) {
        return @"电子报告";
    }
    else if ([type isEqualToString:@"2"]) {
        return @"纸质报告";
    }
    else{
        return @"纸质报告";
    }
}
+(NSString *)reportIdWithString:(NSString *)string{
    if ([string isEqualToString:@"电子报告"]) {
        return @"1";
    }
    else if ([string isEqualToString:@"纸质报告"]) {
        return @"2";
    }
    else if ([string isEqualToString:@"电子+纸质报告"]) {
        return @"3";
    }
    else{
        return @"3";
    }
}

+(void)checkUpDate{
    [NetRequest requestShopWithType:GET_NEW_VERSION withGetUrl:ADDRESS_GET_NEW_VERSION andGetInfo:nil result:^(id resultSuccess) {
        NSDictionary *IOS = [[resultSuccess objectForKey:@"Data"] objectForKey:@"iOS"];
        if ([[IOS objectForKey:@"versionCode"] intValue] != 2) {
            if ([[IOS objectForKey:@"updateAction"] isEqualToString:@"ForceUpdate"]) {
                [UIAlertView showMessage:@"有新的版本" handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[IOS objectForCJKey:@"downloadUrl"]]];
                }];
                
            }
            else{
                [UIAlertView showConfirmMessage:nil Content:@"有更新" ConfrimButton:@"现在就去" CancelButton:@"以后再更新" handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    if (buttonIndex == 1) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[IOS objectForCJKey:@"downloadUrl"]]];
                    }
                }];
            }
        }
        else
        {
            [UIAlertView showMessage:@"已是最新的版本"];
        }
    } err:^(id resultError) {
        
    }];
}

+(NSString *)paymode2String:(NSString *)paymodId andpayStatue:(NSString *)sta{
    NSString *pay = sta?[Common status_id2String:sta]:@"";
    if ([paymodId intValue] == 1) {
        return @"团体支付";
    }
    if ([paymodId intValue] == 2) {
        if (sta) {
            return [NSString stringWithFormat:@"网上支付（%@）",pay];
        }
        return @"网上支付";
        
    }
    if ([paymodId intValue] == 3) {
        return [NSString stringWithFormat:@"门店支付"]; ;
    }
    return @"未知";
}

+(NSString *)status_id2String:(NSString *)sId{
    if ([sId intValue] == 1) {
        return @"已取消";
    }
    if ([sId intValue] == 2) {
        return @"未付款";
    }
    if ([sId intValue] == 3) {
        return @"待安排";
    }
    if ([sId intValue] == 4) {
        return @"已安排";
    }
    if ([sId intValue] == 5) {
        return @"已完成";
    }
    return @"未知";
}

@end

//
//  Common.h
//  康岁健康
//
//  Created by 低调 on 14/11/26.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Common : NSObject
+(NSString *)notNull:(NSString *)string;
+(NSString *)idTypeWithid:(NSString *)type;
+(NSString *)idTypeWithstring:(NSString *)type;
+(NSString *)marryStringWithid:(NSString *)type;
+(NSString *)marryidWithString:(NSString *)type;
+(NSString *)sexStringWithId:(NSString *)type;
+(NSString *)sexIdWithString:(NSString *)string;
+(NSString *)reportTypeWithId:(NSString *)type;
+(NSString *)reportIdWithString:(NSString *)string;

+(void)checkUpDate;

+(NSString *)paymode2String:(NSString *)paymodId andpayStatue:(NSString *)sta;
+(NSString *)status_id2String:(NSString *)sId;
@end

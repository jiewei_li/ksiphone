//
//  colours.h
//  康岁健康
//
//  Created by 低调 on 14-10-12.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#ifndef _____colors_h
#define _____colors_h
#define clearcolor      [UIColor clearColor]
#define redcolor        [UIColor redColor]
#define greencolor      [UIColor greenColor]
#define bluecolor       [UIColor blueColor]
#define blackcolor      [UIColor blackColor]
#define graycolor       [UIColor grayColor]
#define orangecolor     [UIColor orangeColor]
#define yellowcolor     [UIColor yellowColor]



#define Btn_Orange_color     [UIColor colorWithRed:252/255. green:81/255. blue:32/255. alpha:1]
#define NavColor             [UIColor colorWithRed:14./255. green:68./255. blue:91./255. alpha:1]
#define TableBackColor       [UIColor colorWithRed:245./255. green:245./255. blue:245./255. alpha:1]
//体检预约
#define CHECK_APOINTMENT_BEGIN_COLOR            [UIColor colorWithRed:43/255. green:91/255. blue:163/255. alpha:1]
#define CHECK_APOINTMENT_END_COLOR              [UIColor colorWithRed:43/255. green:91/255. blue:163/255. alpha:1]
//健康档案
#define HEALTH_FILE_BEGIN_COLOR                 [UIColor colorWithRed:47/255. green:197/255. blue:207/255. alpha:1]
#define HEALTH_FILE_END_COLOR                   [UIColor colorWithRed:47/255. green:197/255. blue:207/255. alpha:1]
//私人医生
#define PERSIONAL_DOCTOR_BEGIN_COLOR            [UIColor colorWithRed:49/255. green:146/255. blue:122/255. alpha:1]
#define PERSIONAL_DOCTOR_END_COLOR              [UIColor colorWithRed:49/255. green:146/255. blue:122/255. alpha:1]
//慢性病管理
#define CHRONIC_DISEASE_MANAGER_BEGIN_COLOR     [UIColor colorWithRed:83/255. green:109/255. blue:138/255. alpha:1]
#define CHRONIC_DISEASE_MANAGER_END_COLOR       [UIColor colorWithRed:83/255. green:109/255. blue:138/255. alpha:1]
//我的康岁
#define MY_KANGSUI_BEGIN_COLOR                  [UIColor colorWithRed:255/255. green:255/255. blue:255/255. alpha:1]
#define MY_KANGSUI_END_COLOR                    [UIColor colorWithRed:255/255. green:255/255. blue:255/255. alpha:1]
//健康直通车
#define STRICT_TO_HEALTH_BEGIN_COLOR            [UIColor colorWithRed:144/255. green:194/255. blue:48/255. alpha:1]
#define STRICT_TO_HEALTH_END_COLOR              [UIColor colorWithRed:144/255. green:194/255. blue:48/255. alpha:1]
//养生家园
#define HOMETOWN_OF_HEALTH_BEGIN_COLOR          [UIColor colorWithRed:227/255. green:172/255. blue:45/255. alpha:1]
#define HOMETOWN_OF_HEALTH_END_COLOR            [UIColor colorWithRed:227/255. green:172/255. blue:45/255. alpha:1]
//健身计划
#define HELATH_PLANE_BEGIN_COLOR                [UIColor colorWithRed:148/255. green:104/255. blue:224/255. alpha:1]
#define HELATH_PLANE_END_COLOR                  [UIColor colorWithRed:148/255. green:104/255. blue:224/255. alpha:1]
//健康商城
#define HELATH_MARKET_BEGIN_COLOR               [UIColor colorWithRed:240/255. green:100/255. blue:79/255. alpha:1]
#define HELATH_MARKET_END_COLOR                 [UIColor colorWithRed:240/255. green:100/255. blue:79/255. alpha:1]

#endif

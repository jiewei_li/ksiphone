//
//  UserInfos.h
//  康岁健康
//
//  Created by 低调 on 14-10-11.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfos : NSObject
@property NSString *CompanyId;
@property NSString *DefaultPwChanged;
@property NSString *Gender;
@property NSString *Id;
@property NSString *LastTimeActive;
@property NSString *LastTimeLogin;
@property NSString *LastTimeUpd;
@property NSString *Married;
@property NSString *Name;
@property NSString *Session;

+(UserInfos *)userInfo;
-(void)setUserInfo:(NSDictionary *)dic;
+(NSString *)sessionId;
+(NSString *)Id;

+(NSString *)customerId;
+(BOOL )isUserLogIn;
+(void)logOut;
@end

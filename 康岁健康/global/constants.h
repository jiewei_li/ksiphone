//
// Created by wangwei on 6/26/14.
// Copyright (c) 2014 com.qianxs. All rights reserved.
//

//#import "CWStringUtility.h"
//#import "NSBundle+MCategory.h"

#define STRING_FORMAT(...) [NSString stringWithFormat: __VA_ARGS__]

#ifndef IsIOS7
#define IsIOS7 ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)
#endif
#ifndef TAbBACCOLOR
#define TAbBACCOLOR ([UIColor colorWithRed:239/255. green:239/255. blue:244/255. alpha:1])
#endif

#ifndef DEVICE_LARGE
#define DEVICE_LARGE ([[UIScreen mainScreen] bounds].size.height)>480
#endif


#ifndef DEVICE_WIDTH
#define DEVICE_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#endif

#ifndef DEVICE_HEIGHT
#define DEVICE_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#endif

// 字体大小(常规/粗体)
#define FontOfSize(FONTSIZE)    [UIFont systemFontOfSize:FONTSIZE]
#define FONT(NAME, FONTSIZE)    [UIFont fontWithName:(NAME) size:(FONTSIZE)]

#ifndef TABBAR_HEIGHT
#define TABBAR_HEIGHT 48.0f
#endif




#ifndef KITUNES_URL
#define KITUNES_URL @"https://itunes.apple.com/cn/app/qian-xian-sheng/id583292699?mt=8"
#endif
#define PNGIMAGE(NAME)   [UIImage imageNamed:NAME]




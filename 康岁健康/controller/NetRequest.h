//
//  NetRequest.h
//  康岁健康
//
//  Created by cerastes on 14-9-23.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,RequestType){
    LOG_IN = 0,
    TEL_CHECK,
    LOG_OUT,
    SEND_VALIDSMS,
    ADD_EXAMORDER,
    REATION,
    COUNTRY,
    PROVINCE,
    CITY,
    UPDCUSTOMER,
    SAVE_RELATIVES,
    CHANGEPW,
    GETARTICALSBRIEF,
    GETARTICAL,
    GETSERVERURLS,
    CANCEL_EXAMORDER,
    BEGIN_PAY_MOBILE,
    EXAMREPORT,
    GET_NEW_VERSION,
    POST_PAY_MENT,
    GET_EXAMORDER_DETAIL,
    GET_NEW_PASSWORD_SMS,
    SMS_CODE_MATCH,
    CHANGE_TEL,
    
    GET_CUSTOMER,
    EXAM_ORDER_WITH_RELATED,
    GET_ORDER_STATUS,
    EXAM_SET_DETAIL,
    EXAM_PRECAUTION,
    GET_CATOGORY_PAGE_LIST,
    GET_NOTICE,
    EXAM_SET_BRIEF,
    CITY_BY_EXAMSET,
    EXAM_CENTER_BY_CITY_EXAMSET,
    BOOK_INFO,
    PRODUCT_SLIDE_IMAGE_INFO ,
    PRODUCT_PAGE_LIST,
    STORE_OVERVIEW,
    PRODUCT_VARIANT_ATTRIBUTE_BY_PRODUCTID,
    SHOPPING_CART_ITEM,
    PRODUCT_PICTURES,
    PRODUCT_ATTRIBUTE,
    PRODUCT_DETAIL,
    Add2Cart,
    SHOPPING_CART_ITEMS,
    UPDATE_SHOPPING_CART_ITEMS,
    GET_CITY_BY_STATE_PROVINCEID,
    GET_STAEPROVINCES_BY_COUNTRY_ID,
    COUNTRIES,
    COUNTRY_BY_ID,
    PREPARE_NEW_ORDER,
    ORDER_PAGE_LIST,
    ADD_NEW_ORDER,
    ADD_NEW_ADDRESS,
    EDIT_ADDRESS,
    GET_ORDER_DETAIL,
    GET_CUSTOMER_ADDRESS,
    CANCEL_ORDER,
    CONFIRMORDER_COMPLETED,
    GET_PRODUCT_INFO,
    GET_ARTICAL_CLASS,
    COMPANY_ACCESS,
    OtherT
};


typedef void  (^RequestResultSuccess)(id resultSuccess);
typedef void  (^RequestResultErr)(id resultError);

@interface NetRequest : NSObject
+(void)deallWithResuit:(RequestResultSuccess)success err:(RequestResultSuccess)err withRequestResultData:(id)resultSuccess andPostType:(RequestType)requestType;
+(void)requestShopWithType:(RequestType)shopType withPostUrl:(NSString *)url andPostInfo:(NSDictionary *)postInfo result:(RequestResultSuccess)success err:(RequestResultErr)err;
+(void)requestShopWithType:(RequestType)shopType withGetUrl:(NSString *)url andGetInfo:(NSDictionary *)postInfo result:(RequestResultSuccess)success err:(RequestResultErr)err;
@end

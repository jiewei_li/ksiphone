//
//  NetRequest.m
//  康岁健康
//
//  Created by cerastes on 14-9-23.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "NetRequest.h"
#import "DataFiles.h"

@implementation NetRequest
/**
 base post request method
*/
+(void)requestWithPostUrl:(NSString *)url andPostInfo:(NSDictionary *)postInfo result:(RequestResultSuccess)result  err:(RequestResultErr)err{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [manager POST:url parameters:postInfo success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject objectForCJKey:@"Suc"] && [[responseObject objectForCJKey:@"Suc"] integerValue] != 1) {
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[responseObject objectForCJKey:@"Suc"],@"Suc",[responseObject objectForCJKey:@"ErrCode"],@"ErrCode", nil];
            err(dic);
        }
        else
        {
            result(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
}

/**
 base get request method
*/

+(void)requestWithGetUrl:(NSString *)url withGetInfo:(NSDictionary *)postInfo result:(RequestResultSuccess)result err:(RequestResultErr)err{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    if([url containsString:@"/appapi/"] ){
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    }

    [manager GET:url parameters:postInfo success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"success: %@", responseObject );

        if ([[responseObject class] isKindOfClass:[NSError class]]) {
            err(nil);
        }
        else if ([responseObject objectForCJKey:@"Suc"] && [[responseObject objectForCJKey:@"Suc"] integerValue] != 1) {
//            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[responseObject objectForCJKey:@"Suc"],@"Suc",[responseObject objectForCJKey:@"ErrCode"],@"ErrCode", nil];
            err(responseObject);
        }
        else
        {
            result(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ %@", error,url);

        result(error);
    }];
}



//
+(void)requestShopWithType:(RequestType)requestType withPostUrl:(NSString *)url andPostInfo:(NSDictionary *)postInfo result:(RequestResultSuccess)success err:(RequestResultErr)err{
    url = [NetRequest dealWithUrl:url];
    [NetRequest requestWithPostUrl:url andPostInfo:postInfo result:^(id resultT) {
        NSLog(@"post success = %@",resultT);
        [NetRequest deallWithResuit:success err:err withRequestResultData:resultT andPostType:requestType];
    } err:^(id resultError) {
         NSLog(@"post err  = %@   %@",resultError,url);
        err(resultError);
    }];
}

+(void)requestShopWithType:(RequestType)requestType withGetUrl:(NSString *)url andGetInfo:(NSDictionary *)postInfo result:(RequestResultSuccess)success err:(RequestResultErr)err{
    url = [NetRequest dealWithUrl:url];
    [NetRequest requestWithGetUrl:url withGetInfo:postInfo result:^(id resultSuccess) {
        [NetRequest deallWithResuit:success err:err withRequestResultData:resultSuccess andPostType:requestType];
        } err:^(id resultError) {
        NSLog(@"Error: %@ %@", resultError,url);
//            [UIAlertView showMessage:@"获取支付信息失败，请稍后重试"];
        err(resultError);
    }];

}
//deal with result

+(void)deallWithResuit:(RequestResultSuccess)success err:(RequestResultSuccess)err withRequestResultData:(id)resultSuccess andPostType:(RequestType)requestType{
    NSString *restSring = [[NSString stringWithFormat:@"%@",resultSuccess] copy];
//    NSString *errstring = [NSString stringWithString:restSring];
//    NSString * encodedString = (NSString *)CFURLCreateStringByAddingPercentEscapes( kCFAllocatorDefault, (CFStringRef)restSring, NULL, NULL,  kCFStringEncodingUTF8 );
//    char *cc = restSring;
    
    user_defaults_set_string(@"restSring", restSring)
//    NSString * encodedString = [NSString stringWithCString:[restSring UTF8String] encoding:NSUTF8StringEncoding];
    if ([user_defaults_get_object(@"restSring") containsString:@"NSErrorFailingURLKey"]||[user_defaults_get_object(@"restSring") containsString:@"NSCocoaErrorDomain"]) {
//        [UIAlertView showMessage:@"网络问题1，请稍后再试"];
        return;
    }
    
    else if ([[resultSuccess class] isKindOfClass:[NSError class]]) {
//        [UIAlertView showMessage:@"网络问题2，请稍后再试"];
        return ;
    }
    else if ( [[resultSuccess objectForKey:@"Suc"] intValue] != 1 ) {
//        [UIAlertView showMessage:@"网络问题3，请稍后再试"];
        err(nil);
    }
    else if (requestType == LOG_IN){
        success([NetRequest logInInfo:resultSuccess]);
    }
    else if(requestType==COMPANY_ACCESS){
        success([NetRequest companyAccess:resultSuccess]);
    }
    else if(requestType == GET_CUSTOMER){
        success([NetRequest CustomerInfo:resultSuccess]);
    }
    
    else if (requestType == EXAM_ORDER_WITH_RELATED){
        success([NetRequest orderEithRelated:resultSuccess]);
    }
    else if (requestType == EXAM_SET_BRIEF){
        success([NetRequest examSetBrief:resultSuccess]);
    }
    else if (requestType == CITY_BY_EXAMSET){
        success([NetRequest cityByExamset:resultSuccess]);
    }
    else if (requestType == EXAM_CENTER_BY_CITY_EXAMSET){
        success([NetRequest examCenterByCityExamset:resultSuccess]);
    }
    else if (requestType == BOOK_INFO){
        success([NetRequest bookInfo:resultSuccess]);
    }
    
    else if (REATION == requestType){
        success([NetRequest reation:resultSuccess]);
    }
    else if (COUNTRY == requestType){
        success([NetRequest country:resultSuccess]);
    }
    else if (PROVINCE == requestType || CITY == requestType){
        success([NetRequest place:resultSuccess]);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    else if (requestType == PRODUCT_SLIDE_IMAGE_INFO) {
        NSArray *arr = [NetRequest productSlideImageInfo:resultSuccess];
        success(arr);
    }
    else if(requestType == PRODUCT_PAGE_LIST)
    {
        success([NetRequest productPageList:resultSuccess]);
    }
    else if (requestType == GET_PRODUCT_INFO){
        success(resultSuccess);
    }
    else if(requestType == STORE_OVERVIEW)
    {
        [NetRequest storeOverView:resultSuccess];
    }
    else if(requestType == PRODUCT_VARIANT_ATTRIBUTE_BY_PRODUCTID){
        //            success([NetRequest productInfo:resultSuccess]);
    }
    else if(requestType == SHOPPING_CART_ITEM){
        NSLog(@"SHOPPING_CART_ITEM = %@",resultSuccess);
        success([NetRequest shoppingCartItems:resultSuccess]);
    }
    else if(PRODUCT_PICTURES == requestType){
        success([NetRequest productPictures:resultSuccess]);
        NSLog(@"PRODUCT_PICTURES = %@",resultSuccess);
    }
    else if(PRODUCT_ATTRIBUTE == requestType)
    {
        NSLog(@"PRODUCT_ATTRIBUTE = %@",resultSuccess);
        success([NetRequest productAttribute:resultSuccess]);
        
    }
    else if(PRODUCT_DETAIL == requestType)
    {
        NSLog(@"PRODUCT_ATTRIBUTE = %@",resultSuccess);
        success([NetRequest productDetail:resultSuccess]);
        
    }
    
    else if(Add2Cart == requestType){
        success([NetRequest add2Cart:resultSuccess]);
    }
    else if(GET_CITY_BY_STATE_PROVINCEID == requestType){
        success([NetRequest citeByStateProvinceId:resultSuccess]);
    }
    else if(GET_STAEPROVINCES_BY_COUNTRY_ID == requestType){
        success([NetRequest stateProvincesByCountryId:resultSuccess]);
    }
    else if(COUNTRIES == requestType){
        success([NetRequest countries:resultSuccess]);
    }
    else if(COUNTRY_BY_ID == requestType){
        success([NetRequest countryById:resultSuccess]);
    }
    
    else if(ORDER_PAGE_LIST == requestType){
        success([NetRequest orderPageList:resultSuccess]);
    }
    else if(UPDATE_SHOPPING_CART_ITEMS == requestType){
        success(resultSuccess);
    }
    else if (Add2Cart == requestType) {
        success(resultSuccess);
    }
    else if(PREPARE_NEW_ORDER == requestType){
        
        success([NetRequest prepareNewOrder:resultSuccess]);
    }
    else if(ADD_NEW_ORDER == requestType){
        success([NetRequest addNewOrder:resultSuccess]);
    }
    
    else if(GET_ORDER_DETAIL == requestType){
        success([NetRequest getOrderDetail:resultSuccess]);
        
    }
    else if (GET_CUSTOMER_ADDRESS == requestType){
        success([NetRequest customerAddress:resultSuccess]);
    }
    else if(CANCEL_ORDER == requestType){
        success(resultSuccess);

    }
    else
    {
        success(resultSuccess);
    }

    
}

+(NSString *)dealWithUrl:(NSString *)url
{
    NSDictionary *po = user_defaults_get_object(@"ADDRESS_GETSERVERURLS");
    if (po) {
        NSDictionary *data = [po objectForKey:@"Data"];
        NSString *SERVER_EXAM = [data objectForCJKey:@"SERVER_EXAM"];
        NSString *SERVER_HOST = [data objectForCJKey:@"SERVER_HOST"];
        NSString *STORE_DOMAIN = [data objectForCJKey:@"STORE_DOMAIN"];
        NSString *STORE_SERVERS_URL = [data objectForCJKey:@"STORE_SERVERS_URL"];
        url = [url stringByReplacingOccurrencesOfString:@"SERVER_EXAM" withString:SERVER_EXAM];
        url = [url stringByReplacingOccurrencesOfString:@"STORE_SERVERS_URL" withString:STORE_SERVERS_URL];
        url = [url stringByReplacingOccurrencesOfString:@"SERVER_HOST" withString:SERVER_HOST];
        url = [url stringByReplacingOccurrencesOfString:@"STORE_DOMAIN" withString:STORE_DOMAIN];

    }
//    SERVER_EXAM" = "http://122.114.50.232:85";
//    "" = "hppt://www.ihealthink.com";
//    "" = "http://122.114.50.232:81";
//    "STORE_SERVERS_URL" = "http://122.114.50.232:88";
    return url;
}
#pragma mark - deal with result

+(UserInfos *)logInInfo:(NSDictionary *)resultDic {
    UserInfos *info = [[UserInfos alloc]init];
    [info setUserInfo:resultDic];
    return info;
}
+(CompanyAccess*)companyAccess:(NSDictionary*)resultDic{
    CompanyAccess* companyAccess=[[CompanyAccess alloc]init];
    [companyAccess setCompanyAccess:resultDic];
    return companyAccess;
}
+(CustomerData *)CustomerInfo:(NSDictionary *)resultDic {
    CustomerData *customer = [[CustomerData alloc]init];
    [customer setData:resultDic];
    return customer;
}

+(NSArray *)orderEithRelated:(NSDictionary *)resultDic {
    NSMutableArray *customer = [[NSMutableArray alloc]init];
    NSArray *order = [[resultDic objectForKey:@"Data"] objectForKey:@"order"];
    for (NSDictionary *dic in order) {
        ExamOrderData *data = [[ExamOrderData alloc]init];
        [data setData:dic];
        [customer addObject:data];
    }
    return customer;
}

+(NSArray *)examSetBrief:(NSDictionary *)resultDic {
    NSMutableArray *customer = [[NSMutableArray alloc]init];
    NSArray *order = [resultDic objectForKey:@"Data"] ;
    for (NSDictionary *dic in order) {
        ExamSetBriefData *data = [[ExamSetBriefData alloc]init];
        [data setData:dic];
        [customer addObject:data];
    }
    return customer;
}
+(NSArray *)cityByExamset:(NSDictionary *)resultDic {
    NSMutableArray *customer = [[NSMutableArray alloc]init];
    NSArray *order = [resultDic objectForKey:@"Data"] ;
    for (NSDictionary *dic in order) {
        CityByExamsetData *data = [[CityByExamsetData alloc]init];
        [data setData:dic];
        [customer addObject:data];
    }
    return customer;
}
+(NSArray *)examCenterByCityExamset:(NSDictionary *)resultDic {
    NSMutableArray *customer = [[NSMutableArray alloc]init];
    NSArray *order = [resultDic objectForKey:@"Data"] ;
    for (NSDictionary *dic in order) {
        ExamCenterByCityExamsetData *data = [[ExamCenterByCityExamsetData alloc]init];
        [data setData:dic];
        [customer addObject:data];
    }
    return customer;
}

+(NSArray *)bookInfo:(NSDictionary *)resultDic {
    NSMutableArray *customer = [[NSMutableArray alloc]init];
    NSArray *order = [resultDic objectForKey:@"Data"] ;
    for (NSDictionary *dic in order) {
        BookInfoData *data = [[BookInfoData alloc]init];
        [data setData:dic];
        [customer addObject:data];
    }
    return customer;
}
+(NSArray *)reation:(NSDictionary *)resultDic {
    NSMutableArray *customer = [[NSMutableArray alloc]init];
    NSArray *order = [resultDic objectForKey:@"Data"] ;
    for (NSDictionary *dic in order) {
        CustomerData *data = [[CustomerData alloc]init];
        [data setrData:dic];
        [customer addObject:data];
    }
    return customer;
}
+(NSArray *)country:(NSDictionary *)resultDic {
    NSMutableArray *customer = [[NSMutableArray alloc]init];
    NSArray *order = [resultDic objectForKey:@"Data"] ;
    for (NSDictionary *dic in order) {
        CountryData *data = [[CountryData alloc]init];
        [data setData:dic];
        [customer addObject:data];
    }
    return customer;
}

+(NSArray *)place:(NSDictionary *)resultDic {
    NSMutableArray *customer = [[NSMutableArray alloc]init];
    NSArray *order = [resultDic objectForKey:@"Data"] ;
    for (NSDictionary *dic in order) {
        PlaceData *data = [[PlaceData alloc]init];
        [data setData:dic];
        [customer addObject:data];
    }
    return customer;
}

//CITY_BY_EXAMSET
//商品首页顶部广告处理

+(NSArray *)productSlideImageInfo:(NSDictionary *)resultDic{
    NSMutableArray *resultArry = [[NSMutableArray alloc]init];
    NSArray *resultData = [resultDic objectForKey:@"Data"];
    for (NSDictionary *dic in resultData) {
        ProductSlideImageInfoData *data = [[ProductSlideImageInfoData alloc]init];
        data.link = [dic objectForCJKey:@"Link"];
        data.pictureid = [dic objectForCJKey:@"PictureId"];
        data.pictureurl = [dic objectForCJKey:@"PictureUrl"];
        data.text = [dic objectForCJKey:@"Text"];
        data.Action = [dic objectForCJKey:@"Action"];
        data.ActionParameter = [dic objectForCJKey:@"ActionParameter"];
        [resultArry addObject:data];
    }
    return resultArry;
}

//首页商品（主打商品）列表
+(NSArray *)productPageList:(NSDictionary *)resultDic{
    NSArray *arr = [resultDic objectForKey:@"Data"];
    NSMutableArray *producetArry = [[NSMutableArray alloc]init];
    for (NSDictionary *dic in arr) {
        ProductListData *data = [[ProductListData alloc] init];
        data.Id = [dic objectForCJKey:@"Id"];
        data.Name = [dic objectForCJKey:@"Name"];
        data.PictureUrl = [dic objectForCJKey:@"PictureUrl"];
        data.Price = [dic objectForCJKey:@"Price"];
        data.ProductTypeId = [dic objectForCJKey:@"ProductTypeId"];
        data.ShortDescription = [dic objectForCJKey:@"ShortDescription"];
        data.catagoryId = [dic objectForCJKey:@"catagoryId"];
        [producetArry addObject:data];
    }
    return producetArry;
}
//获取商品基本信息
//+(ProductInfoData *)productInfo:(NSDictionary *)resultDic{
//    ProductInfoData *infoData = [[ProductInfoData alloc]init];
//    NSDictionary *data = [resultDic objectForCJKey:@"Data"];
//    infoData.Id = [data objectForCJKey:@"Id"];
//    infoData.Name = [data objectForCJKey:@"Name"];
//    infoData.PictureUrl = [data objectForCJKey:@"PictureUrl"];
//    infoData.Price = [data objectForCJKey:@"Price"];
//    infoData.ProductTypeId = [data objectForCJKey:@"ProductTypeId"];
//    infoData.ShortDescription = [data objectForCJKey:@"ShortDescription"];
//    NSLog(@"%@",resultDic);
//    return infoData;
//}
//

//获取产品图片列表
+(NSMutableArray *)productPictures:(NSDictionary *)resultDic{
    NSArray *dataArr = [resultDic objectForKey:@"Data"];
    NSMutableArray *resultArry = [[NSMutableArray alloc ]init];
    for(NSDictionary *dataDic in dataArr){
        ProductPicturesData *data = [[ProductPicturesData alloc]init];

        data.DisplayOrder = [dataDic objectForCJKey:@"DisplayOrder"];
        data.Id = [dataDic objectForCJKey:@"Id"];
        data.PictureId = [dataDic objectForCJKey:@"PictureId"];
        data.PictureUrl = [dataDic objectForCJKey:@"PictureUrl"];
        [resultArry addObject:data];
    }
    
    return resultArry;
}

+(void)storeOverView:(NSDictionary *)resultDic{
    NSDictionary *data = [resultDic objectForKey:@"Data"];
    NSString *bookingCount = [data objectForCJKey:@"BookingCount"];
    NSString *cartCount = [data objectForCJKey:@"CartCount"];
    NSString *orderCount = [data objectForCJKey:@"OrderCount"];
    [[NSNotificationCenter defaultCenter] postNotificationName:BookingCountNotifocation object:[bookingCount notNull]];
    [[NSNotificationCenter defaultCenter] postNotificationName:CartCountNotifocation object:[cartCount notNull]];
    [[NSNotificationCenter defaultCenter] postNotificationName:OrderCountNotifocation object:[orderCount notNull]];
    user_defaults_set_string(NumberOfCrat, cartCount);

}
//获取商品属性选择参数
+(NSMutableArray *)productAttribute:(NSDictionary *)resultDic{
    NSArray *dataArr = [resultDic objectForKey:@"Data"];
    NSMutableArray *resultArry = [[NSMutableArray alloc ]init];
    for(NSDictionary *dataDic in dataArr){
        ProductAttributeData *data = [[ProductAttributeData alloc]init];
        data.DefaultValue = [dataDic objectForCJKey:@"DefaultValue"];
        data.Id = [dataDic objectForCJKey:@"Id"];
        data.IsRequired = [dataDic objectForCJKey:@"IsRequired"];
        data.SelectionType = [dataDic objectForCJKey:@"SelectionType"];
        data.TextPrompt = [dataDic objectForCJKey:@"TextPrompt"];
        NSArray *attributeValuesArr = [dataDic objectForKey:@"AttributeValues"];
        NSMutableArray *attributeValues = [[NSMutableArray alloc]init];
        for(NSDictionary *attributeValuesDic in attributeValuesArr){
            ProductAttributeValuesData *valueData = [[ProductAttributeValuesData alloc]init];
            valueData.Id              = [attributeValuesDic objectForCJKey:@"Id"];
            valueData.Name            = [attributeValuesDic objectForCJKey:@"Name"];
            valueData.PriceAdjustment = [attributeValuesDic objectForCJKey:@"PriceAdjustment"];
            [attributeValues addObject:valueData];
        }
        data.AttributeValues = attributeValues;
        [resultArry addObject:data];
    }
    
    return resultArry;
}
//获取产品详细介绍和规格参数
+(ProductDetailData *)productDetail:(NSDictionary *)resultDic{
    ProductDetailData *productDetailData = [[ProductDetailData alloc]init];
    NSDictionary *data = [resultDic objectForKey:@"Data"];
    productDetailData.ProductId = [data objectForCJKey:@"ProductId"];
    productDetailData.FullDescription = [data objectForCJKey:@"FullDescription"];
    NSArray *specifications = [data objectForKey:@"Specifications"];
    NSMutableArray *specificationsM = [[NSMutableArray alloc]init];
    for(NSDictionary *specificationDic in specifications){
        ProductDetailSpecificationsData *productDetailSpecificationsData = [[ProductDetailSpecificationsData alloc]init];
        productDetailSpecificationsData.Name = [specificationDic objectForCJKey:@"Name"];
//        productDetailSpecificationsData.DisplayOrder = [specificationDic objectForCJKey:@"DisplayOrder"];
        productDetailSpecificationsData.Value = [specificationDic objectForCJKey:@"Value"];
        [specificationsM addObject:productDetailSpecificationsData];
    }
    productDetailData.Specifications = specificationsM;
    return productDetailData;
}

//商品加入购物车
+(NSDictionary *)add2Cart:(NSDictionary *)resultDic{
    return resultDic;
}

//读取购物车条目
+(NSArray *)shoppingCartItems:(NSDictionary *)resultDic{
    NSMutableArray *resultArry = [[NSMutableArray alloc]init];
    NSArray *data = [resultDic objectForKey:@"Data"];
    for (NSDictionary *dic in data) {
        ShoppingCartItemsData *shoppingCartItemsData = [[ShoppingCartItemsData alloc]init];
        shoppingCartItemsData.Id = [dic objectForCJKey:@"Id"];
        shoppingCartItemsData.PictureUrl = [dic objectForCJKey:@"PictureUrl"];
        shoppingCartItemsData.Price = [dic objectForCJKey:@"Price"];
        shoppingCartItemsData.ProductId = [dic objectForCJKey:@"ProductId"];
        shoppingCartItemsData.ProductName = [dic objectForCJKey:@"ProductName"];
        shoppingCartItemsData.Quantity = [dic objectForCJKey:@"Quantity"];
        shoppingCartItemsData.Selected = [[dic objectForCJKey:@"Selected"] boolValue];
        

//#warning Tmp
        shoppingCartItemsData.Selected = NO;
        NSArray *attrs = [dic objectForKey:@"Attributes"];
        NSMutableArray *attributes = [[NSMutableArray alloc]init];
        if (![attrs isKindOfClass:[NSNull class]]) {
            for (NSDictionary *atDic in attrs) {
                ShoppingCartItemsAttributeData *shoppingCartItemsAttributeData = [[ShoppingCartItemsAttributeData alloc]init];
               
                shoppingCartItemsAttributeData.Id = [atDic objectForCJKey:@"Id"];
                shoppingCartItemsAttributeData.Text = [atDic objectForCJKey:@"Text"];
                shoppingCartItemsAttributeData.AttributeIds = [atDic objectForKey:@"AttributeIds"];
                [attributes addObject:shoppingCartItemsAttributeData];
            }
            shoppingCartItemsData.Attributes = attributes;
        }
        else{
            shoppingCartItemsData.Attributes = [[NSArray alloc]init];
        }
       
        [resultArry addObject:shoppingCartItemsData];
    }
    return resultArry;
}

//4.1.	获得指定省份的城市列表
+(NSArray *)citeByStateProvinceId:(NSDictionary *)resultDic{
    NSMutableArray *resultArry = [[NSMutableArray alloc]init];
    NSArray *datas = [resultDic objectForKey:@"Data"];
    for(NSDictionary *dic in datas){
        CitysByStateProvinceIdData *data = [[CitysByStateProvinceIdData alloc]init];
        data.StateProvinceId  = [dic objectForCJKey:@"StateProvinceId"];
        data.Name  = [dic objectForCJKey:@"Name"];
        data.DisplayOrder  = [dic objectForCJKey:@"DisplayOrder"];
        data.Id  = [dic objectForCJKey:@"Id"];
        data.Abbreviation  = [dic objectForCJKey:@"Abbreviation"];
        [resultArry addObject:data];
    }
    
    return resultArry;
}

//4.2.	获得省份列表
+(NSArray *)stateProvincesByCountryId:(NSDictionary *)resultDic{
    NSMutableArray *resultArry = [[NSMutableArray alloc]init];
    NSArray *datas = [resultDic objectForKey:@"Data"];
    for(NSDictionary *dic in datas){
        StateProvincesByCountryIdData *data = [[StateProvincesByCountryIdData alloc]init];
        data.CountryId  = [dic objectForCJKey:@"CountryId"];
        data.Name  = [dic objectForCJKey:@"Name"];
        data.DisplayOrder  = [dic objectForCJKey:@"DisplayOrder"];
        data.Id  = [dic objectForCJKey:@"Id"];
        data.Abbreviation  = [dic objectForCJKey:@"Abbreviation"];
        [resultArry addObject:data];
    }
    return resultArry;
}
//4.3.	获得国家列表
+(NSArray *)countries:(NSDictionary *)resultDic{
    NSMutableArray *resultArry = [[NSMutableArray alloc]init];
    NSArray *datas = [resultDic objectForKey:@"Data"];
    for(NSDictionary *dic in datas){
        CountriesDate *data = [[CountriesDate alloc]init];
        data.Id  = [dic objectForCJKey:@"Id"];
        data.Name  = [dic objectForCJKey:@"Name"];
        data.DisplayOrder  = [dic objectForCJKey:@"DisplayOrder"];
        data.TwoLetterIsoCode  = [dic objectForCJKey:@"TwoLetterIsoCode"];
        data.NumericIsoCode  = [dic objectForCJKey:@"NumericIsoCode"];
        data.InvoiceAllowed  = [dic objectForCJKey:@"InvoiceAllowed"];
        data.ShippingAllowed = [dic objectForCJKey:@"ShippingAllowed"];
        [resultArry addObject:data];
    }
    return resultArry;
}
//4.4.	获取单个国家信息（建议与4.2合并，参见修改后的4.2接口）
+(NSArray *)countryById:(NSDictionary *)resultDic{
    NSMutableArray *resultArry = [[NSMutableArray alloc]init];
    NSArray *datas = [resultDic objectForKey:@"Data"];
    for(NSDictionary *dic in datas){
        CountryByIdData *data = [[CountryByIdData alloc]init];
        data.Id  = [dic objectForCJKey:@"Id"];
        data.Name  = [dic objectForCJKey:@"Name"];
        data.DisplayOrder  = [dic objectForCJKey:@"DisplayOrder"];
        data.TwoLetterIsoCode  = [dic objectForCJKey:@"TwoLetterIsoCode"];
        data.NumericIsoCode  = [dic objectForCJKey:@"NumericIsoCode"];
        [resultArry addObject:data];
    }
    return resultArry;
}
//5.1.	提交订单
+(PrepareNewOrderData *)prepareNewOrder:(NSDictionary *)resultDic{
    NSDictionary *data = [resultDic objectForKey:@"Data"];
    PrepareNewOrderData *preparData = [[PrepareNewOrderData alloc]init];
    
//    DeliveryAddresses
    NSArray *deliveryAddresses = [data objectForKey:@"DeliveryAddresses"];
    NSMutableArray *deliveryAddressesArr = [[NSMutableArray alloc]init];
    if(deliveryAddresses != nil && ![deliveryAddresses isKindOfClass:[NSNull class]]){
        for(NSDictionary *deliveryAddressesDic in deliveryAddresses){
            AddressesData *addressData = [[AddressesData alloc]init];
            addressData.Address   = [deliveryAddressesDic objectForCJKey:@"Address"];
            addressData.Email     = [deliveryAddressesDic objectForCJKey:@"Email"];
            addressData.FirstName = [deliveryAddressesDic objectForCJKey:@"FisrtName"];
            addressData.Id        = [deliveryAddressesDic objectForCJKey:@"Id"];
            addressData.LastName  = [deliveryAddressesDic objectForCJKey:@"LastName"];
            [deliveryAddressesArr addObject:addressData];
        }
    }
    
    preparData.DeliveryAddresses = deliveryAddressesArr;
    
//   InvoiceAddresses
    NSArray *invoiceAddresses = [data objectForKey:@"InvoiceAddresses"];
    NSMutableArray *invoiceAddressesArr = [[NSMutableArray alloc]init];
    if (![invoiceAddresses isKindOfClass:[NSNull class]]) {
        for(NSDictionary *deliveryAddressesDicDic in invoiceAddresses){
            AddressesData *addressData = [[AddressesData alloc]init];
            addressData.Address   = [deliveryAddressesDicDic objectForCJKey:@"Address"];
            addressData.Email     = [deliveryAddressesDicDic objectForCJKey:@"Email"];
            addressData.FirstName = [deliveryAddressesDicDic objectForCJKey:@"FisrtName"];
            addressData.Id        = [deliveryAddressesDicDic objectForCJKey:@"Id"];
            addressData.LastName  = [deliveryAddressesDicDic objectForCJKey:@"LastName"];
            [invoiceAddressesArr addObject:addressData];
        }
    }
    
    preparData.InvoiceAddresses = invoiceAddressesArr;
    
    preparData.NeedDelivery = [data  objectForCJKey:@"NeedDelivery"];
    preparData.NeedInvoice  = [data  objectForCJKey:@"NeedInvoice"];
    
    
    NSArray *ShippingMethod = [data  objectForKey:@"ShippingMethod"];
    preparData.ShippingMethod = ShippingMethod;
    NSArray *payMethods = [data objectForKey:@"PayMethods"];
    preparData.PayMethods = payMethods;
    
    return preparData;
}
//5.1.	订单列表
+(NSArray *)orderPageList:(NSDictionary *)resultDic{
    NSMutableArray *resultArry = [[NSMutableArray alloc]init];
    NSArray *datas = [resultDic objectForKey:@"Data"];
    for(NSDictionary *dic in datas){
        OrderPageListData *data = [[OrderPageListData alloc]init];
        data.Id  = [dic objectForCJKey:@"Id"];
        data.CreatedOnUtc  = [dic objectForCJKey:@"CreatedOnUtc"];
        data.CustomerId  = [dic objectForCJKey:@"CustomerId"];
        data.OrderItemsCount  = [dic objectForCJKey:@"OrderItemsCount"];
        data.OrderStatusId  = [[dic objectForKey:@"OrderStatus"] objectForCJKey:@"StatusId"];
        data.OrderStatusName  = [[dic objectForKey:@"OrderStatus"] objectForCJKey:@"Name"];
        data.OrderTotal  = [dic objectForCJKey:@"OrderTotal"];
        data.PaymentStatusName  = [[dic objectForKey:@"PaymentStatus"] objectForCJKey:@"Name"];
        data.PaymentStatusStatusId  = [[dic objectForKey:@"PaymentStatus"] objectForCJKey:@"StatusId"];
        data.ShippingStatusName = [[dic objectForKey:@"ShippingStatus"] objectForCJKey:@"Name"];
        data.ShippingStatusId = [[dic objectForKey:@"ShippingStatus"] objectForCJKey:@"StatusId"];

        NSMutableArray *attributes = [[NSMutableArray alloc]init];
        NSArray *attrs = [dic objectForKey:@"OrderItems"];

        if (![attrs isKindOfClass:[NSNull class]]) {
            for (NSDictionary *atDic in attrs) {
                ShoppingCartItemsData *shoppingCartItemsData = [[ShoppingCartItemsData alloc]init];
                shoppingCartItemsData.ProductId   = [atDic objectForCJKey:@"ProductId"];
                shoppingCartItemsData.Id          = [atDic objectForCJKey:@"Id"];
                shoppingCartItemsData.ProductName = [atDic objectForCJKey:@"ProductName"];
                shoppingCartItemsData.Description = [atDic objectForCJKey:@"Description"];
                shoppingCartItemsData.Price       = [atDic objectForCJKey:@"Price"];
                shoppingCartItemsData.Quantity    = [atDic objectForCJKey:@"Quantity"];
                shoppingCartItemsData.PictureUrl  = [atDic objectForCJKey:@"PictureUrl"];
                shoppingCartItemsData.Selected    = NO;
                NSArray *Attributes = [atDic objectForKey:@"Attributes"];
                
                NSMutableArray *sectionsMA = [[NSMutableArray alloc]init];
                if (![Attributes isKindOfClass:[NSNull class]]) {
                    for (NSDictionary *sectionDic in Attributes) {
                        NSString *SelectedId = [sectionDic objectForCJKey:@"Id"];
                        [sectionsMA addObject:SelectedId];
                    }
                }
                
                shoppingCartItemsData.Attributes = sectionsMA;
                [attributes addObject:shoppingCartItemsData];
            }
            data.OrderItems = attributes;
        }
        else{
            data.OrderItems = [[NSArray alloc]init];
        }
        [resultArry addObject:data];
    }
    return resultArry;
}

+(NewOrderData *)addNewOrder:(NSDictionary *)resultDic{
    NewOrderData *data      = [[NewOrderData alloc]init];
    NSDictionary *dataDic   = [resultDic objectForKey:@"Data"];
    data.CreatedOnUtc       = [dataDic objectForKey:@"CreatedOnUtc"];
    data.InvoiceAddress     = [dataDic objectForKey:@"InvoiceAddress"];
    data.InvoiceAddressId   = [dataDic objectForKey:@"InvoiceAddressId"];
    data.OrderId            = [dataDic objectForKey:@"OrderId"];
    data.OrderItemsCount    = [dataDic objectForKey:@"OrderItemsCount"];
    data.OrderStatusName    = [[dataDic objectForKey:@"OrderStatus"] objectForKey:@"Name"];
    data.OrderStatusId      = [[dataDic objectForKey:@"OrderStatus"] objectForKey:@"StatusId"];
    data.OrderTotal         = [dataDic objectForKey:@"OrderTotal"];
    data.PaymentStatusName  = [[dataDic objectForKey:@"PaymentStatus"] objectForKey:@"Name"];
    data.PaymentStatusId    = [[dataDic objectForKey:@"PaymentStatus"] objectForKey:@"StatusId"];
    data.ShippingAddress    = [dataDic objectForKey:@"ShippingAddress"];
    data.ShippingAddressId  = [dataDic objectForKey:@"ShippingAddressId"];
    data.ShippingStatusName = [[dataDic objectForKey:@"ShippingStatus"] objectForKey:@"Name"];
    data.ShippingStatusId   = [[dataDic objectForKey:@"ShippingStatus"] objectForKey:@"StatusId"];

    return data;
}

+(OrderDetailData *)getOrderDetail:(NSDictionary *)resultDic{
    OrderDetailData *detailData = [[OrderDetailData alloc]init];
    NSDictionary *data = [resultDic objectForKey:@"Data"];
    detailData.Id = [data objectForKey:@"Id"];
    detailData.CustomerId = [data objectForKey:@"CustomerId"];
    detailData.TotalAmount = [data objectForKey:@"OrderTotal"];
    detailData.CreatedOnUtc = [data objectForKey:@"CreatedOnUtc"];
    detailData.OrderItemsCount = [data objectForKey:@"OrderItemsCount"];
    detailData.OrderStatusId   = [[data objectForKey:@"OrderStatus"] objectForKey:@"StatusId"];
    detailData.OrderStatusName   = [[data objectForKey:@"OrderStatus"] objectForKey:@"Name"];
    detailData.ShippingMethodId   = [[data objectForKey:@"ShippingMethod"] objectForKey:@"Id"];
    detailData.ShippingMethodName   = [[data objectForKey:@"ShippingMethod"] objectForKey:@"Name"];
    detailData.ShippingMethodPrice   = [[data objectForKey:@"ShippingMethod"] objectForKey:@"Price"];
    detailData.ShippingMethodDescription   = [[data objectForKey:@"ShippingMethod"] objectForKey:@"Description"];
    detailData.PaymentMethod = [data objectForKey:@"PayMethod"];
    
    detailData.InvoiceAddressFirstName   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"FirstName"];
    detailData.InvoiceAddressLastName   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"LastName"];
    detailData.InvoiceAddressEmail   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"Email"];
    detailData.InvoiceAddressCompany   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"Company"];
    detailData.InvoiceAddressCountryId   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"CountryId"];
    detailData.InvoiceAddressProvinceId   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"StateProvinceId"];
    detailData.InvoiceAddressCityId   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"Id"];
    detailData.InvoiceAddressAddress   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"Address"];
    detailData.InvoiceAddressCityName   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"CityName"];
    detailData.InvoiceAddressStateProvinceName   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"StateProvinceName"];
    
    detailData.InvoiceAddressZipPostalCode   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"ZipPostalCode"];
    detailData.InvoiceAddressPhoneNumber   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"PhoneNumber"];
    detailData.InvoiceAddressFaxNumber   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"FaxNumber"];
    detailData.InvoiceAddressCustomerId   = [[data objectForKey:@"InvoiceAddress"] objectForKey:@"CustomerId"];
    
    detailData.ShippingAddressFirstName   = [[data objectForKey:@"ShippingAddress"] objectForKey:@"FirstName"];
    detailData.ShippingAddressLastName   = [[data objectForKey:@"ShippingAddress"] objectForKey:@"LastName"];
    detailData.ShippingAddressEmail   = [[data objectForKey:@"ShippingAddress"] objectForKey:@"Email"];
    detailData.ShippingAddressCompany   = [[data objectForKey:@"ShippingAddress"] objectForKey:@"Company"];
    detailData.ShippingAddressCountryId   = [[data objectForKey:@"ShippingAddress"] objectForKey:@"CountryId"];
    detailData.ShippingAddressStateProvinceId   = [[data objectForKey:@"ShippingAddress"] objectForKey:@"StateProvinceId"];
    detailData.ShippingAddressCityId   = [[data objectForKey:@"ShippingAddress"] objectForKey:@"CityId"];
    detailData.ShippingAddressAddress   = [[data objectForKey:@"ShippingAddress"] objectForKey:@"Address"];
    detailData.ShippingAddressZipPostalCode   = [[data objectForKey:@"ShippingAddress"] objectForKey:@"ZipPostalCode"];
    detailData.ShippingAddressPhoneNumber   = [[data objectForKey:@"ShippingAddress"] objectForKey:@"PhoneNumber"];
    detailData.ShippingAddressFaxNumber   = [[data objectForKey:@"ShippingAddress"] objectForKey:@"FaxNumber"];
    detailData.ShippingAddressCustomerId   = [[data objectForKey:@"ShippingAddress"] objectForKey:@"CustomerId"];

    NSArray *CheckoutAttributes = [data objectForKey:@"CheckoutAttributes"];
    for (NSDictionary *dic in CheckoutAttributes) {
        if ([dic objectForKey:@"Key"] && [[dic objectForKey:@"Key"] isEqualToString:@"NeedInvoice"]) {
            detailData.NeedInvoice = [dic objectForCJKey:@"Value"];
        }
        else if ([dic objectForKey:@"Key"] && [[dic objectForKey:@"Key"] isEqualToString:@"InvoiceTitle"]) {
            detailData.InvoiceTitle = [dic objectForCJKey:@"Value"];
        }
        else if ([dic objectForKey:@"Key"] && [[dic objectForKey:@"Key"] isEqualToString:@"NeedInvoice"]) {
            detailData.NeedInvoice = [dic objectForCJKey:@"Value"];
        }
    }
    
//    @property NSArray  *OrderItems;                    //订单产品列表
    return detailData;
}

+(NSArray *)customerAddress:(NSDictionary *)resultDic{
    if ([[resultDic objectForCJKey:@"Suc"]intValue] != 1) {
        return nil;
    }
    NSMutableArray *addressList = [[NSMutableArray alloc]init];
    NSArray *arr = [resultDic objectForKey:@"Data"];
    for (NSDictionary *addressDic in arr) {
        AddressesData *addressData = [[AddressesData alloc]init];
        addressData.Address = [addressDic objectForCJKey:@"Address"];
        addressData.CityId = [addressDic objectForCJKey:@"CityId"];
        addressData.CityName = [addressDic objectForCJKey:@"CityName"];
        addressData.Company = [addressDic objectForCJKey:@"Company"];
        addressData.CountryId = [addressDic objectForCJKey:@"CountryId"];
        addressData.CustomerId = [addressDic objectForCJKey:@"CustomerId"];
        addressData.Email = [addressDic objectForCJKey:@"Email"];
        addressData.FaxNumber = [addressDic objectForCJKey:@"FaxNumber"];
        addressData.FirstName = [addressDic objectForCJKey:@"FirstName"];
        addressData.Id = [addressDic objectForCJKey:@"Id"];
        addressData.LastName = [addressDic objectForCJKey:@"LastName"];
        addressData.PhoneNumber = [addressDic objectForCJKey:@"PhoneNumber"];
        addressData.StateProvinceId = [addressDic objectForCJKey:@"StateProvinceId"];
        addressData.StateProvinceName = [addressDic objectForCJKey:@"StateProvinceName"];
        addressData.ZipPostalCode = [addressDic objectForCJKey:@"ZipPostalCode"];
        [addressList addObject:addressData];
    }
    
    
    return addressList;
}
@end

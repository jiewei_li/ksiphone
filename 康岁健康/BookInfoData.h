//
//  BookInfoData.h
//  康岁健康
//
//  Created by 低调 on 14/12/15.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookInfoData : NSObject
@property NSString *book_allowed;
@property NSString *day;
@property NSString *dayoff;
@property NSString *left;
@property NSString *month;
-(void)setData:(NSDictionary *)data;

@end

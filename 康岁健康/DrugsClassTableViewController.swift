//
//  DrugsClassTableViewController.swift
//  亳州医保
//
//  Created by ljw on 15/2/22.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit

class DrugsClassTableViewController: UITableViewController,UISearchBarDelegate{

    

    
    var DrugsClass : [String] =  ["药品分类01","药品分类02","药品分类03","药品分类04","药品分类05"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.edgesForExtendedLayout = UIRectEdge.None

        var btn=UIBarButtonItem(title: "返回", style:UIBarButtonItemStyle.Bordered, target: self, action: "btnBackClick")
        
        // var backButton: UIBarButtonItem = UIBarButtonItem(title: "返回", style: UIBarButtonItemStyle.Bordered, target: self, action: "btn返回Click")
        self.navigationItem.leftBarButtonItem=btn;

     //   self.navigationItem.backBarButtonItem?.title="返回"
        

    }
    func btnBackClick()
    {
        
        self.navigationController?.navigationBarHidden=true
      // self.performSegueWithIdentifier("MyYuyue", sender: nil)
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 5
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
     //   let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        
      var    imgname = self.DrugsClass[indexPath.row]as (String)
        // Configure the cell...
       (cell.contentView.viewWithTag(100) as UIImageView).image=UIImage(named: imgname)
        return cell
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        

        
        
        self.performSegueWithIdentifier("DrugsDetail", sender: nil)
    
      //  var cell=tableView.cellForRowAtIndexPath(indexPath)
        //    var p:UIImageView
        
//        // 跳转到预约详细界面
//        var sb =  UIStoryboard(name: "Storyboard", bundle: nil)// [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
//        var view:YuyueDetailViewController = sb.instantiateViewControllerWithIdentifier("yuyuedetail")  as YuyueDetailViewController // [sb instantiateViewControllerWithIdentifier:@"yuyue"];  //test2为viewcontroller的StoryboardId
//        
//        self.navigationController?.pushViewController(view, animated: true)
//    


    

    }

    
    
     func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.performSegueWithIdentifier("DrugsDetail", sender: nil)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}

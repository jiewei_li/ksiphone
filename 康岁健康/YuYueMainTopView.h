//
//  YuYueMainTopView.h
//  康岁健康
//
//  Created by 低调 on 14/12/12.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ExamOrderData;
typedef void (^examSetBlock)(id sender);
typedef void (^examCenterBlock)(id sender);
typedef void (^examDataBlock)(id sender);
typedef void (^examPriceBlock)(id sender);
typedef void (^cancel)(id sender);

typedef void (^payNowBlock)(id sender);@interface YuYueMainTopView : UIView
{
    ExamOrderData *_data;
}
@property (nonatomic)    ExamOrderData *data;
@property IBOutlet UILabel *nameLable;
@property IBOutlet UILabel *realityLable;
@property IBOutlet UILabel *examsetNameLabel;
@property IBOutlet UILabel *examCenterLable;
@property IBOutlet UILabel *dateLable;
@property IBOutlet UILabel *priceLable;
@property IBOutlet UILabel *statusLable;
@property IBOutlet UILabel *leftTimeLable;
@property IBOutlet UIView  *bottomView;
@property IBOutlet UIButton  *payButton;
@property IBOutlet UIButton  *cancelButton;
@property IBOutlet UILabel  *payButtonLabel;
@property IBOutlet UILabel  *payMethodButtonLabel;

@property (strong) examSetBlock examsetBlock;
@property (strong) examCenterBlock examcenterBlock;
@property (strong) examDataBlock examdataBlock;
@property (strong) examPriceBlock exampriceBlock;
@property (strong) payNowBlock paynowBlock;
@property (strong) cancel     cancelYuYue;

-(IBAction)examSetDetail:(id)sender;
-(IBAction)examCenterDetail:(id)sender;
-(IBAction)examDataDetail:(id)sender;
-(IBAction)examPriceDetail:(id)sender;
-(IBAction)payNow:(id)sender;
-(IBAction)cancelYY:(id)sender;
@end

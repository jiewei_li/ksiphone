//
//  YuYueViewController.m
//  康岁健康
//
//  Created by 低调 on 14/12/13.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "YuYueViewController.h"
#import "CJSelectViewViewController.h"
#import "ExamSetBriefData.h"
#import "CityByExamsetData.h"
#import "ExamCenterByCityExamsetData.h"
#import "CJCalendarView.h"
#import "BookInfoData.h"
#import "InSureYuYueViewController.h"
#import "YuYueInfoViewController.h"
@interface YuYueViewController ()
{
    ExamSetBriefData            *_examSetBriefData;
    CityByExamsetData           *_cityByExamsetData;
    ExamCenterByCityExamsetData *_examCenterByCityExamsetData;
    NSDate                      *_selectedDate;
}
@end

@implementation YuYueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"体检预约";
    [self addSubView:self.choseView];
    self.choseView.height = _mainView.height;
    if ([self.customerData.gender intValue]==2 && [self.customerData.married intValue] == 2) {
        [UIAlertView showMessage:@"未婚女性需预约已婚体检套餐，请拨打400-880-0107"];
    }
    CJCalendarView *c = [[CJCalendarView alloc]initWithFrame:CGRectMake(15 , self.checkDateTextField.bottom+20 , self.view.width-75, 280)];
    c.delegate = self;
    
    [self.choseScrollView addSubview:c];
    self.choseScrollView.contentSize = CGSizeMake(self.view.width, c.bottom);
    //kailoon Set the placeholder color
    [self.checkSetTextField setValue:[UIColor orangeColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.checkCityTextField setValue:[UIColor orangeColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.checkCenterTextField setValue:[UIColor orangeColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.checkDateTextField setValue:[UIColor orangeColor] forKeyPath:@"_placeholderLabel.textColor"];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [self checkSetChose:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)selfCheck:(id)sender{

    self.selfCheckButton.selected = YES;
    self.companyCheckButton.selected = NO;
    [self checkSetChose:nil];
    [self reset];
}
-(IBAction)companyCheck:(id)sender{
    self.selfCheckButton.selected = NO;
    self.companyCheckButton.selected = YES;
    [self reset];
}

-(void)reset{
    _examSetBriefData = nil;
    _cityByExamsetData = nil;
    _examCenterByCityExamsetData = nil;
    self.checkCityTextField.text = @"";
    
    self.checkCenterTextField.text = @"";
    self.addrLabel.text = @"";
    self.checkSetTextField.text = @"";
}
-(IBAction)checkSetChose:(id)sender{
//    sessionid = ‘’
//    pagecount=’’ //每页的记录数量
//    page=’’ //索引从０开始
//    customerId :（2014.11.21 备注：为登录人（员工）的id，而不是体检人（如亲友）的id 否则无法获得商城购买的套餐信息
//    type : 0:单位套餐 1：商城个人套餐 2:单位+商城个人套餐  3：客户当前预约的（未安排的）套餐
//    inlineBookAllowed 是否允许在线预约 0：不允许 1：允许 可以不传入
//    employeeType  可以不传入
    NSString *customerId = [UserInfos customerId];
    if (self.customerData) {
        customerId = self.customerData.Id;
    }
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:
                         [UserInfos sessionId],@"sessionid",
                         @"100",@"pagecount",
                         @"0",@"page",
                         customerId,@"customerId",
                         self.companyCheckButton.selected?@"0":@"1",@"type",
                         nil];
    
    
    [NetRequest requestShopWithType:EXAM_SET_BRIEF  withGetUrl:ADDRESS_EXAM_SET_BRIEF  andGetInfo:dic result:^(id resultSuccess)
    {
        
        NSMutableArray *resultArr = resultSuccess;
        
        NSMutableArray *titleArr = [[NSMutableArray alloc]init];
        
        for (ExamSetBriefData *data in resultArr) {
            [titleArr addObject:data.name];
        }
        if (sender) {
            CJSelectViewViewController *s = [[[NSBundle mainBundle]loadNibNamed:@"CJSelectViewViewController" owner:nil options:nil] lastObject];
            s.titleLabel.text = @"套餐选择";
            s.prppreitys =resultArr;
            s.titles = titleArr;
            
            s.callBack = ^(id selectedValue){
                _examSetBriefData = selectedValue;
                _cityByExamsetData = nil;
                _examCenterByCityExamsetData = nil;
                self.checkCityTextField.text = @"";
                self.checkCenterTextField.text = @"";
                self.addrLabel.text = @"";
                self.checkSetTextField.text = _examSetBriefData.name;
            };
            
            s.frame = self.view.frame;
            [self.navigationController.view addSubview:s];
        }
        

    } err:^(id resultError) {
        if ([resultError objectForKey:@"Data"]) {
            [UIAlertView showMessage:[resultError objectForKey:@"Data"]];
        }
    }];

}
-(IBAction)checkCityChose:(id)sender{
    if (!_examSetBriefData) {
        [self checkSetChose:nil];
        return;
    }
    NSString *customerId = [UserInfos customerId];
    if (self.customerData) {
        customerId = self.customerData.Id;
    }
    NSDictionary *dic= [[NSDictionary alloc]initWithObjectsAndKeys:
                        _examSetBriefData.Id,@"id",
                        [UserInfos customerId],@"userid",
                        customerId,@"customerId",
                        nil];
    [NetRequest requestShopWithType:CITY_BY_EXAMSET  withGetUrl:ADDRESS_CITY_BY_EXAMSET  andGetInfo:dic result:^(id resultSuccess)
     {
         
         NSMutableArray *resultArr = resultSuccess;
         
         NSMutableArray *titleArr = [[NSMutableArray alloc]init];
         
         for (ExamSetBriefData *data in resultArr) {
             [titleArr addObject:data.name];
         }
         
         CJSelectViewViewController *s = [[[NSBundle mainBundle]loadNibNamed:@"CJSelectViewViewController" owner:nil options:nil] lastObject];
         s.titleLabel.text = @"体检城市";
         s.prppreitys =resultArr;
         s.titles = titleArr;
         
         s.callBack = ^(id selectedValue){
             _cityByExamsetData = selectedValue;
             _examCenterByCityExamsetData = nil;
             self.checkCenterTextField.text = @"";
             self.addrLabel.text = @"";
             self.checkCityTextField.text = _cityByExamsetData.name;
         };
         
         s.frame = self.view.frame;
         [self.navigationController.view addSubview:s];
         
     } err:^(id resultError) {
         
     }];
}
-(IBAction)checkCenterChose:(id)sender{
    if (!_cityByExamsetData) {
        [self checkCityChose:nil];
        return;
    }
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:
                         _examSetBriefData.Id,@"examsetid",
                         _cityByExamsetData.Id,@"cityid",
                         nil];
    [NetRequest requestShopWithType:EXAM_CENTER_BY_CITY_EXAMSET  withGetUrl:ADDRESS_EXAM_CENTER_BY_CITY_EXAMSET  andGetInfo:dic result:^(id resultSuccess)
     {
         
         NSMutableArray *resultArr = resultSuccess;
         
         NSMutableArray *titleArr = [[NSMutableArray alloc]init];
         
         for (ExamCenterByCityExamsetData *data in resultArr) {
             [titleArr addObject:data.name];
         }
         CJSelectViewViewController *s = [[[NSBundle mainBundle]loadNibNamed:@"CJSelectViewViewController" owner:nil options:nil] lastObject];
         s.titleLabel.text = @"体检城市";
         s.prppreitys =resultArr;
         s.titles = titleArr;
         
         s.callBack = ^(id selectedValue){
             _examCenterByCityExamsetData = selectedValue;
             self.checkCenterTextField.text = _examCenterByCityExamsetData.name;
             self.addrLabel.text = _examCenterByCityExamsetData.addr;
             
             [self requestMonth:[NSDate date]];
         };
         
         s.frame = self.view.frame;
         [self.navigationController.view addSubview:s];
         
     } err:^(id resultError) {
         
     }];
}

-(void)requestMonth:(NSDate *)date{
    
//    sessionid
//    exam_center_id //体检中心ID
//    exam_set_id//体检套餐ID
//    year
//    month
    NSLog(@"%@",date);
    NSInteger year = date.year;
    NSInteger month = date.month;
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:
                         [UserInfos sessionId],@"sessionid",
                         _examSetBriefData.Id,@"exam_set_id",
                         _examCenterByCityExamsetData.Id,@"exam_center_id",
                         [NSString stringWithFormat:@"%d",month],@"month",
                         [NSString stringWithFormat:@"%d",year],@"year",
                         nil];
    [NetRequest requestShopWithType:BOOK_INFO  withGetUrl:ADDRESS_BOOK_INFO  andGetInfo:dic result:^(id resultSuccess)
     {
         
         NSArray *resultArr = resultSuccess;
         for (BookInfoData *bookInfoData in resultArr)
         {
//             if ([bookInfoData.book_allowed intValue] == 1)
//             {
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"dateCoseStete" object:bookInfoData];
                 NSLog(@"%@",bookInfoData.day);
//             }
         }
     } err:^(id resultError) {
         
     }];
}
-(void)didSelectDate:(NSDate *)date{
    _selectedDate = date;
    self.checkDateTextField.text = [NSString stringWithFormat:@"%d年%d月%d日",date.year,date.month,date.day];
}
-(IBAction)checkDateChose:(id)sender{
    
}
-(IBAction)nextStep:(id)sender{
    if (!_examSetBriefData) {
        [UIAlertView showMessage:@"请选择体检套餐"];
        return;
    }
    if (!_cityByExamsetData) {
        [UIAlertView showMessage:@"请选择体检城市"];
        return;
    }
    if (!_examCenterByCityExamsetData) {
        [UIAlertView showMessage:@"请选择体检中心"];
        return;
    }
    if (!_selectedDate) {
        [UIAlertView showMessage:@"请选择体检日期"];
        return;
    }
    InSureYuYueViewController *inSure = [[InSureYuYueViewController alloc]initWithNibName:@"InSureYuYueViewController" bundle:nil];
    inSure.examSetBriefData = _examSetBriefData;
    inSure.cityByExamsetData = _cityByExamsetData;
    inSure.examCenterByCityExamsetData = _examCenterByCityExamsetData;
    inSure.selectedDate = _selectedDate;
    inSure.customerData = self.customerData;
    [self.navigationController pushViewController:inSure animated:YES];
}



-(IBAction)cheatDeatil:(id)sender{
    if (!_examSetBriefData) {
        return;
    }
    NSDictionary *dic= [[NSDictionary alloc]initWithObjectsAndKeys:_examSetBriefData.Id,@"id", nil];
    
    [NetRequest requestShopWithType:EXAM_SET_DETAIL withGetUrl:ADDRESS_EXAM_SET_DETAIL andGetInfo:dic result:^(id resultSuccess) {
        YuYueInfoViewController *infoView = [[YuYueInfoViewController alloc]initWithNibName:@"YuYueInfoViewController" bundle:nil];
        infoView.titleString = @"体检套餐详情";
        infoView.info = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"detail"] ;
        [self.navigationController pushViewController:infoView animated:YES];
    } err:^(id resultError) {
        
    }];

}

-(IBAction)cheatCenterDeatil:(id)sender{
    if (!_examCenterByCityExamsetData) {
        return;
    }
    NSDictionary *dic= [[NSDictionary alloc]initWithObjectsAndKeys:_examCenterByCityExamsetData.Id,@"id", nil];
    [NetRequest requestShopWithType:EXAM_PRECAUTION withGetUrl:ADDRESS_EXAM_PRECAUTION andGetInfo:dic result:^(id resultSuccess) {
        YuYueInfoViewController *infoView = [[YuYueInfoViewController alloc]initWithNibName:@"YuYueInfoViewController" bundle:nil];
        infoView.titleString = @"体检中心须知";
        infoView.info = [resultSuccess objectForKey:@"Data"];
        [self.navigationController pushViewController:infoView animated:YES];
    } err:^(id resultError) {
        
    }];

}
@end

//
//  ZhuanzhenDetailViewController.swift
//  亳州医保
//
//  Created by ljw on 15/2/13.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit

class ZhuanzhenDetailViewController: UIViewController {
    @IBOutlet weak var seg预约目的: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.edgesForExtendedLayout = UIRectEdge.None
        
        self.seg预约目的.removeAllSegments()
        self.seg预约目的.insertSegmentWithTitle("检查/诊断", atIndex: 0, animated: false)
        self.seg预约目的.insertSegmentWithTitle("治疗/手术", atIndex: 1, animated: false)
        self.seg预约目的.insertSegmentWithTitle("复诊", atIndex: 3, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

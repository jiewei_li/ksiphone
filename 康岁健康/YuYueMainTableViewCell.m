//
//  YuYueMainTableViewCell.m
//  康岁健康
//
//  Created by 低调 on 14/12/9.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "YuYueMainTableViewCell.h"
#import "ExamOrderData.h"
@implementation YuYueMainTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setData:(ExamOrderData *)data{
    _data = data;
    self.nameLable.text = data.customer_name;
    self.realityLable.text = [data.customer_id isEqualToString:[UserInfos customerId]]?@"(本人)":@"(亲友)";
    self.examsetNameLabel.text = data.exam_set_name;
    self.examCenterLable.text = data.exam_center_name;
    self.dateLable.text = data.date;
    self.priceLable.text = data.price;
    self.statusLable.text = data.status_id;
    self.leftTimeLable.text = data.leftTime;
    
    [self.contentView bringSubviewToFront:self.b1];
}


-(IBAction)examSetDetail:(id)sender{
    self.examsetBlock(_data);
}
-(IBAction)examCenterDetail:(id)sender{
    self.examcenterBlock(_data);
}
-(IBAction)examDataDetail:(id)sender{
    self.examdataBlock(_data);
}
-(IBAction)examPriceDetail:(id)sender{
    self.exampriceBlock(_data);
}
-(IBAction)payNow:(id)sender{
    self.paynowBlock(_data);
}
@end

//
//  AccountInfoViewController.swift
//  亳州医保
//
//  Created by ljw on 15/3/5.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit

class AccountInfoViewController: UIViewController {
    @IBOutlet var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.contentSize=CGSizeMake(CGFloat(320),CGFloat(850))
        scrollView.pagingEnabled=true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

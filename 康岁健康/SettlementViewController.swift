//
//  SettlementViewController.swift
//  亳州医保
//
//  Created by ljw on 15/3/4.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit

class SettlementViewController: UIViewController {
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn1: UIButton!
    var options : UIViewAnimationOptions = .TransitionNone

    var btnBackColor:UIColor?
    @IBAction func btn3Click(sender: AnyObject) {
        
    
        var btn = sender as UIButton
        btn.backgroundColor=btnBackColor
        btn1.backgroundColor=UIColor.clearColor()
        btn2.backgroundColor=UIColor.clearColor()
        
              var fromVC = self.childViewControllers[0] as UIViewController
        var toVC:UIViewController

         toVC = self.storyboard?.instantiateViewControllerWithIdentifier("L历史账单") as UIViewController
        self.arn_transition(fromVC, toVC: toVC, duration: 1.0, options: options)
        
    }
    @IBAction func btn2Click(sender: AnyObject) {
        var btn = sender as UIButton
        btn.backgroundColor=btnBackColor
        
        btn1.backgroundColor=UIColor.clearColor()
        btn3.backgroundColor=UIColor.clearColor()

        var fromVC = self.childViewControllers[0] as UIViewController
        var toVC:UIViewController
        
        toVC = self.storyboard?.instantiateViewControllerWithIdentifier("D当前账单") as UIViewController
        self.arn_transition(fromVC, toVC: toVC, duration: 1.0, options: options)
        

        
    }

    @IBAction func btn1Click(sender: AnyObject) {
     
        var btn = sender as UIButton
        btn.backgroundColor=btnBackColor
        btn3.backgroundColor=UIColor.clearColor()
        btn2.backgroundColor=UIColor.clearColor()

        var fromVC = self.childViewControllers[0] as UIViewController
        var toVC:UIViewController
        
        toVC = self.storyboard?.instantiateViewControllerWithIdentifier("Z帐户信息") as UIViewController
        self.arn_transition(fromVC, toVC: toVC, duration: 1.0, options: options)
        

        
    }
    @IBAction func segClick(sender: UISegmentedControl) {
      //  var p:UIViewController = self.childViewControllers.last as UIViewController
        
        var options : UIViewAnimationOptions = .TransitionFlipFromLeft
        var fromVC = self.childViewControllers[0] as UIViewController
        var toVC:UIViewController
        
        switch sender.selectedSegmentIndex {
            
   
            
            
        case 1:
             toVC = self.storyboard?.instantiateViewControllerWithIdentifier("D当前账单") as UIViewController
        case 2:
             toVC = self.storyboard?.instantiateViewControllerWithIdentifier("L历史账单") as UIViewController
        default:
             toVC = self.storyboard?.instantiateViewControllerWithIdentifier("Z帐户信息") as UIViewController
        }
        
        self.arn_transition(fromVC, toVC: toVC, duration: 1.0, options: options)
    }
    @IBOutlet weak var conView: UIView!
    @IBOutlet weak var seg: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = UIRectEdge.None
        
        btnBackColor=btn1.backgroundColor
        // Do any additional setup after loading the view.
       
//        seg.setTitle("帐户信息", forSegmentAtIndex: 0)
//        seg.setTitle("当前帐单", forSegmentAtIndex: 1)
//        seg.insertSegmentWithTitle("历史帐单", atIndex: 2, animated: false)//("历史帐单", forSegmentAtIndex: 2)
//        
        //放一个UIbarButtonItem
        var backButton: UIBarButtonItem = UIBarButtonItem(title: "返回", style: UIBarButtonItemStyle.Bordered, target: self, action: "btn返回Click")
        //newNavigationItem.backBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem=backButton
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
     func btn返回Click() {
        
        
        self.navigationController?.navigationBarHidden=true
        self.navigationController?.popViewControllerAnimated(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

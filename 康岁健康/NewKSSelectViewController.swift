//
//  NewKSSelectViewController.swift
//  亳州医保
//
//  Created by ljw on 15/2/28.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit
import SwiftyJSON
class NewKSSelectViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tab2: UITableView!
    @IBOutlet weak var tab1: UITableView!
    
    var lastks1:String!
    var lastks2:String!
    var ksjson:JSON!
    // var ksjson:NSDictionary!
    var  ks1:NSArray!
    var ks2:NSArray!

    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
             self.edgesForExtendedLayout = UIRectEdge.None
        
        if let file = NSBundle.mainBundle().pathForResource("ks", ofType: "json")
            
            
        {
            let data = NSData(contentsOfFile: file)!
            ksjson = JSON(data:data)
            // viewController.json = json
            
            
            // var   ksjson=json.object as NSDictionary;
            //  ksjson.allKeys
            ks1 =  ksjson.dictionaryValue.keys.array
            ks2  =  ksjson.dictionary?[ks1[0] as String]?.object as NSArray
            lastks1=ks1[0] as String
            if(ks2.count>0)
            {
            lastks2=ks2[0] as String
            
            }
        }
            
        else {
            
            NSLog("no json file")
        }
        tab1.frame=CGRectMake(0, 0, 160, 640)
     tab2.frame=CGRectMake(160, 0, 160, 640)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
         return 1
    }
    
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tab1{
             return  ks1.count

        }else{
            return ks2.count
        }
        
        //return doctors.count
        
    }
    
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell=tableView.dequeueReusableCellWithIdentifier("Cell",forIndexPath: indexPath) as UITableViewCell
        
        if tableView == tab1
        {
            
            var str:String! = ks1.objectAtIndex(indexPath.row) as? String
            cell.textLabel?.text=str
        
 var tmp =  ksjson.dictionary?[str]?.object as NSArray
            
            
             var lab =  cell.viewWithTag(100) as UILabel
            if(tmp.count>0)
            {
               
                lab.hidden=false
            }
            else
            {
                lab.hidden=true
            }
        }
        else
        {
            cell.textLabel?.text=ks2.objectAtIndex(indexPath.row) as? String
        }
//
//        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
//        
//        // This will be our color to load into the cell
//        var doctor : String
//        
//        
//        if tableView == self.searchDisplayController!.searchResultsTableView{
//            doctor = self.filteredColors[indexPath.row]as (String)
//        }
//        else
//        {
//            doctor = self.doctors[indexPath.row]as (String)
//        }
//        // doctor = self.doctors[indexPath.row]as (String)
//        
//        
//        //cell.textLabel.text = doctor
//        (cell.contentView.viewWithTag(100) as UIImageView).image=UIImage(named: doctor)
//        (cell.contentView.viewWithTag(110) as UILabel).text=doctor;
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    
        if(tableView==tab1)
        {
            //ksjson.dictionary?["内科"]
            var str=ks1[indexPath.row] as String
            //     var st =    ksjson.dictionaryValue.keys.array[row]
            // ks2 = ksjson.dictionary?[str]
    
            ks2  =  ksjson.dictionary?[str]?.object as NSArray
            
            lastks1=str
            if(ks2.count>0)
            {
            lastks2=ks2[0] as String
            }
            else
            {
            lastks2=""
                
                  self.performSegueWithIdentifier("yuyueDoctor", sender: nil)
            }
  
            tab2.reloadData()
            

        }
        else
        {
       
        self.performSegueWithIdentifier("yuyueDoctor", sender: nil)
        }
    }


}

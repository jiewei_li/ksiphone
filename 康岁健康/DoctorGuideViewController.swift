//
//  DoctorGuideViewController.swift
//  亳州医保
//
//  Created by ljw on 15/2/22.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit

class DoctorGuideViewController: UIViewController {
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        var btn=UIBarButtonItem(title: "返回", style:UIBarButtonItemStyle.Bordered, target: self, action: "btnBackClick")
        
        // var backButton: UIBarButtonItem = UIBarButtonItem(title: "返回", style: UIBarButtonItemStyle.Bordered, target: self, action: "btn返回Click")
        self.navigationItem.leftBarButtonItem=btn;

                      self.edgesForExtendedLayout = UIRectEdge.None
        var url = NSURL(string: "http://rongcity.sinaapp.com/expressdoctor/index.htm")
        var request : NSURLRequest = NSURLRequest(URL: url!)
        self.webView.loadRequest(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func btnBackClick()
    {
        
        self.navigationController?.navigationBarHidden=true
        // self.performSegueWithIdentifier("MyYuyue", sender: nil)
        self.navigationController?.popViewControllerAnimated(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

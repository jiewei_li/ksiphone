//
//  YuYueMainViewController.m
//  康岁健康
//
//  Created by 低调 on 14/12/9.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "YuYueMainViewController.h"
#import "YuYueMainTableViewCell.h"
#import "ExamOrderData.h"
#import "YuYueInfoViewController.h"
#import "YuYueMainTopView.h"
#import "YuYueViewController.h"
#import "PersinalInfoViewController.h"
#import "DataSigner.h"
#import "DataVerifier.h"
#import "AlixLibService.h"
#import "PartnerConfig.h"
#import "AlixPayResult.h"
#import "AlixPayOrder.h"
@interface YuYueMainViewController ()
{
    NSMutableArray *_payArry;
}
@end

@implementation YuYueMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"体检预约记录";
    [self addSubView:self.topView];
    [self addSubView:self.bottomView];
    [self addSubView:self.bottomPayView];
    self.bottomView.bottom = _mainView.height;
    self.bottomPayView.bottom = _mainView.height;
    self.bottomPayView.hidden = YES;
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, self.topView.bottom, self.view.width, self.bottomView.top-self.topView.bottom) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self addSubView:self.tableView];
    _payArry = [[NSMutableArray alloc]init];
    [_mainView bringSubviewToFront:self.tableView];

}
-(void)viewDidAppear:(BOOL)animated{
    if (self.b1.selected) {
        [self waiting2Deall:nil];
    }
    else if (self.b1.selected) {
        [self alreadyDo:nil];
    }
    else if (self.b1.selected) {
        [self finished:nil];
    }
    else{
        [self waiting2Deall:nil];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)waiting2Deall:(id)sender{
    self.b1.selected = YES;
    self.b2.selected = NO;
    self.b3.selected = NO;
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:
                         [UserInfos sessionId],@"sessionid",
                         @"1000",@"pagecount",
                         @"0",@"page",
                         [UserInfos customerId],@"customerid",
                         @"2",@"status",
                         @"3",@"stage_status"
                         , nil];
    [self requestListWithInfo:dic];
}
-(IBAction)alreadyDo:(id)sender{
    self.b1.selected = NO;
    self.b2.selected = YES;
    self.b3.selected = NO;
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:
                         [UserInfos sessionId],@"sessionid",
                         @"1000",@"pagecount",
                         @"0",@"page",
                         [UserInfos customerId],@"customerid",
                         @"4",@"status",
                         @"4",@"stage_status"
                         , nil];
    [self requestListWithInfo:dic];
}
-(IBAction)finished:(id)sender{
    self.b1.selected = NO;
    self.b2.selected = NO;
    self.b3.selected = YES;
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:
                         [UserInfos sessionId],@"sessionid",
                         @"1000",@"pagecount",
                         @"0",@"page",
                         [UserInfos customerId],@"customerid",
                         @"5",@"status",
                         @"5",@"stage_status"
                         , nil];
    [self requestListWithInfo:dic];
}

-(void)requestListWithInfo:(NSDictionary *)dic{
    [self cancelPay:nil];
    [NetRequest requestShopWithType:EXAM_ORDER_WITH_RELATED withGetUrl:ADDRESS_EXAM_ORDER_WITH_RELATED andGetInfo:dic result:^(id resultSuccess) {
        _data = resultSuccess;
        [self.tableView reloadData];
    } err:^(id resultError) {
        
    }];
}
-(IBAction)selfYuYue:(id)sender{
    YuYueViewController *yuyue = [[YuYueViewController alloc]initWithNibName:@"YuYueViewController" bundle:nil];
    [self.navigationController pushViewController:yuyue animated:YES];
}
-(IBAction)realityYuYue:(id)sender{
    PersinalInfoViewController *persional = [[PersinalInfoViewController alloc]initWithNibName:@"PersinalInfoViewController" bundle:nil];
    persional.type = REALTIVEYUYUE;
    [self.navigationController pushViewController:persional animated:YES];

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _data.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *deq = @"dewewqeg";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:deq];
    if (!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:deq];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 0.1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 20.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 245.;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
 
    YuYueMainTopView *cell = [[[NSBundle mainBundle]loadNibNamed:@"YuYueMainTopView" owner:self options:nil] lastObject];
    
    cell.data = [_data objectAtIndex:section];
    cell.examsetBlock = ^(id sender){
        ExamOrderData *data = sender;
        NSDictionary *dic= [[NSDictionary alloc]initWithObjectsAndKeys:data.exam_set_id,@"id", nil];
        
        [NetRequest requestShopWithType:EXAM_SET_DETAIL withGetUrl:ADDRESS_EXAM_SET_DETAIL andGetInfo:dic result:^(id resultSuccess) {
            YuYueInfoViewController *infoView = [[YuYueInfoViewController alloc]initWithNibName:@"YuYueInfoViewController" bundle:nil];
            infoView.titleString = @"体检套餐详情";
            infoView.info = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"detail"] ;
            [self.navigationController pushViewController:infoView animated:YES];
        } err:^(id resultError) {
            
        }];
    };
    cell.examcenterBlock = ^(id sender){
        ExamOrderData *data = sender;
        NSDictionary *dic= [[NSDictionary alloc]initWithObjectsAndKeys:data.exam_center_id,@"id", nil];
        [NetRequest requestShopWithType:EXAM_PRECAUTION withGetUrl:ADDRESS_EXAM_PRECAUTION andGetInfo:dic result:^(id resultSuccess) {
            YuYueInfoViewController *infoView = [[YuYueInfoViewController alloc]initWithNibName:@"YuYueInfoViewController" bundle:nil];
            infoView.titleString = @"体检中心须知";
            infoView.info = [resultSuccess objectForKey:@"Data"];
            [self.navigationController pushViewController:infoView animated:YES];
        } err:^(id resultError) {
            
        }];

    };
    cell.examdataBlock = ^(id sender){
        ExamOrderData *data = sender;
        NSDictionary *dic= [[NSDictionary alloc]initWithObjectsAndKeys:data.Id,@"examOrderId", nil];
        [NetRequest requestShopWithType:GET_NOTICE withGetUrl:ADDRESS_GET_NOTICE andGetInfo:dic result:^(id resultSuccess) {
            YuYueInfoViewController *infoView = [[YuYueInfoViewController alloc]initWithNibName:@"YuYueInfoViewController" bundle:nil];
            infoView.titleString = @"体检套餐详情";
            infoView.info = [resultSuccess objectForKey:@"Data"] ;
            [self.navigationController pushViewController:infoView animated:YES];
        } err:^(id resultError) {
            
        }];

            };
    cell.exampriceBlock = ^(id sender){
    };
    cell.paynowBlock = ^(id sender){
        ExamOrderData *data = sender;
        BOOL contains = NO;
        for (ExamOrderData *datat in _payArry) {
            if ([data isEqual:datat]) {
                contains = YES;
                [_payArry removeObject:datat];
            }
        }
        if (!contains) {
            [_payArry addObject:sender];
        }
        [self resetPay];
    };
    cell.cancelYuYue =^(id sender){
         ExamOrderData *data = sender;
//        string sessionid,int customerId, int orderId
        NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:data.Id,@"orderId",data.customer_id,@"customerId",[UserInfos sessionId],@"sessionid", nil];
        [NetRequest requestShopWithType:CANCEL_EXAMORDER withGetUrl:ADDRESS_CANCEL_EXAMORDER andGetInfo:dic result:^(id resultSuccess) {
            [_data removeObject:data];
            [self.tableView reloadData];
        } err:^(id resultError) {
            if ([resultError objectForKey:@"Data"]) {
                [UIAlertView showMessage:[resultError objectForKey:@"Data"]];
            }
        }];
    };
    return cell;
}

-(void)resetPay{
    if (_payArry.count>0) {
        self.bottomPayView.hidden = NO;
        self.bottomView.hidden = YES;
        float sum = 0;
        for (ExamOrderData *datat in _payArry) {
            sum+= [datat.price floatValue];
        }
        self.sumValue.text = [NSString stringWithFormat:@"%.2f元",sum];
    }
    else
    {
        self.bottomView.hidden = NO;
        self.bottomPayView.hidden = YES;
    }
}
-(IBAction)goPay:(id)sender{
    NSString *exam_order_ids = @"";
    for (ExamOrderData *data in _payArry) {
        if (exam_order_ids.length == 0) {
            exam_order_ids = [NSString stringWithFormat:@"%@",data.Id];
        }
        else
        {
            exam_order_ids = [NSString stringWithFormat:@",%@",data.Id];
        }
    }
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:[UserInfos sessionId],@"sessionid",exam_order_ids,@"exam_order_ids", nil];
    [NetRequest requestShopWithType:BEGIN_PAY_MOBILE withGetUrl:ADDRESS_BEGIN_PAY_MOBILE andGetInfo:dic result:^(id resultSuccess) {
        NSString *appScheme = @"kang";
        NSDictionary *data = [resultSuccess objectForKey:@"Data"];

        NSString *productName = [data objectForCJKey:@"subject"];
        NSString *productDescription = [data objectForCJKey:@"body"];
        AlixPayOrder *order = [[AlixPayOrder alloc] init];
        order.partner = PartnerID;
        order.seller = SellerID;
        order.tradeNO = [data objectForCJKey:@"order_no"]; //订单ID（由商家自行制定）
        
        order.productName = productName; //商品标题
        order.productDescription = productDescription; //商品描述
        order.amount = [data objectForCJKey:@"price"]; //商品价格
        order.notifyURL =  [data objectForCJKey:@"notify_url"]; //回调URL
        order.paymentType = @"1";
        order.inputCharset = @"utf-8";
        NSString* orderInfo = [order description];
        
        NSString* signedStr = [self doRsa:orderInfo];
        
        NSLog(@"%@",orderInfo);
        
        NSString *orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                                 orderInfo, signedStr, @"RSA"];
        
        [AlixLibService payOrder:orderString AndScheme:appScheme seletor:@selector(cancelPay:) target:self];

    } err:^(id resultError) {
        if ([resultError objectForKey:@"Data"]) {
            [UIAlertView showMessage:[resultError objectForKey:@"Data"]];
        }
    }];
}
-(NSString*)doRsa:(NSString*)orderInfo
{
    id<DataSigner> signer;
    signer = CreateRSADataSigner(PartnerPrivKey);
    NSString *signedString = [signer signString:orderInfo];
    return signedString;
}
-(IBAction)cancelPay:(id)sender{
    self.bottomPayView.hidden = YES;
    self.bottomView.hidden = NO;
    [_payArry removeAllObjects];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"YuYueMainViewControllerPayCancel" object:nil];
}


@end

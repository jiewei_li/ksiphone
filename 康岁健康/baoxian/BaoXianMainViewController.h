//
//  BaoXianMainViewController.h
//  康岁健康
//
//  Created by 低调 on 15/1/8.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"

@interface BaoXianMainViewController : BaseNextViewController<UITableViewDataSource,UITableViewDelegate>
@property IBOutlet UIView      *topView;
@property IBOutlet UIImageView *topImageView;

@property IBOutlet UIView      *page1;
@property IBOutlet UILabel      *nameLabel;
@property IBOutlet UILabel      *cardNumberLabel;


@property IBOutlet UIView      *page2;
@property IBOutlet UIView      *pageim;



@property IBOutlet UIView      *page3;



-(IBAction)pg1:(id)sender;
-(IBAction)pg2:(id)sender;
-(IBAction)pg3:(id)sender;
@end

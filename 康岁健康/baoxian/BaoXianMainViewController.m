//
//  BaoXianMainViewController.m
//  康岁健康
//
//  Created by 低调 on 15/1/8.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

#import "BaoXianMainViewController.h"
#import "CustomerData.h"
#import "ProductListData.h"
#import "ProductInfoViewController.h"
@interface BaoXianMainViewController ()
{
    UITableView *table;
    NSArray *dataSource;
}
@end

@implementation BaoXianMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"健康保险";
    [self addSubView:self.topView];
    [self addSubView:self.page1];
    [self addSubView:self.page2];
    [self addSubView:self.page3];
    
    self.page1.top = self.topView.bottom;
    self.page2.top = self.topView.bottom;
    self.page3.top = self.topView.bottom;
    
    self.page1.hidden = NO;
    self.page2.hidden = YES;
    self.page3.hidden = YES;
    
    [self setp1];
    [self setp2];
    [self setp3];
    
    
}

-(void)setp1{
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:[UserInfos sessionId],@"Sessionid",[UserInfos Id],@"Id", nil];
    [NetRequest requestShopWithType:GET_CUSTOMER withGetUrl:ADDRESS_GET_CUSTOMER andGetInfo:dic result:^(id resultSuccess) {
        CustomerData *customerData = resultSuccess;
        self.nameLabel.text = customerData.login_name;
        self.cardNumberLabel.text = customerData.ID_number;
    } err:^(id resultError) {
        
    }];
}
-(void)setp2{
    self.page2.height = _mainView.height-self.topView.bottom;
    UIScrollView *sc = [[UIScrollView alloc]initWithFrame:self.page2.bounds];
//    sc.backgroundColor = [];
    [self.page2 addSubview:sc];
    [sc addSubview:self.pageim];
    sc.contentSize = CGSizeMake(sc.width, self.pageim.height);
}

-(void)setp3{
    self.page3.height = _mainView.height-self.topView.bottom;
    
    NSDictionary *dic = user_defaults_get_object(@"ADDRESS_GETSERVERURLS");
    if (dic) {
//        NSString *s =
        [NetRequest requestShopWithType:OtherT withGetUrl:[[dic objectForKey:@"Data"] objectForKey:@"url_insurance_products"] andGetInfo:nil result:^(id resultSuccess) {
            dataSource = [resultSuccess objectForKey:@"Data"];
            table = [[UITableView alloc]initWithFrame:self.page3.bounds style:UITableViewStyleGrouped];
            table.delegate = self;
            table.dataSource = self;
            [self.page3 addSubview:table];
        } err:^(id resultError) {
            
        }];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)pg1:(id)sender{
    self.topImageView.image = [UIImage imageNamed:@"baoxiantop1"];
    self.page1.hidden = NO;
    self.page2.hidden = YES;
    self.page3.hidden = YES;
}
-(IBAction)pg2:(id)sender{
    self.topImageView.image = [UIImage imageNamed:@"baoxiantop2"];
    self.page1.hidden = YES;
    self.page2.hidden = NO;
    self.page3.hidden = YES;
}
-(IBAction)pg3:(id)sender{
    self.topImageView.image = [UIImage imageNamed:@"baoxiantop3"];
    self.page1.hidden = YES;
    self.page2.hidden = YES;
    self.page3.hidden = NO;
}


-(int)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataSource.count-1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *deq = @"baoxiantt";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:deq];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:deq];
    }
    cell.textLabel.text = [[dataSource objectAtIndex:indexPath.row+1] objectForKey:@"Name"];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell.textLabel sizeToFit];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, table.width, 44)];
    headView.backgroundColor = CJClearColor;
    UILabel *l = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300, 24)];
    l.backgroundColor = CJClearColor;
    if (dataSource && dataSource.count>0) {
        l.text = [[dataSource objectAtIndex:0] objectForKey:@"Name"];

    }
    [headView addSubview:l];
    return headView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [table deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dic = [dataSource objectAtIndex:indexPath.row+1];
//    ProductListData *data = [[ProductListData alloc]init];
//    
//    data.Id = [dic objectForKey:@"Id"];
//    data.Name = [dic objectForKey:@"Name"];
//    data.PictureUrl = [dic objectForKey:@""];
//    data.Price = [dic objectForKey:@""];
//    data.ProductTypeId = [dic objectForKey:@"Type"];
//    data.ShortDescription = [dic objectForKey:@"Description"];
//    data.catagoryId = [dic objectForKey:@""];
    
//    ProductInfoViewController *productInfoViewController = [[ProductInfoViewController alloc]initWithProductInfo:data];
//    [self.navigationController pushViewController:productInfoViewController animated:YES];
    
    
    NSDictionary *productIdDic = [NSDictionary dictionaryWithObjectsAndKeys:[dic objectForKey:@"Id"],@"productId", nil];
    
    [NetRequest requestShopWithType:GET_PRODUCT_INFO withGetUrl:ADDRESS_GET_PRODUCT_INFO andGetInfo:productIdDic result:^(id resultSuccess) {
        ProductListData *data = [[ProductListData alloc]init];
        data.Id = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"Id"];
        data.Name = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"Name"];
        data.PictureUrl = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"PictureUrl"];
        data.Price = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"Price"];
        data.ProductTypeId = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"ProductTypeId"];
        data.ShortDescription = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"ShortDescription"];
        ProductInfoViewController *productInfoViewController = [[ProductInfoViewController alloc]initWithProductInfo:data];
        [self.navigationController pushViewController:productInfoViewController animated:YES];
        
    }err:^(id resultError) {
    }];
}

@end

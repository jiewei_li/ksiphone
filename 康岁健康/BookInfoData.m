//
//  BookInfoData.m
//  康岁健康
//
//  Created by 低调 on 14/12/15.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BookInfoData.h"

@implementation BookInfoData
-(void)setData:(NSDictionary *)data{
    self.book_allowed = [data objectForCJKey:@"book_allowed"];
    self.day = [data objectForCJKey:@"day"];
    self.dayoff = [data objectForCJKey:@"dayoff"];
    self.left = [data objectForCJKey:@"left"];
    self.month = [data objectForCJKey:@"month"];
}
@end

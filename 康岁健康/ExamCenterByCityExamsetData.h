//
//  ExamCenterByCityExamsetData.h
//  康岁健康
//
//  Created by 低调 on 14/12/14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExamCenterByCityExamsetData : NSObject
@property NSString *Id;
@property NSString *addr;
@property NSString *cost;
@property NSString *name;
-(void)setData:(NSDictionary *)data;
@end

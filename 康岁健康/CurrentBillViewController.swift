//
//  CurrentBillViewController.swift
//  亳州医保
//
//  Created by ljw on 15/3/5.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit

class CurrentBillViewController: UIViewController {
 @IBOutlet var scrollView: UIScrollView!
    
    var PriceDetail = [
        ["类别":"西药费","总费用":"2312.00","自付费":"1800.00"]
         ,["类别":"中成药费","总费用":"210.50","自付费":"180.00"]
           ,["类别":"检验费","总费用":"568.00","自付费":"128.00"]
           ,["类别":"检查费","总费用":"233.00","自付费":"138.00"]
           ,["类别":"诊疗费","总费用":"90.00","自付费":"90.00"]
           ,["类别":"护理费","总费用":"89.00","自付费":"89.00"]
           ,["类别":"床位费","总费用":"700.00","自付费":"355.00"]
           ,["类别":"合计费","总费用":"4202.50","自付费":"2780.00"]
        
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return PriceDetail.count
        //return doctors.count
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell=tableView.dequeueReusableCellWithIdentifier("Cell",forIndexPath: indexPath) as PriceDetailTableViewCell
        var d =  PriceDetail[indexPath.row] as NSDictionary
      //  var strphoto=d["照片"] as String

        cell.lab类别.text=d["类别"] as String
        cell.lab总费用.text=d["总费用"] as String
        cell.lab自付费.text=d["自付费"] as String
        return cell

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    


}

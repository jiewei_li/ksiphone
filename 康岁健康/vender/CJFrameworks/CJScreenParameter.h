//
//  CJScreenParameter.h
//  views
//
//  Created by cerastes on 14-10-14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CJScreenParameter : NSObject
#define DEVICE_BOUNDS [[UIScreen mainScreen] bounds]
#define DEVICE_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define DEVICE_WIDTH ([[UIScreen mainScreen] bounds].size.width)

@end

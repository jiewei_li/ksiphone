//
//  CJFoundation.m
//  views
//
//  Created by cerastes on 14-10-14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "CJFoundation.h"

@implementation CJFoundation
NSDate *CJString2Date(NSString *dateString,NSString *dateFormate){
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:dateFormate];
    NSDate *date=[formatter dateFromString:dateString];
    return date;
}

NSString *CJDate2String(NSString *dateFormate,NSDate *date)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormate];
    NSString *date2string = [dateFormatter stringFromDate:date];
    return date2string;
}

NSString *CJstring(id sender){
    if ([sender isKindOfClass:[NSNull class]]) {
        return @"";
    }
    else
        return [NSString stringWithFormat:@"%@",sender];
}
+(NSString*)dataTOjsonString:(id)object
{
    NSString *jsonString = nil;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"," withString:@"&"];
    return jsonString;
}
@end

//
//  CJColor.h
//  views
//
//  Created by cerastes on 14-10-14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CJColor : NSObject
#define CJBlackColor     [UIColor blackColor]
#define CJDarkGrayColor  [UIColor darkGrayColor]
#define CJLightGrayColor [UIColor lightGrayColor]
#define CJWhiteColor     [UIColor whiteColor]
#define CJGrayColor      [UIColor grayColor]
#define CJRedcolor       [UIColor redColor]
#define CJGreenColor     [UIColor greenColor]
#define CJBlueColor      [UIColor blueColor]
#define CJCyanColor      [UIColor cyanColor]
#define CJYellowColor    [UIColor yellowColor]
#define CJMagentaColor   [UIColor magentaColor]
#define CJOrangeColor    [UIColor orangeColor]
#define CJBrownColor     [UIColor brownColor]
#define CJClearColor     [UIColor clearColor]
#define CJPurpleColor    [UIColor purpleColor]

#define CJColorWithRedGreenBlueAlpha(r,g,b,al)  [UIColor colorWithHue:r saturation:g brightness:b alpha:al]

@end

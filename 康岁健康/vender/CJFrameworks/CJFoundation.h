//
//  CJFoundation.h
//  views
//
//  Created by cerastes on 14-10-14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CJFoundation : NSObject
#define  CJ_STRING_FORMAT(...) [NSString stringWithFormat: __VA_ARGS__]

NSDate   *CJString2Date(NSString *dateString,NSString *dateFormate);
NSString *CJDate2String(NSString *dateFormate,NSDate *date);
NSString *CJstring(id sender);
+(NSString*)dataTOjsonString:(id)object;
@end

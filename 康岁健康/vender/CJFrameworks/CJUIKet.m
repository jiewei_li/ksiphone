//
//  ShortCutView.m
//  views
//
//  Created by cerastes on 14-10-14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "CJUIKet.h"

@implementation CJUIKet

UILabel *CJLabel(NSString *title){
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:12];
    return label;
}

UIButton    *CJButtonV(void){
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = [UIColor clearColor];
    return btn;
}

UIButton    *CJButtonF(CGRect frame){
    UIButton *btn = CJButtonV();
    btn.frame = frame;
    return btn;
}

void    CJButtonFB(UIButton *btn , CGRect frame){
    btn.frame = frame;
}

UIButton    *CJButtonT(NSString *title,NSString *highlightedTitle){
    UIButton *btn = CJButtonV();
    [btn setTitle:title forState:UIControlStateNormal];
    if (highlightedTitle) {
        [btn setTitle:highlightedTitle forState:UIControlStateHighlighted];
    }
    return btn;
}

void    CJButtonTB(UIButton *btn,NSString *title,NSString *highlightedImageTitle){
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:highlightedImageTitle forState:UIControlStateHighlighted];
}

UIButton    *CJButtonI(UIImage  *normalImage,UIImage *highlightedImage){
    UIButton *btn = CJButtonV();
    [btn setBackgroundImage:normalImage forState:UIControlStateNormal];
    if (highlightedImage) {
        [btn setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    }
    return btn;
}

void    CJButtonIB(UIButton *btn,UIImage  *normalImage,UIImage *selectedImage){
    [btn setBackgroundImage:normalImage forState:UIControlStateNormal];
    if (selectedImage) {
        [btn setBackgroundImage:selectedImage forState:UIControlStateSelected];
    }
}

UIButton    *CJButtonSI(UIImage *selectedImage){
    UIButton *btn = CJButtonV();
    [btn setBackgroundImage:selectedImage forState:UIControlStateSelected];
    return btn;
}

void        CJButtonSIB(UIButton *btn,UIImage *selectedImage){
    [btn setBackgroundImage:selectedImage forState:UIControlStateSelected];
}

void        CJButtonSCB(UIButton *btn,UIColor *selectedcolor){
    [btn setTitleColor:selectedcolor forState:UIControlStateSelected];
}
UIButton    *CJButtonC(UIColor  *normalTitleColor,UIColor *highlightedTitleColor){
    UIButton *btn = CJButtonV();
    [btn setTitleColor:normalTitleColor forState:UIControlStateNormal];
    if (highlightedTitleColor) {
        [btn setTitleColor:highlightedTitleColor forState:UIControlStateHighlighted];
    }
    return btn;
}

void    CJButtonCB(UIButton *btn,UIColor  *normalTitleColor,UIColor *highlightedTitleColor){
    [btn setTitleColor:normalTitleColor forState:UIControlStateNormal];
    if (highlightedTitleColor) {
        [btn setTitleColor:highlightedTitleColor forState:UIControlStateHighlighted];
    }
}

void    CJButtonCSB(UIButton *btn,UIColor  *selectedColor){
    [btn setTitleColor:selectedColor forState:UIControlStateSelected];
    
}

UIButton    *CJButtonN(CGRect frame,NSString *title,UIImage *bacImage){
    UIButton *btn = CJButtonV();
    [btn setTitle:title forState:UIControlStateNormal];
    btn.frame = frame;
    [btn setBackgroundImage:bacImage forState:UIControlStateNormal];
    return btn;
}

void        CJButtonBC(UIButton *btn,UIColor *color){
    [btn setBackgroundColor:color];
}

UIButton    *CJButtonS(id target,SEL select){
    UIButton *btn = CJButtonV();
    [btn addTarget:target action:select forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

void        CJButtonSB(UIButton *btn,id target,SEL select){
    [btn addTarget:target action:select forControlEvents:UIControlEventTouchUpInside];
}

UIImage     *CJImage(NSString *name){
    return [UIImage imageNamed:name];
}

UIImageView *CJImageView(UIImage *imge){
    UIImageView *imagev = [[UIImageView alloc]initWithImage:imge];
    imagev.backgroundColor = [UIColor clearColor];
    return imagev;
}

@end

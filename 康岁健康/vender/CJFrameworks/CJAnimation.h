//
// Created by wangwei on 6/26/14.
// Copyright (c) 2014 com.qianxs. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 * 动画
 */
@interface CJAnimation : NSObject
+ (CABasicAnimation *)showAnimation:(NSInteger)animationDuration;
+ (CABasicAnimation *)hideAnimation:(NSInteger)animationDuration;
+ (CABasicAnimation *)rotationAnimation;
+ (CABasicAnimation *)shakeAnimation;
+ (CAKeyframeAnimation *)getKeyframeAnimation;
+ (CAKeyframeAnimation *)getMetroViewAnimation;
@end
//
//  ShortCutView.h
//  views
//
//  Created by cerastes on 14-10-14.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CJUIKet : UIView



UILabel     *CJLabel(NSString *title);

UIButton    *CJButtonV(void);

UIButton    *CJButtonF(CGRect frame);
void        CJButtonFB(UIButton *btn , CGRect frame);

UIButton    *CJButtonT(NSString *title,NSString *highlightedTitle);
void         CJButtonTB(UIButton *btn,NSString *title,NSString *highlightedTitle);

UIButton    *CJButtonI(UIImage  *normalImage,UIImage *selectedImage);
void        CJButtonIB(UIButton *btn,UIImage  *normalImage,UIImage *highlightedImage);

UIButton    *CJButtonSI(UIImage *selectedImage);
void        CJButtonSIB(UIButton *btn,UIImage *selectedImage);
void        CJButtonSCB(UIButton *btn,UIColor *selectedcolor);

UIButton    *CJButtonC(UIColor  *normalTitleColor,UIColor *highlightedTitleColor);
void        CJButtonCB(UIButton *btn,UIColor  *normalTitleColor,UIColor *highlightedTitleColor);
void    CJButtonCSB(UIButton *btn,UIColor  *selectedColor);

UIButton    *CJButtonN(CGRect frame,NSString *title,UIImage *bacImage);

UIButton    *CJButtonS(id target,SEL select);
void        CJButtonSB(UIButton *btn,id target,SEL select);

void        CJButtonBC(UIButton *btn,UIColor *color);


UIImage     *CJImage(NSString *name);

UIImageView *CJImageView(UIImage *imge);


@end

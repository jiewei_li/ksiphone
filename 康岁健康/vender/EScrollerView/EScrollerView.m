//
//  EScrollerView.m
//  icoiniPad
//
//  Created by Ethan on 12-11-24.
//
//

#import "EScrollerView.h"
#import "UIImageView+WebCache.h"
#import "ProductSlideImageInfoData.h"
@implementation EScrollerView

- (id)initWithFrameRect:(CGRect)rect ImageArray:(NSArray *)imgArr Block:(void (^)(id))block {

    if ((self = [super initWithFrame:rect])) {
        self.userInteractionEnabled = YES;
        if (imgArr == nil || [imgArr count] == 0) {
            return self;
        }
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:imgArr];
        [tempArray insertObject:[imgArr objectAtIndex:([imgArr count] - 1)] atIndex:0];
        [tempArray addObject:[imgArr objectAtIndex:0]];
        imageArray = [NSArray arrayWithArray:tempArray];
        viewSize = rect;
        NSUInteger pageCount = [imageArray count];
        if(!scrollView){
            scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, viewSize.size.width, viewSize.size.height)];
        }
        scrollView.pagingEnabled = imageArray.count > 3;
        scrollView.contentSize = CGSizeMake(viewSize.size.width * pageCount, viewSize.size.height);
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.scrollsToTop = NO;
        scrollView.delegate = self;
        for (int i = 0; i < pageCount; i++) {
            NSString *imgURL = [imageArray objectAtIndex:i];
            UIImageView *imgView = [[UIImageView alloc] init];
            if ([imgURL hasPrefix:@"http://"] || [imgURL hasPrefix:@"https://"]) {
                //网络图片 请使用ego异步图片库
                [imgView sd_setImageWithURL:[NSURL URLWithString:imgURL]];
            }
            else {
                UIImage *img = [UIImage imageNamed:[imageArray objectAtIndex:i]];
                [imgView setImage:img];
            }

            [imgView setFrame:CGRectMake(viewSize.size.width * i, 0, viewSize.size.width, viewSize.size.height)];
            imgView.tag = i;
            imgView.userInteractionEnabled = YES;

            [scrollView addSubview:imgView];
        }
        [scrollView setContentOffset:CGPointMake(viewSize.size.width, 0)];
        [self addSubview:scrollView];
        //说明文字层
        // UIView *noteView=[[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height-33,self.bounds.size.width,33)];
        //        [noteView setBackgroundColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.5]];
        float pageControlWidth = (pageCount - 2) * 10.0f + 40.f;
        float pagecontrolHeight = 20.0f;
        pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake((self.frame.size.width - pageControlWidth) / 2, self.frame.size.height - pagecontrolHeight, pageControlWidth, pagecontrolHeight)];
        pageControl.currentPage = 0;
        pageControl.numberOfPages = (pageCount - 2);
        if (imageArray.count > 3) {
            [self addSubview:pageControl];
        }
        //        noteTitle=[[UILabel alloc] initWithFrame:CGRectMake(5, 6, self.frame.size.width-pageControlWidth-15, 20)];
        //        [noteTitle setText:[titleArray objectAtIndex:0]];
        //        [noteTitle setBackgroundColor:[UIColor clearColor]];
        //        [noteTitle setFont:[UIFont systemFontOfSize:13]];
        //        [noteView addSubview:noteTitle];
        //
        //        [self addSubview:noteView];
        //        [noteView release];

        autoIndex = 1;
        if (imageArray.count > 3) {
            [self performSelector:@selector(next) withObject:nil afterDelay:1];
        }
        else {
            scrollView.scrollEnabled = NO;
        }
    }
    return self;
}

-(void)setImageArray:(NSArray *)imgArr Block:(void (^)(id))block{
    if (imgArr == nil || [imgArr count] == 0) {
        return ;
    }
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:imgArr];
    [tempArray insertObject:[imgArr objectAtIndex:([imgArr count] - 1)] atIndex:0];
    [tempArray addObject:[imgArr objectAtIndex:0]];
    imageArray = [NSArray arrayWithArray:tempArray];
    viewSize = self.bounds;
    NSUInteger pageCount = [imageArray count];
    if(!scrollView){
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, viewSize.size.width, viewSize.size.height)];
    }
    scrollView.pagingEnabled = imageArray.count > 3;
    scrollView.contentSize = CGSizeMake(viewSize.size.width * pageCount, viewSize.size.height);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = NO;
    scrollView.delegate = self;
    for (int i = 0; i < pageCount; i++) {
        ProductSlideImageInfoData *imgURLData = [imageArray objectAtIndex:i];
        NSString *imgURL = imgURLData.pictureurl;
        UIImageView *imgView = [[UIImageView alloc] init];
        if ( ![imgURL isNull] && ([imgURL hasPrefix:@"http://"] || [imgURL hasPrefix:@"https://"])) {
            //网络图片 请使用ego异步图片库
//            imgURL = [imgURL stringByReplacingOccurrencesOfString:@"http://" withString:@""];
            [imgView sd_setImageWithURL:[NSURL URLWithString:imgURL]placeholderImage:[UIImage imageNamed:@"backImage_pressed"]];
        }
        else if(![imgURL isNull]){
            UIImage *img = [UIImage imageNamed:imgURL];
            [imgView setImage:img];
        }
        else{
            imgView.image = [UIImage imageNamed:@"backImage_pressed"];
        }
        
        [imgView setFrame:CGRectMake(viewSize.size.width * i, 0, viewSize.size.width, viewSize.size.height)];
        imgView.tag = i;
        imgView.userInteractionEnabled = YES;
        [imgView bk_whenTapped:^{
            block(imgURLData);
        }];
        [scrollView addSubview:imgView];
    }
    [scrollView setContentOffset:CGPointMake(viewSize.size.width, 0)];
    [self addSubview:scrollView];
    //说明文字层
    // UIView *noteView=[[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height-33,self.bounds.size.width,33)];
    //        [noteView setBackgroundColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.5]];
    float pageControlWidth = (pageCount - 2) * 10.0f + 40.f;
    float pagecontrolHeight = 20.0f;
    pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake((self.frame.size.width - pageControlWidth) / 2, self.frame.size.height - pagecontrolHeight, pageControlWidth, pagecontrolHeight)];
    pageControl.currentPage = 0;
    pageControl.numberOfPages = (pageCount - 2);
    if (imageArray.count > 3) {
        [self addSubview:pageControl];
    }
    //        noteTitle=[[UILabel alloc] initWithFrame:CGRectMake(5, 6, self.frame.size.width-pageControlWidth-15, 20)];
    //        [noteTitle setText:[titleArray objectAtIndex:0]];
    //        [noteTitle setBackgroundColor:[UIColor clearColor]];
    //        [noteTitle setFont:[UIFont systemFontOfSize:13]];
    //        [noteView addSubview:noteTitle];
    //
    //        [self addSubview:noteView];
    //        [noteView release];
    
    autoIndex = 1;
    if (imageArray.count > 3) {
        [self performSelector:@selector(next) withObject:nil afterDelay:1];
    }
    else {
        scrollView.scrollEnabled = NO;
    }

}
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    currentPageIndex = page;
    autoIndex = currentPageIndex;
    pageControl.currentPage = (page - 1);
//    int titleIndex = page - 1;
    //    if (titleIndex==[titleArray count]) {
    //        titleIndex=0;
    //    }
    //    if (titleIndex<0) {
    //        titleIndex=[titleArray count]-1;
    //    }
    //    [noteTitle setText:[titleArray objectAtIndex:titleIndex]];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)_scrollView {
    if (currentPageIndex == 0) {

        [_scrollView setContentOffset:CGPointMake(([imageArray count] - 2) * viewSize.size.width, 0)];
    }
    if (currentPageIndex == ([imageArray count] - 1)) {

        [_scrollView setContentOffset:CGPointMake(viewSize.size.width, 0)];

    }

}
//- (void)imagePressed:(UITapGestureRecognizer *)sender
//{
//
//    if ([delegate respondsToSelector:@selector(EScrollerViewDidClicked:)]) {
//        [delegate EScrollerViewDidClicked:sender.view.tag];
//    }
//}

- (void)next {
    //    CGFloat contentOffsetX = scrollView.contentOffset.x;
    //    if (contentOffsetX+320 >= scrollView.contentSize.width) {
    //        [scrollView setContentOffset:CGPointMake(320, 0) animated:YES];
    //    }
    //    else
    //    {
    //        [scrollView setContentOffset:CGPointMake(contentOffsetX+320, 0) animated:YES];
    //    }

    //    CGFloat pageWidth = scrollView.frame.size.width;
    //    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    //    currentPageIndex=page;
    //    NSLog(@"currentPageIndex = %d",page);
    [scrollView setContentOffset:CGPointMake(viewSize.size.width * autoIndex, 0) animated:YES];
    if (autoIndex == 0) {
        [scrollView setContentOffset:CGPointMake(([imageArray count] - 2) * viewSize.size.width, 0)];
    }
    if (autoIndex == ([imageArray count] - 1)) {
        [scrollView setContentOffset:CGPointMake(0, 0)];
    }
    autoIndex++;
    [self performSelector:@selector(next) withObject:nil afterDelay:1];
}

@end

//
//  EScrollerView.h
//  icoiniPad
//
//  Created by Ethan on 12-11-24.
//
//

#import <UIKit/UIKit.h>


@interface EScrollerView : UIView<UIScrollViewDelegate> {
	CGRect viewSize;
	UIScrollView *scrollView;
	NSArray *sourceArray;
	NSArray *imageArray;
    UIPageControl *pageControl;
    int currentPageIndex;
    UILabel *noteTitle;
    
    int    autoIndex;
}
-(void)setImageArray:(NSArray *)imgArr Block:(void (^)(id))block;
-(id)initWithFrameRect:(CGRect)rect ImageArray:(NSArray *)imgArr  Block:(void (^)(id))block;
@end

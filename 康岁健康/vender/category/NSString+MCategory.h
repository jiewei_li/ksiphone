//
//  NSString+MCategory.h
//  康岁健康
//
//  Created by cerastes on 14-9-24.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MCategory)
-(BOOL)isNull;
-(NSString *)moneyString;
- (BOOL)isEmpty;
- (NSString *)trim ;
-(NSString *)notEmpty;
-(bool) containsString: (NSString *) string;
/** 邮箱验证 */
- (BOOL)isEmail;

/** 手机号码验证 */
- (BOOL)isPhoneNum;

@end

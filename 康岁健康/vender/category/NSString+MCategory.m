//
//  NSString+MCategory.m
//  康岁健康
//
//  Created by cerastes on 14-9-24.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "NSString+MCategory.h"

@implementation NSString (MCategory)
-(BOOL)isNull{
    if ([self isMemberOfClass:[NSNull class]] ) {
        return YES;
    }
    return NO;
}

-(NSString *)moneyString{
    return [NSString stringWithFormat:@"¥%@",self];
}

- (BOOL)isEmpty {
    return [[self class] isKindOfClass:[NSNull class]] || self == nil || [[self trim] isEqualToString:@""];
}
- (NSString *)trim {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

-(NSString *)notEmpty{
    return [self isEmpty]?@" ":self;
}
-(bool) containsString: (NSString *) string
{
    NSRange result;
    result = [self rangeOfString: string options: NSCaseInsensitiveSearch];
    return (result.location != NSNotFound);
}

/* 邮箱验证 MODIFIED BY HELENSONG */
- (BOOL)isEmail
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

/* 手机号码验证 MODIFIED BY HELENSONG */
- (BOOL)isPhoneNum
{
    //手机号以13， 15，18开头，八个 \d 数字字符
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(18[0,0-9]))\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:self];
}

@end

//
//  NSObject+MCategory.m
//  康岁健康
//
//  Created by cerastes on 14-9-27.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "NSObject+MCategory.h"

@implementation NSObject (MCategory)
-(NSString *)notNull{
    if ([self isKindOfClass:[NSNull class]]) {
        return @"";
    }
    else
    {
        return [NSString stringWithFormat:@"%@",self];
    }
}
@end

//
//  NSDictionary+MCategory.m
//  康岁健康
//
//  Created by 低调 on 14-10-7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "NSDictionary+MCategory.h"

@implementation NSDictionary (MCategory)
- (id)objectForCJKey:(id)aKey{
    return [[self objectForKey:aKey] notNull];
}
@end

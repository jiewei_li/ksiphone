//
// Created by wangwei on 7/4/14.
// Copyright (c) 2014 com.qianxs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@class MBProgressHUD;


/*
 * Completion handler invoked when user taps a button.
 *
 * @param alertView The alert view being shown.
 * @param buttonIndex The index of the button tapped.
 */
typedef void(^UIAlertViewHandler)(UIAlertView *alertView, NSInteger buttonIndex);


@interface UIAlertView (MCategory)

//-(id)initWithTitl1e:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...;

/**
* Shows the receiver alert with the given handler.
*
* @param handler The handler that will be invoked in user interaction.
*/
- (void)showWithHandler:(UIAlertViewHandler)handler;


/**
* 创建只有文本到对话框
*/
+ (void)showMessage:(NSString *)content;

/**
* 创建只有文本到对话框，确认action
*/
+ (void)showMessage:(NSString *)content handler:(UIAlertViewHandler)handler;

/**
* 创建只有文本到对话框，确认action
*/
+ (void)showMessage:(NSString *)title Content:(NSString *)content handler:(UIAlertViewHandler)handler;

+ (void)showMessage:(NSString *)title Content:(NSString *)content ConfrimButton:(NSString *)text1 handler:(UIAlertViewHandler)handler ;

+ (void)showConfirmMessage:(NSString *)content handler:(UIAlertViewHandler)handler;


+ (void)showConfirmMessage:(NSString *)title Content:(NSString *)content handler:(UIAlertViewHandler)handler;

+ (void)showConfirmMessage:(NSString *)title Content:(NSString *)content ConfrimButton:(NSString *)text1 CancelButton:(NSString *)text2 handler:(UIAlertViewHandler)handler;

/**
* 显示单行输入框
*/
+ (void)showSingleInput:(NSString *)content Value:(NSString *)defaultValue handler:(UIAlertViewHandler)handler;


/**
* 创建有加载loading对话框
*/
+ (void)showProgress:(UIView *)parentView Content:(NSString *)content ProgressShowHandler:(void (^)())showHandler ProgressDismissHandler:(void (^)())dismissHandler;

/**
* 创建有进度条的对话框
*/
+ (void)showProgress:(UIView *)parentView Title:(NSString *)title Content:(NSString *)content ProgressShowHandler:(void (^)())showHandler ProgressDismissHandler:(void (^)())dismissHandler;

+ (void)showProgress:(UIView *)parentView Title:(NSString *)title Content:(NSString *)content CompletionTips:(Boolean)showCompeletion ProgressShowHandler:(void (^)())showHandler ProgressDismissHandler:(void (^)())dismissHandler;

+ (MBProgressHUD*)showProgress:(UIView *)parentView  Content:(NSString *)content;


@end
//
// Created by wangwei on 8/18/14.
// Copyright (c) 2014 com.qianxs. All rights reserved.
//

#import "NSObject+NewCategory.h"


@implementation NSObject (NewCategory)
static NSObject *sharedSingleton_ = nil;

+ (instancetype)instance {
    if (sharedSingleton_ == nil) {
        sharedSingleton_ = [[self alloc] init];
    }

    return sharedSingleton_;
}

@end
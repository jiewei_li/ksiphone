//
// Created by wangwei on 7/4/14.
// Copyright (c) 2014 com.qianxs. All rights reserved.
//

#import <objc/runtime.h>
#import "UIAlertView+MCategory.h"
#import "UIView+MCategory.h"
#import "MBProgressHUD.h"

/*
* Runtime association key.
*/
static void const *kHandlerAssociatedKey = @"CW_kHandlerAssociatedKey";

static NSString *kAlertViewTitle = @"";

@implementation UIAlertView (MCategory)

//-(id)initWithTitl1e:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...{
//    return nil;
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



#pragma mark - Showing

/*
 * Shows the receiver alert with the given handler.
 */
- (void)showWithHandler:(UIAlertViewHandler)handler {
    objc_setAssociatedObject(self, kHandlerAssociatedKey, handler, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self setDelegate:self];
    [self show];
}

#pragma mark - UIAlertViewDelegate

/*
 * Sent to the delegate when the user clicks a button on an alert view.
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    UIAlertViewHandler completionHandler = objc_getAssociatedObject(self, kHandlerAssociatedKey);
    if (completionHandler != nil) {
        completionHandler(alertView, buttonIndex);
    }
}

#pragma mark - Utility methods

+ (void)showMessage:(NSString *)content {
    [self showMessage:kAlertViewTitle Content:content handler:nil];
}

+ (void)showMessage:(NSString *)content handler:(UIAlertViewHandler)handler {
    [self showMessage:kAlertViewTitle Content:content handler:handler];
}

+ (void)showMessage:(NSString *)title Content:(NSString *)content handler:(UIAlertViewHandler)handler {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:content
                                                   delegate:nil
                                          cancelButtonTitle:@"确认"
                                          otherButtonTitles:nil];
    [alert showWithHandler:handler];
}


+ (void)showMessage:(NSString *)title Content:(NSString *)content ConfrimButton:(NSString *)text1 handler:(UIAlertViewHandler)handler {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:content
                                                   delegate:nil
                                          cancelButtonTitle:text1
                                          otherButtonTitles:nil];
    [alert showWithHandler:handler];
}

+ (void)showConfirmMessage:(NSString *)content handler:(UIAlertViewHandler)handler {
    [self showConfirmMessage:kAlertViewTitle Content:content handler:handler];
}

+ (void)showConfirmMessage:(NSString *)title Content:(NSString *)content handler:(UIAlertViewHandler)handler {
    [self showConfirmMessage:kAlertViewTitle Content:content ConfrimButton:@"确认" CancelButton:@"取消" handler:handler];
}


+ (void)showConfirmMessage:(NSString *)title Content:(NSString *)content ConfrimButton:(NSString *)text1 CancelButton:(NSString *)text2 handler:(UIAlertViewHandler)handler {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:content
                                                   delegate:nil
                                          cancelButtonTitle:text2
                                          otherButtonTitles:text1, nil];
    [alert showWithHandler:handler];
}


+ (void)showSingleInput:(NSString *)content Value:(NSString *)defaultValue handler:(UIAlertViewHandler)handler {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:content
                                                   delegate:nil
                                          cancelButtonTitle:@"取消"
                                          otherButtonTitles:@"确认", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *myTextField = [alert textFieldAtIndex:0];
    myTextField.text = defaultValue;
    myTextField.keyboardType = UIKeyboardTypeAlphabet;
    [alert showWithHandler:handler];
}

+ (void)showProgress:(UIView *)parentView Content:(NSString *)content ProgressShowHandler:(void (^)())showHandler ProgressDismissHandler:(void (^)())dismissHandler {
    [self showProgress:parentView Title:@"" Content:content ProgressShowHandler:showHandler ProgressDismissHandler:dismissHandler];
}


+ (void)showProgress:(UIView *)parentView Title:(NSString *)title Content:(NSString *)content ProgressShowHandler:(void (^)())showHandler ProgressDismissHandler:(void (^)())dismissHandler {
    [self showProgress:parentView Title:title Content:content CompletionTips:NO ProgressShowHandler:showHandler ProgressDismissHandler:dismissHandler];
}

+ (void)showProgress:(UIView *)parentView Title:(NSString *)title Content:(NSString *)content CompletionTips:(Boolean)showCompeletion ProgressShowHandler:(void (^)())showHandler ProgressDismissHandler:(void (^)())dismissHandler {
    MBProgressHUD *progressHUD = [[MBProgressHUD alloc] initWithView:parentView];
    progressHUD.labelText = title;
    progressHUD.detailsLabelText = content;
    if (content != nil && ![content isEmpty]) {
        progressHUD.opacity = 0.5f;
    }
    [progressHUD showAnimated:YES whileExecutingBlock:^{
        //handler message
        showHandler();

        if (showCompeletion) {
            // UIImageView is a UIKit class, we have to initialize it on the main thread
            __block UIImageView *imageView;
            dispatch_sync(dispatch_get_main_queue(), ^{
                UIImage *image = [UIImage imageNamed:@"37x-Checkmark.png"];
                imageView = [[UIImageView alloc] initWithImage:image];
            });
            progressHUD.customView = imageView;
            progressHUD.mode = MBProgressHUDModeCustomView;
            progressHUD.labelText = @"  操作成功  ";
            progressHUD.detailsLabelText = @"";
        }
        sleep(1);
    }         completionBlock:dismissHandler];
    [parentView addSubview:progressHUD];
}

+ (MBProgressHUD *)showProgress:(UIView *)parentView Content:(NSString *)content {
    MBProgressHUD *progressHUD = [[MBProgressHUD alloc] initWithView:parentView];
    progressHUD.labelText = @"";
    progressHUD.detailsLabelText = content;
    progressHUD.yOffset = -parentView.height / 3;
    if (content != nil && ![content isEmpty]) {
        progressHUD.opacity = 0.5f;
    }
    [progressHUD show:YES];
    [progressHUD setRemoveFromSuperViewOnHide:YES];
    [parentView addSubview:progressHUD];
    return progressHUD;
}


+ (void)showCompleteProgress:(UIView *)parentView {
    MBProgressHUD *progressHUD = [[MBProgressHUD alloc] initWithView:parentView];
    progressHUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    progressHUD.mode = MBProgressHUDModeCustomView;
    progressHUD.labelText = @"操作成功！";
    [progressHUD show:YES];
    [progressHUD hide:YES afterDelay:2];
    [parentView addSubview:progressHUD];

}


@end
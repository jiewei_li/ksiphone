//
//  MyCalendar.h
//  MYCalendar
//
//  Created by cerastes on 14-10-15.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
#import "CJCalendarCellData.h"

@protocol CJCalendarViewDelegate <NSObject>

-(void)requestMonth:(NSDate *)date;
-(void)didSelectDate:(NSDate *)date;
@end
#import <UIKit/UIKit.h>

@interface CJCalendarView : UIView<UICollectionViewDataSource,UICollectionViewDelegate>
{
    UICollectionView *_collectionView;
    NSArray          *_monthDays;
    UILabel          *dateLable;
    NSDate           *_currentMonthDate;
    //kailoon 2015-1-15 增加选择日期及对应cell记录，用于高亮显示
    UICollectionViewCell * lastSelectedCell;
    CJCalendarCellData   *selectedData;
}
@property  id<CJCalendarViewDelegate> delegate;
@end

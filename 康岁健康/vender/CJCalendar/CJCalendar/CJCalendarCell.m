//
//  Cell.m
//  MYCalendar
//
//  Created by cerastes on 14-10-15.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "CJCalendarCell.h"
#import "BookInfoData.h"
@implementation CJCalendarCell
#define yuyueColor [UIColor colorWithRed:0 green:207/255. blue:52/255. alpha:1];
#define weekEndColor [UIColor colorWithRed:255/255. green:204/255. blue:0/255. alpha:1];

- (void)awakeFromNib {
    // Initialization code
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeState:) name:@"dateCoseStete" object:nil];
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dateCoseStete" object:nil];
}
-(void)setData:(CJCalendarCellData *)adata{
    _data = adata;
    self.dayLabel.text = [NSString stringWithFormat:@"%ld",(long)_data.day];
    [self.dayLabel sizeToFit];
    self.dayLabel.center = self.contentView.center;
}

-(void)setIsCurrentMonth:(bool)isCurrentMonth{
    if (isCurrentMonth) {
        self.userInteractionEnabled = YES;
        self.dayLabel.textColor = [UIColor grayColor];

    }
    else
    {
        self.dayLabel.textColor = [UIColor grayColor];
        self.userInteractionEnabled = NO;
    }
}

- (void)changeState:(NSNotification *)notification {
    BookInfoData *data = notification.object;
    if ([data.day intValue] == self.data.date.day && self.data.date.month == [data.month intValue]) {
        if ([data.book_allowed intValue] != 1){
            self.userInteractionEnabled = NO;
            self.dayLabel.textColor = CJGrayColor;
        }
        if ([data.left intValue] <= 0 && [data.book_allowed intValue] == 1) {
            self.userInteractionEnabled = NO;
            self.dayLabel.textColor = CJRedcolor;
        }
        if([data.dayoff intValue] == 1)
        {
            self.userInteractionEnabled = NO;
            self.dayLabel.textColor = [UIColor colorWithRed:255/255. green:203/255. blue:0 alpha:1];
        }
        else if([data.book_allowed intValue] == 1)
        {
            self.userInteractionEnabled = YES;
            self.dayLabel.textColor = yuyueColor;
        }
    }
//    if(notification.object && [notification.object isKindOfClass:[Test class]]){
//   }
}
@end

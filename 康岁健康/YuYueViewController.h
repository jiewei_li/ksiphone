//
//  YuYueViewController.h
//  康岁健康
//
//  Created by 低调 on 14/12/13.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"
#import "CJCalendarView.h"
#import "CustomerData.h"
@interface YuYueViewController : BaseNextViewController<CJCalendarViewDelegate>
{
    
}
@property  CustomerData   *customerData;
@property IBOutlet UIView   *choseView;
@property IBOutlet UIScrollView   *choseScrollView;
@property IBOutlet UIButton *companyCheckButton;
@property IBOutlet UIButton *selfCheckButton;
@property IBOutlet UILabel  *addrLabel;
@property IBOutlet UITextField *checkSetTextField;
@property IBOutlet UITextField *checkCityTextField;
@property IBOutlet UITextField *checkCenterTextField;
@property IBOutlet UITextField *checkDateTextField;

-(IBAction)selfCheck:(id)sender;
-(IBAction)companyCheck:(id)sender;
-(IBAction)checkSetChose:(id)sender;
-(IBAction)checkCityChose:(id)sender;
-(IBAction)checkCenterChose:(id)sender;
-(IBAction)checkDateChose:(id)sender;
-(IBAction)nextStep:(id)sender;

-(IBAction)cheatDeatil:(id)sender;
-(IBAction)cheatCenterDeatil:(id)sender;
@end

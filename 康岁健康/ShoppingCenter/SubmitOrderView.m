//
//  SubmitOrderView.m
//  康岁健康
//
//  Created by 低调 on 14/11/1.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "SubmitOrderView.h"
#import "SubmitOrderProductItem.h"
#import "ShoppingCartItemsData.h"
#import "ShoppingCartItemsAttributeData.h"
#import "PrepareNewOrderData.h"
#import "AddressesData.h"
#import "MyOrderList.h"

#import "PartnerConfig.h"
#import "AlixPayResult.h"
#import "AlixPayOrder.h"
#import "NewOrderData.h"
#import "DataSigner.h"
#import "DataVerifier.h"
#import "AlixLibService.h"
#import "OrderDetailData.h"

#import "PayAndKuaiDiChose.h"
@interface SubmitOrderView ()
{
    PrepareNewOrderData *_prepareNewOrderData;
    AddressesData       *_address;
    NSDictionary        *_shippingMethod;//快递
    BOOL                _shouletoOrderList;
}
@end

@implementation SubmitOrderView
@synthesize buyProducts=_buyProducts;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"订单填写";
    
    [self addSubView:self.mainScrollView];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeAddress:) name:@"changeAddress" object:nil];
    _mainScrollView.height = _mainView.height -44;
    _mainScrollView.top = 0;
    self.sumMoneyView.top = self.userInfoView.bottom+10;
    int i = 0;
    float itemheight = 0.0;
    int   count = 0;
    float sumPay = 0.0;
    for (ShoppingCartItemsData *data in _buyProducts) {
        SubmitOrderProductItem *item = [SubmitOrderProductItem loadFromNIB];
        item.top = self.sumMoneyView.bottom+i*item.height;
        i++;
        item.itemData = data;
        [self.mainScrollView addSubview:item];
        itemheight = item.height;
        sumPay += [data.Quantity intValue]*[data.Price floatValue];
        count += [data.Quantity intValue];
    }
    self.totalPriceLabel.text = [NSString stringWithFormat:@"¥ %.2f",sumPay];
    self.shouldPayLabel.text = [NSString stringWithFormat:@"¥ %.2f",sumPay];
    self.countLabel.text = [NSString stringWithFormat:@"%d",count];
    self.payMethodView.top = self.sumMoneyView.bottom+_buyProducts.count*itemheight+10;
    self.moneyListView.top = self.payMethodView.bottom+10;
    _shouletoOrderList = NO;
    _mainScrollView.contentSize = CGSizeMake(self.view.width, self.moneyListView.bottom+10);

    [self addSubView:self.bottomView];
    self.bottomView.bottom = _mainView.height;
    
    _checkoutAttribute = [[NSDictionary alloc]initWithObjectsAndKeys:@"false",@"NeedInvoice",@"",@"InvoiceTitle",@"明细",@"InvoiceDescription", nil];
    NSMutableArray *cartItems = [[NSMutableArray alloc]init];
    for (ShoppingCartItemsData *data in _buyProducts) {
        NSMutableDictionary *CartItemsDic = [[NSMutableDictionary alloc]init];

        NSMutableArray *Attributes = [[NSMutableArray alloc]init];
        NSMutableArray *attributeIds = [[NSMutableArray alloc]init];
        for (ShoppingCartItemsAttributeData *att in data.Attributes) {
            [attributeIds addObject:[[NSDictionary alloc]initWithObjectsAndKeys:[att.Id notNull],@"AttributeId", nil]];
            
        }
        [Attributes addObject:[[NSDictionary alloc]initWithObjectsAndKeys:attributeIds,@"AttributeIds",@"111",@"Id", nil]];
        [CartItemsDic setObject:Attributes forKey:@"Attributes"];
        if (data.Id) {
            [CartItemsDic setObject:[data.Id notNull] forKey:@"Id"];
        }
        else
        {
            [CartItemsDic setObject:@"12" forKey:@"Id"];
        }
        [CartItemsDic setObject:data.ProductId forKey:@"ProductId"];
        [CartItemsDic setObject:data.Quantity forKey:@"Quantity"];
        [cartItems addObject:CartItemsDic];
    }
    NSMutableDictionary *dataDic = [[NSMutableDictionary alloc]init];
    [dataDic setObject:cartItems forKey:@"CartItems"];
    [dataDic setObject:@"10" forKey:@"Tax"];
    [dataDic setObject: [NSString stringWithFormat:@"%.2f",sumPay] forKey:@"OrderTotal"];
    [dataDic setObject:[UserInfos customerId] forKey:@"CustomerId"];
    [dataDic setObject:cartItems forKey:@"CartItems"];

    [NetRequest requestShopWithType:PREPARE_NEW_ORDER withPostUrl:ADDRESS_PREPARE_NEW_ORDER andPostInfo:dataDic result:^(id resultSuccess) {
        _prepareNewOrderData = resultSuccess;
        NSArray *DeliveryAddressesArr = _prepareNewOrderData.DeliveryAddresses;
//        if (DeliveryAddressesArr && DeliveryAddressesArr.count>0) {
//            _address = [DeliveryAddressesArr objectAtIndex:0];
//            [self reflashAddress];
//        }
//        else{
//            self.addAddressView.hidden = NO;
//
//        }
        if (_prepareNewOrderData.ShippingMethod && _prepareNewOrderData.ShippingMethod.count>0) {
            _shippingMethod = [_prepareNewOrderData.ShippingMethod objectAtIndex:0];
            [self reflashShippingMethod];
        }
    } err:^(id resultError) {
        
    }];
    [self requestAddress];
}

-(void)viewDidAppear:(BOOL)animated{
    if (_shouletoOrderList) {
        MyOrderList *list = [[MyOrderList alloc]init];
        list.parentViewController = self;
        [self.navigationController pushViewController:list animated:YES];
    }
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"changeAddress" object:nil];
}
-(void)changeAddress:(NSNotification *)data{
    _address = data.object;
    [self reflashAddress];
}

-(void)requestAddress{
    [NetRequest requestShopWithType:GET_CUSTOMER_ADDRESS withGetUrl:ADDRESS_GET_CUSTOMER_ADDRESS andGetInfo:[NSDictionary dictionaryWithObjectsAndKeys:[UserInfos customerId],@"CustomerId", nil] result:^(id resultSuccess) {
        NSArray *arr = resultSuccess;
        if (arr.count>0) {
            _address = [arr objectAtIndex:0];
            [self reflashAddress];
        }
    } err:^(id resultError) {
        
    }];
}
-(void)reflashAddress{
    self.addAddressView.hidden = YES;
    self.userNameLabel.text = [NSString stringWithFormat:@"%@",_address.FirstName];
    self.phoneLabel.text    = _address.PhoneNumber;
    self.addressLabel.text  = [NSString stringWithFormat:@"%@ %@ %@",_address.StateProvinceName,_address.CityName,_address.Address];
    
}
-(void)reflashShippingMethod{
    NSString *totalPrice = [self.totalPriceLabel.text stringByReplacingOccurrencesOfString:@"¥" withString:@""];
    float sumMoney = [[_shippingMethod objectForCJKey:@"Price"] floatValue]+[totalPrice floatValue];
    self.bottomSumLabel.text = [NSString stringWithFormat:@"¥%.2f",sumMoney];
    self.freightLabel.text = [_shippingMethod objectForCJKey:@"Price"];
    self.payMethodLabel.text = [NSString stringWithFormat:@"支付宝支付\n%@",[_shippingMethod objectForCJKey:@"Name"]];
    if ([_prepareNewOrderData.NeedInvoice boolValue]) {
        self.invoiceLabel.text = @"需要发票";
    }
    else {
        self.invoiceLabel.text = @"不需要发票";
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(IBAction)goPay:(id)sender{

    UIButton *b= sender;
    b.enabled =NO;
    if ([self.isDriectToBuy isEqualToString:@"YES"]) {
        [UIAlertView showMessage:@"直接支付，开发中"];
    }
    else
    {
        [NetRequest requestShopWithType:ADD_NEW_ORDER withPostUrl:ADDRESS_ADD_NEW_ORDER andPostInfo:[self addNewOrder] result:^(id resultSuccess) {
            NewOrderData *newOrderData = resultSuccess;
//            NSString *url = STRING_FORMAT(@"%@?orderid=%@",ADDRESS_GET_ORDER_DETAIL,newOrderData.OrderId);

            NSDictionary *getDic = [[NSDictionary alloc]initWithObjectsAndKeys:newOrderData.OrderId,@"orderid", nil];
            [NetRequest requestShopWithType:GET_ORDER_DETAIL withGetUrl:ADDRESS_GET_ORDER_DETAIL andGetInfo:getDic result:^(id resultSuccess) {
//                OrderDetailData *resultData = resultSuccess;
                
                [NetRequest requestShopWithType:POST_PAY_MENT withGetUrl:ADDRESS_POST_PAY_MENT andGetInfo:getDic result:^(id resultSuccess) {
                    NSDictionary *payData = [resultSuccess objectForKey:@"Data"];
                    NSString *appScheme = @"kang";
                    NSString *productName = [payData objectForCJKey:@"Subject"];
                    NSString *productDescription = [payData objectForCJKey:@"Detail"];
                    AlixPayOrder *order = [[AlixPayOrder alloc] init];
                    order.partner = PartnerID;
                    order.seller = SellerID;
                    order.tradeNO = [payData objectForCJKey:@"OrderId"]; //订单ID（由商家自行制定）
                    
                    order.productName = productName; //商品标题
                    order.productDescription = productDescription; //商品描述
                    order.amount =  [payData objectForCJKey:@"Price"]; //商品价格
                    order.notifyURL =  [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"Notify_url"]; //回调URL
                    order.paymentType = @"1";
                    order.inputCharset = @"utf-8";
                    
                    NSString* orderInfo = [order description];
                    
                    NSString* signedStr = [self doRsa:orderInfo];
                    
                    NSLog(@"%@",orderInfo);
                    
                    NSString *orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                                             orderInfo, signedStr, @"RSA"];
                    
                    [AlixLibService payOrder:orderString AndScheme:appScheme seletor:@selector(paymentResult:) target:self];

                    _shouletoOrderList = YES;
                    NSString *TransTips = [payData objectForCJKey:@"TransTips"];
                    user_defaults_set_string(@"TransTips", TransTips);
                    b.enabled =YES;

                } err:^(id resultError) {
                    b.enabled =YES;
                }];
            } err:^(id resultError) {
                b.enabled =YES;
            }];
        } err:^(id resultError) {
            b.enabled =YES;
        }];
    }
}


-(NSDictionary *)addNewOrder{
//    NSDictionary *DeliveryAddress= [[NSDictionary alloc]initWithObjectsAndKeys:
//                                    [_address.Id notEmpty],@"Id",
//                                    [_address.FirstName notEmpty],@"FirstName",
//                                    [_address.LastName notEmpty],@"LastName",
//                                    [_address.Email notEmpty],@"Email",
//                                    [_address.Company notEmpty],@"Company",
//                                    [_address.CountryId notEmpty],@"CountryId",
//                                    [_address.StateProvinceId notEmpty],@"StateProvinceId",
//                                    [_address.CityId notEmpty],@"CityId",
//                                    [_address.Address notEmpty],@"Address",
//                                    nil];
    NSDictionary *DeliveryAddress= [[NSDictionary alloc]initWithObjectsAndKeys:
                                    [_address.Id notEmpty],@"AddressId",
                                    nil];
    
    NSMutableArray *CheckoutAttributes = [[NSMutableArray alloc]init];
    NSArray *keys = [_checkoutAttribute allKeys];
    for (NSString *key in keys) {
        NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:key,@"Key",[_checkoutAttribute objectForKey:key],@"Value", nil];
        [CheckoutAttributes addObject:dic];
    }
    
    
    NSMutableArray *items = [[NSMutableArray alloc]init];
    float money = 0.;

    for (ShoppingCartItemsData *itemsData in _buyProducts) {
        NSDictionary *item = [[NSDictionary alloc]initWithObjectsAndKeys:
                              itemsData.Id,@"Id",
                              nil];
        money += [itemsData.Price floatValue]*[itemsData.Quantity integerValue];
        [items addObject:item];
    }
    
    money +=  [[_shippingMethod objectForCJKey:@"Price"] integerValue];
    NSDictionary *data = [[NSDictionary alloc]initWithObjectsAndKeys:
                          [NSNumber numberWithBool:YES],@"NeedDelivery",
                          [NSNumber numberWithBool:YES],@"NeedInvoice",
                          [NSString stringWithFormat:@"%.2f",money],@"OrderTotal",
                          @"Alipay",@"PayMethod",
                          @"hahahah",@"CheckoutAttributeDescription",
                          CheckoutAttributes,@"CheckoutAttributes",
                          DeliveryAddress,@"DeliveryAddress",
                          DeliveryAddress,@"InvoiceAddress",
                          _shippingMethod,@"ShippingMethod",
                          items,@"CartItems",
                          [UserInfos customerId],@"CustomerId",
                          nil];
    return data;
}
-(NSString*)doRsa:(NSString*)orderInfo
{
    id<DataSigner> signer;
    signer = CreateRSADataSigner(PartnerPrivKey);
    NSString *signedString = [signer signString:orderInfo];
    return signedString;
}
-(void)paymentResult:(id)result{
    NSDictionary *dic = result;
    NSString *s = [NSString stringWithFormat:@"%@",result];
    NSLog(@"paymentResult = %@",s);
    
    NSString *TransTips = user_defaults_get_string(@"TransTips");
    if (TransTips && TransTips.length>0) {
        [UIAlertView showMessage:TransTips];
        user_defaults_set_string(@"TransTips", @"");
    }
}
-(IBAction)choseAddress:(id)sender{
    AddressListViewController *addressListViewController = [[AddressListViewController alloc]init];
    addressListViewController.addressArry = _prepareNewOrderData.DeliveryAddresses;
    [self.navigationController pushViewController:addressListViewController animated:YES];
}

-(IBAction)changePayAndKuaiDi:(id)sender{
    PayAndKuaiDiChose *payChose = [[PayAndKuaiDiChose alloc] initWithNibName:@"PayAndKuaiDiChose" bundle:nil];
    [payChose setPayMethod:_prepareNewOrderData.PayMethods andShippingMethod:_prepareNewOrderData.ShippingMethod];
    [self.navigationController pushViewController:payChose animated:YES];
}

-(IBAction)changeFaPiao:(id)sender{
    InvoiceChose *chose = [[InvoiceChose alloc]initWithNibName:@"InvoiceChose" bundle:nil];
    chose.delegate = self;
    [self.navigationController pushViewController:chose animated:YES];
}

-(void)invoiceChoseFinished:(NSDictionary *)invoiceInfo{
    if ([[invoiceInfo objectForCJKey:@"NeedInvoice"] isEqualToString:@"false"]) {
        self.invoiceLabel.text = @"无需发票";
    }
    else {
        self.invoiceLabel.text = @"需要发票";
    }
    _checkoutAttribute = invoiceInfo;
}
@end

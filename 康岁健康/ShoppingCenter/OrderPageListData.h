//
//  OrderPageListData.h
//  康岁健康
//
//  Created by 低调 on 14-10-15.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderPageListData : NSObject
@property (nonatomic) NSString *Id;
@property (nonatomic) NSString *CustomerId;
@property (nonatomic) NSString *OrderTotal;
@property (nonatomic) NSString *CreatedOnUtc;
@property (nonatomic) NSString *OrderItemsCount;
@property (nonatomic) NSString *OrderStatusId;
@property (nonatomic) NSString *OrderStatusName;
@property (nonatomic) NSString *PaymentStatusName;
@property (nonatomic) NSString *PaymentStatusStatusId;
@property (nonatomic) NSString *ShippingStatusName;
@property (nonatomic) NSString *ShippingStatusId;
@property (nonatomic) NSArray  *OrderItems;      //ShoppingCartItemsData
@end

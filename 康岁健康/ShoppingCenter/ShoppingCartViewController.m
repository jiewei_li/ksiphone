//
//  ShoppingCartViewController.m
//  康岁健康
//
//  Created by 低调 on 14-10-11.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ShoppingCartViewController.h"
#import "ShoppingCartItemsData.h"
#import "ShoppingCartTableViewCell.h"
#import "SubmitOrderView.h"
@interface ShoppingCartViewController ()
{
    NSMutableArray  *_cart;
    NSMutableSet    *_chosedItems;
    UIButton        *_editBut;
}
@end

@implementation ShoppingCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"购物车";
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, _mainView.height-50) style:UITableViewStyleGrouped];
    _chosedItems = [[NSMutableSet alloc]init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self addSubView:_tableView];
    
    _editBut = [UIButton buttonWithType:UIButtonTypeCustom];
    _editBut.frame = CGRectMake(self.view.width-55, _navView.height-33, 44, 22);
    _editBut.titleLabel.font = FontOfSize(15);
    [_editBut setTitle:@"编辑" forState:UIControlStateNormal];
    [_editBut setTitle:@"完成" forState:UIControlStateSelected];
    [_editBut addTarget:self action:@selector(editCart:) forControlEvents:UIControlEventTouchUpInside];
    [_navView addSubview:_editBut];

    [self requestList];
    [self createBottonView];
}

-(void)viewDidAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addCart:) name:AddCartItem object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ReduceCart:) name:ReduceCartItem object:nil];
}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AddCartItem object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ReduceCartItem object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)createBottonView{
    _selectAllButton = [[UIButton alloc]init];
    _selectAllButton.frame = CGRectMake(22, _mainView.height-36, 22, 22);
    [_selectAllButton addTarget:self action:@selector(selectAllCell) forControlEvents:UIControlEventTouchUpInside];
    [_selectAllButton setBackgroundImage:[UIImage imageNamed:@"cartItemUnSelected"] forState:UIControlStateNormal];
    [_selectAllButton setBackgroundImage:[UIImage imageNamed:@"cartItemSelected"]   forState:UIControlStateSelected];
    [self addSubView:_selectAllButton];
    
    _sumPayLabel = [[UILabel alloc]initWithFrame:CGRectMake(_selectAllButton.right+15, _selectAllButton.top, 180, _selectAllButton.height)];
    _sumPayLabel.backgroundColor = clearcolor;
    _sumPayLabel.textColor       = Btn_Orange_color;
    _sumPayLabel.text            = @"合计：¥0.0";
    _sumPayLabel.font            = FontOfSize(14);
    [self addSubView:_sumPayLabel];
    
    _goPayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _goPayButton.frame = CGRectMake(self.view.width-100, _mainView.height-42, 90, 34);
    [_goPayButton setBackgroundImage:[UIImage imageNamed:@"jiesuan"] forState:UIControlStateNormal];
    [_goPayButton setBackgroundImage:[UIImage imageNamed:@"jiesuanPressed"] forState:UIControlStateHighlighted];
    [_goPayButton addTarget:self action:@selector(submitList) forControlEvents:UIControlEventTouchUpInside];
    [self addSubView:_goPayButton];
}
#pragma mark - createViews

-(void)requestList{
    NSDictionary *productIdDic = [NSDictionary dictionaryWithObjectsAndKeys:[UserInfos customerId],@"CustomerId", nil];
    [NetRequest requestShopWithType:SHOPPING_CART_ITEM withGetUrl:ADDRESS_GET_SHOPPING_CART_ITEM andGetInfo:productIdDic result:^(id resultSuccess) {
        _cart = resultSuccess;
        [_tableView reloadData];
    }err:^(id resultError) {
    }];
}

-(void)selectAllCell{
    if (_selectAllButton.selected) {
        [[NSNotificationCenter defaultCenter]postNotificationName:UnSelectAllCartItem object:nil];
        for (ShoppingCartItemsData *data in _cart) {
            data.Selected = NO;
        }
        _selectAllButton.selected = NO;
        [_chosedItems removeAllObjects];
        [self frflashBotton];
    }
    else
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:SelectAllCartItem object:nil];
        for (ShoppingCartItemsData *data in _cart) {
            data.Selected = YES;
        }
        _selectAllButton.selected = YES;
        [_chosedItems removeAllObjects];
        [_chosedItems addObjectsFromArray:_cart];
    }
    
    
    
}
#pragma mark - tableviewdelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _cart.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identify = @"ShoppingCartTableViewCell";
    
    ShoppingCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ShoppingCartTableViewCell" owner:self options:nil] lastObject];
    }
    cell.data = [_cart objectAtIndex:indexPath.row];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 77.;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
#pragma mark - notifications
-(void)addCart:(NSNotification *)sender{
    ShoppingCartItemsData *data = [sender object];
    if (![_chosedItems containsObject:data]) {
        [_chosedItems addObject:data];
        [self frflashBotton];
    }
    NSLog(@"%@",_chosedItems);
    if (_chosedItems.count == _cart.count) {
        _selectAllButton.selected = YES;
    }
}
-(void)ReduceCart:(NSNotification *)sender{
    _selectAllButton.selected = NO;
    ShoppingCartItemsData *data = [sender object];
    if ([_chosedItems containsObject:data]) {
        [_chosedItems removeObject:data];
        [self frflashBotton];
    }
    NSLog(@"%@",_chosedItems);
    
}

#pragma actions
-(void)frflashBotton{
    float sum = 0.;
    for(ShoppingCartItemsData *data in _chosedItems){
        sum += [data.Price floatValue]*[data.Quantity intValue];
    }
    _sumPayLabel.text = [NSString stringWithFormat:@"合计：¥%.2f",sum];
}

-(void)editCart:(UIButton *)sender{
    if (sender.selected) {
        [[NSNotificationCenter defaultCenter]postNotificationName:CartItemFinishEdit object:nil];
        [_goPayButton setBackgroundImage:[UIImage imageNamed:@"jiesuan"] forState:UIControlStateNormal];
        [_goPayButton setBackgroundImage:[UIImage imageNamed:@"jiesuanPressed"] forState:UIControlStateHighlighted];
        
        NSMutableArray *upDateArry = [[NSMutableArray alloc]init];
        for (ShoppingCartItemsData *data in _cart) {
            NSDictionary *delegateDic = [[NSDictionary alloc]initWithObjectsAndKeys:data.Id,@"Id",data.Quantity,@"Quantity",@"Update",@"Action", nil];
            [upDateArry addObject:delegateDic];
        }
        [self updateList:upDateArry];
    }
    else
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:CartItemEdit object:nil];
        [_goPayButton setBackgroundImage:[UIImage imageNamed:@"delegateButton"] forState:UIControlStateNormal];
        [_goPayButton setBackgroundImage:[UIImage imageNamed:@"delegateButtonPressed"] forState:UIControlStateHighlighted];
    }
    sender.selected = !sender.selected;
    [self frflashBotton];
}
#pragma mark - 结算和删除
-(void)submitList{
    if (_editBut.selected) {
        NSLog(@"**************购物车删除。。。。。。。*************");
        NSMutableArray *delegateArry = [[NSMutableArray alloc]init];
        for (ShoppingCartItemsData *data in _chosedItems) {
            NSDictionary *delegateDic = [[NSDictionary alloc]initWithObjectsAndKeys:data.Id,@"Id",data.Quantity,@"Quantity",@"Remove",@"Action", nil];
            [delegateArry addObject:delegateDic];
            [_cart removeObject:data];
        }
        [_chosedItems removeAllObjects];
        [_tableView reloadData];
        [self updateList:delegateArry];
    }
    else
    {
        NSLog(@"**************购物车提交。。。。。。。*************");
//        ShoppingCartItemsData *data = [[ShoppingCartItemsData alloc]init];
//        data.ProductId = _data.Id;
//        data.ProductName = _data.Name;
//        data.Description = _data.ShortDescription;
//        data.Price = _data.Price;
//        data.Quantity = [NSString stringWithFormat:@"%d",_buyCount];
//        data.PictureUrl = _data.PictureUrl;
//        NSMutableArray *attributes = [[NSMutableArray alloc]init];
//        for (ProductAttributeData *data in _selectedAtt1) {
//            for (ProductAttributeValuesData *valueData in data.AttributeValues) {
//                ShoppingCartItemsAttributeData *itemData = [[ShoppingCartItemsAttributeData alloc]init];
//                itemData.Id = valueData.Id;
//                itemData.Name = valueData.Name;
//                itemData.attId = data.Id;
//                [attributes addObject:itemData];
//            }
//        }
//        data.Attributes = attributes;
//        
        SubmitOrderView *sub = [[SubmitOrderView alloc]init];
        sub.buyProducts = (NSArray *)_chosedItems;
        [self.navigationController pushViewController:sub animated:YES];
    }
}

-(void)updateList:(NSArray *)changeArr{
    NSDictionary *productIdDic = [NSDictionary dictionaryWithObjectsAndKeys:[UserInfos customerId],@"CustomerId",changeArr,@"Actions", nil];
    
    [NetRequest requestShopWithType:UPDATE_SHOPPING_CART_ITEMS withPostUrl:ADDRESS_GET_UPDATE_SHOPPING_CART_ITEMS andPostInfo:productIdDic result:^(id resultSuccess) {
        
    }err:^(id resultError) {
    }];
}

@end

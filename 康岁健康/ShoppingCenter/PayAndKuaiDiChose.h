//
//  PayAndKuaiDiChose.h
//  康岁健康
//
//  Created by 低调 on 14/11/29.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"

@interface PayAndKuaiDiChose : BaseNextViewController
{
    NSArray *_payMethod;
    NSArray *_shippingMethod;
}
@property IBOutlet UIView *payChoseView;
@property IBOutlet UIView *kuaiDiChoseView;
-(void)setPayMethod:(NSArray *)payMethod andShippingMethod:(NSArray *)shippingMethod;
@end

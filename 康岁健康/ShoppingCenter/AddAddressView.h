//
//  AddAddressView.h
//  康岁健康
//
//  Created by 低调 on 14/11/4.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"
#import "AddressesData.h"
typedef NS_ENUM(NSInteger,AdressType){
    other = 0,
    SetProvince,
    SetCity,
};
typedef NS_ENUM(NSInteger,AdressSaveType){
    AddAddress,
    UpDataAddress,
};
@interface AddAddressView : BaseNextViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    AdressType _adderssType;
    NSArray    *_stateProvincesArry;
    NSArray    *_citysArry;
    int        _chosedProvinceId;
    int        _chosedCityId;
    NSString   *_chosedProvinceName;
    NSString   *_chosedCityName;

}
@property AddressesData        *addressData;
@property AdressSaveType       adressSaveType;
@property NSString             *addressId;//地址id修改时使用
@property IBOutlet UIView      *addressVIew;
@property IBOutlet UIView      *addressChoseVIew;
@property IBOutlet UITextField *nameTextField;
@property IBOutlet UITextField *phoneNumberTextField;
@property IBOutlet UITextField *emailTextField;
@property IBOutlet UITextField *detailAddressTextField;
@property IBOutlet UITextField *postNumberTextField;//邮编
@property IBOutlet UILabel     *addressLabel;
@property IBOutlet UIButton    *nextBigBUtton;
@property IBOutlet UIButton    *nextSmallButton;
@property IBOutlet UIButton    *formerSmallBUtton;

@property IBOutlet UITableView *tableView;
-(IBAction)addressChose:(id)sender;
-(IBAction)save:(id)sender;
-(IBAction)hiddenAddressChose:(id)sender;

-(IBAction)bigNext:(id)sender;
-(IBAction)smallNext:(id)sender;
-(IBAction)samllFormer:(id)sender;
@end

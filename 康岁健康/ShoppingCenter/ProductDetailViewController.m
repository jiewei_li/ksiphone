//
//  ProductDetailViewController.m
//  康岁健康
//
//  Created by 低调 on 14-10-7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "ProductDetailData.h"

@interface ProductDetailViewController ()
{
    ProductDetailData *_productDetailData;
}
@end

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"商品信息";
    TopSwitchView *top = [[TopSwitchView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 44) leftTitle:@"商品详情" rightTitle:@"规格参数"];
    top.delgate = self;
    [self addSubView:top];
    
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 44, self.view.width, _mainView.height-44)];
    [self addSubView:_webView];
    NSDictionary *productIdDic = [NSDictionary dictionaryWithObjectsAndKeys:self.Id,@"productId", nil];
//#warning  TMPDATA
//    NSDictionary *productIdDic = [NSDictionary dictionaryWithObjectsAndKeys:@"75",@"productId", nil];

    [NetRequest requestShopWithType:PRODUCT_DETAIL withGetUrl:ADDRESS_GET_PRODUCT_DETAIL andGetInfo:productIdDic result:^(id resultSuccess) {
        _productDetailData = resultSuccess;
        [_webView loadHTMLString:_productDetailData.FullDescription baseURL:nil];
    }err:^(id resultError) {
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)leftChosed{
    [_webView loadHTMLString:_productDetailData.FullDescription baseURL:nil];
}
-(void)rightChosed{
    [_webView loadHTMLString:_productDetailData.FullDescription baseURL:nil];
}
@end

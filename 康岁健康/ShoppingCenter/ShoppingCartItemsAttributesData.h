//
//  ShoppingCartItemsAttributesData.h
//  康岁健康
//
//  Created by 低调 on 14-10-7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShoppingCartItemsAttributesData : NSObject

@property (nonatomic)NSString *AttribureId;//属性id，如“体检城市”属性

@property (nonatomic)NSArray  *Selections;//选择项的Id，支持多选
@end

//
//  PayAndKuaiDiChose.m
//  康岁健康
//
//  Created by 低调 on 14/11/29.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "PayAndKuaiDiChose.h"
#import "SwitchButton.h"
@interface PayAndKuaiDiChose ()
{
    NSDictionary *_selectedPayMethod;
    NSDictionary *_selectedshippingMethod;
}
@end

@implementation PayAndKuaiDiChose
-(void)setPayMethod:(NSArray *)payMethod andShippingMethod:(NSArray *)shippingMethod{
    _payMethod = payMethod;
    _shippingMethod = shippingMethod;
    if (_payMethod.count>0) {
        _selectedPayMethod = [_payMethod objectAtIndex:0];
    }
    if(_shippingMethod.count>0)
    {
        _selectedshippingMethod = [_shippingMethod objectAtIndex:0];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"支付和配送方式";
// Do any additional setup after loading the view from its nib.
//    SwitchButton *s = [SwitchButton buttonWithType:UIButtonTypeCustom];
//    [s setTitle:@"111" type:@"11" andFrame:CGRectMake(0, 0, 100, 100)];
//    [self addSubView:s];
//
//    SwitchButton *s2 = [SwitchButton buttonWithType:UIButtonTypeCustom];
//    [s2 setTitle:@"1121" type:@"11" andFrame:CGRectMake(0, 200, 100, 100)];
//    [self addSubView:s2];
    
    for (int i=0; i<_payMethod.count; i++) {
        SwitchButton *s = [SwitchButton buttonWithType:UIButtonTypeCustom];
        [s setTitle:[[_payMethod objectAtIndex:i] objectForCJKey:@"PayMethod"] type:@"payMethodChose" andFrame:CGRectMake(50, 40+50*i, 200, 30)];
        [self.payChoseView addSubview:s];
        s.data = [_payMethod objectAtIndex:i] ;
        if (i==0) {
            s.selected = YES;
        }
        self.payChoseView.height = s.bottom+10;
    }
    [self addSubView:self.payChoseView];
    
    for (int i=0; i<_shippingMethod.count; i++) {
        SwitchButton *s = [SwitchButton buttonWithType:UIButtonTypeCustom];
        [s setTitle:[[_shippingMethod objectAtIndex:i] objectForCJKey:@"Name"] type:@"shippingMethod" andFrame:CGRectMake(50, 40+50*i, 200, 30)];
        s.data = [_shippingMethod objectAtIndex:i] ;
        [self.kuaiDiChoseView addSubview:s];
        UILabel *l = [[UILabel alloc]initWithFrame:CGRectMake(s.width-40, s.height-20, 30, 15)];
        l.text = [NSString stringWithFormat:@"¥ %@",[[_shippingMethod objectAtIndex:i] objectForCJKey:@"Price"] ];
        l.textColor = CJWhiteColor;
        [s addSubview:l];
        if (i==0) {
            s.selected = YES;
        }
        self.kuaiDiChoseView.height = s.bottom+10;
    }
    
//    CGRect frameRect = CGRectMake(20, 90, self.frame.size.width-40, self.frame.size.height-180);
//    UIView   *self.kuaiDiChoseView = [[UIView alloc] initWithFrame:frameRect] ;
    self.kuaiDiChoseView.layer.borderWidth = 1;
    self.kuaiDiChoseView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.kuaiDiChoseView.layer.masksToBounds = YES;
    self.kuaiDiChoseView.layer.cornerRadius = 5.; //圆角
    
    self.payChoseView.layer.borderWidth = 1;
    self.payChoseView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.payChoseView.layer.masksToBounds = YES;
    self.payChoseView.layer.cornerRadius = 5.; //圆角
    
    self.payChoseView.top = 10;
    self.kuaiDiChoseView.top = self.payChoseView.bottom+20;
    self.kuaiDiChoseView.left = self.payChoseView.left=8;
    [self addSubView:self.kuaiDiChoseView];
    
    UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
    b.frame = CGRectMake(50, self.kuaiDiChoseView.bottom+44, 220, 44);
    [b setBackgroundColor:[UIColor orangeColor]];
    [b setTitle:@"确定" forState:UIControlStateNormal];
    [b addTarget:self action:@selector(popself) forControlEvents:UIControlEventTouchUpInside];
    [self addSubView:b];
}

-(void)viewDidAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(payMethodChose:) name:@"payMethodChose" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(shippingMethod:) name:@"shippingMethod" object:nil];
}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"PayMethod" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"shippingMethod" object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)shippingMethod:(NSNotification *)sender{
    SwitchButton *b = sender.object;
    _selectedshippingMethod = b.data;
//    PayMethod
}
-(void)payMethodChose:(NSNotification *)sender{
    SwitchButton *b = sender.object;
    _selectedPayMethod = b.data;
}

-(void)popself{
    [self.navigationController popViewControllerAnimated:YES];
}
@end

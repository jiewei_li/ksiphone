//
//  InvoiceChose.m
//  康岁健康
//
//  Created by 低调 on 14/11/26.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "InvoiceChose.h"

@interface InvoiceChose ()

@end

@implementation InvoiceChose

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置发票信息";
    [self addSubView:self.choseView];
    self.needButton.selected = YES;
    self.text.delegate = self;
    [self noneed:nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyBoard)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view from its nib.
}
-(void)hideKeyBoard{
    [self.text resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)sure:(id)sender{
    if (self.needButton.selected) {
        if ([self.text.text isEmpty]) {
            [UIAlertView showMessage:@"请输入发票类容"];
            return;
        }
        else
        {
            NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:@"true",@"NeedInvoice",self.text.text,@"InvoiceTitle",@"明细",@"InvoiceDescription", nil];
            [self.delegate invoiceChoseFinished:dic];
        }
    }
    else{
        NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:@"false",@"NeedInvoice",@" ",@"InvoiceTitle",@"明细",@"InvoiceDescription", nil];
        [self.delegate invoiceChoseFinished:dic];
    }
    [self.text resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)need:(id)sender{
    self.noNeedbutton.selected = NO;
    self.needButton.selected = YES;
    self.detailView.hidden = NO;
}
-(IBAction)noneed:(id)sender{
    self.noNeedbutton.selected = YES;
    self.needButton.selected = NO;
    self.detailView.hidden = YES;

}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    _mainView.top = -150;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    _mainView.top = _navView.bottom;

}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    [self.text resignFirstResponder];

    return YES;
}
@end

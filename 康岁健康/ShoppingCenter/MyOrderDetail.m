//
//  MyOrderDetail.m
//  康岁健康
//
//  Created by 低调 on 14/11/27.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "MyOrderDetail.h"
#import "OrderDetailData.h"
#import "MyOrderListItemTableViewCell.h"
#import "ShoppingCartItemsData.h"
@interface MyOrderDetail ()

@end

@implementation MyOrderDetail
-(id)initOrderPageListData:(OrderPageListData *)data{
    self = [super init];
    _data = data;
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"订单详情";
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, _mainView.height) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self addSubView:self.tableView];
    
    NSString *url = STRING_FORMAT(@"%@?orderid=%@",ADDRESS_GET_ORDER_DETAIL,_data.Id);
    
    [NetRequest requestShopWithType:GET_ORDER_DETAIL withGetUrl:url andGetInfo:nil result:^(id resultSuccess) {
        OrderDetailData *detailData = resultSuccess;
        NSLog(@"%@",detailData.CustomerId);

        self.orderStateLabel.text = [NSString stringWithFormat:@"%@",detailData.OrderStatusName];
        self.orderNumberLabel.text = [NSString stringWithFormat:@"%@",detailData.Id];
        self.countMoneyLabel.text = [NSString stringWithFormat:@"%@",detailData.TotalAmount];
        self.productMoneyLabel.text = [NSString stringWithFormat:@"%.2f",([detailData.TotalAmount floatValue]-[detailData.ShippingMethodPrice floatValue])];
        self.yunFeiLabel.text = [NSString stringWithFormat:@"%@",detailData.ShippingMethodPrice];
        
        
        self.userNameLabel.text = [NSString stringWithFormat:@"%@",detailData.ShippingAddressFirstName];
        self.userPhoneLabel.text = [NSString stringWithFormat:@"%@",detailData.ShippingAddressPhoneNumber];
        self.userAddressLabel.text = [NSString stringWithFormat:@"%@ %@ %@",detailData.InvoiceAddressStateProvinceName,detailData.InvoiceAddressCityName,detailData.ShippingAddressAddress];
        self.payMethodLabel.text = [NSString stringWithFormat:@"%@",detailData.PaymentMethod];
        self.sendMethodLabel.text = [NSString stringWithFormat:@"%@",detailData.ShippingMethodName];
        self.fapiaoInfoLabel.text = [NSString stringWithFormat:@"%@",[detailData.NeedInvoice isEqualToString:@"false"]?@"无需发票":@"需要发票"];
        self.fapiaoTitleLabel.text = [NSString stringWithFormat:@"%@",detailData.InvoiceTitle];
        
 
    } err:^(id resultError) {
        
    }];

    // Do any additional setup after loading the view from its nib.
}

//-(void)loadHeadView
//{
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return self.bottonView;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.headView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return self.bottonView.height;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return self.headView.height;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _data.OrderItems.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyOrderListItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyOrderListItemTableViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MyOrderListItemTableViewCell" owner:self options:nil] lastObject];
    }
//    OrderPageListData *orderData = [_data.OrderItems objectAtIndex:indexPath.row];
    ShoppingCartItemsData *idata = [_data.OrderItems objectAtIndex:indexPath.row];
    [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:idata.PictureUrl] placeholderImage:nil];
    cell.productName.text = idata.ProductName;
    cell.productAtt.text = idata.ProductId;
    cell.productPrice.text = [NSString stringWithFormat:@"¥ %@",idata.Price];
    cell.productQulity.text = [NSString stringWithFormat:@"X%@",idata.Quantity];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64.;
}
@end

//
//  ProductPicturesData.h
//  康岁健康
//
//  Created by 低调 on 14-10-6.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductPicturesData : NSObject
@property (nonatomic)NSString *DisplayOrder;
@property (nonatomic)NSString *Id;
@property (nonatomic)NSString *PictureId;
@property (nonatomic)NSString *PictureUrl;
@end

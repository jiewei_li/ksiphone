//
//  ShoppingMainViewController.h
//  康岁健康
//
//  Created by cerastes on 14-9-21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"
#import "ShoppingMainItem.h"
#import "ProductItem.h"

@interface ShoppingMainViewController : BaseNextViewController<toGoodsDetailDelegate,ShopMainChoseDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UITableView   *_productList;
    NSMutableArray *_producetArry;
    int      _pageIndex;
}
@end

//
//  ProductDetailData.h
//  康岁健康
//
//  Created by 低调 on 14-10-7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductDetailData : NSObject
@property (nonatomic)NSString *ProductId;
@property (nonatomic)NSString *FullDescription; //产品的描述Html内容
@property (nonatomic)NSArray *Specifications;
@end

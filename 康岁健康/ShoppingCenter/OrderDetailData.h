//
//  OrderDetailData.h
//  康岁健康
//
//  Created by 低调 on 14/11/13.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderDetailData : NSObject
@property NSString *Id;                            //订单Id
@property NSString *CustomerId;
@property NSString *TotalAmount;                   //订单总额
@property NSString *CreatedOnUtc;                  //订单生成日期
@property NSString *OrderItemsCount;               //产品个数
@property NSString *OrderStatusId;
@property NSString *OrderStatusName;               //状态名称
@property NSString *ShippingMethodId;
@property NSString *ShippingMethodName;
@property NSString *ShippingMethodPrice;
@property NSString *ShippingMethodDescription;
@property NSString *PaymentMethod;                 //支付方式
@property NSString *InvoiceAddressFirstName;       //名字，APP采用FirstName保存姓名
@property NSString *InvoiceAddressLastName;        //姓  不使用
@property NSString *InvoiceAddressEmail;
@property NSString *InvoiceAddressCompany;         //可选项
@property NSString *InvoiceAddressCountryId;       //国家Id,
@property NSString *InvoiceAddressProvinceId;      //省份Id
@property NSString *InvoiceAddressCityId;          //城市Id
@property NSString *InvoiceAddressAddress;         //街道地址
@property NSString *InvoiceAddressZipPostalCode;   //邮编
@property NSString *InvoiceAddressPhoneNumber;     //联系电话
@property NSString *InvoiceAddressFaxNumber;       //传真号
@property NSString *InvoiceAddressCustomerId;
@property NSString *InvoiceAddressCityName;
@property NSString *InvoiceAddressStateProvinceName;
@property NSString *ShippingAddressFirstName;      //名字，APP采用FirstName保存姓名
@property NSString *ShippingAddressLastName;       //姓  不使用
@property NSString *ShippingAddressEmail;          //
@property NSString *ShippingAddressCompany;        //可选项
@property NSString *ShippingAddressCountryId;      //国家Id,
@property NSString *ShippingAddressStateProvinceId;//省份Id
@property NSString *ShippingAddressCityId;         //城市Id
@property NSString *ShippingAddressAddress;        //街道地址
@property NSString *ShippingAddressZipPostalCode;  //邮编
@property NSString *ShippingAddressPhoneNumber;    //联系电话
@property NSString *ShippingAddressFaxNumber;      //传真号
@property NSString *ShippingAddressCustomerId;
@property NSString *InvoiceTitle;
@property NSString *InvoiceDescription;
@property NSString *NeedInvoice;
@property NSArray  *OrderItems;                    //订单产品列表
//    }
//    "OrderItems":[  //订单产品列表
//    {  //第一个产品
//        "ProductId":60,//产品编号
//        "Name": "LifeTrckSmart",//产品名称
//    Description://产品简介
//        "Price":0.0100,//价格
//        "Quantity":1, //数量
//        "PictureUrl":"http://localhost:8838/Content/Pictures/19.jpg",//产品图片
//    Attributes:
//        [
//         {
//         Id: 12 //属性id，如“体检城市”属性
//             Name：“上海”
//         }，
//        {
//        Id: 22 //属性id，如“体检城市”属性
//            Name：“北京”
//        }
//         ]
//    }
//    {……}  //第二个产品
//                  ]
//    ]
//}
@end

//
//  AddressListViewController.h
//  康岁健康
//
//  Created by 低调 on 14/11/4.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"

@interface AddressListViewController : BaseNextViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    int         _selectedIndex;
}
@property UITableView *tableView;
@property NSArray *addressArry;
@property IBOutlet UIView *headView;
-(IBAction)addAddress:(id)sender;
@end

//
//  ProductInfoViewController.h
//  康岁健康
//
//  Created by cerastes on 14-10-6.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductListData.h"
#import "ProductDetailItem.h"
#import "ProductAttributeItem.h"
#import "ProductAttSimpleChoseView.h"
#import "ProductCountSetItem.h"
#import "ProductBuyBottom.h"

@interface ProductInfoViewController : BaseNextViewController<ProductDetailItemDelegate,ProductAttributeItemDelegate,ProductAttSimpleChoseViewDelegate,ProductCountSetItemDelegate,ProductBuyBottomDelegate>
{
    NSString *_productId;
    NSMutableArray  *_mutableSelection;//多选属性
    NSMutableArray  *_unMutableSelection;//单选属性
    NSMutableArray  *_selectedAtt1;//选择的单选属性
    ProductAttSimpleChoseView *_simpleChoseView;
    int             _buyCount;//购买数量
    ProductBuyBottom *_bottomBuy;
    BOOL              _isSimpleAttChosed;
}
@property (nonatomic) NSString *productId;
-(id)initWithProductInfo:(ProductListData *)data;
@end

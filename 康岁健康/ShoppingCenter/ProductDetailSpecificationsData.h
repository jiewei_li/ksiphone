//
//  ProductDetailSpecificationsData.h
//  康岁健康
//
//  Created by 低调 on 14-10-7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductDetailSpecificationsData : NSObject
@property (nonatomic)NSString *Name;        //规格名称

@property (nonatomic)NSString *DisplayOrder;//显示顺序（顺序在后台处理）

@property (nonatomic)NSString *Value;       //规格参数值
@end

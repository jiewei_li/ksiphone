//
//  PrepareNewOrderData.h
//  康岁健康
//
//  Created by 低调 on 14-10-9.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PrepareNewOrderData : NSObject
@property NSString *NeedDelivery;     //是否需要送货，如果是，则客户端设置收货地址
@property NSString *NeedInvoice;      //是否需要发票，如果是，则客户端设置发票地址
@property NSArray  *PayMethods;       //"Id":20,"PayMethod":"货到付款","Description":"货到付款"}
@property NSArray  *DeliveryAddresses;//可选的送货地址，用户可填写新地址，并设置Id=0。此时需要获得国家、省份及城市信息，相应接口见5
@property NSArray  *InvoiceAddresses; //可选账单地址，格式同上，如果不需要发票，则为空
@property NSArray  *ShippingMethod;   //配送方式选项
@end

//
//  ProductInfoData.h
//  康岁健康
//
//  Created by cerastes on 14-9-25.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductListData : NSObject
@property (nonatomic)NSString *Id;
@property (nonatomic)NSString *Name;
@property (nonatomic)NSString *PictureUrl;
@property (nonatomic)NSString *Price;
@property (nonatomic)NSString *ProductTypeId;
@property (nonatomic)NSString *ShortDescription;
@property (nonatomic)NSString *catagoryId;

@end

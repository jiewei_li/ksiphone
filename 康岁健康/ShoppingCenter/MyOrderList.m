//
//  MyOrderList.m
//  康岁健康
//
//  Created by 低调 on 14-10-15.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "MyOrderList.h"
#import "OrderPageListData.h"
#import "MyOrderListItemHead.h"
#import "MyOrderListItemTableViewCell.h"
#import "ShoppingCartItemsData.h"
#import "MyOrderListItembotton.h"
#import "PartnerConfig.h"
#import "AlixPayResult.h"
#import "AlixPayOrder.h"
#import "NewOrderData.h"
#import "DataSigner.h"
#import "DataVerifier.h"
#import "AlixLibService.h"
#import "OrderDetailData.h"
#import "MyOrderDetail.h"

@implementation MyOrderList
-(void)viewDidLoad{
    [super viewDidLoad];
    [self createTopChoseView];
    
    unFinishedOrder = [[NSMutableArray alloc]init];
    finishedOrder   = [[NSMutableArray alloc]init];
    _unfinishedPage = 0;
    _finishPage     = 0;
    [self requestUnFinishedOrder];
}

-(void)viewDidAppear:(BOOL)animated{

}
-(void)createTopChoseView{
    self.title = @"我的订单";

    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 44)];
    [self addSubView:v];
    
    
    
    leftBtn = CJButtonF(CGRectMake(0, 0, v.width/2, 44));
    CJButtonTB(leftBtn,@"待处理", @"待处理");
    CJButtonCB(leftBtn,CJBlackColor, NavColor);
//    26166182
    CJButtonSCB(leftBtn, [UIColor colorWithRed:26/255. green:166/255. blue:182/255. alpha:1]);
    CJButtonSB(leftBtn, self, @selector(requestUnFinishedOrder));
    leftBtn.titleLabel.font = FONT(@"Helvetica-Bold", 18);
    [v addSubview:leftBtn];
    
    rightBtn = CJButtonF(CGRectMake(v.width/2, 0, v.width/2, 44));
    CJButtonTB(rightBtn,@"已完成", @"已完成");
    CJButtonCB(rightBtn,CJBlackColor, NavColor);
    CJButtonSCB(rightBtn, [UIColor colorWithRed:26/255. green:166/255. blue:182/255. alpha:1]);
    CJButtonSB(rightBtn, self, @selector(requestFinishedOrder));
    rightBtn.titleLabel.font = FONT(@"Helvetica-Bold", 18);
    rightBtn.selected = YES;
    [v addSubview:rightBtn];
    
    tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, _mainView.width, _mainView.height-44) style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self addSubView:tableView];
}


-(void)requestUnFinishedOrder{
    rightBtn.selected = NO;
    leftBtn.selected  = YES;
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:STRING_FORMAT(@"%d",_unfinishedPage),@"Pageindex",@"10",@"pagesize",[UserInfos customerId],@"customerId", @"20",@"orderStatus",nil];
    _unfinishedPage++;
    [NetRequest requestShopWithType:ORDER_PAGE_LIST withGetUrl:ADDRESS_GET_ORDER_PAGE_LIST andGetInfo:dic result:^(id resultSuccess) {
        [unFinishedOrder addObjectsFromArray:resultSuccess];
        [tableView reloadData];
    } err:^(id resultError) {
        
    }];
}

-(void)requestFinishedOrder{
    rightBtn.selected = YES;
    leftBtn.selected  = NO;

    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:STRING_FORMAT(@"%d",_finishPage),@"Pageindex",@"10",@"pagesize",[UserInfos customerId],@"customerId", @"30",@"orderStatus",nil];
    _finishPage++;
    [NetRequest requestShopWithType:ORDER_PAGE_LIST withGetUrl:ADDRESS_GET_ORDER_PAGE_LIST andGetInfo:dic result:^(id resultSuccess) {
        [finishedOrder addObjectsFromArray:resultSuccess];
        [tableView reloadData];
    } err:^(id resultError) {
        
    }];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (leftBtn.selected) {
        OrderPageListData *unFinishedOrderData = [unFinishedOrder objectAtIndex:section];
        return unFinishedOrderData.OrderItems.count;
    }
    OrderPageListData *finishedOrderData   = [finishedOrder objectAtIndex:section];

    return finishedOrderData.OrderItems.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (leftBtn.selected) {
        return unFinishedOrder.count;
    }
    return finishedOrder.count;
}

-(UITableViewCell *)tableView:(UITableView *)atableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyOrderListItemTableViewCell *cell = [atableView dequeueReusableCellWithIdentifier:@"MyOrderListItemTableViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MyOrderListItemTableViewCell" owner:self options:nil] lastObject];
    }
    OrderPageListData *orderData;
    if (leftBtn.selected) {
        orderData = [unFinishedOrder objectAtIndex:indexPath.section];
    }
    else
    {
        orderData = [finishedOrder  objectAtIndex:indexPath.section];
    }
    ShoppingCartItemsData *data = [orderData.OrderItems objectAtIndex:indexPath.row];
    [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:data.PictureUrl] placeholderImage:nil];
    cell.productName.text = data.ProductName;
    cell.productAtt.text = data.ProductId;
    cell.productPrice.text = [NSString stringWithFormat:@"¥ %@",data.Price];
    cell.productQulity.text = [NSString stringWithFormat:@"X%@",data.Quantity];
    return cell;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (leftBtn.selected) {
        OrderPageListData *unFinishedOrderData = [unFinishedOrder objectAtIndex:section];
        MyOrderListItemHead *head = [[[NSBundle mainBundle]loadNibNamed:@"MyOrderListItemHead" owner:self options:nil] lastObject];
        head.data = unFinishedOrderData;
        head.backgroundColor = [UIColor clearColor];
        return head;
    }
    else
    {
        OrderPageListData *finishedOrderData = [finishedOrder objectAtIndex:section];
        MyOrderListItemHead *head = [[[NSBundle mainBundle]loadNibNamed:@"MyOrderListItemHead" owner:self options:nil] lastObject];
        head.data = finishedOrderData;
        head.backgroundColor = [UIColor clearColor];
        return head;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (leftBtn.selected) {
        OrderPageListData *unFinishedOrderData = [unFinishedOrder objectAtIndex:section];
        MyOrderListItembotton *botton = [[[NSBundle mainBundle]loadNibNamed:@"MyOrderListItembotton" owner:self options:nil] lastObject];
        botton.delegate = self;
        botton.backgroundColor = [UIColor clearColor];
        botton.data = unFinishedOrderData;
        return botton;
    }
   else
       return [[UIView alloc]init];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    if (leftBtn.selected) {
        return 72;
//    }
//    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (leftBtn.selected) {
        return 55;
    }
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64.;
}

-(void)tableView:(UITableView *)atableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [atableView deselectRowAtIndexPath:indexPath animated:YES];
    
    OrderPageListData *orderData;
    if (leftBtn.selected) {
        orderData = [unFinishedOrder objectAtIndex:indexPath.section];
    }
    else
    {
        orderData = [finishedOrder  objectAtIndex:indexPath.section];
    }
    MyOrderDetail *detail = [[MyOrderDetail alloc]initWithNibName:@"MyOrderDetail" bundle:nil];
    detail.data = orderData;
    [self.navigationController pushViewController:detail animated:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint offset = scrollView.contentOffset;  // 当前滚动位移
    CGRect bounds = scrollView.bounds;          // UIScrollView 可视高度
    CGSize size = scrollView.contentSize;         // 滚动区域
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if (y > (h + reload_distance)) {
        if (leftBtn.selected) {
            [self requestUnFinishedOrder];
        }
        else
        {
            [self requestFinishedOrder];
        }
    }
}
//-(CGFloat)ta
#pragma mark - MyOrderListItembottonDelegate

-(void)delegateList:(id)data{
    OrderPageListData *ldata = data;
    
    NSString *url = STRING_FORMAT(@"%@?OrderId=%@",ADDRESS_CANCEL_ORDER,ldata.Id);

    [NetRequest requestShopWithType:CANCEL_ORDER withPostUrl:url andPostInfo:nil result:^(id resultSuccess) {
        [UIAlertView showMessage:@"取消成功"];
        [self reflashUnFinishedList];
        
    } err:^(id resultError) {
        [UIAlertView showMessage:@"取消失败，请稍后再试"];

    }];
    

}
-(void)payList:(id)data{
    OrderPageListData *ldata = data;

//    NSDictionary *getDic = [[NSDictionary alloc]initWithObjectsAndKeys:ldata.Id,@"orderId", nil];
//    NSString *url = STRING_FORMAT(@"%@?orderid=%@",ADDRESS_GET_ORDER_DETAIL,ldata.Id);
    NSDictionary *getDic = [[NSDictionary alloc]initWithObjectsAndKeys:ldata.Id,@"orderid", nil];
    [NetRequest requestShopWithType:GET_ORDER_DETAIL withGetUrl:ADDRESS_GET_ORDER_DETAIL andGetInfo:getDic result:^(id resultSuccess) {

        [NetRequest requestShopWithType:POST_PAY_MENT withGetUrl:ADDRESS_POST_PAY_MENT andGetInfo:getDic result:^(id resultSuccess) {
            NSDictionary *payData = [resultSuccess objectForKey:@"Data"];
            NSString *appScheme = @"kang";
            NSString *productName = [payData objectForCJKey:@"Subject"];
            NSString *productDescription = [payData objectForCJKey:@"Detail"];
            AlixPayOrder *order = [[AlixPayOrder alloc] init];
            order.partner = PartnerID;
            order.seller = SellerID;
            order.tradeNO = [payData objectForCJKey:@"OrderId"]; //订单ID（由商家自行制定）
            
            order.productName = productName; //商品标题
            order.productDescription = productDescription; //商品描述
            order.amount =  [payData objectForCJKey:@"Price"]; //商品价格
            order.notifyURL =  [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"Notify_url"]; //回调URL
            order.paymentType = @"1";
            order.inputCharset = @"utf-8";
            
            NSString* orderInfo = [order description];
            
            NSString* signedStr = [self doRsa:orderInfo];
            
            NSLog(@"%@",orderInfo);
            
            NSString *orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                                     orderInfo, signedStr, @"RSA"];
            
            [AlixLibService payOrder:orderString AndScheme:appScheme seletor:@selector(paymentResult:) target:self];
            
            NSString *TransTips = [payData objectForCJKey:@"TransTips"];
            user_defaults_set_string(@"TransTips", TransTips);
        } err:^(id resultError) {
            
        }];

        
//        NSString *appScheme = @"kang";
//        NSString *productName = resultData.ShippingMethodName;
//        NSString *productDescription = resultData.ShippingMethodDescription;
//        AlixPayOrder *order = [[AlixPayOrder alloc] init];
//        order.partner = PartnerID;
//        order.seller = SellerID;
//        order.tradeNO = ldata.Id; //订单ID（由商家自行制定）
//        
//        order.productName = productName; //商品标题
//        order.productDescription = productDescription; //商品描述
//        order.amount = [NSString stringWithFormat:@"0.01"]; //商品价格
//        order.notifyURL =  alipay_notify_url; //回调URL
//        order.paymentType = @"1";
//        order.inputCharset = @"utf-8";
//        NSString* orderInfo = [order description];
//        
//        NSString* signedStr = [self doRsa:orderInfo];
//        
//        NSLog(@"%@",orderInfo);
//        
//        NSString *orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
//                                 orderInfo, signedStr, @"RSA"];
//        
//        [AlixLibService payOrder:orderString AndScheme:appScheme seletor:@selector(paymentResult:) target:self];

    } err:^(id resultError) {
        
    }];



}
-(void)sureList:(id)data{
    OrderPageListData *ldata = data;
    
    NSString *url = STRING_FORMAT(@"%@?orderid=%@",ADDRESS_CONFIRMORDER_COMPLETED,ldata.Id);
    [NetRequest requestShopWithType:CONFIRMORDER_COMPLETED withPostUrl:url andPostInfo:nil result:^(id resultSuccess) {
        
        [self reflashUnFinishedList];

        
    } err:^(id resultError) {
        
    }];

}

-(void)tiXing{
    [self performSelector:@selector(tiXingDelay) withObject:nil afterDelay:0.5];
}

-(void)tiXingDelay{
    [UIAlertView showMessage:@"提醒成功"];
}
#pragma mark - ALIPAY
-(NSString*)doRsa:(NSString*)orderInfo
{
    id<DataSigner> signer;
    signer = CreateRSADataSigner(PartnerPrivKey);
    NSString *signedString = [signer signString:orderInfo];
    return signedString;
}
-(void)paymentResult:(id)result{
    NSString *s = [NSString stringWithFormat:@"%@",result];
    NSLog(@"paymentResult = %@",s);
}

-(void)reflashUnFinishedList{
    _unfinishedPage = 0;
    _finishPage     = 0;
    [self requestFinishedOrder];
    [self requestUnFinishedOrder];

}
-(void)goBack{
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}
@end

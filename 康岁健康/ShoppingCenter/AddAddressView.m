//
//  AddAddressView.m
//  康岁健康
//
//  Created by 低调 on 14/11/4.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "AddAddressView.h"
#import "StateProvincesByCountryIdData.h"
#import "CitysByStateProvinceIdData.h"

@interface AddAddressView ()

@end

@implementation AddAddressView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"新建收货地址";
    if (self.adressSaveType == UpDataAddress) {
        self.title = @"修改收货地址";
    }
    _adderssType =  SetProvince;
    _chosedProvinceId = -1;
    _chosedCityId     = -1;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.addressVIew.frame = CGRectMake(0, 0, _mainView.width, _mainView.height);
    self.addressChoseVIew.frame = CGRectMake(0, 0, _mainView.width, _mainView.height);
    self.addressChoseVIew.hidden = YES;
    [self addSubView:self.addressVIew];
    [self addSubView:self.addressChoseVIew];
}

-(void)viewDidAppear:(BOOL)animated{
    if (self.adressSaveType == UpDataAddress) {
        self.addressId = self.addressData.Id;
        self.detailAddressTextField.text = self.addressData.Address;
        self.emailTextField.text = self.addressData.Email;
        self.nameTextField.text = [NSString stringWithFormat:@"%@%@",self.addressData.FirstName,self.addressData.LastName];
        self.phoneNumberTextField.text = self.addressData.PhoneNumber;
        self.addressLabel.text = STRING_FORMAT(@"%@ %@",self.addressData.StateProvinceName,self.addressData.CityName);
        self.postNumberTextField.text = self.addressData.ZipPostalCode;
        _chosedCityId = [self.addressData.CityId intValue];
        _chosedProvinceId = [self.addressData.StateProvinceId intValue];
        
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)addressChose:(id)sender{
    [self loadProvinceView];
}


-(IBAction)save:(id)sender{
    if ([self.nameTextField.text isEmpty]) {
        [UIAlertView showMessage:@"请输入您的姓名"];
        return;
    }
    if ([self.emailTextField.text isEmpty]) {
        [UIAlertView showMessage:@"请输入您的邮箱地址"];
        return;
    }
    if ([self.detailAddressTextField.text isEmpty]) {
        [UIAlertView showMessage:@"请输入您的具体地址"];
        return;
    }
    if ([self.postNumberTextField.text isEmpty]) {
        [UIAlertView showMessage:@"请输入您的邮编"];
        return;
    }
    if ([self.phoneNumberTextField.text isEmpty]) {
        [UIAlertView showMessage:@"请输入您的手机号"];
        return;
    }
    if(-1 == _chosedCityId || -1 == _chosedProvinceId){
        [UIAlertView showMessage:@"请选择城市"];
        return;

    }
    if (self.adressSaveType == AddAddress) {
        NSDictionary *adderss = [[NSDictionary alloc]initWithObjectsAndKeys:
                                 @"0",@"Id",
                                 self.nameTextField.text,@"FirstName",
                                 @"",@"LastName",
                                 self.emailTextField.text,@"Email",
                                 @"",@"Company",
                                 @"21",@"CountryId",
                                 [NSString stringWithFormat:@"%d",_chosedProvinceId],@"StateProvinceId",
                                 [NSString stringWithFormat:@"%d",_chosedCityId],@"CityId",
                                 self.detailAddressTextField.text,@"Address",
                                 self.postNumberTextField.text,@"ZipPostalCode",
                                 self.phoneNumberTextField.text,@"PhoneNumber",
                                 @"",@"FaxNumber",
                                 [UserInfos customerId],@"customerId",
                                  nil];
        [NetRequest requestShopWithType:ADD_NEW_ADDRESS withPostUrl:ADDRESS_ADD_NEW_ADDRESS andPostInfo:adderss result:^(id resultSuccess) {
            if ([[resultSuccess objectForKey:@"Suc"] intValue] == 1) {
                [UIAlertView showMessage:@"添加成功"];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"addAddressSuccessed" object:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }
            else {
                [UIAlertView showMessage:@"添加失败，请稍后再试"];
            }
        } err:^(id resultError) {
            
        }];
    }
    else if(self.adressSaveType == UpDataAddress){
        
        NSDictionary *adderss = [[NSDictionary alloc]initWithObjectsAndKeys:
                                 [NSString stringWithFormat:@"%@",self.addressId],@"Id",
                                 self.nameTextField.text,@"FirstName",
                                 @"",@"LastName",
                                 self.emailTextField.text,@"Email",
                                 @"",@"Company",
                                 @"21",@"CountryId",
                                 [NSString stringWithFormat:@"%d",_chosedProvinceId],@"StateProvinceId",
                                 [NSString stringWithFormat:@"%d",_chosedCityId],@"CityId",
                                 self.detailAddressTextField.text,@"Address",
                                 self.postNumberTextField.text,@"ZipPostalCode",
                                 self.phoneNumberTextField.text,@"PhoneNumber",
                                 @"",@"FaxNumber",
                                 [UserInfos customerId],@"customerId",
                                 nil];

        [NetRequest requestShopWithType:EDIT_ADDRESS withPostUrl:ADDRESS_EDIT_ADDRESS andPostInfo:adderss result:^(id resultSuccess) {
            if ([[resultSuccess objectForKey:@"Suc"] intValue] == 1) {
                [UIAlertView showMessage:@"修改成功"];
            }
            else {
                [UIAlertView showMessage:@"修改失败，请稍后再试"];
            }
        } err:^(id resultError) {
            
        }];
    }
    
}
-(IBAction)hiddenAddressChose:(id)sender
{
    self.addressChoseVIew.hidden = YES;
    self.addressLabel.text = [NSString stringWithFormat:@"%@ %@",_chosedProvinceName,_chosedCityName];
}

-(IBAction)bigNext:(id)sender{
    if (_chosedProvinceId == -1) {
        [UIAlertView showMessage:@"请选择所在省"];
        return;
    }
    [self loadCityChoseView];
}
-(IBAction)smallNext:(id)sender{
    if (_chosedCityId == -1) {
        [UIAlertView showMessage:@"请选择所在省"];
        return;
    }
    [self hiddenAddressChose:nil];
    
}
-(IBAction)samllFormer:(id)sender{
    _chosedProvinceId = -1;
    _chosedCityId     = -1;
    [self loadProvinceView];
}

-(void)loadProvinceView{
    _adderssType = SetProvince;
    self.addressChoseVIew.hidden = NO;
    self.nextBigBUtton.hidden = NO;
    self.nextSmallButton.hidden = YES;
    self.formerSmallBUtton.hidden = YES;
    [self getStateProvincesByCountryId];
}

-(void)loadCityChoseView{
    _adderssType = SetCity;
    self.addressChoseVIew.hidden = NO;
    self.nextBigBUtton.hidden = YES;
    self.nextSmallButton.hidden = NO;
    self.formerSmallBUtton.hidden = NO;
    [self getCitysByStateProvinceId];
}

-(void)getStateProvincesByCountryId{
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:@"21",@"CountryId", nil];
    [NetRequest requestShopWithType:GET_STAEPROVINCES_BY_COUNTRY_ID withGetUrl:ADDRESS_GET_STAEPROVINCES_BY_COUNTRY_ID andGetInfo:dic result:^(id resultSuccess) {
        _stateProvincesArry = resultSuccess;
        [self.tableView reloadData];
    } err:^(id resultError) {
        
    }];
}
-(void)getCitysByStateProvinceId{
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",_chosedProvinceId],@"StateProvinceId",nil];
    [NetRequest requestShopWithType:GET_CITY_BY_STATE_PROVINCEID withGetUrl:ADDRESS_GET_CITY_BY_STATE_PROVINCEID andGetInfo:dic result:^(id resultSuccess) {
        _citysArry = resultSuccess;
        [self.tableView reloadData];
    } err:^(id resultError) {
        
    }];
}

#pragma mark -tableDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (_adderssType == SetProvince) {
        return _stateProvincesArry.count;
    }
    else if (_adderssType == SetCity) {
        return _citysArry.count;
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = @"addressCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.text = @"加载中";
    if (_adderssType == SetProvince && _stateProvincesArry && _stateProvincesArry.count>=indexPath.section) {
        StateProvincesByCountryIdData *data = [_stateProvincesArry objectAtIndex:indexPath.section];
        cell.textLabel.text = data.Name;
    }
    else if (_adderssType == SetCity && _citysArry && _citysArry.count>=indexPath.section) {
        CitysByStateProvinceIdData *data = [_citysArry objectAtIndex:indexPath.section];
        cell.textLabel.text = data.Name;
    }
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_adderssType == SetProvince && _stateProvincesArry && _stateProvincesArry.count>=indexPath.section) {
        StateProvincesByCountryIdData *data = [_stateProvincesArry objectAtIndex:indexPath.section];
        _chosedProvinceId   = [data.Id intValue];
        _chosedProvinceName = data.Name;
    }
    else if (_adderssType == SetCity && _citysArry && _citysArry.count>=indexPath.section) {
        CitysByStateProvinceIdData *data = [_citysArry objectAtIndex:indexPath.section];
        _chosedCityId   = [data.Id intValue];
        _chosedCityName = data.Name;
    }

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


@end

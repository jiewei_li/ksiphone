//
//  MyOrderList.h
//  康岁健康
//
//  Created by 低调 on 14-10-15.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"
#import "MyOrderListItembotton.h"

@interface MyOrderList : BaseNextViewController<UITableViewDataSource,UITableViewDelegate,MyOrderListItembottonDelegate>
{
    UITableView *tableView;
    UIButton *leftBtn;
    UIButton *rightBtn;
    NSMutableArray  *unFinishedOrder;
    NSMutableArray  *finishedOrder;
    int     _unfinishedPage;
    int     _finishPage;
}

@property UIViewController *parentViewController;
@end

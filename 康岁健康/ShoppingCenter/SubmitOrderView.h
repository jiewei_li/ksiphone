//
//  SubmitOrderView.h
//  康岁健康
//
//  Created by 低调 on 14/11/1.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"
#import "AddressListViewController.h"
#import "InvoiceChose.h"

@interface SubmitOrderView : BaseNextViewController<InvoiceChoseDelegate>
{
    NSArray   *_buyProducts;
    NSDictionary *_checkoutAttribute;
}
@property  NSArray                *buyProducts;
@property  NSString               *isDriectToBuy;
@property IBOutlet UIScrollView   *mainScrollView;
@property IBOutlet UILabel        *userNameLabel;//姓名
@property IBOutlet UILabel        *phoneLabel;//联系方式
@property IBOutlet UILabel        *addressLabel;//地址
@property IBOutlet UILabel        *totalPriceLabel;//总价格
@property IBOutlet UILabel        *countLabel;//购买总数数量
@property IBOutlet UILabel        *payMethodLabel;//支付方式
@property IBOutlet UILabel        *invoiceLabel;//发票信息
@property IBOutlet UILabel        *shouldPayLabel;//商品总金额
@property IBOutlet UILabel        *freightLabel;//运费

@property IBOutlet UIView         *userInfoView;//顶部地址view
@property IBOutlet UIView         *sumMoneyView;//第二行价格view
@property IBOutlet UIView         *payMethodView;//支付方式，发票view
@property IBOutlet UIView         *moneyListView;//总价格view
@property IBOutlet UIView         *addAddressView;//总价格view
@property IBOutlet UIView         *bottomView;   //底部view
@property IBOutlet UILabel        *bottomSumLabel; //底部总价格
-(IBAction)goPay:(id)sender;
-(IBAction)choseAddress:(id)sender;
-(IBAction)changePayAndKuaiDi:(id)sender;
-(IBAction)changeFaPiao:(id)sender;
@end

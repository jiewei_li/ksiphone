//
//  CitysByStateProvinceIdData.h
//  康岁健康
//
//  Created by 低调 on 14-10-9.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CitysByStateProvinceIdData : NSObject
@property NSString *StateProvinceId;
@property NSString *Name;
@property NSString *DisplayOrder;
@property NSString *Id;
@property NSString *Abbreviation;
@end

//
//  ProductInfoViewController.m
//  康岁健康
//
//  Created by cerastes on 14-10-6.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ProductInfoViewController.h"
#import "EScrollerView.h"
#import "ProductSlideImageInfoData.h"
#import "ProductPicturesData.h"
#import "ProductDetailViewController.h"
#import "ProductAttributeValuesData.h"
#import "SubmitOrderView.h"
#import "ShoppingCartItemsData.h"
#import "ShoppingCartItemsAttributeData.h"
#import "ShoppingCartViewController.h"
#import "WebViewController.h"
#import "LogInViewController.h"
@interface ProductInfoViewController ()
{
    ProductListData   *_data;
    UIImageView       *_topImageView;
    EScrollerView     *_top;
    ProductDetailItem *_productDetailItem;
    UIScrollView      *_mainScrollView;
    NSMutableArray    *_attributes;
    ProductCountSetItem *_pitem;
}
@end

@implementation ProductInfoViewController
@synthesize productId = _productId;

-(id)initWithProductInfo:(ProductListData *)data{
    self = [super init];
    
    if(self){
        _data = data;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _data.Name;
    self.title = @"商品购买";
    _buyCount = 1;
    _isSimpleAttChosed = NO;
    _selectedAtt1 = [[NSMutableArray alloc]init];
    [self createMainView];

}

-(void)createMainView{
    
    _bottomBuy = [ProductBuyBottom loadFromNIB];
    _bottomBuy.bottom = _mainView.height;
    _bottomBuy.delegate = self;
    [self addSubView:_bottomBuy];
    
    _topImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 11, self.view.width, (self.view.width-44)*0.68)];
    _mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, _mainView.width, _mainView.height-_bottomBuy.height)];
    [self addSubView:_mainScrollView];
    
    [_mainScrollView addSubview:_topImageView];
    [_topImageView sd_setImageWithURL:[NSURL URLWithString:_data.PictureUrl] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    _top = [[EScrollerView alloc]initWithFrame:CGRectMake(0, 11, self.view.width, 200)];
    [_mainScrollView addSubview:_top];
    _top.backgroundColor = [UIColor clearColor];
    
    _productDetailItem = [[[NSBundle mainBundle]loadNibNamed:@"ProductDetailItem" owner:self options:nil] lastObject];
    _productDetailItem.data = _data;
    _productDetailItem.delegate = self;
    [_mainScrollView addSubview:_productDetailItem];
    _productDetailItem.top = _top.bottom+11;
    
    
    _pitem = [ProductCountSetItem loadFromNIB];
    _pitem.top = _productDetailItem.bottom;
    [_mainScrollView addSubview:_pitem];
    _pitem.delegate = self;
    
    _simpleChoseView = [ProductAttSimpleChoseView loadFromNIB];
    _simpleChoseView.hidden = YES;
    [_simpleChoseView.productImgView sd_setImageWithURL:[NSURL URLWithString:_data.PictureUrl] placeholderImage:nil];
    _simpleChoseView.productNameLabel.text  = _data.Name;
    _simpleChoseView.productPriceLabel.text = _data.Price;
    _simpleChoseView.delegate = self;
    [self.view addSubview:_simpleChoseView];
    [self getProductPictures];
    [self GetProductAttribute];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)GetProductAttribute{
    NSDictionary *productIdDic = [NSDictionary dictionaryWithObjectsAndKeys:_data.Id,@"productId", nil];
//#warning TMPDATA
//    NSDictionary *productIdDic = [NSDictionary dictionaryWithObjectsAndKeys:@"67",@"productId", nil];

    [NetRequest requestShopWithType:PRODUCT_ATTRIBUTE withGetUrl:ADDRESS_GET_PRODUCT_ATTRIBUTE andGetInfo:productIdDic result:^(id resultSuccess) {
        [self createAttributesViews:resultSuccess];
    }err:^(id resultError) {
    }];
}

-(void)createAttributesViews:(NSArray *)atts{
    for(UIView *view in _mainScrollView.subviews){
        if([view isKindOfClass:[ProductAttributeItem class]]){
            [view removeFromSuperview];
        }
    }
    if(!_attributes){
        _attributes = [[NSMutableArray alloc]init];
    }
    if (!_mutableSelection) {
        _mutableSelection = [[NSMutableArray alloc]init];
    }
    if (!_unMutableSelection) {
        _unMutableSelection = [[NSMutableArray alloc]init];
    }
    [_attributes removeAllObjects];
    [_mutableSelection removeAllObjects];
    [_unMutableSelection removeAllObjects];
    [_attributes addObjectsFromArray:atts];
    
    for (ProductAttributeData *data in atts) {
        if ([data.SelectionType integerValue] == 0) {
            [_unMutableSelection addObject:data];
        }
        else
        {
            [_mutableSelection addObject:data];
        }
    }
    _simpleChoseView.attArry = _unMutableSelection;

    NSString *unmutableAttrString = @"";
    NSString *unmutableDefaultValue = @"";
    for (ProductAttributeData *data in _unMutableSelection) {
        unmutableAttrString = [NSString stringWithFormat:@"%@ %@",[unmutableAttrString notNull],[data.TextPrompt notNull]];
        unmutableDefaultValue = [NSString stringWithFormat:@"%@ %@",[unmutableAttrString notNull],[data.DefaultValue notNull]];
    }
    CGFloat itemHeihght = 0.0;
//    单选选择按钮
    ProductAttributeItem *itemM = [[[NSBundle mainBundle]loadNibNamed:@"ProductAttributeItem" owner:self options:nil] lastObject];
    [_mainScrollView addSubview:itemM];
    itemM.top = _productDetailItem.bottom ;
    itemM.textPromptLabel.text = unmutableAttrString;
    itemM.type = UN_MUTABLE_ATT_CHOSE ;
    itemM.delegate = self;
    itemHeihght = itemM.height;
    _mainScrollView.contentSize = CGSizeMake(self.view.width, itemM.bottom);
    
//    多选选择按钮
    if (_unMutableSelection && _unMutableSelection.count>0) {
        for(int i = 0;i<_mutableSelection.count;i++){
            ProductAttributeItem *item = [[[NSBundle mainBundle]loadNibNamed:@"ProductAttributeItem" owner:self options:nil] lastObject];
            [_mainScrollView addSubview:item];
            item.top = _productDetailItem.bottom + (i+1)*item.height;
            item.data = [_unMutableSelection objectAtIndex:i];
            item.type = MUTABLE_ATT_CHOSE ;
            item.delegate = self;
            itemHeihght = item.height;
        }
    }
    
    
    _pitem.top = _productDetailItem.bottom + (_mutableSelection.count+_unMutableSelection.count==0?0:1)*itemHeihght;
    
    _mainScrollView.contentSize = CGSizeMake(self.view.width, _pitem.bottom);
}
-(void)getProductPictures{
    NSDictionary *productIdDic = [NSDictionary dictionaryWithObjectsAndKeys:_data.Id,@"productId", nil];
    [NetRequest requestShopWithType:PRODUCT_PICTURES withGetUrl:ADDRESS_GET_PRODUCT_PICTURES andGetInfo:productIdDic result:^(id resultSuccess) {
        NSMutableArray *resultArr = [[NSMutableArray alloc]init];
        for(ProductPicturesData *picData in resultSuccess){
            ProductSlideImageInfoData *data = [[ProductSlideImageInfoData alloc]init];
            data.link       = @"";
            data.pictureid  = picData.PictureId;
            data.pictureurl = picData.PictureUrl;
            data.text       = @"";
//            NSString *url = picData.PictureUrl;
            [resultArr addObject:data];
            
        }
        [_top setImageArray:resultArr Block:^(id i) {
            NSLog(@"i");
        }];
    }err:^(id resultError) {
    }];
}
-(void)reloadViews{
    
}


#pragma mark ProductDetailItemDelegate
-(void)ProductDetailItemPressed:(id)sender{
    ProductDetailItem *item = sender;
    NSLog(@"%@",item);
    WebViewController *web = [[WebViewController alloc]init];
    web.titleS = @"商品详情";
    web.url = [NSString stringWithFormat:@"http://mall.ihealthink.com/product/ShowProductDetails?productId=%@",_data.Id];
//    ProductDetailViewController *detail = [[ProductDetailViewController alloc]init];
//    detail.Id = _data.Id;
    [self.navigationController pushViewController:web animated:YES];

}

-(void)choseAtt:(id)sender{
    ProductAttributeItem *item = sender;
    if (item.type == MUTABLE_ATT_CHOSE) {
        
    }
    else
    {
        _simpleChoseView.hidden = NO;
    }
}

-(BOOL)isAttChosed{
    if (!_isSimpleAttChosed && _unMutableSelection.count>0) {
        return NO;
    }
    return YES;
}
#pragma mark - ProductAttSimpleChoseViewDelegate
-(void)productAttSimpleChoseViewFinished:(NSMutableArray *)selectedAtt{
    _selectedAtt1 = selectedAtt;
    NSString * atts = @"";
    for (ProductAttributeData *dat in selectedAtt) {
        for (ProductAttributeValuesData *valueData in dat.AttributeValues) {
            atts = [NSString stringWithFormat:@"%@ %@",atts,valueData.Name];
        }
        
    }
    for (ProductAttributeItem *item in _mainScrollView.subviews) {
        if ([item isKindOfClass:[ProductAttributeItem class]] && item.type == UN_MUTABLE_ATT_CHOSE) {
            item.textPromptLabel.text = atts;
            item.nameLabel.text = @"已选择";
        }
    }
    _isSimpleAttChosed = YES;
}
#pragma mark - ProductCountSetItemDelegate
-(void)productCountSet:(int)count{
    _buyCount = count;
}


#pragma mark - 
-(void)addCart:(BOOL)toPay{
    if (![UserInfos isUserLogIn]) {
        [self logIn];
        return;
    }
    if (![self isAttChosed]) {
        ProductAttributeItem *item = [[ProductAttributeItem alloc]init];
        item.type = UN_MUTABLE_ATT_CHOSE;
        [self choseAtt:item];
        return;
    }
    NSMutableArray *attributes = [[NSMutableArray alloc]init];

    for (ProductAttributeData *attData in _selectedAtt1) {
        NSMutableArray *attributeIds = [[NSMutableArray alloc]init];
        for (ProductAttributeData *valueData in attData.AttributeValues) {
            NSDictionary *valuedic = [[NSDictionary alloc]initWithObjectsAndKeys:valueData.Id,@"AttributeId", nil];
            [attributeIds addObject:valuedic];
        }
        NSMutableDictionary *attribute = [[NSMutableDictionary alloc]init];

        [attribute setObject:attData.Id forKey:@"Id"];
        [attribute setObject:attributeIds forKey:@"AttributeIds"];
//        
        [attributes addObject:attribute];
    }
    NSDictionary *data = [[NSDictionary alloc]initWithObjectsAndKeys:
                          [UserInfos customerId],@"CustomerId",
                          _data.Id,@"ProductId",
                          [NSString stringWithFormat:@"%d",_buyCount],@"Quantity",
                           _data.Price,@"Price",
                          attributes,@"Attributes",
                          nil];

    [NetRequest requestShopWithType:Add2Cart withPostUrl:ADDRESS_Add2Cart andPostInfo:data result:^(id resultSuccess) {
        NSDictionary *dic = resultSuccess;
        if ([[dic objectForKey:@"Suc"] intValue] == 1) {
            
            
            if (toPay) {
                if (![self isAttChosed]) {
                    ProductAttributeItem *item = [[ProductAttributeItem alloc]init];
                    item.type = UN_MUTABLE_ATT_CHOSE;
                    [self choseAtt:item];
                    return;
                }
                ShoppingCartItemsData *dataS = [[ShoppingCartItemsData alloc]init];
                dataS.ProductId = _data.Id;
                dataS.ProductName = _data.Name;
                dataS.Description = _data.ShortDescription;
                dataS.Price = _data.Price;
                dataS.Quantity = [NSString stringWithFormat:@"%d",_buyCount];
                dataS.PictureUrl = _data.PictureUrl;
                NSMutableArray *attributes = [[NSMutableArray alloc]init];
                for (ProductAttributeData *data in _selectedAtt1) {
                    for (ProductAttributeValuesData *valueData in data.AttributeValues) {
                        ShoppingCartItemsAttributeData *itemData = [[ShoppingCartItemsAttributeData alloc]init];
                        itemData.Id = valueData.Id;
                        itemData.Text = valueData.Name;
                        [attributes addObject:itemData];
                    }
                }
                dataS.Attributes = attributes;
                dataS.Id = [[dic objectForKey:@"Data"] objectForCJKey:@"CartItemId"];
                SubmitOrderView *sub = [[SubmitOrderView alloc]init];
                sub.buyProducts = [[NSArray alloc]initWithObjects:dataS, nil];
                sub.isDriectToBuy = @"NO";
                [self.navigationController pushViewController:sub animated:YES];
                return;
            }
            
            
            
            
            [UIAlertView showMessage:@"添加成功"];
            
            NSString *cartCount = user_defaults_get_object(NumberOfCrat);
            int count = [cartCount integerValue];
            count++;
            NSString *newCount = [NSString stringWithFormat:@"%d",count];
            [[NSNotificationCenter defaultCenter] postNotificationName:CartCountNotifocation object:newCount];
            user_defaults_set_object(NumberOfCrat, newCount);
            [_bottomBuy setCount:newCount];
        }
        else{
            [UIAlertView showMessage:@"添加失败"];
        }
    }err:^(id resultError) {
    }];
//    [NetRequest requestShopWithType:Add2Cart withGetUrl:ADDRESS_Add2Cart withGetInfo:data result:^(id resultSuccess) {
//        
//    }err:^(id resultError) {
//    }];
}


-(void)buy{
    if (![UserInfos isUserLogIn]) {
        [self logIn];
        return;
    }
    [self addCart:YES];
    
    
}
-(void)toCart{
    if (![UserInfos isUserLogIn]) {
        [self logIn];
        return;
    }
    ShoppingCartViewController *shoppingCartViewController = [[ShoppingCartViewController alloc]init];
    [self.navigationController pushViewController:shoppingCartViewController animated:YES];
}

-(void)logIn{
    LogInViewController *logIn = [[LogInViewController alloc]initWithNibName:@"LogInViewController" bundle:nil];
    [self.navigationController pushViewController:logIn animated:YES];
}
@end

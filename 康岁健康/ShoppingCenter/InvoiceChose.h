//
//  InvoiceChose.h
//  康岁健康
//
//  Created by 低调 on 14/11/26.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol InvoiceChoseDelegate <NSObject>

-(void)invoiceChoseFinished:(NSDictionary *)invoiceInfo;

@end
#import "BaseNextViewController.h"

@interface InvoiceChose : BaseNextViewController<UITextFieldDelegate>
@property IBOutlet UIView *choseView;
@property IBOutlet UIView *detailView;
@property IBOutlet UITextField *text;
@property IBOutlet UIButton *needButton;
@property IBOutlet UIButton *noNeedbutton;
-(IBAction)sure:(id)sender;
-(IBAction)need:(id)sender;
-(IBAction)noneed:(id)sender;
@property id<InvoiceChoseDelegate>delegate;
@end

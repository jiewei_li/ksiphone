//
//  ProductAttributeValuesData.h
//  康岁健康
//
//  Created by 低调 on 14-10-7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductAttributeValuesData : NSObject
@property (nonatomic)NSString *Name;            //属性名称
@property (nonatomic)NSString *Id;              //属性Id
@property (nonatomic)NSString *PriceAdjustment; //价格调整
@end

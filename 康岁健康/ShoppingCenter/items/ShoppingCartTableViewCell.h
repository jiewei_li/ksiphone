//
//  ShoppingCartTableViewCell.h
//  康岁健康
//
//  Created by 低调 on 14-10-12.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingCartItemsData.h"

@interface ShoppingCartTableViewCell : UITableViewCell
{
    ShoppingCartItemsData *_data;
    BOOL                  _isSelected;
}
@property IBOutlet UIImageView *selectStateImg;
@property IBOutlet UIImageView *goodsImg;
@property IBOutlet UILabel     *goodsNameLable;
@property IBOutlet UILabel     *goodsAttrLabel;
@property IBOutlet UILabel     *goodsPriceLabel;
@property IBOutlet UILabel     *goodsCountLabel;
@property IBOutlet UILabel     *changeGoodsCountLabel;
@property IBOutlet UIView      *showCountView;
@property IBOutlet UIView      *changeCountView;

@property BOOL                 isSelected;
@property (nonatomic)ShoppingCartItemsData *data;
-(IBAction)setSelectState:(id)sender;
-(IBAction)addCount:(id)sender;
-(IBAction)reduceCount:(id)sender;

@end

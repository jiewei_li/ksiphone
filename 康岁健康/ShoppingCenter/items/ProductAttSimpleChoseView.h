//
//  ProductAttSimpleChoseView.h
//  康岁健康
//
//  Created by 低调 on 14/10/25.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol ProductAttSimpleChoseViewDelegate <NSObject>

-(void)productAttSimpleChoseViewFinished:(NSMutableArray *)selectedAtt;

@end
#import <UIKit/UIKit.h>

@interface ProductAttSimpleChoseView : UIView<UITableViewDelegate,UITableViewDataSource>
{
    BOOL _isfirst;
    NSMutableArray *_attArry;
    NSMutableArray *_selectedAtt;
}
@property  (nonatomic)NSMutableArray *attArry;

@property id<ProductAttSimpleChoseViewDelegate> delegate;
@property IBOutlet UIView      *mainChoseView;
@property IBOutlet UIImageView *productImgView;
@property IBOutlet UILabel     *productNameLabel;
@property IBOutlet UILabel     *productPriceLabel;
@property IBOutlet UITableView *choseAttView;
@property IBOutlet UIButton     *sureButton;

-(IBAction)setSelfHidden:(id)sender;
-(IBAction)sure:(id)sender;

@end

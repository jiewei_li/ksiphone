//
//  SwitchButton.h
//  康岁健康
//
//  Created by 低调 on 14/11/30.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitchButton : UIButton
{
    NSString *_type;
}

@property    (nonatomic) NSString *type;
@property    (nonatomic) id data;

-(void)setTitle:(NSString *)title type:(NSString *)type andFrame:(CGRect)frame;

@end

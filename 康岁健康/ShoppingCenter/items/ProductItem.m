//
//  ProductItem.m
//  康岁健康
//
//  Created by cerastes on 14-9-25.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ProductItem.h"
@implementation ProductItem
@synthesize data = _data;
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

-(void)setData:(ProductListData *)data{
    _data = data;
    [self.prouductImage sd_setImageWithURL:[NSURL URLWithString:[data.PictureUrl notNull]] placeholderImage:[UIImage imageNamed:@""]];
    self.productName.text = [data.Name notNull];
    self.price.text = [NSString stringWithFormat:@"¥%.1f",[[data.Price notNull] floatValue]];
    int price = [[data.Price notNull] floatValue]*100;
    if (price%10>0) {
        self.price.text = [NSString stringWithFormat:@"¥%.2f",[[data.Price notNull] floatValue]];

    }
    self.shortDescription.text = [data.ShortDescription notNull];
    self.tag = [data.Id integerValue];
}

-(IBAction)toGoodsDetail:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(toGoodsDetailPressed:)]) {
        [self.delegate toGoodsDetailPressed:self];
    }
}
@end

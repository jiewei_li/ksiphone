//
//  ProductDetailItem.m
//  康岁健康
//
//  Created by 低调 on 14-10-6.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ProductDetailItem.h"

@implementation ProductDetailItem
@synthesize titleLabel;
@synthesize describtionLabel;
@synthesize priceLabel;
@synthesize data;
-(void)setData:(ProductListData *)datat{
    _data = datat;
    self.titleLabel.text = _data.Name;
    self.priceLabel.text = [[_data.Price notNull] moneyString];
    self.describtionLabel.text = _data.ShortDescription;
}
-(IBAction)itemPressed:(id)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(ProductDetailItemPressed:)]){
        [self.delegate ProductDetailItemPressed:self];
    }
}
@end

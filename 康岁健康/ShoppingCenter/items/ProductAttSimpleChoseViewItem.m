//
//  ProductAttSimpleChoseViewItem.m
//  康岁健康
//
//  Created by 低调 on 14/10/26.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ProductAttSimpleChoseViewItem.h"
#import "ProductAttributeData.h"
#import "ProductAttributeValuesData.h"
@implementation ProductAttSimpleChoseViewItem

- (void)awakeFromNib {
    // Initialization code
    self.selectedData = [[ProductAttributeData alloc]init];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setData:(ProductAttributeData *)data{
    _data = data;
    self.selectedData.Id = data.Id;
    self.selectedData.Name = data.Name;
    self.selectedData.TextPrompt = data.TextPrompt;
    self.selectedData.DefaultValue = data.DefaultValue;
    self.selectedData.IsRequired = data.IsRequired;
    self.selectedData.SelectionType = data.SelectionType;

    
    self.nameLabel.text = data.TextPrompt;

    NSArray *attArr= data.AttributeValues;
    self.height = attArr.count/3*23+attArr.count%3>0?36:0+15;

    for (int i = 0 ;i<attArr.count;i++) {
        ProductAttributeValuesData *data = [attArr objectAtIndex:i];
        ProductAttSimpleChoseButton *bt = [ProductAttSimpleChoseButton loadFromNIB];
        bt.data = data;
        bt.left = i%3*73+85;
        bt.top = i/3*23+12;
        bt.delegate = self;
        [self.contentView addSubview:bt];

    }

    self.bottomLine.bottom = self.height;
}
-(void)productAttSimpleChoseButtonChosed:(id)sender{
    ProductAttSimpleChoseButton *btn = sender;
    for (ProductAttSimpleChoseButton *bb in self.contentView.subviews) {
        if ([bb isKindOfClass:[ProductAttSimpleChoseButton class]]) {
            if ([btn.data.Id isEqual:bb.data.Id]) {
                self.selectedData.AttributeValues = [[NSArray alloc]initWithObjects:btn.data, nil];
            }
            else {
                [bb setUnselected];
            }
        }
    }
}
@end

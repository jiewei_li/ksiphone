//
//  ProductCountSetItem.m
//  康岁健康
//
//  Created by 低调 on 14/10/30.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ProductCountSetItem.h"

@implementation ProductCountSetItem

-(IBAction)addCount:(id)sender{
    int i = [self.countLabel.text intValue];
    i++;
    self.countLabel.text = [NSString stringWithFormat:@"%d",i];
    [self setCount:i];
}
-(IBAction)reduceCount:(id)sender{
    int i = [self.countLabel.text intValue];
    if (i>1) {
        i--;
    }
    self.countLabel.text = [NSString stringWithFormat:@"%d",i];
    [self setCount:i];
}

-(void)setCount:(int)count{
    if (self.delegate && [self.delegate respondsToSelector:@selector(productCountSet:)]) {
        [self.delegate productCountSet:count];
    }
}
@end

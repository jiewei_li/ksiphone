//
//  ShoppingMainItem.m
//  康岁健康
//
//  Created by cerastes on 14-9-24.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ShoppingMainItem.h"

@interface ShoppingMainItem ()

@end

@implementation ShoppingMainItem
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if ( self) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.height/3.*2-7, self.width, self.height/3)];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = FontOfSize(15);
        _titleLabel.textColor = [UIColor grayColor];
        [self addSubview:_titleLabel];
        
        _iconImage = [UIButton buttonWithType:UIButtonTypeCustom];
        int spike = 40;
        _iconImage.frame = CGRectMake(spike, 10, self.width-2*spike , self.width-2*spike);
//        _iconImage.frame = CGRectMake(_iconImage.origin.x+_iconImage.width/4, _iconImage.origin.y+_iconImage.height/4, _iconImage.width/2, _iconImage.height/2);
        [_iconImage addTarget:self action:@selector(ItemPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_iconImage];
        _titleLabel.top = _iconImage.bottom+5;

//        _IconButton.frame = CGRectMake(spike, spike, self.width-2*spike , self.height/3*2);
//        _IconButton.top = _iconImage.top;
//        _IconButton.right = _iconImage.right;
//        [self addSubview:_IconButton];
        
        _bridgeView = [UIButton buttonWithType:UIButtonTypeCustom];
        [_bridgeView setTitle:@"19" forState:UIControlStateNormal];
        _bridgeView.titleLabel.font = FontOfSize(7);
        _bridgeView.titleLabel.textAlignment = NSTextAlignmentCenter;
        [_bridgeView setBackgroundImage:[UIImage imageNamed:@"bridgeValue"] forState:UIControlStateNormal];
        _bridgeView.frame = CGRectMake(65, 10, 12, 12);
        _bridgeView.hidden = YES;
        [self addSubview:_bridgeView];
    }
    return self;
}
-(void)setTitle:(NSString *)title{
    _titleLabel.text = title;
    _titleLabel.font = [UIFont systemFontOfSize:12];
}

-(void)setIcon:(NSString *)icon
{
    [_iconImage setBackgroundImage:[UIImage imageNamed:icon] forState:UIControlStateNormal];
    NSLog(@"%@",[NSString stringWithFormat:@"%@Pressed",icon]);
    [_iconImage setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@Pressed",icon]] forState:UIControlStateHighlighted];
}

-(void)ItemPressed:(id)sender{
    NSLog(@"item pressed hahahaha**********************");
    if (self.delegate && [self.delegate respondsToSelector:@selector(shopTypeItemChosed:)]) {
        [self.delegate shopTypeItemChosed:self];
    }
}

-(void)setType:(ItemType)type
{
//    ShoppingItem     ,
//    DealShoppingList ,
//    OrderChectting
//    
//    //已购买的体检套餐数
//#define BookingCountNotifocation @"BookingCountNotifocation"
//    //购物车中商品数
//#define CartCountNotifocation @"CartCountNotifocation"
//    //待处理订单
//#define OrderCountNotifocation @"OrderCountNotifocation"
    _type = type;
    if (type == ShoppingItem) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setBridgeVaule:) name:CartCountNotifocation object:nil];
    }
    else if (type == DealShoppingList) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setBridgeVaule:) name:OrderCountNotifocation object:nil];
    }
    else if (type == OrderChectting) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setBridgeVaule:) name:BookingCountNotifocation object:nil];
    }
}

-(void)setBridgeVaule:(NSNotification *)bridgeVaule{
    NSString *value = [[bridgeVaule object] notNull];
//    if ([value intValue] == 0) {
//        _bridgeView.hidden = YES;
//    }
//    else
//    {
        _bridgeView.hidden = NO;
//        if ([value intValue] >10) {
//            _bridgeView.width = 20;
//        }
//        else
//        {
//            _bridgeView.width = 11;
//        }
//        [_bridgeView setTitle:[value notNull] forState:UIControlStateNormal];
//    }
    [_bridgeView setTitle:[value notNull] forState:UIControlStateNormal];

    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end

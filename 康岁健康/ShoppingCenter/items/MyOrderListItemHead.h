//
//  MyOrderListItemHead.h
//  康岁健康
//
//  Created by 低调 on 14-10-16.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderPageListData.h"

@interface MyOrderListItemHead : UIView
{
    OrderPageListData *_data;
}
@property (nonatomic) OrderPageListData *data;
@property IBOutlet UILabel *OrderStatusName;
@property IBOutlet UILabel *CreatedOnUtc;
@property IBOutlet UILabel *Price;
@property IBOutlet UILabel *OrderNumber;
@end

//
//  MyOrderListItemHead.m
//  康岁健康
//
//  Created by 低调 on 14-10-16.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "MyOrderListItemHead.h"
#import "ShoppingCartItemsData.h"
@implementation MyOrderListItemHead
-(void)setData:(OrderPageListData *)adata{
    _data = adata;
    self.OrderStatusName.text = adata.OrderTotal;
    self.CreatedOnUtc.text = adata.CreatedOnUtc;
    self.OrderNumber.text = adata.Id;
//    float price = 0.;
//    for (ShoppingCartItemsData *iData in adata.OrderItems) {
//        price += [iData.Quantity floatValue]*[iData.Price floatValue];
//    }
    self.Price.text = adata.OrderTotal;
//    if ([adata.PaymentStatusStatusId integerValue] == 0) {
//        self.OrderStatusName.text = @"未付款";
//    }
//    else if ([adata.PaymentStatusStatusId integerValue] == 10) {
//        self.OrderStatusName.text = @"待安排";
//    }
//    else if ([adata.PaymentStatusStatusId integerValue] == 20) {
//        self.OrderStatusName.text = @"待完成";
//    }
    self.OrderStatusName.text = adata.OrderStatusName;
//    else if ([adata.PaymentStatusStatusId integerValue] == 30) {
//        self.OrderStatusName.text = @"已完成";
//    }
//    else if ([adata.PaymentStatusStatusId integerValue] == 40) {
//        self.OrderStatusName.text = @"已取消";
//    }
}
@end

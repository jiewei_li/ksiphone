//
//  ProductDetailItem.h
//  康岁健康
//
//  Created by 低调 on 14-10-6.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol ProductDetailItemDelegate <NSObject>

-(void)ProductDetailItemPressed:(id)sender;

@end

#import <UIKit/UIKit.h>
#import "ProductListData.h"

@interface ProductDetailItem : UIView
{
    ProductListData   *_data;
}
@property (nonatomic) ProductListData   *data;
@property IBOutlet UILabel *titleLabel;
@property IBOutlet UILabel *describtionLabel;
@property IBOutlet UILabel *priceLabel;
@property (nonatomic) id<ProductDetailItemDelegate> delegate;
-(IBAction)itemPressed:(id)sender;
@end

//
//  ProductAttSimpleChoseButton.m
//  康岁健康
//
//  Created by 低调 on 14/10/27.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ProductAttSimpleChoseButton.h"

@implementation ProductAttSimpleChoseButton
@synthesize data = _data;
-(void)setData:(ProductAttributeValuesData *)data{
    _data = data;
    CJButtonSIB(self.choseButton, [UIImage imageNamed:@"ProductAttSimpleChoseButtonPressed"]);
    CJButtonIB(self.choseButton, [UIImage imageNamed:@"ProductAttSimpleChoseButtonUnPressed"], [UIImage imageNamed:@"ProductAttSimpleChoseButtonPressed"]);
    CJButtonCB(self.choseButton, CJGrayColor, CJWhiteColor);
    [self.choseButton setTitleColor:CJWhiteColor forState:UIControlStateSelected];
    CJButtonTB(self.choseButton, _data.Name, _data.Name);
}
-(IBAction)selfSelect:(id)sender{
    [self.delegate productAttSimpleChoseButtonChosed:self];
    self.choseButton.selected = YES;
}
-(void)setUnselected{
    self.choseButton.selected = NO;
}
@end

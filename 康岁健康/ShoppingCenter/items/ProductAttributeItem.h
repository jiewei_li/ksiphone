//
//  ProductAttributeItem.h
//  康岁健康
//
//  Created by 低调 on 14-10-7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

@protocol ProductAttributeItemDelegate <NSObject>

-(void)choseAtt:(id)sender;

@end
#import <UIKit/UIKit.h>
#import "ProductAttributeData.h"
typedef NS_ENUM(NSInteger,SelectionType){
    MUTABLE_ATT_CHOSE = 0,
    UN_MUTABLE_ATT_CHOSE
};
@interface ProductAttributeItem : UIView
{
    ProductAttributeData *_data;
}
@property  SelectionType type;
@property  id<ProductAttributeItemDelegate>delegate;
@property (nonatomic)ProductAttributeData *data;
@property IBOutlet UILabel *textPromptLabel;
@property IBOutlet UILabel *nameLabel;
-(IBAction)pressAction:(id)sender;
@end

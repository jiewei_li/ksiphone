//
//  MyOrderListItembotton.h
//  康岁健康
//
//  Created by 低调 on 14-10-16.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol MyOrderListItembottonDelegate <NSObject>

-(void)delegateList:(id)data;
-(void)payList:(id)data;
-(void)sureList:(id)data;
-(void)tiXing;
@end
#import <UIKit/UIKit.h>
#import "OrderPageListData.h"

@interface MyOrderListItembotton : UIView
{
    OrderPageListData *_data;
}
@property (nonatomic) OrderPageListData *data;
@property id<MyOrderListItembottonDelegate> delegate;

@property IBOutlet UIButton *delegateBtn;
@property IBOutlet UIButton *payBtn;
@property IBOutlet UIButton *sureReveivedBtn;
@property IBOutlet UIButton *tiXingBtn;


-(IBAction)deletePressed:(id)sender;
-(IBAction)toPay:(id)sender;
-(IBAction)sureReceivedPressed:(id)sender;
-(IBAction)tiXingPressed:(id)sender;

@end

//
//  AddressCell.h
//  康岁健康
//
//  Created by 低调 on 14/11/4.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressesData.h"
@interface AddressCell : UITableViewCell
{
    AddressesData       *_address;

}
@property (nonatomic)AddressesData   *address;
@property IBOutlet UILabel           *nameLabel;
@property IBOutlet UILabel           *addressLabel;
@property IBOutlet UILabel           *phoneLabel;
@property IBOutlet UIImageView       *isSelectedImage;
@property IBOutlet UIImageView       *editImage;
-(void)setImgHidden:(BOOL)isSlelcted;
-(void)setEdit:(BOOL)isedit;
@end

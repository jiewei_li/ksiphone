//
//  ShoppingMainItem.h
//  康岁健康
//
//  Created by cerastes on 14-9-24.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol ShopMainChoseDelegate <NSObject>

-(void)shopTypeItemChosed:(id)sender;

@end
#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,ItemType){
    ShoppingItem     = 0,
    DealShoppingList ,
    OrderChectting   
};
@interface ShoppingMainItem : UIView
{
    UILabel     *_titleLabel;
    UIButton    *_iconImage;
}
@property (nonatomic)NSString  *bridgeValue;
@property (nonatomic)NSString  *title;
@property (nonatomic)NSString  *icon;
@property (nonatomic)ItemType  type;
@property (nonatomic)UIButton *bridgeView;
@property id<ShopMainChoseDelegate> delegate;
-(void)ItemPressed:(id)sender;
@end

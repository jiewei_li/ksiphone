//
//  MyOrderListItemTableViewCell.h
//  康岁健康
//
//  Created by 低调 on 14-10-16.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOrderListItemTableViewCell : UITableViewCell
@property IBOutlet UIImageView *productImageView;
@property IBOutlet UILabel     *productName;
@property IBOutlet UILabel     *productAtt;
@property IBOutlet UILabel     *productPrice;
@property IBOutlet UILabel     *productQulity;
@end

//
//  ShoppingCartTableViewCell.m
//  康岁健康
//
//  Created by 低调 on 14-10-12.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ShoppingCartTableViewCell.h"
#import "SDWebImageManager.h"
#import "ShoppingCartItemsAttributeData.h"
@implementation ShoppingCartTableViewCell
@synthesize isSelected = _isSelected;
- (void)awakeFromNib {
    // Initialization code
    _isSelected = NO;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeCountShow) name:CartItemEdit object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeCountHiden) name:CartItemFinishEdit object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(allSelect) name:SelectAllCartItem object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(allUnSelect) name:UnSelectAllCartItem object:nil];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
}

-(void)dealloc{
    
}
-(void)setData:(ShoppingCartItemsData *)adata{
    _data = adata;
    self.showCountView.hidden = NO;
    self.changeCountView.hidden = YES;
    [self.goodsImg sd_setImageWithURL:[NSURL URLWithString:adata.PictureUrl] placeholderImage:nil];
    self.goodsNameLable.text = adata.ProductName;
    NSString *att = @"";
    for (ShoppingCartItemsAttributeData *attData in adata.Attributes) {
        att = [NSString stringWithFormat:@"%@%@:",att,attData.Text];
        for (NSDictionary *dic in attData.AttributeIds) {
            att = [NSString stringWithFormat:@"%@ %@",att,[dic objectForKey:@"Name"]];
        }
        att = [NSString stringWithFormat:@"%@ ;",att];
    }
    self.changeGoodsCountLabel.text = adata.Quantity;
    self.goodsAttrLabel.text = att;
    self.goodsPriceLabel.text = [NSString stringWithFormat:@"¥%@",adata.Price];
    self.goodsCountLabel.text = [NSString stringWithFormat:@"x%@",adata.Quantity];
    _isSelected = adata.Selected;
    [self setSelectState];
}
-(IBAction)setSelectState:(id)sender{
    _isSelected = !_isSelected;
    _data.Selected = _isSelected;
    [self setSelectState];
}

-(void)setSelectState{
    if (!_isSelected) {
        self.selectStateImg.image = [UIImage imageNamed:@"cartItemUnSelected"];
        [[NSNotificationCenter defaultCenter]postNotificationName:ReduceCartItem object:_data];
    }
    else{
        self.selectStateImg.image = [UIImage imageNamed:@"cartItemSelected"];
        [[NSNotificationCenter defaultCenter]postNotificationName:AddCartItem object:_data];
    }
}
-(void)changeCountShow{
    self.showCountView.hidden = YES;
    self.changeCountView.hidden = NO;
}
-(void)changeCountHiden{
    self.showCountView.hidden = NO;
    self.changeCountView.hidden = YES;
}

-(IBAction)addCount:(id)sender{
    int count = [_data.Quantity intValue];
    count++;
    NSString *counts = [NSString stringWithFormat:@"%d",count];
    self.changeGoodsCountLabel.text = counts;
    _data.Quantity = counts;
    self.goodsCountLabel.text = [NSString stringWithFormat:@"x%@",_data.Quantity];
}
-(IBAction)reduceCount:(id)sender{
    int count = [_data.Quantity intValue];
    if (count>0) {
        count--;
    }
    else
    {
        count = 0;
    }
    NSString *counts = [NSString stringWithFormat:@"%d",count];
    self.changeGoodsCountLabel.text = counts;
    _data.Quantity = counts;
    self.goodsCountLabel.text = [NSString stringWithFormat:@"x%@",_data.Quantity];
}

-(void)allSelect{
    self.selectStateImg.image = [UIImage imageNamed:@"cartItemSelected"];
    _isSelected = YES;
    [[NSNotificationCenter defaultCenter]postNotificationName:AddCartItem object:_data];
}
-(void)allUnSelect{
    self.selectStateImg.image = [UIImage imageNamed:@"cartItemUnSelected"];
    _isSelected = NO;
    [[NSNotificationCenter defaultCenter]postNotificationName:ReduceCartItem object:_data];

}
@end

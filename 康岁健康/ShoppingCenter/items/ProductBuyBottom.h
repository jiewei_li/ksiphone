//
//  ProductBuyBottom.h
//  康岁健康
//
//  Created by 低调 on 14/10/30.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol ProductBuyBottomDelegate <NSObject>

-(void)addCart:(BOOL)toPay;
-(void)buy;
-(void)toCart;


@end
#import <UIKit/UIKit.h>

@interface ProductBuyBottom : UIView
@property id<ProductBuyBottomDelegate>delegate;
@property IBOutlet UIButton *cartNumber;
-(void)setCount:(NSString *)count;
-(IBAction)addCart:(id)sender;
-(IBAction)buy:(id)sender;
-(IBAction)toCart:(id)sender;

@end

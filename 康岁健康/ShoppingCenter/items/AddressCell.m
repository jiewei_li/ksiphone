//
//  AddressCell.m
//  康岁健康
//
//  Created by 低调 on 14/11/4.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "AddressCell.h"

@implementation AddressCell
//@synthesize isSlelcted = _isSlelcted;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setImgHidden:(BOOL)isSlelcted{
        self.isSelectedImage.hidden = isSlelcted;
}
-(void)setAddress:(AddressesData *)address{
    self.nameLabel.text = [NSString stringWithFormat:@"%@%@",address.LastName,address.FirstName];
    self.addressLabel.text = [NSString stringWithFormat:@"%@ %@",address.CityName,address.Address];
    self.phoneLabel.text = address.PhoneNumber;
}
-(void)setEdit:(BOOL)isedit{
    if (isedit) {
        [self setImgHidden:YES];
        self.editImage.hidden = NO;
    }
    else
    {
        self.editImage.hidden = YES;
    }
}
@end

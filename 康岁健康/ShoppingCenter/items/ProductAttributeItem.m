//
//  ProductAttributeItem.m
//  康岁健康
//
//  Created by 低调 on 14-10-7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ProductAttributeItem.h"

@implementation ProductAttributeItem
@synthesize data;
-(void)setData:(ProductAttributeData *)dataT{
    _data = dataT;
    self.textPromptLabel.text = dataT.TextPrompt;
}
-(IBAction)pressAction:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(choseAtt:)]) {
        [self.delegate choseAtt:self];
    }
}
@end

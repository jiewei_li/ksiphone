//
//  ProductCountSetItem.h
//  康岁健康
//
//  Created by 低调 on 14/10/30.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol ProductCountSetItemDelegate <NSObject>

-(void)productCountSet:(int)count;

@end
#import <UIKit/UIKit.h>

@interface ProductCountSetItem : UIView
@property id<ProductCountSetItemDelegate> delegate;
@property IBOutlet UILabel *countLabel;
-(IBAction)addCount:(id)sender;
-(IBAction)reduceCount:(id)sender;
@end

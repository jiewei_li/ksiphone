//
//  ProductAttSimpleChoseButton.h
//  康岁健康
//
//  Created by 低调 on 14/10/27.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol ProductAttSimpleChoseButtonDelegate <NSObject>

-(void)productAttSimpleChoseButtonChosed:(id)sender;

@end
#import <UIKit/UIKit.h>
#import "ProductAttributeValuesData.h"

@interface ProductAttSimpleChoseButton : UIView
{
    ProductAttributeValuesData *_data;
}
@property (nonatomic) ProductAttributeValuesData *data;
@property IBOutlet UIButton *choseButton;
@property id<ProductAttSimpleChoseButtonDelegate>delegate;
-(IBAction)selfSelect:(id)sender;
-(void)setUnselected;
@end

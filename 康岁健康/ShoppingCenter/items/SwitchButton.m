//
//  SwitchButton.m
//  康岁健康
//
//  Created by 低调 on 14/11/30.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "SwitchButton.h"

@implementation SwitchButton
-(void)setTitle:(NSString *)title type:(NSString *)type andFrame:(CGRect)frame{
    if (self) {
        self.frame = frame;
        [self setBackgroundImage:[UIImage imageNamed:@"支付方式-背景"] forState:UIControlStateNormal];
        [self setBackgroundImage:[UIImage imageNamed:@"支付方式选择-背景"] forState:UIControlStateSelected];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [self setTitleColor:NavColor forState:UIControlStateNormal];
        [self setTitle:title forState:UIControlStateNormal];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeState:) name:type object:nil];
        [self addTarget:self action:@selector(choseSelf) forControlEvents:UIControlEventTouchUpInside];
        _type = type;
    }
}

-(void)changeState:(NSNotification *)s{
    SwitchButton *bu = s.object;
    if ([self.titleLabel.text isEqualToString:bu.titleLabel.text] && [self.type isEqualToString:bu.type]) {
        self.selected = YES;
    }
    else
    {
        self.selected = NO;
    }
}

-(void)choseSelf{
    self.selected = YES;
    [[NSNotificationCenter defaultCenter]postNotificationName:_type object:self];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:_type object:nil];
}
@end

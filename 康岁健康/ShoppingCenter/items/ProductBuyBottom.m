//
//  ProductBuyBottom.m
//  康岁健康
//
//  Created by 低调 on 14/10/30.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ProductBuyBottom.h"

@implementation ProductBuyBottom

-(void)awakeFromNib{
    [self.cartNumber setTitle:user_defaults_get_string(NumberOfCrat) forState:UIControlStateNormal];
}
-(void)setCount:(NSString *)count{
    [self.cartNumber setTitle:count forState:UIControlStateNormal];
}
-(IBAction)addCart:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(addCart:)]) {
        [self.delegate addCart:NO];
    }
}
-(IBAction)buy:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(buy)]) {
        [self.delegate buy];
    }
}
-(IBAction)toCart:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(toCart)]) {
        [self.delegate toCart];
    }
}

@end

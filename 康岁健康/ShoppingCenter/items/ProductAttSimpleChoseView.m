//
//  ProductAttSimpleChoseView.m
//  康岁健康
//
//  Created by 低调 on 14/10/25.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ProductAttSimpleChoseView.h"
#import "ProductAttSimpleChoseViewItem.h"
@implementation ProductAttSimpleChoseView

-(void)awakeFromNib{
    self.frame = DEVICE_BOUNDS;
//    _isfirst = YES;
    self.sureButton.bottom = self.mainChoseView.height-5;
    self.mainChoseView.top = self.height;
    _selectedAtt = [[NSMutableArray alloc]init];
}
-(void)setHidden:(BOOL)hidden{
    if (!_isfirst) {
         super.hidden = hidden;
        self.mainChoseView.top = self.height;
        _isfirst = YES;
    }
    else if (hidden) {
        [UIView animateWithDuration:1 animations:^{
            self.mainChoseView.top = self.height;
        } completion:^(BOOL finished) {
           self.mainChoseView.top = self.height;
            super.hidden = hidden;
        }];
    }
    else
    {
        super.hidden = hidden;
        [UIView animateWithDuration:1 animations:^{
            self.mainChoseView.bottom = self.height;
        } completion:^(BOOL finished) {
            self.mainChoseView.bottom = self.height;
        }];
    }
}

-(void)setAttArry:(NSMutableArray *)attArry{
    _attArry = attArry;
    self.choseAttView.delegate = self;
    self.choseAttView.dataSource =self;
    [self.choseAttView reloadData];
}

-(IBAction)setSelfHidden:(id)sender{
    self.hidden = YES;
}
-(IBAction)sure:(id)sender{
//    ProductAttSimpleChoseViewItem *cell = [tableView dequeueReusableCellWithIdentifier:identify];

    [_selectedAtt removeAllObjects];
    for(int i = 0; i<_attArry.count; i++){
       NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
       ProductAttSimpleChoseViewItem *cell =  (ProductAttSimpleChoseViewItem *)[self.choseAttView cellForRowAtIndexPath:indexPath];
        if (!cell.selectedData) {
            ProductAttributeData *data = [_attArry objectAtIndex:i];
            if ([data.IsRequired intValue] == 1) {
                [UIAlertView showMessage:[NSString stringWithFormat:@"请先选择属性：%@",data.TextPrompt]];
                return;
            }
            
        }
        else
        {
            [_selectedAtt addObject:cell.selectedData];
        }
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(productAttSimpleChoseViewFinished:)]) {
        [self.delegate productAttSimpleChoseViewFinished:_selectedAtt];
    }
    self.hidden = YES;
}


#pragma mark - table
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _attArry.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identify = @"ProductAttSimpleChoseViewItem";
    ProductAttSimpleChoseViewItem *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [ProductAttSimpleChoseViewItem loadFromNIB];
    }
    cell.data = [_attArry objectAtIndex:indexPath.row];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identify = @"ProductAttSimpleChoseViewItem";
    ProductAttSimpleChoseViewItem *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [ProductAttSimpleChoseViewItem loadFromNIB];
    }
    cell.data = [_attArry objectAtIndex:indexPath.row];
    NSLog(@"%f",cell.height);
    return cell.height+15;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end

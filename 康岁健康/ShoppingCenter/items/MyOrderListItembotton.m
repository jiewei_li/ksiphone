//
//  MyOrderListItembotton.m
//  康岁健康
//
//  Created by 低调 on 14-10-16.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "MyOrderListItembotton.h"

@implementation MyOrderListItembotton
-(IBAction)deletePressed:(id)sender{
    [self.delegate delegateList:_data];
}
-(IBAction)toPay:(id)sender{
    [self.delegate payList:_data];
}
-(IBAction)sureReceivedPressed:(id)sender{
    [self.delegate sureList:_data];
}
-(IBAction)tiXingPressed:(id)sender{
    [self.delegate tiXing];
}

-(void)setData:(OrderPageListData *)adata{
    _data = adata;    
    if ([adata.PaymentStatusStatusId intValue]== 10) {
        self.sureReveivedBtn.hidden = YES;
        
    }
    else if([adata.PaymentStatusStatusId intValue]== 50){
        self.sureReveivedBtn.hidden = YES;
        self.payBtn.hidden = YES;
    }
    else if([adata.PaymentStatusStatusId intValue]== 30){
        
         if([adata.ShippingStatusId intValue]== 10){
            self.sureReveivedBtn.hidden = YES;
            self.payBtn.hidden = YES;
             self.delegateBtn.hidden = YES;
        }
         else if([adata.ShippingStatusId intValue]== 30){
             self.payBtn.hidden = YES;
             self.delegateBtn.hidden = YES;
         }
         else if([adata.ShippingStatusId intValue]== 20){
             self.sureReveivedBtn.hidden = YES;
             self.payBtn.hidden = YES;
             self.delegateBtn.hidden = YES;
             self.tiXingBtn.hidden = NO;
         }

    }
    else {
        self.sureReveivedBtn.hidden = YES;
    }

    if (self.payBtn.hidden) {
        self.delegateBtn.right = self.sureReveivedBtn.right;

    }
}

@end

//
//  ProductAttSimpleChoseViewItem.h
//  康岁健康
//
//  Created by 低调 on 14/10/26.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductAttributeData.h"
#import "DrawLineView.h"
#import "ProductAttSimpleChoseButton.h"
@interface ProductAttSimpleChoseViewItem : UITableViewCell<ProductAttSimpleChoseButtonDelegate>
{
    ProductAttributeData       *_data;
    ProductAttributeData *_selectedData;

}
@property (nonatomic) ProductAttributeData       *selectedData;
@property (nonatomic) ProductAttributeData       *data;
@property IBOutlet    UILabel                    *nameLabel;
@property IBOutlet    DrawLineView               *bottomLine;
@end

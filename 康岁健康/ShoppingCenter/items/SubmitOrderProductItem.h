//
//  SubmitOrderProductItem.h
//  康岁健康
//
//  Created by 低调 on 14/11/1.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingCartItemsData.h"
@interface SubmitOrderProductItem : UIView
@property (nonatomic)ShoppingCartItemsData *itemData;
@property IBOutlet UIImageView *productImage;
@property IBOutlet UILabel     *productNameLabel;
@property IBOutlet UILabel     *productAttLabel;
@property IBOutlet UILabel     *priceLabel;
@property IBOutlet UILabel     *qualityLabel;

@end

//
//  ProductItem.h
//  康岁健康
//
//  Created by cerastes on 14-9-25.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol toGoodsDetailDelegate <NSObject>

-(void)toGoodsDetailPressed:(id)sender;

@end
#import <UIKit/UIKit.h>
#import "ProductListData.h"

@interface ProductItem : UIView
{
    ProductListData *_data;
}
-(id)initWithFrame:(CGRect)frame;
@property IBOutlet UIImageView *prouductImage;
@property IBOutlet UILabel *productName;
@property IBOutlet UILabel *shortDescription;
@property IBOutlet UILabel *price;
@property (nonatomic) ProductListData *data;
@property id<toGoodsDetailDelegate> delegate;
-(IBAction)toGoodsDetail:(id)sender;
@end

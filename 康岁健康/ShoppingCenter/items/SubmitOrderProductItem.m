//
//  SubmitOrderProductItem.m
//  康岁健康
//
//  Created by 低调 on 14/11/1.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "SubmitOrderProductItem.h"
#import "ShoppingCartItemsAttributeData.h"
@implementation SubmitOrderProductItem

-(void)setItemData:(ShoppingCartItemsData *)itemData{
    [self.productImage sd_setImageWithURL:[NSURL URLWithString:[itemData.PictureUrl notNull]] placeholderImage:[UIImage imageNamed:@""]];
    self.productNameLabel.text = itemData.ProductName;
    NSString *att = @"";
    for (ShoppingCartItemsAttributeData *data in itemData.Attributes) {
        for (NSDictionary *dic in data.AttributeIds) {
            att = [NSString stringWithFormat:@"%@ %@",att,[dic objectForKey:@"Name"]];
        }
    }
    self.productAttLabel.text = att;
    self.priceLabel.text = itemData.Price;
    self.qualityLabel.text = itemData.Quantity;
}

@end

//
//  ShoppingCartViewController.h
//  康岁健康
//
//  Created by 低调 on 14-10-11.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"

@interface ShoppingCartViewController : BaseNextViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    UILabel     *_sumPayLabel;
    UIButton    *_goPayButton;
    UIButton    *_selectAllButton;
}

@end

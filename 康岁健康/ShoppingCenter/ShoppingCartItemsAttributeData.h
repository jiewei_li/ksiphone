//
//  ShoppingCartItemsAttributeData.h
//  康岁健康
//
//  Created by 低调 on 14-10-11.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShoppingCartItemsAttributeData : NSObject
@property NSString  *Id;
@property NSString *Text;
@property NSArray  *AttributeIds;
@end

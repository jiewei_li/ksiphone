//
//  MyOrderDetail.h
//  康岁健康
//
//  Created by 低调 on 14/11/27.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"
#import "OrderPageListData.h"
@interface MyOrderDetail : BaseNextViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    OrderPageListData *_data;
}
@property IBOutlet UITableView *tableView;
@property OrderPageListData *data;
@property IBOutlet UIView  *headView;
@property IBOutlet UILabel *orderStateLabel;
@property IBOutlet UILabel *orderNumberLabel;
@property IBOutlet UILabel *countMoneyLabel;
@property IBOutlet UILabel *productMoneyLabel;
@property IBOutlet UILabel *yunFeiLabel;


@property IBOutlet UIView  *bottonView;
@property IBOutlet UILabel *userNameLabel;
@property IBOutlet UILabel *userPhoneLabel;
@property IBOutlet UILabel *userAddressLabel;
@property IBOutlet UILabel *payMethodLabel;
@property IBOutlet UILabel *sendMethodLabel;
@property IBOutlet UILabel *fapiaoInfoLabel;
@property IBOutlet UILabel *fapiaoTitleLabel;

-(id)initOrderPageListData:(OrderPageListData *)data;
@end

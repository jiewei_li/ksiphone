//
//  ShoppingMainViewController.m
//  康岁健康
//
//  Created by cerastes on 14-9-21.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ShoppingMainViewController.h"
#import "EScrollerView.h"
#import "ProductSlideImageInfoData.h"
#import "ProductListData.h"
#import "ProductInfoViewController.h"
#import "ShoppingCartViewController.h"
#import "MyOrderList.h"
#import "YuYueMainViewController.h"
#import "GoodList.h"
#import "LogInViewController.h"
@interface ShoppingMainViewController ()
{
    EScrollerView *_top;
    NSMutableArray *_idS;
    NSMutableArray *_idS2;

    UIView         *_headView;
}
@end

@implementation ShoppingMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"健康365商城";
    _headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 170)];
    _pageIndex = -1;
    _productList = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.width,_mainView.height)style:UITableViewStyleGrouped];
//    _productList.backgroundColor = [UIColor colorWithRed:248/255. green:248/255. blue:248/255. alpha:1];
    _productList.delegate = self;
    _productList.dataSource = self;
    _productList.tableHeaderView = _headView;

    [self addSubView:_productList];
    
    _top = [[EScrollerView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, self.view.width*0.31)];
    [_headView addSubview:_top];
    _top.backgroundColor = [UIColor clearColor];
    
    
    _producetArry = [[NSMutableArray alloc]init];
    
    [NetRequest requestShopWithType:GET_CATOGORY_PAGE_LIST withGetUrl:ADDRESS_GET_CATOGORY_PAGE_LIST andGetInfo:nil result:^(id resultSuccess) {
        _idS = [[NSMutableArray alloc]initWithArray:[resultSuccess objectForKey:@"Data"]];
        _idS2 = [[NSMutableArray alloc]init];

        NSMutableArray *ii = [[NSMutableArray alloc]init];
        for (NSDictionary *dic in _idS) {
            NSArray *arr = [dic objectForKey:@"SubCatagories"];
//            for (NSDictionary *Dd in arr) {
                [ii addObjectsFromArray:arr];
//            }
        }
        [_idS2 addObjectsFromArray:ii];
        [self requestProductList];
    } err:^(id resultError) {
        
    }];
    
    [self createTopImages];

    [self createMainChose];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _idS2.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"dsf"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"dsf"];
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSMutableArray *arr = [self dataForIndex:section];
    if (arr.count==0) {
        return 0.1;
    }
    return 44;
}

//-(float)ta
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSMutableArray *arr = [self dataForIndex:section];
    if (arr.count==0) {
        return [[UIView alloc]init];;
    }
    GoodList *list = [[[NSBundle mainBundle]loadNibNamed:@"GoodList" owner:self options:nil] lastObject];
    NSDictionary *dic = [_idS2 objectAtIndex:section];
    
    list.tltle1.text = [dic objectForCJKey:@"Name"];
    NSString *s= @"";
    for (NSDictionary *dicT in _idS) {
        if ([[dicT objectForKey:@"Id"] integerValue] == [[dic objectForKey:@"ParentCatagoryId"] integerValue]) {
            s = [dicT objectForKey:@"Name"];
        }
    }
    list.tltle2.text = s;
    return list;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *bov = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 10)];
    NSMutableArray *arr = [self dataForIndex:section];
    for (int i = 0;i<arr.count ;i++) {
        ProductItem *item = [[[NSBundle mainBundle]loadNibNamed:@"ProductItem" owner:self options:nil] lastObject];
        item.data = [arr objectAtIndex:i];
        item.top = i/2*item.height;
        item.left = i%2*item.width;
        item.delegate = self;
        [bov addSubview:item];
    }
    bov.frame = CGRectMake(0, 0, self.view.width, arr.count>0?(arr.count+1)/2*174:0);

//    int i = arr.count%2;
//    bov.height = (arr.count+1)/2*223+170;

    return bov;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    NSMutableArray *arr = [self dataForIndex:section];
    if (arr.count == 0) {
        return 0.1;
    }
    return (arr.count+1)/2*174>0?(arr.count+1)/2*174+10:10;
}
-(NSMutableArray *)dataForIndex:(NSInteger)section{
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    NSDictionary *dic = [_idS2 objectAtIndex:section];
    for (ProductListData *data in _producetArry) {
        if ([data.catagoryId integerValue] == [[dic objectForKey:@"Id"] intValue])
        {
            
            [arr addObject:data];
        }
    }
    return arr;
}

-(void)viewDidAppear:(BOOL)animated
{
     [self getStoreOverview];
}
-(void)createTopImages{
    [NetRequest requestShopWithType:PRODUCT_SLIDE_IMAGE_INFO withGetUrl:ADDRESS_GET_PRODUCT_SLIDE_IMAGE_INFO andGetInfo:nil result:^(id resultSuccess) {
        NSLog(@"*********%@*********",resultSuccess);
        NSMutableArray *imageArr = [[NSMutableArray alloc]init];
        for (ProductSlideImageInfoData *idata in resultSuccess) {
            [imageArr addObject:idata];
        }
        if (imageArr.count==0) {
            UIImageView *bac = [[UIImageView alloc]initWithFrame:_top.bounds];
            bac.image = [UIImage imageNamed:@"topImage.png"];
            [_top addSubview:bac];
        }
        __block ShoppingMainViewController *wself = self;
        [_top setImageArray:imageArr Block:^(id i) {
            ProductSlideImageInfoData *data = i;
//            pData.
//            @property (nonatomic)NSString *Id;
//            @property (nonatomic)NSString *Name;
//            @property (nonatomic)NSString *PictureUrl;
//            @property (nonatomic)NSString *Price;
//            @property (nonatomic)NSString *ProductTypeId;
//            @property (nonatomic)NSString *ShortDescription;
            NSDictionary *productIdDic = [NSDictionary dictionaryWithObjectsAndKeys:data.ActionParameter,@"productId", nil];
            
            [NetRequest requestShopWithType:GET_PRODUCT_INFO withGetUrl:ADDRESS_GET_PRODUCT_INFO andGetInfo:productIdDic result:^(id resultSuccess) {
                ProductListData *data = [[ProductListData alloc]init];
                data.Id = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"Id"];
                data.Name = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"Name"];
                data.PictureUrl = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"PictureUrl"];
                data.Price = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"Price"];
                data.ProductTypeId = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"ProductTypeId"];
                data.ShortDescription = [[resultSuccess objectForKey:@"Data"] objectForCJKey:@"ShortDescription"];
                ProductInfoViewController *productInfoViewController = [[ProductInfoViewController alloc]initWithProductInfo:data];
                [wself.navigationController pushViewController:productInfoViewController animated:YES];

            }err:^(id resultError) {
            }];



            NSLog(@"%@",data.pictureurl);
        }];
        
    } err:^(id resultError) {
        UIImageView *bac = [[UIImageView alloc]initWithFrame:_top.bounds];
        bac.image = [UIImage imageNamed:@"topImage.png"];
        [_top addSubview:bac];
    }];
}
-(void)createMainChose{
    NSArray *iconArry   = @[@"shoppingCart",@"dealList",@"goCheatIng"];
    NSArray *titleArr   = @[@"购物车",@"订单处理",@"体检预约"];
    NSArray *bridgeArry = @[@"0",@"0",@"0"];
    for (int i = 0; i <3; i++) {
        ShoppingMainItem *item = [[ShoppingMainItem alloc]initWithFrame:CGRectMake(i*self.view.width/3, self.view.width*0.31+5, self.view.width/3, 44)];
        item.icon        = iconArry[i];
        item.title       = titleArr[i];
        item.bridgeValue = bridgeArry[i];
        if (i == 0) {
            item.type = ShoppingItem;
        }
        else if (i == 1) {
            item.type = DealShoppingList;
        }
        else if (i == 2) {
            item.type = OrderChectting;
        }
        item.delegate = self;
        item.frame = CGRectMake(i*self.view.width/3, self.view.width*0.31+5, self.view.width/3, self.view.width/3+30);
        [_headView addSubview:item];
    }
}

-(void)requestProductList{
//    if (_pageIndex == -2) {
//        return;
//    }
//    else{
//        _pageIndex++;
//    }
//    Pageindex：开始商品  pagesize：商
    for (NSDictionary *dic in _idS2) {
//        NSMutableArray *ii = [[NSMutableArray alloc]init];
//        [ii addObject:[dic objectForKey:@"Id"]];
//        for (NSString  *s in ii) {
            NSDictionary *postDic = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",0],@"Pageindex",@"100",@"pageSize",[dic objectForKey:@"Id"],@"catagoryId", nil];
            [self unableSelf];
            [self performSelector:@selector(enableSelf) withObject:nil afterDelay:5];
            [NetRequest requestShopWithType:PRODUCT_PAGE_LIST withGetUrl:ADDRESS_GET_PRODUCT_PAGE_LIST andGetInfo:postDic result:^(id resultSuccess) {
                [self enableSelf];
                for (id data in resultSuccess) {
                    [_producetArry addObject:data];
                }
                
                [self reloadProductList];
                [_productList reloadData];
                if ([(NSArray *)resultSuccess count] != 10) {
                    _pageIndex = -2;
                }
            }err:^(id resultError) {
                [self enableSelf];
            }];

//        }
    }
}

-(void)reloadProductList{
//    [_productList removeAllSubviews];
//    for (UIView *v in _productList.subviews) {
//        if ([v isKindOfClass:[ProductItem class]]) {
//            [v removeFromSuperview];
//        }
//    }
//    for (int i = 0;i<_producetArry.count ;i++) {
//        ProductItem *item = [[[NSBundle mainBundle]loadNibNamed:@"ProductItem" owner:self options:nil] lastObject];
//        item.data = [_producetArry objectAtIndex:i];
//        item.top = i/2*item.height+170;
//        item.left = i%2*item.width;
//        item.delegate = self;
//        [_productList addSubview:item];
//    }
//    int i = _producetArry.count%2;
//    _productList.contentSize = CGSizeMake(self.view.width, (i == 0?_producetArry.count:_producetArry.count+1)/2*223+170);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark - 点击去商品页
-(void)toGoodsDetailPressed:(id)sender{
    ProductItem *item = sender;
    NSLog(@"GoodsId = %@",item.data.Id);
    ProductInfoViewController *productInfoViewController = [[ProductInfoViewController alloc]initWithProductInfo:item.data];
    [self.navigationController pushViewController:productInfoViewController animated:YES];
}

#pragma mark - 点击顶部选项按钮
-(void)shopTypeItemChosed:(id)sender{
    if (![UserInfos isUserLogIn]) {
        LogInViewController *logIn = [[LogInViewController alloc]initWithNibName:@"LogInViewController" bundle:nil];
        [self.navigationController pushViewController:logIn animated:YES];
        return;
    }

    ShoppingMainItem *item = sender;
    NSLog(@"ShoppingMainItemchosed : %d",item.type);
    if (item.type == ShoppingItem) {
        [self getShoppingCartItems];
    }
    else if (item.type == DealShoppingList){
        MyOrderList *list = [[MyOrderList alloc]init];
        [self.navigationController pushViewController:list animated:YES];
    }
    else if(item.type == OrderChectting){
        YuYueMainViewController *yuyueMain = [[YuYueMainViewController alloc]initWithNibName:@"YuYueMainViewController" bundle:nil];
        [self.navigationController pushViewController:yuyueMain animated:YES];
    }
}

-(void)getStoreOverview{
    [NetRequest requestShopWithType:STORE_OVERVIEW withGetUrl:ADDRESS_GET_Store_OverView andGetInfo:[[NSDictionary alloc]initWithObjectsAndKeys:[UserInfos customerId],@"customerid", nil] result:^(id resultSuccess) {
    }err:^(id resultError) {
    }];

}
#pragma mark - scrollviewdelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    CGPoint offset = scrollView.contentOffset;  // 当前滚动位移
//    CGRect bounds = scrollView.bounds;          // UIScrollView 可视高度
//    CGSize size = scrollView.contentSize;         // 滚动区域
//    UIEdgeInsets inset = scrollView.contentInset;
//    float y = offset.y + bounds.size.height - inset.bottom;
//    float h = size.height;
//    
//    float reload_distance = 10;
//    if (y > (h + reload_distance)) {
//        [self requestProductList];
//    }
}



-(void)getShoppingCartItems{
    ShoppingCartViewController *shoppingCartViewController = [[ShoppingCartViewController alloc]init];
    [self.navigationController pushViewController:shoppingCartViewController animated:YES];
}
@end

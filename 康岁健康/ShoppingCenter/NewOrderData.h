//
//  NewOrderData.h
//  康岁健康
//
//  Created by 低调 on 14/11/4.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewOrderData : NSObject
@property NSString *CreatedOnUtc;
@property NSString *InvoiceAddress;
@property NSString *InvoiceAddressId;
@property NSString *OrderId;
@property NSString *OrderItemsCount;
@property NSString *OrderStatusName;
@property NSString *OrderStatusId;
@property NSString *OrderTotal ;
@property NSString *PaymentStatusName ;
@property NSString *PaymentStatusId;
@property NSString *ShippingAddress;
@property NSString *ShippingAddressId;
@property NSString *ShippingStatusName;
@property NSString *ShippingStatusId;

@end

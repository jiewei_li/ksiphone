//
//  ProductSlideImageInfo.h
//  康岁健康
//
//  Created by cerastes on 14-9-23.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductSlideImageInfoData : NSObject
@property NSString *link;
@property NSString *pictureid;
@property NSString *pictureurl;
@property NSString *ActionParameter;
@property NSString *Action;
@property NSString *text;
@end

//
//  ProductAttributeData.h
//  康岁健康
//
//  Created by 低调 on 14-10-7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductAttributeData : NSObject
@property (nonatomic)NSString *Id;              //属性Id号，如属性“体检城市”的Id
@property (nonatomic)NSString *Name;            //属性名称
@property (nonatomic)NSString *TextPrompt;      //属性提示
@property (nonatomic)NSString *DefaultValue;    //缺省值；无缺省值时选择第一个
@property (nonatomic)NSString *IsRequired;      //是否强制选择
@property (nonatomic)NSString *SelectionType;   //是否多选
@property (nonatomic)NSArray  *AttributeValues; //是否多选
@end

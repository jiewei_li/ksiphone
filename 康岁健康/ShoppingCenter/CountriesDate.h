//
//  CountriesDate.h
//  康岁健康
//
//  Created by 低调 on 14-10-9.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountriesDate : NSObject
@property NSString *Id;
@property NSString *Name;
@property NSString *DisplayOrder;      //1 注:所有需要排序的都在后台处理，客户端按照处理后的自然顺序做处理。比如，后台可能按照国家的首字母排序，而不是按照DisplayOrder顺序来排。
@property NSString *TwoLetterIsoCode;  //两个字母ISO代码,
@property NSString *NumericIsoCode;    // ISO数字代码
@property NSString *InvoiceAllowed;    //是否允许账单 ，暂不使用
@property NSString *ShippingAllowed;   //是否允许配送，暂不使用
@end

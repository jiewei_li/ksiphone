//
//  CountryByIdData.h
//  康岁健康
//
//  Created by 低调 on 14-10-9.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountryByIdData : NSObject
@property NSString *Id;
@property NSString *Name;
@property NSString *DisplayOrder;
@property NSString *TwoLetterIsoCode;
@property NSString *NumericIsoCode;
@end

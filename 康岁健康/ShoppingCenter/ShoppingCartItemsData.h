//
//  ShoppingCartItemsData.h
//  康岁健康
//
//  Created by 低调 on 14-10-7.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShoppingCartItemsData : NSObject
@property (nonatomic)NSString *ProductId;//产品编号

@property (nonatomic)NSString *Id;//产品编号

@property (nonatomic)NSString *ProductName;//产品名称

@property (nonatomic)NSString *Description;//产品简介

@property (nonatomic)NSString *Price;//价格

@property (nonatomic)NSString *Quantity;//数量

@property (nonatomic)NSString *PictureUrl;//产品图片

@property (nonatomic)BOOL     Selected;//产品图片

@property (nonatomic)NSArray  *Attributes;
@end

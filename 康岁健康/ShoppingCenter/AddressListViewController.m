//
//  AddressListViewController.m
//  康岁健康
//
//  Created by 低调 on 14/11/4.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "AddressListViewController.h"
#import "AddressCell.h"
#import "AddAddressView.h"

@interface AddressListViewController ()
{
    UIButton *_editButton;
}
@end

@implementation AddressListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"收货地址";
    _selectedIndex = 0;
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, self.headView.height, _mainView.width, _mainView.height-self.headView.height) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self addSubView:self.tableView];
    
    [self addSubView:self.headView];
    _editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _editButton.frame = CGRectMake(_navView.width-70, _navView.height-35, 66, 30);
    [_editButton setTitle:@"编辑" forState:UIControlStateNormal];
    [_editButton setTitle:@"完成" forState:UIControlStateSelected];
    [_editButton setTitleColor:CJWhiteColor forState:UIControlStateNormal];
    [_editButton addTarget:self action:@selector(edit) forControlEvents:UIControlEventTouchUpInside];
    [_navView addSubview:_editButton];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addAddressSuccessed) name:@"addAddressSuccessed" object:nil];


}

-(void)viewDidAppear:(BOOL)animated{
    [NetRequest requestShopWithType:GET_CUSTOMER_ADDRESS withGetUrl:ADDRESS_GET_CUSTOMER_ADDRESS andGetInfo:[NSDictionary dictionaryWithObjectsAndKeys:[UserInfos customerId],@"CustomerId", nil] result:^(id resultSuccess) {
        self.addressArry = resultSuccess;
        [self.tableView reloadData];
    } err:^(id resultError) {
        
    }];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"addAddressSuccessed" object:nil];
}

//-(void)requestList{
//    
//}

-(void)addAddressSuccessed{
        _selectedIndex = 0;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)addAddress:(id)sender{
    AddAddressView *add = [[AddAddressView alloc]init];
    add.adressSaveType = AddAddress;
    [self.navigationController pushViewController:add animated:YES];
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.addressArry.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *deq = @"AddressCell";
    AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:deq];
    if (!cell) {
        cell = [AddressCell loadFromNIB:deq];
    }
    if (indexPath.section == _selectedIndex) {
        [cell setImgHidden:NO];
    }
    else
    {
        [cell setImgHidden:YES];
    }
    if(_editButton.selected){
        [cell setEdit:YES];
    }
    else{
        [cell setEdit:NO];
    }
    cell.address = [_addressArry objectAtIndex:indexPath.section];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 5;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    if (section == 0) {
//        return 96;
//    }
    return 1;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_editButton.selected) {
        AddressesData *data = [_addressArry objectAtIndex:indexPath.section];
        AddAddressView *add = [[AddAddressView alloc]init];
        add.adressSaveType = UpDataAddress;
        add.addressData = data;
        [self.navigationController pushViewController:add animated:YES];
    }
    else
    {
        _selectedIndex = indexPath.section;
        AddressesData *addressData = [_addressArry objectAtIndex:indexPath.section];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeAddress" object:addressData];
        [tableView reloadData];
    }
   
}

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    if (section == 0) {
//        return self.headView;
//    }
//    else
//        return [[UIView alloc]init];
//}

-(void)edit{
    if (_editButton.selected) {
        _editButton.selected = NO;
    }
    else
    {
        _editButton.selected = YES;
    }
    [self.tableView reloadData];
    
}
@end

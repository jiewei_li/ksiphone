//
//  AddressesData.h
//  康岁健康
//
//  Created by 低调 on 14/11/3.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressesData : NSObject
@property NSString *Id;
@property NSString *FirstName;
@property NSString *LastName;
@property NSString *Email;
@property NSString *Company;
@property NSString *CountryId;
@property NSString *CustomerId;
@property NSString *StateProvinceId;
@property NSString *StateProvinceName;
@property NSString *CityId;
@property NSString *CityName;
@property NSString *Address;
@property NSString *ZipPostalCode;
@property NSString *PhoneNumber;
@property NSString *FaxNumber;

@end

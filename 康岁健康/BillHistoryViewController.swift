//
//  BillHistoryViewController.swift
//  亳州医保
//
//  Created by ljw on 15/3/5.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit

class BillHistoryViewController: UIViewController {
    @IBOutlet weak var tb2: UITableView!
  
    @IBOutlet weak var tb1: UITableView!
 @IBOutlet var scrollView: UIScrollView!
    
    
    var BillHistory = [
        ["医院":"亳州市人民医院","总费用":"2312.00","报销金额":"1800.00","报销日期":"2015-1-1"]
        , ["医院":"亳州市人民医院","总费用":"2312.00","报销金额":"1800.00","报销日期":"2015-1-2"]
        , ["医院":"亳州市人民医院","总费用":"2312.00","报销金额":"1800.00","报销日期":"2015-1-3"]
        
        
        
    ]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.scrollView.contentSize = CGSizeMake(320, 640-44-33);
        self.scrollView.scrollEnabled = true;
       // self.scrollView.delegate = self;
        self.scrollView.bounces = true;
      //  self.scrollView.pagingEnabled = YES;
       self.scrollView.showsHorizontalScrollIndicator = false;
  var sb =  UIStoryboard(name: "Storyboard", bundle: nil)// [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    var vc =    sb.instantiateViewControllerWithIdentifier("tbhead") as UIViewController
    var tbhead1 =     vc.view.viewWithTag(100)!
 //        var tbhead2 =     vc.view.viewWithTag(110)
//        var headview = UIView(frame: CGRectMake(0, 0, 320, 34))
//        var headview1 = UIView(frame: CGRectMake(0, 0, 320, 34))
//        var img = UIImage(named: "b1")
//        var imgv=UIImageView(frame: CGRectMake(0, 0, 110, 34))
//        imgv.image=img
//        
//        var img1 = UIImage(named: "b1")
//        var imgv1=UIImageView(frame: CGRectMake(0, 0, 214, 34))
//        imgv1.image=img1
//        
//        headview.addSubview(imgv)
//        headview1.addSubview(imgv1)
//        //  headview.backgroundColor = UIColor(red: 204, green: 204, blue: 204, alpha: 1)
  

      var c =  tbhead1.backgroundColor
        self.tb1.rowHeight=44
        self.tb2.rowHeight=44
        self.tb1.tableHeaderView=tbhead1
       //  self.tb2.tableHeaderView=tbhead2
//        self.tb2.tableHeaderView=headview1
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return BillHistory.count
        //return doctors.count
        
    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
       // let cell=tableView.dequeueReusableCellWithIdentifier("Cell",forIndexPath: indexPath) as UITableViewCell
   
        var d =  BillHistory[indexPath.row] as NSDictionary
        
        if(tableView==self.tb1)
        {
            let c1=tableView.dequeueReusableCellWithIdentifier("Cell",forIndexPath: indexPath) as BillHistoryTableViewCell1

            c1.lab医院.text=d["医院"] as String
            // .textLabel.text=d["医院"] as String
            return c1
        }
        else
        {
            let c2=tableView.dequeueReusableCellWithIdentifier("Cell",forIndexPath: indexPath) as BillHistoryTableViewCell2
            c2.lab总费用.text=d["总费用"]  as String
            c2.lab报销日期.text = d["报销日期"] as String
            c2.lab报销金额.text = d["报销金额"] as String
            

        return c2
        }

    }
    
    
    
//     func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        var headview = UIView(frame: CGRectMake(0, 0, 320, 30))
//        headview.backgroundColor=UIColor(red: 204, green: 204, blue: 204, alpha: 1)
//        return headview
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

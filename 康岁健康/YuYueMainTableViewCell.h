//
//  YuYueMainTableViewCell.h
//  康岁健康
//
//  Created by 低调 on 14/12/9.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ExamOrderData;
typedef void (^examSetBlock)(id sender);
typedef void (^examCenterBlock)(id sender);
typedef void (^examDataBlock)(id sender);
typedef void (^examPriceBlock)(id sender);
typedef void (^payNowBlock)(id sender);
@interface YuYueMainTableViewCell : UITableViewCell
{
    ExamOrderData *_data;
}
@property (nonatomic)    ExamOrderData *data;
@property IBOutlet UILabel *nameLable;
@property IBOutlet UILabel *realityLable;
@property IBOutlet UILabel *examsetNameLabel;
@property IBOutlet UILabel *examCenterLable;
@property IBOutlet UILabel *dateLable;
@property IBOutlet UILabel *priceLable;
@property IBOutlet UILabel *statusLable;
@property IBOutlet UILabel *leftTimeLable;
@property IBOutlet UIView  *bottomView;
@property IBOutlet UIButton  *b1;

@property (strong) examSetBlock examsetBlock;
@property (strong) examCenterBlock examcenterBlock;
@property (strong) examDataBlock examdataBlock;
@property (strong) examPriceBlock exampriceBlock;
@property (strong) payNowBlock paynowBlock;

-(IBAction)examSetDetail:(id)sender;
-(IBAction)examCenterDetail:(id)sender;
-(IBAction)examDataDetail:(id)sender;
-(IBAction)examPriceDetail:(id)sender;
-(IBAction)payNow:(id)sender;

@end

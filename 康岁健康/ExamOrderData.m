//
//  ExamOrderData.m
//  康岁健康
//
//  Created by 低调 on 14/12/9.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "ExamOrderData.h"

@implementation ExamOrderData
-(void)setData:(NSDictionary *)dic
{
    self.customer_id = [dic objectForCJKey:@"customer_id"];
    self.customer_name = [dic objectForCJKey:@"customer_name"];

    self.date = [dic objectForCJKey:@"date"];

    self.exam_center_id = [dic objectForCJKey:@"exam_center_id"];

    self.exam_center_name = [dic objectForCJKey:@"exam_center_name"];

    self.exam_set_id = [dic objectForCJKey:@"exam_set_id"];

    self.exam_set_name = [dic objectForCJKey:@"exam_set_name"];

    self.Id = [dic objectForCJKey:@"id"];

    self.leftTime = [dic objectForCJKey:@"leftTime"];
    self.pay_mode_id = [dic objectForCJKey:@"pay_mode_id"];

    self.price = [dic objectForCJKey:@"price"];

    self.status_id = [dic objectForCJKey:@"status_id"];


}

-(BOOL)isEqual:(id)object{
    if (![object isKindOfClass:[self class]]) {
        return NO;
    }
    else
    {
        ExamOrderData *data = object;
        if ([data.customer_id isEqualToString:self.customer_id]
            &&[data.customer_name isEqualToString:self.customer_name]
            &&[data.date isEqualToString:self.date]
            &&[data.exam_center_id isEqualToString:self.exam_center_id]
            &&[data.exam_center_name isEqualToString:self.exam_center_name]
            &&[data.Id isEqualToString:self.Id]
            &&[data.exam_set_name isEqualToString:self.exam_set_name]
            &&[data.exam_set_id isEqualToString:self.exam_set_id]
            &&[data.leftTime isEqualToString:self.leftTime]
            &&[data.pay_mode_id isEqualToString:self.pay_mode_id]
            &&[data.price isEqualToString:self.price]
            &&[data.status_id isEqualToString:self.status_id]) {
            return YES;
        }
    }
    return NO;
}
@end

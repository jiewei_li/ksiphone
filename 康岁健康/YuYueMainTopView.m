//
//  YuYueMainTopView.m
//  康岁健康
//
//  Created by 低调 on 14/12/12.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "YuYueMainTopView.h"
#import "ExamOrderData.h"

@implementation YuYueMainTopView
-(void)awakeFromNib{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelPay) name:@"YuYueMainViewControllerPayCancel" object:nil];
}
-(void)setData:(ExamOrderData *)data{
    _data = data;
    self.nameLable.text = data.customer_name;
    self.realityLable.text = [data.customer_id isEqualToString:[UserInfos customerId]]?@"(本人)":@"(亲友)";
    [self.nameLable sizeToFit];
    self.nameLable.left = 14;
    self.realityLable.left = self.nameLable.right+5;
    self.examsetNameLabel.text = data.exam_set_name;
    self.examCenterLable.text = data.exam_center_name;
    self.dateLable.text = [[data.date componentsSeparatedByString:@"T"] objectAtIndex:0];
    self.priceLable.text = [NSString stringWithFormat:@"%.1f",[data.price floatValue]];
    int money = [data.price floatValue]*100;
    if (money%10>0) {
        self.priceLable.text = [NSString stringWithFormat:@"%.2f",[data.price floatValue]];

    }
    [self.priceLable sizeToFit];

    self.payMethodButtonLabel.text =[[Common paymode2String: data.pay_mode_id andpayStatue:data.status_id] stringByReplacingOccurrencesOfString:@"待安排" withString:@"已付款"];
    self.payMethodButtonLabel.left = self.priceLable.right+5;
    self.statusLable.text = [Common status_id2String:data.status_id];
    if ([data.leftTime integerValue] == -1) {
        self.leftTimeLable.text = @"";
    }
    else
        self.leftTimeLable.text = [NSString stringWithFormat:@"剩余时间：%@小时",data.leftTime];
    
    if ([data.status_id integerValue] == 2) {
        self.payButton.hidden = NO;
        self.payButtonLabel.hidden = NO;
    }
    else
    {
        self.payButton.hidden = YES;
        self.payButtonLabel.hidden = YES;
    }
    
//    先判断支付方式，（1）如果是单位统付或者门店支付，在订单状态为"已安排"前，都可以取消；（2）如果是网上支付，则付款后即不能取消
    if (([data.status_id intValue] <3 && [data.pay_mode_id intValue] == 2)|| ([data.status_id intValue] < 4 && [data.pay_mode_id intValue] != 2)) {
        self.cancelButton.hidden = NO;
    }
    else
    {
        self.cancelButton.hidden = YES;
    }

}

-(IBAction)examSetDetail:(id)sender{
    self.examsetBlock(_data);
}
-(IBAction)examCenterDetail:(id)sender{
    self.examcenterBlock(_data);
}
-(IBAction)examDataDetail:(id)sender{
    self.examdataBlock(_data);
}
-(IBAction)examPriceDetail:(id)sender{
    self.exampriceBlock(_data);
}
-(IBAction)payNow:(id)sender{
    UIButton *b = sender;
    if (b.selected) {
        b.selected = NO;
    }
    else
    {
        b.selected = YES;
    }
    self.paynowBlock(_data);
}

-(IBAction)cancelYY:(id)sender{
    [UIAlertView showConfirmMessage:@"取消预约" Content:@"您真得要取消预约吗" ConfrimButton:@"确定" CancelButton:@"取消" handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            self.cancelYuYue(_data);
        }

    }];
}
-(void)cancelPay{
    self.payButton.selected = NO;

}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"YuYueMainViewControllerPayCancel" object:nil];
}
@end

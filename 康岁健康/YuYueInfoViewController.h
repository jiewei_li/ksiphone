//
//  YuYueInfoViewController.h
//  康岁健康
//
//  Created by 低调 on 14/12/10.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"

@interface YuYueInfoViewController : BaseNextViewController
@property NSString *info;
@property NSString *titleString;
@property IBOutlet UIView *infoView;
@property IBOutlet UIWebView *infoLabel;
@end

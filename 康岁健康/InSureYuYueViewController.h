//
//  InSureYuYueViewController.h
//  康岁健康
//
//  Created by 低调 on 14/12/16.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "BaseNextViewController.h"
#import "CustomerData.h"
@class ExamSetBriefData;
@class CityByExamsetData;
@class ExamCenterByCityExamsetData;
@interface InSureYuYueViewController : BaseNextViewController<UITextFieldDelegate>
@property  CustomerData               *customerData;
@property ExamSetBriefData            *examSetBriefData;
@property CityByExamsetData           *cityByExamsetData;
@property ExamCenterByCityExamsetData *examCenterByCityExamsetData;
@property NSDate                      *selectedDate;
@property IBOutlet UIView *sureMainView;
@property IBOutlet UIScrollView *mainScrollView;
@property IBOutlet UILabel *userNameLabel;
@property IBOutlet UILabel *userSexLabel;
@property IBOutlet UILabel *userPhoneNumberLabel;
@property IBOutlet UILabel *userMarriedLabel;
@property IBOutlet UILabel *userBirthdayLabel;
@property IBOutlet UILabel *userWorkNumberLabel;
@property IBOutlet UILabel *userCardTypeLabel;
@property IBOutlet UILabel *userCardNumberLabel;


@property IBOutlet UILabel *examSetNameLabel;
@property IBOutlet UILabel *examTypeLabel;
@property IBOutlet UILabel *examDateLabel;
@property IBOutlet UILabel *examTimeLabel;
@property IBOutlet UILabel *exametCenterLabel;
@property IBOutlet UILabel *ExamAddressLabel;

@property IBOutlet UIButton *eButton;
@property IBOutlet UIButton *pButton;
@property IBOutlet UIButton *epButton;
@property IBOutlet UIView *eView;
@property IBOutlet UIView *pView;
@property IBOutlet UIView *bottomView;
@property IBOutlet UITextField *eTextField;
@property IBOutlet UITextField *addTextField;
@property IBOutlet UITextField *pCodeField;

-(IBAction)eChose:(id)sender;
-(IBAction)pChose:(id)sender;
-(IBAction)epChose:(id)sender;
-(IBAction)submit:(id)sender;
//@property IBOutlet UILabel *userCardNumberLabel;
@end

//
//  TopSwitchView.m
//  康岁健康
//
//  Created by 低调 on 14/10/24.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//

#import "TopSwitchView.h"
@implementation TopSwitchView
-(id)initWithFrame:(CGRect)frame leftTitle:(NSString *)leftTitle rightTitle:(NSString *)rightTitle{
    self = [super init];
    self.frame = CGRectMake(0, 0, DEVICE_WIDTH, 44);
    if (self) {
        _leftButton = CJButtonF(CGRectMake(0, 0, self.width/2, self.height));
        CJButtonSB(_leftButton, self, @selector(leftSelect));
        CJButtonTB(_leftButton, leftTitle, leftTitle);
        CJButtonCB(_leftButton, CJBlackColor, NavColor);
        CJButtonCSB(_leftButton, NavColor);
        
        _rightButton = CJButtonF(CGRectMake(self.width/2, 0, self.width/2, self.height));
        CJButtonSB(_rightButton, self, @selector(righttSelect));
        CJButtonTB(_rightButton, rightTitle, rightTitle);
        CJButtonCB(_rightButton, CJBlackColor, NavColor);
        CJButtonCSB(_rightButton, NavColor);
        
        [self addSubview:_leftButton];
        
        [self addSubview:_rightButton];
        _leftButton.selected = YES;
        _rightButton.selected = NO;
    }
    
    return  self;
}


-(void)leftSelect{
    _leftButton.selected = YES;
    _rightButton.selected = NO;
    if(self.delgate && [self.delgate respondsToSelector:@selector(leftChosed)]){
        [self.delgate leftChosed];
    }
}
-(void)righttSelect{
    _leftButton.selected = NO;
    _rightButton.selected = YES;
    if(self.delgate && [self.delgate respondsToSelector:@selector(rightChosed)]){
        [self.delgate rightChosed];
    }
}
@end

//
//  TopSwitchView.h
//  康岁健康
//
//  Created by 低调 on 14/10/24.
//  Copyright (c) 2014年 cerastes. All rights reserved.
//
@protocol TopSwitchViewDelegate <NSObject>

-(void)leftChosed;
-(void)rightChosed;

@end
#import <UIKit/UIKit.h>

@interface TopSwitchView : UIView
{
    UIButton *_leftButton;
    UIButton *_rightButton;
}
@property id<TopSwitchViewDelegate>delgate;
-(id)initWithFrame:(CGRect)frame leftTitle:(NSString *)leftTitle rightTitle:(NSString *)rightTitle;
@end

//
//  doctorViewController.swift
//  康岁健康
//
//  Created by ljw on 15/2/12.
//  Copyright (c) 2015年 cerastes. All rights reserved.
//

import UIKit

class doctorViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.edgesForExtendedLayout = UIRectEdge.None 
        //self.extendedLayoutIncludesOpaqueBars=true
        scrollView.contentSize=CGSizeMake(CGFloat(850),CGFloat(80))
        scrollView.pagingEnabled=true
        
        
        // 生成预约时间
        var x:CGFloat=20.0;
        var width:CGFloat=33.0;
        var date=NSDate()
       // var formater=NSDateFormatter()
        //formater.dateFormat="MM-dd"
        for i in 1...24
        {
              date =     date.dateByAddingDays(1)
            var lab=UILabel(frame: CGRectMake(x, 0, width, 22))
            lab.text = date.toString(format:.Custom( "MM-dd"))+"\n"+date.weekdayToString() //"2-27\n星期五"
      
           // date=date.dateByAddingTimeInterval(60*60*24*i)
            lab.numberOfLines=2
            lab.lineBreakMode =  NSLineBreakMode.ByCharWrapping// UILineBreakModeCharacterWrap
            lab.font=UIFont.systemFontOfSize(7)
            scrollView.addSubview(lab);
            x=x+width+2
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
